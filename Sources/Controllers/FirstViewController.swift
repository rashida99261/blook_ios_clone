//
//  FirstViewController.swift
//  bloook
//
//  Created by Tech Astha on 06/01/21.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let strUserId = "\(defult .object(forKey: Constants.ISLOGIN) ?? "")"
        print(strUserId)
        if UserDefaults.standard.bool(forKey: Constants.ISLOGIN){
             self.presentDashBoardViewController(true)
         }else{
             self.presentLoginViewController(true)
         }
    }
    func presentLoginViewController(_ animated: Bool) {
        if let objViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
            self.navigationController?.pushViewController(objViewController, animated: false)
        }
    }
    
    func presentDashBoardViewController(_ animated: Bool) {
        
        let otherStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
        if let objTab = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as? TabView {
            self.navigationController?.pushViewController(objTab, animated: false)
        }
    }

}
