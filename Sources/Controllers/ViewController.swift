//
//  ViewController.swift
//  blook
//
//  Created by Tech Astha on 24/12/20.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

     @IBAction func signUpBtnAction(_ sender: Any) {
        let objUserNameRegister = self.storyboard?.instantiateViewController(withIdentifier: "UserNameRegisterVC") as? UserNameRegisterVC
        self.navigationController?.pushViewController(objUserNameRegister!, animated: true)
    }
    @IBAction func signInBtnAction(_ sender: Any) {
        let objLogin = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        self.navigationController?.pushViewController(objLogin!, animated: true)
        
//        let objProfilePic = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePicView") as! ProfilePicView
//        self.navigationController?.pushViewController(objProfilePic, animated: true)
    }
    @IBAction func inviteFriendBtnAction(_ sender: Any) {
        let objInviteFriend = self.storyboard?.instantiateViewController(withIdentifier: "InviteFriendVC") as? InviteFriendVC
        self.navigationController?.pushViewController(objInviteFriend!, animated: true)
    }
}

