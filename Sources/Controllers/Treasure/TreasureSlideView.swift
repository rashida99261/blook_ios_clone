//
//  TreasureSlideView.swift
//  Bloook
//
//  Created by Suresh Varma on 24/07/21.
//

import UIKit

class TreasureSlideView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}
