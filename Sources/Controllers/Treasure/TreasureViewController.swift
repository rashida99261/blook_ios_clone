//
//  TreasureViewController.swift
//  Bloook
//
//  Created by Suresh Varma on 21/07/21.
//

import GWInfinitePickerView
import UIKit

protocol TreasureVCDelegate {
    func dismissVC()
}

class TreasureViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }

    @IBOutlet weak var pageControl: UIPageControl!

    @IBOutlet weak var numberPicker: UIPickerView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var coinValueLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var gameView: UIView!
    @IBOutlet weak var introView: UIView!
    var delegate: TreasureVCDelegate?
    var pickerData = [[String]]()

    var hundredsColumn = 0
    var tensColumn = 0
    var singleDigits = 0
    var timer1: Timer?
    var timer2: Timer?
    var timer3: Timer?
    var gameTimer: Timer?
    var availableLimit = 999
    var slides: [TreasureSlideView] = []

    init() {
        super.init(
            nibName: String(describing: TreasureViewController.self),
            bundle: Bundle(for: TreasureViewController.self)
        )
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        numberPicker.delegate = self
        numberPicker.dataSource = self
        numberPicker.showsSelectionIndicator = false
        numberPicker.subviews.forEach { $0.backgroundColor = .clear }
        coinValueLabel.superview?.roundCorners([.bottomLeft, .bottomRight], radius: 20)
        initialSetup()
        fakeGameData()
        getAvailableCoins()
    }

    // MARK: - Private

    private func fakeGameData() {
        titleLabel.text = "Treasure Hunt"
        descriptionLabel.text = SavedSetting.shared.setting?.treasureHunt ?? "-Press the play button and then press stop. If you win a prize it will automatically be sent to your bloook wallet. Good luck!"
        descriptionLabel.addLineSpacing(spacing: 4)
    }

    private func initialSetup() {
        slides = createSlides()
        setupSlideScrollView(slides: slides)

        pickerData = [
            ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
            ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
            ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
        ]

        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        pageControl.superview?.bringSubviewToFront(pageControl)
        gameView.isHidden = true
        continueButton.isHidden = true
    }

    private func createSlides() -> [TreasureSlideView] {
        let slide1: TreasureSlideView = Bundle.main.loadNibNamed("TreasureSlideView", owner: self, options: nil)?.first as! TreasureSlideView
        slide1.titleLabel.text = "Treasure Hunt Onboarding"
        slide1.descriptionLabel.text = SavedSetting.shared.setting?.treasureHuntOnboarding ?? "-everyone has a chance to win big with the randomizer treasure hunt. Press the “Play Game” button and let the action begin"
        slide1.descriptionLabel.addLineSpacing(spacing: 4)
        return [slide1]
    }

    private func autoScrollToPosition() {
        numberPicker.selectRow(hundredsColumn, inComponent: 0, animated: true)
        numberPicker.selectRow(tensColumn, inComponent: 1, animated: true)
        numberPicker.selectRow(singleDigits, inComponent: 2, animated: true)
    }

    func getAvailableCoins() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I",
                     "type": "game"] as [String: Any]

        PSWebServiceAPI.availableCoin(param, completion: { response in
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let array = response[Constants.KEY_POSTDATA] as? [Any]
                    let coins = array?.first as? NSDictionary
                    let availableBalance = coins?["available_coins"] as? String
                    self.availableLimit = Int(availableBalance ?? "999") ?? 0
                    if self.availableLimit < 0 {
                        self.availableLimit = 0
                    }
                }
            }
        })
    }

    func storeCoins(coins: String) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I",
                     "type": "game",
                     "coin": coins] as [String: Any]

        PSWebServiceAPI.storeCoin(param, completion: { response in
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {}
            }
        })
    }

    // MARK: - IBActions

    @IBAction func autoscroll() {
        if timer1 != nil || timer2 != nil || timer3 != nil {
            var point = hundredsColumn%10 * 100 + tensColumn%10 * 10 + singleDigits%10
            print("Points By Scroll = \(point)")
            if point > availableLimit {
                if availableLimit == 0 {
                    hundredsColumn = 0
                    tensColumn = 0
                    singleDigits = 0
                } else {
                    let randomInt = Int.random(in: 0 ..< availableLimit)
                    hundredsColumn = (hundredsColumn + 10 - hundredsColumn%10) + randomInt / 100
                    tensColumn = (tensColumn + (10 - tensColumn%10)) + (randomInt / 10)%10
                    singleDigits = (tensColumn + (10 - tensColumn%10)) + randomInt%10
                    print("--> Generated Int", randomInt)
                }
                autoScrollToPosition()
            }

            gameTimer?.invalidate()
            gameTimer = nil
            timer1?.invalidate()
            timer1 = nil
            timer2?.invalidate()
            timer2 = nil
            timer3?.invalidate()
            timer3 = nil
            point = hundredsColumn%10 * 100 + tensColumn%10 * 10 + singleDigits%10
            print("Final Points => \(point)")
            coinValueLabel.text = "\(point)"
            playButton.setImage(UIImage(named: "playGame"), for: .normal)
            playButton.isEnabled = false
            if point > 0 {
                storeCoins(coins: "\(point)")
            }
            return
        }
        playButton.setImage(UIImage(named: "stopGame"), for: .normal)
        playButton.setImage(UIImage(named: "stopGame"), for: .highlighted)
        timer1 = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(fireOne), userInfo: nil, repeats: true)

        timer2 = Timer.scheduledTimer(timeInterval: 0.15, target: self, selector: #selector(fireTwo), userInfo: nil, repeats: true)

        timer3 = Timer.scheduledTimer(timeInterval: 0.18, target: self, selector: #selector(fireThree), userInfo: nil, repeats: true)

        gameTimer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(autoscroll), userInfo: nil, repeats: false)
    }

    @IBAction func dismiss() {
        delegate?.dismissVC()
    }

    @IBAction func playGameTapped() {
        introView.isHidden = true
        gameView.isHidden = false
        continueButton.isHidden = false
    }

    // MARK: - Timers

    @objc func fireOne() {
        hundredsColumn += 1
        numberPicker.selectRow(hundredsColumn, inComponent: 0, animated: true)
    }

    @objc func fireTwo() {
        tensColumn += 1
        numberPicker.selectRow(tensColumn, inComponent: 1, animated: true)
    }

    @objc func fireThree() {
        singleDigits += 1
        numberPicker.selectRow(singleDigits, inComponent: 2, animated: true)
    }
}

extension TreasureViewController: UIPickerViewDelegate, UIPickerViewDataSource, GWInfinitePickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 80.0
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 45.0
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData[component].count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[component][row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        hundredsColumn = 0
        tensColumn = 0
        singleDigits = 0
    }
}

extension TreasureViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        pageControl.currentPage = index
    }

    func setupSlideScrollView(slides: [TreasureSlideView]) {
        guard let view = scrollView.superview else { return }
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: scrollView.frame.width * CGFloat(slides.count), height: 0)
        scrollView.isPagingEnabled = true

        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x / view.frame.width)
        pageControl.currentPage = Int(pageIndex)

        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x

        // vertical
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y

        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
    }
}
