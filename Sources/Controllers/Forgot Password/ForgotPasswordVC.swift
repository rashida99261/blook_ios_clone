//
//  ForgotPasswordVC.swift
//  bloook
//
//  Created by Tech Astha on 10/05/21.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    //var progressView : AcuantProgressView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var topStatusView: UIView!
    
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true

   }
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    func doForgotPassword() {
    
        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        
        let defult =  UserDefaults.standard
        let strDeviceToken = "\(defult .object(forKey: Constants.DEVICETOKEN) ?? "")"
        let param = [Constants.KEY_EMAIL :emailField.text!,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken)",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]
        
       // PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doForgotPassword(param , completion: { (response) in
            PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    print(msg)
                   // PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                   // let dict =  response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
//                    var strToken =  "\(dict.value(forKey: "access_token") ?? "")"
//                    let strUserId = "\(dict.value(forKey: Constants.KEY_ID) ?? "")"
//                    strToken = "Bearer " + strToken
//                    UserDefaults.standard.set(strToken, forKey: "access_token")
//                    UserDefaults.standard.set(strUserId, forKey: Constants.KEY_ID)
//                    UserDefaults.standard.set(self.passwordField.text, forKey: "forPassword")
//                    UserDefaults.standard.set(true, forKey: Constants.ISLOGIN)
//
                    let objResetPassword = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                    self.navigationController?.pushViewController(objResetPassword, animated: true)
                    
                 }else{
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    if msg == nil {
                        
                    }else{
                      PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
           // self.progressView.startAnimation()
           // self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
    private func hideProgressView(){
        DispatchQueue.main.async {
            //self.progressView.stopAnimation()
            //self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    
    }
    @IBAction func confirmPasswordBtnAction(_ sender: Any) {
        doForgotPassword()
    }
}
