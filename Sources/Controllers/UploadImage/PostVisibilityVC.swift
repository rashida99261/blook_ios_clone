//
//  PostVisibilityVC.swift
//  bloook
//
//  Created by Tech Astha on 21/01/21.
//

import UIKit

class PostVisibilityVC: UIViewController {
    var strVisibilitySelection = "Public"
    var strVisibilityType = String()

    @IBOutlet weak var followerBoostRadioImg: UIImageView!
    @IBOutlet weak var publicRadioImg: UIImageView!
    @IBOutlet weak var followerRadioImg: UIImageView!
    @IBOutlet weak var closeFriendRadioImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func followerBoostBtnAction(_ sender: Any) {
        followerBoostRadioImg.image = UIImage(named:"radiobutton2")
        publicRadioImg.image = UIImage(named:"radiobutton1")
        followerRadioImg.image = UIImage(named:"radiobutton1")
        closeFriendRadioImg.image = UIImage(named:"radiobutton1")
        strVisibilitySelection = "Follower Boost"
        strVisibilityType = "Applied to this post only"
    }
    @IBAction func publicBtnAction(_ sender: Any) {
        followerBoostRadioImg.image = UIImage(named:"radiobutton1")
        publicRadioImg.image = UIImage(named:"radiobutton2")
        followerRadioImg.image = UIImage(named:"radiobutton1")
        closeFriendRadioImg.image = UIImage(named:"radiobutton1")
        strVisibilitySelection = "Public"
        strVisibilityType = "Visible to anyone on blook"
    }
    @IBAction func flowersBtnAction(_ sender: Any) {
        followerBoostRadioImg.image = UIImage(named:"radiobutton1")
        publicRadioImg.image = UIImage(named:"radiobutton1")
        followerRadioImg.image = UIImage(named:"radiobutton2")
        closeFriendRadioImg.image = UIImage(named:"radiobutton1")
        strVisibilitySelection = "Follower"
        strVisibilityType = "Only Visible to your followers"
    }
    @IBAction func closeFriendBtnAction(_ sender: Any) {
        followerBoostRadioImg.image = UIImage(named:"radiobutton1")
        publicRadioImg.image = UIImage(named:"radiobutton1")
        followerRadioImg.image = UIImage(named:"radiobutton1")
        closeFriendRadioImg.image = UIImage(named:"radiobutton2")
        strVisibilitySelection = "Close Friends"
        strVisibilityType = "Only Visible to close Friends"
    }
    @IBAction func doneBtnAction(_ sender: Any) {
        let homeDetailStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
        let objUserUploadPost = homeDetailStoryboard.instantiateViewController(withIdentifier: "UserUploadPostVC") as? UserUploadPostVC
       // objUserUploadPost?.strVisibilitySelection = strVisibilitySelection
        UserDefaults.standard.set(strVisibilitySelection, forKey: "forSelectVisibility")
        UserDefaults.standard.set(strVisibilityType, forKey: "forVisibilityType")
        self.navigationController?.pushViewController(objUserUploadPost!, animated: true)
    }
}
