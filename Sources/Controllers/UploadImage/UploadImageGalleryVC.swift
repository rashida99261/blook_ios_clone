//
//  UploadImageGalleryVC.swift
//  blook
//
//  Created by Tech Astha on 05/01/21.
//

import AVFoundation
import AVKit
import MediaPlayer
import Metal
import MetalKit
import MobileCoreServices
import Photos
import UIKit
import YPImagePicker

class UploadImageGalleryVC: BaseViewController, YPImagePickerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerHasNoItemsInLibrary(_ picker: YPImagePicker) {}
    
    func noPhotos() {}

    func shouldAddToSelection(indexPath: IndexPath, numSelections: Int) -> Bool {
        return true
    }

    @IBOutlet var blogDescriptionView: UITextView!
    var descriptionTextViewPlaceHolder = "Tap to type..."
    @IBOutlet var videoBtn: UIButton!
    @IBOutlet var photoBtn: UIButton!
    @IBOutlet var blogBtn: UIButton!
    @IBOutlet var continueBtn: UIButton!
    @IBOutlet var playImg: UIImageView!
    @IBOutlet var playBtn: UIButton!
    @IBOutlet var selectedVideoImg: UIImageView!
    var controller = UIImagePickerController()
    let videoFileName = "/video.mp4"
    @IBOutlet var blogView: UIView!
    @IBOutlet var photoMainView: UIView!
    var allVideos: PHFetchResult<PHAsset>!

    var imageArray = [Any]()
    @IBOutlet var videoGalleryCollectionView: UICollectionView!
    @IBOutlet var videoView: UIView!
    @IBOutlet var videoLibraryView: UIView!
    var imagePicker = UIImagePickerController()
    var selectedItems = [YPMediaItem]()
    let selectedImageV = UIImageView()
    var videoPicker = UIImagePickerController()
    // var videoUrl : URL!
    // var videoUrl : AVAsset
    var strVideo = String()
    var selectionType = 1
    var strPhotoVideoSelectionId = String()
    
    @IBOutlet var videoGalleryView: UIView!
    @IBOutlet var photoView: UIView!
    @IBOutlet var albumView: UIView!
    fileprivate var scrollView: MTScrollView!
    fileprivate var selectedAsset: PHAsset?
    fileprivate var albumController: AlbumPhotoViewController?
    
    private var numberOfItemsInRow = 4
    private var minimumSpacing = 1
    private var edgeInsetPadding = 1
    var strBtnId = String()
    var playerLayer = AVPlayerLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        // Set
        let savedPlaceHolder = "\(defult.object(forKey: "forSaveDescriptionData") ?? "")"
        if savedPlaceHolder != "" {
            descriptionTextViewPlaceHolder = savedPlaceHolder
        }
        defaults.set(descriptionTextViewPlaceHolder, forKey: "forSaveDescriptionData")

        photoBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        
        strBtnId = "0"
        playImg.isHidden = true
        videoGalleryView.isHidden = true
        photoMainView.isHidden = false
        blogView.isHidden = true
        let background = UIView()
        background.translatesAutoresizingMaskIntoConstraints = false
        background.backgroundColor = .red
        view.addSubview(background)
        
        blogDescriptionView.text = descriptionTextViewPlaceHolder
        blogDescriptionView.textColor = UIColor.lightGray
        blogDescriptionView.delegate = self
        
        //  videoGalleryCollectionView.isHidden = true
        // let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkActionAddDetail))
        setupScrollView()
        requestPhoto()
    }

    override func viewDidAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        playerLayer.player?.pause()
    }
    
//    @objc func checkActionAddDetail(sender : UITapGestureRecognizer) {
//                let controller = AVPlayerViewController()
//                controller.player = player
//                present(controller, animated: true) {
//                    player.play()
//
//              }
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        playerLayer.player?.pause()
    }

    private func setupScrollView() {
        scrollView = MTScrollView(frame: photoView.frame)
        photoView.addSubview(scrollView)
    }

    fileprivate func requestPhoto() {
        PHPhotoLibrary.requestAuthorization { status in
            switch status {
            case .authorized:
                DispatchQueue.main.async {
                    PHPhotoLibrary.shared().register(self)
                    self.loadPhotos()
                    self.loadVideo()
                }
            case .notDetermined:
                break
            default:
                break
            }
        }
    }
    
    fileprivate func loadPhotos() {
        let options = PHFetchOptions()
        options.wantsIncrementalChangeDetails = false
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        let result = PHAsset.fetchAssets(with: .image, options: options)
        if let firstAsset = result.firstObject {
            loadImageFor(firstAsset)
        }
        if let controller = albumController {
            controller.update(dataSource: result)
        } else {
            let albumController = AlbumPhotoViewController(dataSource: result)
            albumController.didSelectAssetHandler = { [weak self] selectedAsset in
                self?.loadImageFor(selectedAsset)
            }
            albumController.view.frame = albumView.bounds
            albumView.addSubview(albumController.view)
            addChild(albumController)
            albumController.didMove(toParent: self)
            self.albumController = albumController
        }
    }
    
    fileprivate func loadImageFor(_ asset: PHAsset) {
        let options = PHImageRequestOptions()
        options.deliveryMode = .highQualityFormat
        options.isSynchronous = true
        var targetSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
        while targetSize.height > 3000 || targetSize.width > 3000 {
            targetSize = CGSize(width: targetSize.width - 500, height: targetSize.height - 500)
        }
        PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: .default, options: options) { image, _ in
            DispatchQueue.main.async {
                self.scrollView.image = image
            }
        }
        selectedAsset = asset
    }

    @IBAction func cancelBtnAction(_ sender: Any) {
//        let otherStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
//        let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
//        navigationController?.pushViewController(objTabView, animated: false)
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
    }
    
    @IBAction func continueBtnAction(_ sender: Any) {
        if strBtnId == "0" {
            let ImgStoryboard = UIStoryboard(name: "ImgStoryboard", bundle: nil)
            guard let editorController = ImgStoryboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as? PhotoEditorViewController else {
                return
            }
            if scrollView.croppedImage == nil {
                PSUtil.showAlertFromController(controller: self, withMessage: "You don't have access to any image in the Gallery, Please Capture the image from Camera")
                return
            }
            editorController.croppedImage = scrollView.croppedImage
            editorController.strBtnId = strBtnId
            navigationController?.pushViewController(editorController, animated: false)
        } else if strBtnId == "1" {
            playerLayer.player?.pause()
            print(strVideo)
            if scrollView.croppedImage == nil {
                PSUtil.showAlertFromController(controller: self, withMessage: "You don't have access to any video in the Gallery,Please Capture the Video from Camera")
                return
            }
            if strVideo == "" {
                PSUtil.showAlertFromController(controller: self, withMessage: "Please select a video")
                return
            }
            let strSelectedType = "video"
            strPhotoVideoSelectionId = "0"
            let homeDetailStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
            let objUserUploadPost = homeDetailStoryboard.instantiateViewController(withIdentifier: "UserUploadPostVC") as? UserUploadPostVC
            objUserUploadPost?.strVideoImg = strVideo
            objUserUploadPost?.strBtnId = strBtnId
            objUserUploadPost?.strSelectedType = strSelectedType
            objUserUploadPost?.strPhotoVideoSelectionId = strPhotoVideoSelectionId
            navigationController?.pushViewController(objUserUploadPost!, animated: true)
        } else {
            doUploadBlogPost()
        }
    }
        
    func doUploadBlogPost() {
        if blogDescriptionView.text == descriptionTextViewPlaceHolder { return }
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        var params = [Constants.KEY_MYUSER_ID: strUserId,
                      Constants.DEVICE_TYPE: "I",
                      Constants.KEY_DESCRIPTION: blogDescriptionView.text!,
                      Constants.KEY_TYPE: "blog",
                      Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                      Constants.KEY_FILE_NAME: ""] as [String: Any]
        print("==0=0=0=0=0=0====> \(defult.value(forKey: Constants.Bounce.bounceUserId))")
        if defult.value(forKey: Constants.Bounce.bounceUserId) != nil {
            params["bounce_user"] = defult.value(forKey: Constants.Bounce.bounceUserId)
        }
        let visiblity = defult.value(forKey: "forSelectVisibility") ?? "Public"
        params["visibility"] = visiblity
        
        print(params)
        
        // PSUtil.showProgressHUD()  //"\(String(describing: strDeviceToken))"
        showProgressView(text: "Loading...")
        PSWebServiceAPI.doUploadPostBlog(params as? [String: Any], completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    DispatchQueue.main.async {
                        PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong, andHandler: { (_: UIAlertAction!) in
                            let otherStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
                            let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
                            self.navigationController?.pushViewController(objTabView, animated: true)
                        })
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }

    @IBAction func backBtnAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func photoBtnAction(_ sender: Any) {
        photoBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        videoBtn.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        blogBtn.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        playerLayer.player?.pause()
        strBtnId = "0"
        videoGalleryView.isHidden = true
        photoMainView.isHidden = false
        blogView.isHidden = true
        setupScrollView()
        requestPhoto()
    }

    @IBAction func videoBtnAction(_ sender: Any) {
        photoBtn.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        videoBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        blogBtn.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        
        // func showPicker() {
        strBtnId = "1"
        selectionType = 2
        videoGalleryView.isHidden = false
        photoMainView.isHidden = true
        blogView.isHidden = true

//        if videoGallery.isEmpty {
//          //  processVideo()
//
//        } else {
        videoGalleryCollectionView.delegate = self
        videoGalleryCollectionView.dataSource = self
        videoGalleryCollectionView.reloadData()
//
//            guard let videoUrl = self.videoGallery.first else { return }
//            strVideo = "\((videoUrl as! AVURLAsset).url)"
//
//            let playerItem = AVPlayerItem(asset: videoUrl)
//            let player = AVPlayer(playerItem: playerItem)
//
//            // 3. Create AVPlayerLayer object
//
//            if playerLayer.player == nil {
//                playerLayer = AVPlayerLayer(player: player)
//                playerLayer.frame = selectedVideoImg.bounds // bounds of the view in which AVPlayer should be displayed
//                playerLayer.videoGravity = .resizeAspect
//                selectedVideoImg.layer.addSublayer(playerLayer)
//
//            } else {
//                playerLayer.player = player
//            }
//            // 5. Play Video
//            player.play()
//        }
    }
    
//    func processVideo() {
//        let group = DispatchGroup()
//
//        let options = PHFetchOptions()
//        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
//        options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
//        let videoAssets = PHAsset.fetchAssets(with: options)
//        self.showProgressView(text: "Processing")
//        videoAssets.enumerateObjects { asset, _, _ in
//            let option = PHVideoRequestOptions()
//            group.enter()
//
//            PHImageManager.default().requestAVAsset(forVideo: asset, options: option) { avAsset, _, _ in
//
//                guard let avAssetValue = avAsset else { return }
//                self.videoGallery.append(avAssetValue)
//
    ////                //saveimages
    ////                let asset = avAssetValue
    ////                let imageGenerator = AVAssetImageGenerator(asset: asset)
    ////                imageGenerator.appliesPreferredTrackTransform = true
    ////                let durationSeconds = CMTimeGetSeconds(asset.duration)
    ////                let midpoint = CMTimeMakeWithSeconds(Float64(durationSeconds / 2.0), preferredTimescale: 600)
    ////
    ////                // get the image from
    ////                let cgImage: CGImage
    ////                do {
    ////                    cgImage = try imageGenerator.copyCGImage(at: midpoint, actualTime: nil)
    ////                    autoreleasepool {
    ////                    let uiImage = UIImage(cgImage: cgImage)
    ////                        self.videoImages.append(uiImage)
    ////                    }
    ////
    ////                } catch {
    ////                    print("Error: \(error)")
    ////                }
//                group.leave()
//
//            }
//        }
//        print(videoGallery.count)
//        group.notify(queue: .main) {
//            self.hideProgressView()
//            self.videoGalleryCollectionView.delegate = self
//            self.videoGalleryCollectionView.dataSource = self
//            print(self.videoGallery)
//            guard let videoUrl = self.videoGallery.first else { return }
//            let str = (videoUrl as! AVURLAsset).url
//            self.strVideo = String(describing: str)
//            let playerItem = AVPlayerItem(asset: videoUrl)
//            let player = AVPlayer(playerItem: playerItem)
//
//            // 3. Create AVPlayerLayer object
//
//            if self.playerLayer.player == nil {
//                self.playerLayer = AVPlayerLayer(player: player)
//                self.playerLayer.frame = self.selectedVideoImg.bounds // bounds of the view in which AVPlayer should be displayed
//                self.playerLayer.videoGravity = .resizeAspect
//                self.selectedVideoImg.layer.addSublayer(self.playerLayer)
//
//            } else {
//                self.playerLayer.player = player
//            }
//            // 5. Play Video
//            if self.tabBarController?.selectedIndex == 2 {
//            player.play()
//            }
//            self.videoGalleryCollectionView.reloadData()
//        }
//    }
    
    func getAssetFromPhoto() {
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        allVideos = PHAsset.fetchAssets(with: options)
        videoGalleryCollectionView.delegate = self
        videoGalleryCollectionView.dataSource = self
        videoGalleryCollectionView.reloadData() // reload your collectionView
        if allVideos.count > 0 {
            PHCachingImageManager().requestAVAsset(forVideo: allVideos!.firstObject!, options: nil) { avAsset, _, _ in
                DispatchQueue.main.async { [weak self] in
            
                    guard let self = self else { return }
                    guard let videoAsset = avAsset as? AVURLAsset else { return }
                    let videoUrl = videoAsset.url
                    let str = videoUrl
                    self.strVideo = String(describing: str)
                    let playerItem = AVPlayerItem(asset: videoAsset)
                    let player = AVPlayer(playerItem: playerItem)
            
                    // 3. Create AVPlayerLayer object
            
                    if self.playerLayer.player == nil {
                        self.playerLayer = AVPlayerLayer(player: player)
                        self.playerLayer.frame = self.selectedVideoImg.bounds // bounds of the view in which AVPlayer should be displayed
                        self.playerLayer.videoGravity = .resizeAspect
                        self.selectedVideoImg.layer.addSublayer(self.playerLayer)
            
                    } else {
                        self.playerLayer.player = player
                    }
                    // 5. Play Video
                    if self.tabBarController?.selectedIndex == 2 {
                        player.play()
                        player.pause()
                    }
                    self.videoGalleryCollectionView.reloadData()
                }
            }
        } else {
            videoGalleryCollectionView.reloadData()
        }
    }
    
    func loadVideo() {
        getAssetFromPhoto()
    }

    //   func loadVideo11() {
//       let group = DispatchGroup()
//
//       let options = PHFetchOptions()
//       options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
//       options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
//       let videoAssets = PHAsset.fetchAssets(with: options)
//       videoAssets.enumerateObjects { asset, _, _ in
//           let option = PHVideoRequestOptions()
//           group.enter()
//
//           PHImageManager.default().requestAVAsset(forVideo: asset, options: option) { avAsset, _, _ in
//
//               guard let avAssetValue = avAsset else { return }
//               self.videoGallery.append(avAssetValue)
//
    ////               //saveimages
    ////               let asset = avAssetValue
    ////               let imageGenerator = AVAssetImageGenerator(asset: asset)
    ////               imageGenerator.appliesPreferredTrackTransform = true
    ////               let durationSeconds = CMTimeGetSeconds(asset.duration)
    ////               let midpoint = CMTimeMakeWithSeconds(Float64(durationSeconds / 2.0), preferredTimescale: 600)
    ////
    ////               // get the image from
    ////               let cgImage: CGImage
    ////               do {
    ////                   cgImage = try imageGenerator.copyCGImage(at: midpoint, actualTime: nil)
    ////                   let uiImage = UIImage(cgImage: cgImage)
    ////                   self.videoImages.append(uiImage)
    ////
    ////               } catch {
    ////                   print("Error: \(error)")
    ////               }
//
//               group.leave()
//
//           }
//       }
//       print(videoGallery.count)
//       group.notify(queue: .main) {
//           self.hideProgressView()
//           self.videoGalleryCollectionView.delegate = self
//           self.videoGalleryCollectionView.dataSource = self
//           print(self.videoGallery)
//           guard let videoUrl = self.videoGallery.first else { return }
//           let str = (videoUrl as! AVURLAsset).url
//           self.strVideo = String(describing: str)
//           let playerItem = AVPlayerItem(asset: videoUrl)
//           let player = AVPlayer(playerItem: playerItem)
//
//           // 3. Create AVPlayerLayer object
//
//           if self.playerLayer.player == nil {
//               self.playerLayer = AVPlayerLayer(player: player)
//               self.playerLayer.frame = self.selectedVideoImg.bounds // bounds of the view in which AVPlayer should be displayed
//               self.playerLayer.videoGravity = .resizeAspect
//               self.selectedVideoImg.layer.addSublayer(self.playerLayer)
//
//           } else {
//               self.playerLayer.player = player
//           }
//           // 5. Play Video
//           if self.tabBarController?.selectedIndex == 2 {
//               player.play()
//               player.pause()
//           }
//           self.videoGalleryCollectionView.reloadData()
//       }
    //   }
    
    @IBAction func blogBtnAction(_ sender: Any) {
        photoBtn.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        videoBtn.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        blogBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        strBtnId = "2"
        playerLayer.player?.pause()
        videoGalleryView.isHidden = true
        photoMainView.isHidden = true
        blogView.isHidden = false
        blogDescriptionView.isHidden = false
    }

    @IBAction func captureBtnAction(_ sender: Any) {
        playerLayer.player?.pause()
        //  imagePicker = UIImagePickerController()
        if strBtnId == "0" {
            imagePicker.delegate = self
            openImagePickerController()
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                // 2 Present UIImagePickerController to take video
                controller.sourceType = .camera
                controller.mediaTypes = [kUTTypeMovie as String]
                controller.delegate = self
                controller.mediaTypes = ["public.movie"]
                controller.videoQuality = .typeHigh
                
                present(controller, animated: true, completion: nil)
            } else {
                print("Camera is not available")
            }
        }
    }
    
    func openImagePickerController() {
        let alrt = UIAlertController(title: "Select Source Type", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        })
    
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            
        })
        
        alrt.addAction(cameraAction)
        alrt.addAction(cancelAction)
        present(alrt, animated: true, completion: nil)
    }
    
    @IBAction func libraryBtnAction(_ sender: Any) {}

    // MARK: - Add image to Library

    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }

//       //MARK: - Done image capture here
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if strBtnId == "1" {
            if let selectedVideo: URL = (info[UIImagePickerController.InfoKey.mediaURL] as? URL) {
                // Save video to the main photo album
                let selectorToCall = #selector(UploadImageGalleryVC.videoSaved(_:didFinishSavingWithError:context:))
                UISaveVideoAtPathToSavedPhotosAlbum(selectedVideo.relativePath, self, selectorToCall, nil)
                // Save the video to the app directory so we can play it later
                let videoData = try? Data(contentsOf: selectedVideo)
                let paths = NSSearchPathForDirectoriesInDomains(
                    FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                let documentsDirectory = URL(fileURLWithPath: paths[0])
                let dataPath = documentsDirectory.appendingPathComponent(videoFileName)
                try! videoData?.write(to: dataPath, options: [])
            }
            picker.dismiss(animated: true)
            
        } else {
            imagePicker.dismiss(animated: true, completion: nil)
            guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            else {
                return
            }

            // imageTake.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            let ImgStoryboard = UIStoryboard(name: "ImgStoryboard", bundle: nil)
            guard let editorController = ImgStoryboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as? PhotoEditorViewController else {
                return
            }
            editorController.croppedImage = image
            navigationController?.pushViewController(editorController, animated: false)
            dismiss(animated: true, completion: nil)
        }
    }

//    @objc func videoSaved(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
//        if let theError = error {
//            print("error saving the video = \(theError)")
//        } else {
//            DispatchQueue.main.async(execute: { () -> Void in
//            })
//        }
//    }
    
    @objc func videoSaved(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer) {
        if let theError = error {
            print("error saving the video = \(theError)")
        } else {
            DispatchQueue.main.async { () -> Void in
                let strSelectedType = "video"
                let str = video
                self.strVideo = String(describing: str)
                let homeDetailStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
                let objUserUploadPost = homeDetailStoryboard.instantiateViewController(withIdentifier: "UserUploadPostVC") as? UserUploadPostVC
                objUserUploadPost?.strVideoImg = self.strVideo
                objUserUploadPost?.strBtnId = self.strBtnId
                objUserUploadPost?.strSelectedType = strSelectedType
                self.navigationController?.pushViewController(objUserUploadPost!, animated: true)
            }
        }
    }
    
    @IBAction func playBtnAction(_ sender: Any) {
        if playerLayer.player?.rate != 0, playerLayer.player?.error == nil {
            // player is playing
            playImg.isHidden = false
            playerLayer.player?.pause()
        } else {
            playImg.isHidden = true
            playerLayer.player?.play()
        }
    }
}

extension UploadImageGalleryVC: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        DispatchQueue.main.async {
            self.loadPhotos()
        }
    }
}

extension UploadImageGalleryVC {
    /* Gives a resolution for the video by URL */
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}

extension UploadImageGalleryVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectionType != 1 {
            print("PHOTOS count NO row: \(allVideos.count)")
            return allVideos.count
        } else {
            return 0
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotoCollectionViewCell
//        let asset = photos!.object(at: indexPath.row)
//        let width: CGFloat = 150
//        let height: CGFloat = 150
//        let size = CGSize(width:width, height:height)
//        PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: PHImageContentMode.aspectFill, options: nil) { (image, userInfo) -> Void in
//
//            self.photoImageView.image = image
//            self.lblVideoTime.text = String(format: "%02d:%02d",Int((asset.duration / 60)),Int(asset.duration) % 60)
//
//        }
//        return cell
//    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellVideo", for: indexPath) as? VideoGalleryCollectionViewCell {
//            let asset = videoGallery[indexPath.item]
//            let durationSeconds = CMTimeGetSeconds(asset.duration)
//            asset.generateThumbnail { [weak self] (image) in
//                            DispatchQueue.main.async {
//                                guard let image = image else { return }
//                                cell.imageCell.image = image
//                            }
//                        }
//            cell.lblVideoTime.text = "\(indexPath.row)"
            cell.lblVideoTime.textColor = UIColor.white
//            let strTime = Constants.secondsToMinutesSeconds(Int(durationSeconds))
//            cell.lblVideoTime.text = strTime
//

            if indexPath.row == 0 { // for first cell in the collection
                cell.backgroundColor = UIColor.orange
            } else {
                cell.backgroundColor = UIColor.blue
            }
            
            let width: CGFloat = 150
            let height: CGFloat = 150
            let size = CGSize(width: width, height: height)
            let asset = allVideos!.object(at: indexPath.row)

            PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: PHImageContentMode.aspectFill, options: nil) { (image, _) -> Void in
            
                cell.imageCell.image = image
                cell.lblVideoTime.text = String(format: "%02d:%02d", Int(asset.duration / 60), Int(asset.duration) % 60)
            }
                
            return cell
        }

        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCell = collectionView.cellForItem(at: indexPath) as! VideoGalleryCollectionViewCell
        PHCachingImageManager().requestAVAsset(forVideo: allVideos!.object(at: indexPath.row), options: nil) { avAsset, _, _ in
            DispatchQueue.main.async { [weak self] in
             
                guard let videoAsset = avAsset as? AVURLAsset else { return }
                self?.strVideo = "\(videoAsset.url)"
                let playerItem = AVPlayerItem(asset: videoAsset)
                let player = AVPlayer(playerItem: playerItem)
                // 3. Create AVPlayerLayer object
                var playerLayer = self?.playerLayer
                if playerLayer?.player == nil {
                    playerLayer = AVPlayerLayer(player: player)
                    playerLayer?.frame = (self?.selectedVideoImg.bounds)! // bounds of the view in which AVPlayer should be displayed
                    playerLayer?.videoGravity = .resizeAspect
                    self?.selectedVideoImg.layer.addSublayer((playerLayer)!)
            
                } else {
                    playerLayer?.player = player
                }
                // 5. Play Video
                playerLayer?.player?.play()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 1)
        edgeInsetPadding = Int(inset.left + inset.right)
        return inset
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(minimumSpacing)
    }
       
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(minimumSpacing)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (Int(UIScreen.main.bounds.size.width) - (numberOfItemsInRow - 1) * minimumSpacing - edgeInsetPadding) / numberOfItemsInRow
        return CGSize(width: width, height: width)
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return ((rate != 0) && (error == nil))
    }
}

extension UploadImageGalleryVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if blogDescriptionView.textColor == UIColor.lightGray {
            blogDescriptionView.text = nil
            blogDescriptionView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if blogDescriptionView.text.isEmpty {
            blogDescriptionView.text = descriptionTextViewPlaceHolder
            blogDescriptionView.textColor = UIColor.lightGray
        }
    }
}

extension AVAsset {
    func generateThumbnail(completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global().async {
            let imageGenerator = AVAssetImageGenerator(asset: self)
            let time = CMTime(seconds: 0.0, preferredTimescale: 600)
            let times = [NSValue(time: time)]
            imageGenerator.generateCGImagesAsynchronously(forTimes: times, completionHandler: { _, image, _, _, _ in
                if let image = image {
                    completion(UIImage(cgImage: image))
                } else {
                    completion(nil)
                }
            })
        }
    }
}
