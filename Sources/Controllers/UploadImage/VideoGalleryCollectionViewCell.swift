//
//  VideoGalleryCollectionViewCell.swift
//  bloook
//
//  Created by Tech Astha on 19/01/21.
//

import UIKit

class VideoGalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var lblVideoTime: UILabel!
    
}
