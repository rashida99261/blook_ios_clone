//
//  VideoCollectionViewCell.swift
//  bloook
//
//  Created by Tech Astha on 29/05/21.
//

import AVFoundation
import Kingfisher
import UIKit


class TopVideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var videoImg: UIImageView!
    @IBOutlet weak var lblVideoTime: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        videoImg.kf.cancelDownloadTask()
        videoImg.kf.setImage(with: URL(string: ""))
        videoImg.image = UIImage(named: "postPlaceholder")
    }
}

