//
//  TopScreenVC.swift
//  bloook
//
//  Created by Tech Astha on 09/03/21.
//

import AVKit
import Kingfisher
import UIKit

class TopScreenVC: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet var tipPhotoView: UIView!
    @IBOutlet var topVideoView: UIView!
    @IBOutlet var topBlogView: UIView!
    @IBOutlet var topQuantityView: UIView!
    @IBOutlet var topLikeView: UIView!
    @IBOutlet var topFollowCountView: UIView!
    @IBOutlet var topLooksView: UIView!

    @IBOutlet var annualBtn: UIButton!
    @IBOutlet var dailyBtn: UIButton!
    @IBOutlet var lifetimeBtn: UIButton!
    @IBOutlet var dailyVideoBtn: UIButton!
    @IBOutlet var dailyAnnualVideoBtn: UIButton!
    @IBOutlet var lifetimeVideoBtn: UIButton!
    @IBOutlet var dailyBlogBtn: UIButton!
    @IBOutlet var annualBlogBtn: UIButton!
    @IBOutlet var lifetimeBlogBtn: UIButton!
    @IBOutlet var dailyQuantityBtn: UIButton!
    @IBOutlet var dailyAnnualQuantityBtn: UIButton!
    @IBOutlet var lifetimeQuantityBtn: UIButton!
    @IBOutlet var dailyLikeBtn: UIButton!
    @IBOutlet var dailyAnnualLikeBtn: UIButton!
    @IBOutlet var lifetimeLikeBtn: UIButton!
    @IBOutlet var dailyFollowCountBtn: UIButton!
    @IBOutlet var AnnualFollowCountBtn: UIButton!
    @IBOutlet var lifetimeFollowCountBtn: UIButton!
    @IBOutlet var dailyLooksBtn: UIButton!
    @IBOutlet var annualLooksBtn: UIButton!
    @IBOutlet var lifetimeLookBtn: UIButton!

    @IBOutlet var quantityOtherView: UIView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    var viewWidth: CGFloat = 0.0
    var viewHeight = CGFloat()
    var page: CGFloat = 0.0
    @IBOutlet var imageView: UIView!
    @IBOutlet var videoView: UIView!
    @IBOutlet var blogView: UIView!
    @IBOutlet var quantityView: UIView!
    @IBOutlet var likeView: UIView!
    @IBOutlet var followCountView: UIView!
    @IBOutlet var looksView: UIView!
    @IBOutlet var imgLineView: UIView!
    @IBOutlet var videoLineView: UIView!
    @IBOutlet var blogLineView: UIView!
    @IBOutlet var quantityLineView: UIView!
    @IBOutlet var likeLineView: UIView!
    @IBOutlet var followCountLineView: UIView!
    @IBOutlet var lookLineView: UIView!
    @IBOutlet var lineView: UIView!
    @IBOutlet var collectionImgView: UICollectionView!
    @IBOutlet var collectionVideoView: UICollectionView!
    @IBOutlet var blogTable: UITableView!
    @IBOutlet var quantityTableView: UITableView!
    @IBOutlet var likeTableView: UITableView!
    @IBOutlet var followCountTableView: UITableView!
    @IBOutlet var looksTableView: UITableView!
    
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    var strType = String()
    var strEarnSelectesType = String()
   // var progressView: AcuantProgressView!
    var strId = String()

    private var gradientLayer = CAGradientLayer()
    private var gradientLayer1 = CAGradientLayer()
    private var gradientLayer2 = CAGradientLayer()
    private var gradientLayer3 = CAGradientLayer()

    var allArr: [Any]?
    var imagesArr: [LBPost]?
    @IBOutlet var mainView: UIView!
    @IBOutlet var photoImgView: UIView!

    var MenuImage = [Any]()
    var postDictt = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true
        self.MenuImage = ["img8", "img9", "img10", "img11", "img12", "img13", "img14", "img15", "img16", "img17", "img18", "img19", "img20", "img21", "img22", "img23", "img24", "img25", "img26", "img27", "img28", "img1", "img2", "img3", "user", "img5", "img7"]

        self.titleLbl.text = "Top Photos"
        self.dailyBtn.setTitleColor(UIColor.black, for: .normal)
        self.dailyVideoBtn.setTitleColor(UIColor.black, for: .normal)
        self.dailyBlogBtn.setTitleColor(UIColor.black, for: .normal)
        self.dailyQuantityBtn.setTitleColor(UIColor.black, for: .normal)
        self.dailyLikeBtn.setTitleColor(UIColor.black, for: .normal)
        self.dailyFollowCountBtn.setTitleColor(UIColor.black, for: .normal)
        self.dailyLooksBtn.setTitleColor(UIColor.black, for: .normal)

        self.collectionImgView.delegate = self
        self.collectionImgView.delegate = self
        self.collectionImgView.reloadData()
        // blogTable.reloadData()
        self.strId = "0"
        // doTopUserPointListFromServer()
        self.strType = "daily"
        self.strEarnSelectesType = "image"
        self.blogTable.register(UINib(nibName: BlogTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: BlogTableViewCell.reuseIdentifier)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.doTopPostsListFromServer()
        // doTopUserPointListFromServer()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.addPagesView()
    }

    // MARK: - for Top Post

    func doTopPostsListFromServer() {
        self.imagesArr?.removeAll()
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TYPE: self.strType,
                     Constants.KEY_POST_TYPE: self.strEarnSelectesType,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.getTopPostList(param, completion: { response, error in
            DispatchQueue.main.async {
                self.hideProgressView()
                if error != nil {
                    self.checkForAuthorization(message: error?.localizedDescription)
                } else {
                    if response?.status == 200 {
                        guard let posts = response?.lbPosts else { return }
                        self.imagesArr = posts
                        DispatchQueue.main.async {
                            if self.strId == "2" {
                                self.blogTable.reloadData()
                            } else if self.strId == "0" {
                                self.collectionImgView.reloadData()
                                self.hideProgressView()
                            } else {
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) { [weak self] in
                                    self?.collectionVideoView.reloadData()
                                }

                                self.hideProgressView()
                            }
                        }

                    } else {
                        self.checkForAuthorization(message: response?.message)
                    }
                }
            }
        })
    }

    private func checkForAuthorization(message: String?) {
        if message == "Unauthorized" {
            tabBarController?.tabBar.isHidden = true

            PSUtil.showAlertFromController(controller: self, withMessage: message ?? "", andHandler: { _ in
                UserDefaults.standard.set(false, forKey: Constants.ISLOGIN)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
                self.navigationController?.pushViewController(objVC!, animated: true)
            })
        }
    }

    // MARK: - for User Achievements

    func doTopUserPointListFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TYPE: self.strType,
                     Constants.KEY_EARN_TYPE: self.strEarnSelectesType,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        // PSUtil.showProgressHUD()
        print(param)
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doTopUserPointList(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    self.allArr = response[Constants.KEY_POSTDATA] as? [Any] ?? [Any]()
                    print(self.allArr)
                    if self.strId == "3" {
                        self.quantityTableView.reloadData()
                    } else if self.strId == "4" {
                        self.likeTableView.reloadData()
                    } else if self.strId == "5" {
                        self.followCountTableView.reloadData()
                    } else if self.strId == "6" {
                        self.looksTableView.reloadData()
                    } else {
                        self.blogTable.reloadData()
                    }

                    //  self.likeTableView.reloadData()
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }

    override func viewDidLayoutSubviews() {
        self.tipPhotoView.layer.masksToBounds = true
        self.tipPhotoView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        self.topVideoView.layer.masksToBounds = true
        self.topVideoView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        self.topBlogView.layer.masksToBounds = true
        self.topBlogView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        self.topQuantityView.layer.masksToBounds = true
        self.topQuantityView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        self.topLikeView.layer.masksToBounds = true
        self.topLikeView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        self.topFollowCountView.layer.masksToBounds = true
        self.topFollowCountView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        self.topLooksView.layer.masksToBounds = true
        self.topLooksView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }

    // Add
    func addPagesView() {
        self.scrollView.delegate = self
        self.viewWidth = self.view.frame.width
        self.viewHeight = self.scrollView.frame.height
        print(self.viewWidth)
        self.scrollView.contentSize = CGSize(width: self.viewWidth * 7, height: self.viewHeight)
        self.imageView.frame = CGRect(x: 0, y: 0, width: self.viewWidth, height: self.viewHeight)
        self.videoView.frame = CGRect(x: self.viewWidth, y: 0, width: self.viewWidth, height: self.viewHeight)
        self.blogView.frame = CGRect(x: self.viewWidth * 2, y: 0, width: self.viewWidth, height: self.viewHeight)
        self.quantityView.frame = CGRect(x: self.viewWidth * 3, y: 0, width: self.viewWidth, height: self.viewHeight)
        self.likeView.frame = CGRect(x: self.viewWidth * 4, y: 0, width: self.viewWidth, height: self.viewHeight)
        self.followCountView.frame = CGRect(x: self.viewWidth * 5, y: 0, width: self.viewWidth, height: self.viewHeight)
        self.looksView.frame = CGRect(x: self.viewWidth * 6, y: 0, width: self.viewWidth, height: self.viewHeight)

        if self.imageView.superview == nil {
            self.scrollView.addSubview(self.imageView)
        }
        if self.videoView.superview == nil {
            self.scrollView.addSubview(self.videoView)
        }
        if self.blogView.superview == nil {
            self.scrollView.addSubview(self.blogView)
        }
        if self.quantityView.superview == nil {
            self.scrollView.addSubview(self.quantityView)
        }
        if self.likeView.superview == nil {
            self.scrollView.addSubview(self.likeView)
        }
        if self.followCountView.superview == nil {
            self.scrollView.addSubview(self.followCountView)
        }
        if self.looksView.superview == nil {
            self.scrollView.addSubview(self.looksView)
        }
        self.scrollView.isPagingEnabled = true
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }

    func scrollViewDidScroll(_ sender: UIScrollView) {
        if sender != self.scrollView {
            return
        }
        let pageWidth: CGFloat = self.scrollView.frame.size.width
        self.page = self.scrollView.contentOffset.x / pageWidth
        if self.page >= 0.0, self.page <= 7.0 {
            if self.page == 0 {
                self.imgLineView.backgroundColor = UIColor.black
                self.videoLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.blogLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.quantityLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.likeLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.followCountLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.lookLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.titleLbl.text = "Top Photos"
                self.strId = "0"
                self.strType = "daily"
                self.strEarnSelectesType = "image"
                self.doTopPostsListFromServer()
            } else if self.page == 1 {
                self.imgLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.videoLineView.backgroundColor = UIColor.black
                self.blogLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.quantityLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.likeLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.followCountLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.lookLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.titleLbl.text = "Top Videos"
                self.strId = "1"
                self.strType = "daily"
                self.strEarnSelectesType = "video"
                self.doTopPostsListFromServer()
            } else if self.page == 2 {
                self.imgLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.videoLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.blogLineView.backgroundColor = UIColor.black
                self.quantityLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.likeLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.followCountLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.lookLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.titleLbl.text = "Top Blogs"
                self.strId = "2"
                self.strType = "daily"
                self.strEarnSelectesType = "blog"
                self.doTopPostsListFromServer()
            } else if self.page == 3 {
                self.imgLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.videoLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.blogLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.quantityLineView.backgroundColor = UIColor.black
                self.likeLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.followCountLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.lookLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.strId = "3"
                self.titleLbl.text = "Bouncing"
                self.strType = "daily"
                self.strEarnSelectesType = "quantity"
                self.doTopUserPointListFromServer()
            } else if self.page == 4 {
                self.imgLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.videoLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.blogLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.quantityLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.likeLineView.backgroundColor = UIColor.black
                self.followCountLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.lookLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.strId = "4"
                self.strEarnSelectesType = "like"
                self.strType = "daily"
                self.titleLbl.text = "View/Like"
                self.doTopUserPointListFromServer()
            } else if self.page == 5 {
                self.imgLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.videoLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.blogLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.quantityLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.likeLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.followCountLineView.backgroundColor = UIColor.black
                self.lookLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.strId = "5"
                self.titleLbl.text = "Follow Count"
                self.strEarnSelectesType = "follower"
                self.strType = "daily"
                self.doTopUserPointListFromServer()
            } else if self.page == 6 {
                self.imgLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.videoLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.blogLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.quantityLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.likeLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.followCountLineView.backgroundColor = Constants.colorConstants.CLR_LIGHTGRAY_THEME
                self.lookLineView.backgroundColor = UIColor.black
                self.strId = "6"
                self.titleLbl.text = "Looks"
                self.strEarnSelectesType = "look"
                self.strType = "daily"
                self.doTopUserPointListFromServer()
            }
        }
    }

    @IBAction func backBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func dailyBtnAction(_ sender: Any) {
        self.dailyBtn.setTitleColor(UIColor.black, for: .normal)
        self.annualBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "daily"
        self.strEarnSelectesType = "image"
        self.doTopPostsListFromServer()
    }

    @IBAction func annualBtnAction(_ sender: Any) {
        self.dailyBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.annualBtn.setTitleColor(UIColor.black, for: .normal)
        self.lifetimeBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "annual"
        self.strEarnSelectesType = "image"
        self.doTopPostsListFromServer()
    }

    @IBAction func lifetimePhotoBtnAction(_ sender: Any) {
        self.dailyBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.annualBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeBtn.setTitleColor(UIColor.black, for: .normal)
        self.strType = "lifetime"
        self.strEarnSelectesType = "image"
        self.doTopPostsListFromServer()
    }

    @IBAction func dailyVideoBtnAction(_ sender: Any) {
        self.dailyVideoBtn.setTitleColor(UIColor.black, for: .normal)
        self.dailyAnnualVideoBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeVideoBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "daily"
        self.strEarnSelectesType = "video"
        self.doTopPostsListFromServer()
    }

    @IBAction func annualVideoBtnAction(_ sender: Any) {
        self.dailyVideoBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.dailyAnnualVideoBtn.setTitleColor(UIColor.black, for: .normal)
        self.lifetimeVideoBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "annual"
        self.strEarnSelectesType = "video"
        self.doTopPostsListFromServer()
    }

    @IBAction func lifetimeVideoBtnAction(_ sender: Any) {
        self.dailyVideoBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.dailyAnnualVideoBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeVideoBtn.setTitleColor(UIColor.black, for: .normal)
        self.strType = "lifetime"
        self.strEarnSelectesType = "video"
        self.doTopPostsListFromServer()
    }

    @IBAction func dailyBlogBtn(_ sender: Any) {
        self.dailyBlogBtn.setTitleColor(UIColor.black, for: .normal)
        self.annualBlogBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeBlogBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "daily"
        self.strEarnSelectesType = "blog"
        self.doTopPostsListFromServer()
    }

    @IBAction func dailyAnnualBlogBtn(_ sender: Any) {
        self.dailyBlogBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.annualBlogBtn.setTitleColor(UIColor.black, for: .normal)
        self.lifetimeBlogBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "annual"
        self.strEarnSelectesType = "blog"
        self.doTopPostsListFromServer()
    }

    @IBAction func lifetimeBlogBtnAction(_ sender: Any) {
        self.dailyBlogBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.annualBlogBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeBlogBtn.setTitleColor(UIColor.black, for: .normal)
        self.strType = "lifetime"
        self.strEarnSelectesType = "blog"
        self.doTopPostsListFromServer()
    }

    @IBAction func dailyQuantityBtnAction(_ sender: Any) {
        self.dailyQuantityBtn.setTitleColor(UIColor.black, for: .normal)
        self.dailyAnnualQuantityBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeQuantityBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "daily"
        self.strEarnSelectesType = "quantity"
        self.doTopUserPointListFromServer()
    }

    @IBAction func dailyAnnualQuantityBtnAction(_ sender: Any) {
        self.dailyQuantityBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.dailyAnnualQuantityBtn.setTitleColor(UIColor.black, for: .normal)
        self.lifetimeQuantityBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "annual"
        self.strEarnSelectesType = "quantity"
        self.doTopUserPointListFromServer()
    }

    @IBAction func lifetimeQuantityBtnAction(_ sender: Any) {
        self.dailyQuantityBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.dailyAnnualQuantityBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeQuantityBtn.setTitleColor(UIColor.black, for: .normal)
        self.strType = "lifetime"
        self.strEarnSelectesType = "quantity"
        self.doTopUserPointListFromServer()
    }

    @IBAction func dailyLikeBtnBtnAction(_ sender: Any) {
        self.dailyLikeBtn.setTitleColor(UIColor.black, for: .normal)
        self.dailyAnnualLikeBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeLikeBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "daily"
        self.strEarnSelectesType = "like"
        self.doTopUserPointListFromServer()
    }

    @IBAction func dailyAnnualLikeBtnAction(_ sender: Any) {
        self.dailyLikeBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.dailyAnnualLikeBtn.setTitleColor(UIColor.black, for: .normal)
        self.lifetimeLikeBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "annual"
        self.strEarnSelectesType = "like"
        self.doTopUserPointListFromServer()
    }

    @IBAction func lifetimeLikeBtnAction(_ sender: Any) {
        self.dailyLikeBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.dailyAnnualLikeBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeLikeBtn.setTitleColor(UIColor.black, for: .normal)
        self.strType = "lifetime"
        self.strEarnSelectesType = "like"
        self.doTopUserPointListFromServer()
    }

    @IBAction func dailyFollowCountBtnAction(_ sender: Any) {
        self.dailyFollowCountBtn.setTitleColor(UIColor.black, for: .normal)
        self.AnnualFollowCountBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeFollowCountBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "daily"
        self.strEarnSelectesType = "follower"
        self.doTopUserPointListFromServer()
    }

    @IBAction func annualFollowCountBtnAction(_ sender: Any) {
        self.dailyFollowCountBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.AnnualFollowCountBtn.setTitleColor(UIColor.black, for: .normal)
        self.lifetimeFollowCountBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "annual"
        self.strEarnSelectesType = "follower"
        self.doTopUserPointListFromServer()
    }

    @IBAction func lifetimeFollowCountBtnAction(_ sender: Any) {
        self.dailyFollowCountBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.AnnualFollowCountBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeFollowCountBtn.setTitleColor(UIColor.black, for: .normal)
        self.strType = "lifetime"
        self.strEarnSelectesType = "follower"
        self.doTopUserPointListFromServer()
    }

    @IBAction func dailyLooksBtnAction(_ sender: Any) {
        self.dailyLooksBtn.setTitleColor(UIColor.black, for: .normal)
        self.annualLooksBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeLookBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "daily"
        self.strEarnSelectesType = "look"
        self.doTopUserPointListFromServer()
    }

    @IBAction func annualLooksBtnAction(_ sender: Any) {
        self.dailyLooksBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.annualLooksBtn.setTitleColor(UIColor.black, for: .normal)
        self.lifetimeLookBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.strType = "annual"
        self.strEarnSelectesType = "look"
        self.doTopUserPointListFromServer()
    }

    @IBAction func lifetimeLookBtnAction(_ sender: Any) {
        self.dailyLooksBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.annualLooksBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeLookBtn.setTitleColor(UIColor.black, for: .normal)
        self.strType = "lifetime"
        self.strEarnSelectesType = "look"
        self.doTopUserPointListFromServer()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionImgView {
            return self.imagesArr?.count ?? 0
        } else if collectionView == self.collectionVideoView {
            return self.imagesArr?.count ?? 0
        } else {
            return 10
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let lbpost = self.imagesArr?[indexPath.row]
        let post = lbpost?.posts
        if collectionView == self.collectionImgView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! ImageCollectionViewCell
            let strPostPhoto = post?.filename
            if strPostPhoto == "" {
                cell.topImg.kf.setImage(with: URL(string: "img10"), placeholder: UIImage(named: "placeholder.png"))
            } else {
                cell.topImg.kf.setImage(with: post?.thumbImageURL(), placeholder: UIImage(named: "placeholder.png"))
            }

            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! TopVideoCollectionViewCell
            let strFileType = post?.type
            if strFileType == .video {
                let strThumnailImg = post?.thumbnail ?? ""
                let placeholderImage = UIImage(named: "postPlaceholder")
                cell.videoImg.kf.setImage(with: URL(string: strThumnailImg), placeholder: placeholderImage)
                //cell.configureThumbnail(fileName: post?.filename ?? "")
                cell.lblVideoTime.text = post?.videoDuration()
                cell.lblVideoTime.textColor = UIColor.white
            }
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionImgView {
            let itemsPerRow: CGFloat = 3
            let hardCodedPadding: CGFloat = 2
            let itemWidth = (self.collectionImgView.bounds.width / itemsPerRow) - hardCodedPadding
            return CGSize(width: itemWidth, height: itemWidth)
        } else if collectionView == self.collectionVideoView {
            let itemsPerRow: CGFloat = 3
            let hardCodedPadding: CGFloat = 2
            let itemWidth = (self.collectionVideoView.bounds.width / itemsPerRow) - hardCodedPadding
            return CGSize(width: itemWidth, height: itemWidth)
        } else {
            let itemsPerRow: CGFloat = 3
            let hardCodedPadding: CGFloat = 2
            let itemWidth = (self.collectionImgView.bounds.width / itemsPerRow) - hardCodedPadding
            return CGSize(width: itemWidth, height: itemWidth)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let lbpost = self.imagesArr?[indexPath.row]
        let post = lbpost?.posts
        let otherStoryboard = UIStoryboard(name: "Posts", bundle: nil)
        let objTopPhoto = otherStoryboard.instantiateViewController(withIdentifier: "PostsViewController") as? PostsViewController
        objTopPhoto?.singlePost = post
        if post?.type == .image {
            objTopPhoto?.topTitle = "Top Photos"
        } else if post?.type == .video {
            objTopPhoto?.topTitle = "Top Videos"
        }
        self.navigationController?.pushViewController(objTopPhoto!, animated: true)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.blogTable {
            return self.imagesArr?.count ?? 0
        } else if tableView == self.quantityTableView {
            return self.allArr?.count ?? 0
        } else if tableView == self.likeTableView {
            return self.allArr?.count ?? 0
        } else if tableView == self.followCountTableView {
            return self.allArr?.count ?? 0
        } else if tableView == self.looksTableView {
            return self.allArr?.count ?? 0
        } else {
            return 10
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.blogTable {
            let lbpost = self.imagesArr?[indexPath.row]
            let post = lbpost?.posts
            let cell = tableView.dequeueReusableCell(withIdentifier: BlogTableViewCell.reuseIdentifier, for: indexPath) as! BlogTableViewCell
            cell.delegate = self
            cell.configure(post: post)
            return cell

        } else if tableView == self.quantityTableView {
            let dict = self.allArr?[indexPath.row] as? NSDictionary ?? NSDictionary()
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QuantityTableViewCell
            cell.selectionStyle = .none

//            let gradient = CAGradientLayer()
//                gradient.frame =  CGRect(origin: CGPoint.zero, size: cell.profileView.frame.size)
//                gradient.colors = [UIColor.blue.cgColor, UIColor.green.cgColor]
//
//                let shape = CAShapeLayer()
//                shape.lineWidth = 2
//                shape.path = UIBezierPath(rect: cell.profileView.bounds).cgPath
//                shape.strokeColor = UIColor.red.cgColor
//                shape.fillColor = UIColor.clear.cgColor
//                gradient.mask = shape

            // cell.profileView.layer.addSublayer(gradient)

            // cell.profileView
            let userDict = dict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
            let strName = "\(userDict.value(forKey: Constants.KEY_NAME) ?? "")"
            cell.userNameLbl.text = strName
            let strTotalPoints = "\(dict.value(forKey: Constants.KEY_TOTAL_POINTS) ?? "")"
            cell.pointLbl.text = strTotalPoints + " points"
            let strEarnType = "\(dict.value(forKey: Constants.KEY_EARN_TYPE) ?? "")"
            cell.descriptionLbl.text = strEarnType
            let strProfilePhoto = "\(userDict.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
            if strProfilePhoto == "" {
                cell.profileImg.sd_setImage(with: URL(string: "img10"), placeholderImage: UIImage(named: "placeholder.png"))
            } else {
                cell.profileImg.sd_setImage(with: URL(string: strProfilePhoto), placeholderImage: UIImage(named: "placeholder.png"))
            }

//            let colorTop =  UIColor(red: 114.0/255.0, green: 146.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
//                    let colorBottom = UIColor(red: 177.0/255.0, green: 178.0/255.0, blue: 254.0/255.0, alpha: 1.0).cgColor
//
//                    let gradientLayer = CAGradientLayer()
//                    gradientLayer.colors = [colorTop, colorBottom]
//                    gradientLayer.locations = [0.0, 1.0]
//                    gradientLayer.frame = cell.bounds
//                    cell.layer.insertSublayer(gradientLayer, at:0)
            cell.backgroundColor = UIColor.clear

            return cell
        } else if tableView == self.likeTableView {
            var dict = NSDictionary()
            dict = self.allArr?[indexPath.row] as? NSDictionary ?? NSDictionary()

            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LikeTableViewCell
            cell.selectionStyle = .none

            let userDict = dict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
            let strName = "\(userDict.value(forKey: "name") ?? "")"
            cell.userNameLbl.text = strName
            let strTotalPoints = "\(dict.value(forKey: Constants.KEY_TOTAL_POINTS) ?? "")"
            // let myInt = Int(strTotalPoints)
            // let str = String(myInt!)
            cell.pointLbl.text = strTotalPoints + " points"
            let strEarnType = "\(dict.value(forKey: Constants.KEY_EARN_TYPE) ?? "")"
            cell.descriptionLbl.text = strEarnType
            let strProfilePhoto = "\(userDict.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
            cell.profileImg.sd_setImage(with: URL(string: strProfilePhoto), placeholderImage: UIImage(named: "placeholder.png"))
            cell.backgroundColor = UIColor.clear
            return cell
        } else if tableView == self.followCountTableView {
            let dict = self.allArr?[indexPath.row] as? NSDictionary ?? NSDictionary()
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FollowCountTableViewCell
            cell.selectionStyle = .none
            let userDict = dict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
            let strName = "\(userDict.value(forKey: "name") ?? "")"
            cell.userNameLbl.text = strName
            let strTotalPoints = "\(dict.value(forKey: Constants.KEY_TOTAL_POINTS) ?? "")"
            cell.pointLbl.text = strTotalPoints + " points"
            let strEarnType = "\(dict.value(forKey: Constants.KEY_EARN_TYPE) ?? "")"
            cell.descriptionLbl.text = strEarnType
            let strProfilePhoto = "\(userDict.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
            cell.profileImg.sd_setImage(with: URL(string: strProfilePhoto), placeholderImage: UIImage(named: "placeholder.png"))
            cell.backgroundColor = UIColor.clear
            return cell
        } else if tableView == self.looksTableView {
            let dict = self.allArr?[indexPath.row] as? NSDictionary ?? NSDictionary()
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LooksTableViewCell
            cell.selectionStyle = .none
            let userDict = dict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
            let strName = "\(userDict.value(forKey: "name") ?? "")"
            cell.userNameLbl.text = strName
            let strTotalPoints = "\(dict.value(forKey: Constants.KEY_TOTAL_POINTS) ?? "")"
            cell.pointLbl.text = strTotalPoints + " points"
            let strEarnType = "\(dict.value(forKey: Constants.KEY_EARN_TYPE) ?? "")"
            cell.descriptionLbl.text = strEarnType
            let strProfilePhoto = "\(userDict.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
            cell.profileImg.sd_setImage(with: URL(string: strProfilePhoto), placeholderImage: UIImage(named: "placeholder.png"))
            cell.backgroundColor = UIColor.clear
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QuantityTableViewCell
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QuantityTableViewCell
        // let cell = tableView.cellForRow(at: indexPath)! as! QuantityTableViewCell
        if tableView == self.quantityTableView {
            let dict = self.allArr?[indexPath.row] as? NSDictionary ?? NSDictionary()
            let strId = "\(dict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
            print(strId)
            let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            self.tabBarController?.tabBar.isHidden = false
            UserDefaults.standard.set(strUserId, forKey: Constants.KEY_ID)
            let otherStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
            objMyProfile?.selectedUserId = strId
            self.navigationController?.pushViewController(objMyProfile!, animated: true)
        } else if tableView == self.likeTableView {
            let dict = self.allArr?[indexPath.row] as? NSDictionary ?? NSDictionary()
            let strId = "\(dict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
            print(strId)

            self.tabBarController?.tabBar.isHidden = false

            let otherStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
            objMyProfile?.selectedUserId = strId
            self.navigationController?.pushViewController(objMyProfile!, animated: true)

        } else if tableView == self.followCountTableView {
            let dict = self.allArr?[indexPath.row] as? NSDictionary ?? NSDictionary()
            let strId = "\(dict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
            print(strId)
            let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"

            self.tabBarController?.tabBar.isHidden = false
            UserDefaults.standard.set(strUserId, forKey: Constants.KEY_ID)
            let otherStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
            objMyProfile?.selectedUserId = strId
            self.navigationController?.pushViewController(objMyProfile!, animated: true)

        } else if tableView == self.looksTableView {
            let dict = self.allArr?[indexPath.row] as? NSDictionary ?? NSDictionary()
            let strId = "\(dict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
            print(strId)
            let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            
                self.tabBarController?.tabBar.isHidden = false
                UserDefaults.standard.set(strUserId, forKey: Constants.KEY_ID)
                let otherStoryboard = UIStoryboard(name: "Profile", bundle: nil)
                let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
            objMyProfile?.selectedUserId = strId
                self.navigationController?.pushViewController(objMyProfile!, animated: true)
        
        } else if tableView == self.blogTable {
            let lbpost = self.imagesArr?[indexPath.row]
            let post = lbpost?.posts
            let otherStoryboard = UIStoryboard(name: "Posts", bundle: nil)
            let objTopPhoto = otherStoryboard.instantiateViewController(withIdentifier: "PostsViewController") as? PostsViewController
            objTopPhoto?.singlePost = post

            objTopPhoto?.topTitle = "Top Blog"
            self.navigationController?.pushViewController(objTopPhoto!, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.blogTable {
            return UITableView.automaticDimension
        } else {
            return 90
        }
    }

    private func showProgressView(text: String = "") {
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
           // self.progressView.startAnimation()
           // self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }

    private func hideProgressView() {
        DispatchQueue.main.async {
            self.viewLoader.isHidden = true
           // self.progressView.stopAnimation()
           // self.progressView.removeFromSuperview()
        }
    }
}

extension TopScreenVC: BlogTableViewDelegate {
    func profileImageTapped(post: Post?) {
        let otherStoryboard = UIStoryboard(name: "Posts", bundle: nil)
        let objTopPhoto = otherStoryboard.instantiateViewController(withIdentifier: "PostsViewController") as? PostsViewController
        objTopPhoto?.singlePost = post
        self.navigationController?.pushViewController(objTopPhoto!, animated: true)
    }
}
