//
//  QuantityTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 10/03/21.
//

import UIKit

class QuantityTableViewCell: UITableViewCell {
    private var gradientLayer = CAGradientLayer()
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var pointLbl: UILabel!
    @IBOutlet weak var profileView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
