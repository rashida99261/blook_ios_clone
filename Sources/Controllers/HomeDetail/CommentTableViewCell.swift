//
//  CommentTableViewswift
//  bloook
//
//  Created by Tech Astha on 27/01/21.
//

import UIKit
protocol CommentDelegate {
    func commentActionTapped(comment: Comment)
    func replyTapped(comment: Comment)
    func hideReplyTapped(comment: CommentClass, shouldHide: Bool)
}

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var createCommentDateLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var replyBtn: UIButton!
    @IBOutlet weak var hideReplyBtn: UIButton!
    @IBOutlet weak var commentselectionBtn: UIButton!
    @IBOutlet weak var commentUserProfileImg: UIImageView!
    @IBOutlet weak var leadingConstraints: NSLayoutConstraint!
    var delegate: CommentDelegate?
    var comment: Comment?
    var commentClass: CommentClass?
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(commentClass: CommentClass?) {
        
        guard let commentC = commentClass else { return }
        let comment = commentC.comment
        self.comment = commentC.comment
        self.commentClass = commentC
        let isChild = commentC.isChild
        leadingConstraints.constant = isChild ? 50 : 16
        
        commentLbl.text = comment.comment
        userNameLbl.text = comment.user?.fullName()
        commentUserProfileImg.kf.setImage(with: comment.user?.profilePhotoURL(), placeholder: UIImage(named: "placeholder.png"))
        createCommentDateLbl.text = comment.createdAt?.toDate()?.timeAgoSinceDate()
        replyBtn.addTarget(self, action: #selector(replyTapped(_:)), for: UIControl.Event.touchUpInside)
        hideReplyBtn.addTarget(self, action: #selector(hideReplyTapped(_:)), for: UIControl.Event.touchUpInside)
        commentselectionBtn.addTarget(self, action: #selector(commentselectionBtnAction(_:)), for: UIControl.Event.touchUpInside)
        if isChild ||  (comment.children?.count ?? 0) == 0 {
            hideReplyBtn.isHidden = true
        
        } else {
            hideReplyBtn.isHidden = false
            hideReplyBtn.setTitle("Unhide \(comment.children?.count ?? 0) replies", for: .normal)
            hideReplyBtn.setTitle("Hide \(comment.children?.count ?? 0) replies", for: .selected)
            hideReplyBtn.isSelected = commentC.isOpen
        }
    }

    @objc func replyTapped(_ sender: UIButton) {
        guard let comment = comment else { return }
        delegate?.replyTapped(comment: comment)
    }
    
    @objc func hideReplyTapped(_ sender: UIButton) {
        guard let commentClass = commentClass else { return }
        delegate?.hideReplyTapped(comment: commentClass, shouldHide: !sender.isSelected)
        
    }

    @objc func commentselectionBtnAction(_ sender: UIButton) {
        guard let comment = comment else { return }
        delegate?.commentActionTapped(comment: comment)
    }
}
