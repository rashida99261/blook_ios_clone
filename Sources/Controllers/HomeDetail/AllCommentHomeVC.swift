//
//  AllCommentHomeVC.swift
//  bloook
//
//  Created by Tech Astha on 06/01/21.
//

import IQKeyboardManagerSwift
import UIKit
import Toaster

class AllCommentHomeVC: BaseViewController {
    @IBOutlet var switchView: UIView!
    @IBOutlet var turnOffLbl: UILabel!
    @IBOutlet var commentCountLbl: UILabel!

    @IBOutlet var switchImg: UIImageView!
    @IBOutlet var commentTable: UITableView!
    @IBOutlet var commentField: UITextField!
    @IBOutlet var topStatusView: UIView!
    @IBOutlet var currentUserView: CurrentUserView!
    @IBOutlet var enterCommentView: UIView!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
  

    var selectedPostId: String!
    var post: Post?
    var strBtnId = String()
    var strIndexId = String()

    var selectedComment: Comment?
    var allComments = [CommentClass]()
    var commentsFromServer: [Comment]?
    var unCollapsedCommentIDs = [Int]()
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        self.hideKeyboardWhenTappedAround()
        // commentTable.reloadData()

        // self.tabBarController?.tabBar.isHidden = false
//        navigationController?.interactivePopGestureRecognizer?.delegate = self
//        navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        self.doPostCommentListFromServer()

        let nib = UINib(nibName: "CommentTableViewCell", bundle: nil)
        commentTable.register(nib, forCellReuseIdentifier: String(describing: "CommentTableViewCell"))
        // commentField.becomeFirstResponder()
    }

    override func viewDidLayoutSubviews() {
        self.topStatusView.layer.masksToBounds = true
        self.topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }

    override func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.commentTable.addGestureRecognizer(tap)
    }

    // MARK: - for Comment List

    func doPostCommentListFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"

        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: self.selectedPostId ?? "",
                     Constants.KEY_PAGE: "200",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        self.allComments.removeAll()
        PSWebServiceAPI.getCommentPostList(param, completion: { response, error in

            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.post = response?.postDetail?.post
                    guard let post = self.post else { return }

                    let commentCount = "\(post.commentsCount ?? post.comments?.count ?? 0)"
                    self.commentCountLbl.text = commentCount + " Comments"
                    self.currentUserView.configure(post: post)
                    self.currentUserView.delegate = self
                    let strOwnPost = response?.postDetail?.ownPost
                    DispatchQueue.main.async {
                        if strOwnPost == 0 {
                            self.switchView.isHidden = true
                            self.switchImg.isHidden = true
                            self.turnOffLbl.isHidden = true

                        } else {
                            self.switchView.isHidden = false
                            self.switchImg.isHidden = false
                            self.turnOffLbl.isHidden = false

                            self.changeCommentStatus(off: post.commentStatus?.boolValue() ?? false)
                        }

                        self.enterCommentView.isHidden = !(post.commentStatus?.boolValue() ?? true)
                        if (post.commentStatus ?? 0).boolValue() == false {
                            self.commentField.resignFirstResponder()
                        }
                        self.commentsFromServer = post.comments
                        self.processComment(comments: post.comments)
                        self.commentTable.reloadData()
                        self.dismissKeyboard()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }

    private func processComment(comments: [Comment]?) {
        allComments.removeAll()
        guard let comments = comments else { return }
        for mainComment in comments {
            
            let results = unCollapsedCommentIDs.filter { $0 ==  mainComment.id }
            if (results.isEmpty) == false {
                let commentClass = CommentClass(comment: mainComment, isChild: false, isOpen: true)
                allComments.append(commentClass)
                if let child = mainComment.children {
                    for childComment in child {
                        let commentClass = CommentClass(comment: childComment, isChild: true, isOpen: true)
                        allComments.append(commentClass)
                    }
                }
            } else {
                let commentClass = CommentClass(comment: mainComment, isChild: false, isOpen: false)
                allComments.append(commentClass)
            }
        }
    }

    func turnOffComments(turnOff: Bool = true) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"

        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: self.selectedPostId ?? "",
                     "comment_status": turnOff ? "0" : "1",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.updateCommentSetting(param, completion: { response in

            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    PSUtil.hideProgressHUD()
                    DispatchQueue.main.async {
                        self.doPostCommentListFromServer()
                        self.commentField.text! = ""
                    }

                } else {
                    PSUtil.hideProgressHUD()
                    guard let msg = response[Constants.KEY_MESSAGE] as? String else {
                        PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
                        return
                    }
                    PSUtil.showAlertFromController(controller: self, withMessage: msg)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }

    @IBAction func switchBtnAction(_ sender: Any) {
        if self.strBtnId == "0" {
            self.changeCommentStatus(off: false)
        } else {
            self.changeCommentStatus(off: true)
        }
        self.turnOffComments(turnOff: self.strBtnId == "0" ? false : true)
    }

    func changeCommentStatus(off: Bool = true) {
        if off {
            self.switchImg.image = UIImage(named: "switchOff")
            self.strBtnId = "1"
        } else {
            self.switchImg.image = UIImage(named: "switchon")
            self.strBtnId = "0"
        }
        self.enterCommentView.isHidden = !off
    }

    @IBAction func backBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
        // self.navigationController?.interactivePopGestureRecognizer.delegate = self
    }

    @IBAction func moreBtnAction(_ sender: Any) {}

    @IBAction func commentPostBtnAction(_ sender: Any) {
        if (self.commentField.text?.count)! < 1 {
            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter Comment First.")
            return

        } else {
            self.doPostCommentFromServer()
        }
    }

    // MARK: - for Comment Post

    func doPostCommentFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let parentId = self.selectedComment?.parentCommentID ?? self.selectedComment?.id ?? 0
        var param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: "\(self.post?.id ?? 0)",
                     Constants.KEY_POST_USERId: self.post?.userID ?? "",
                     Constants.KEY_COMMENT: self.commentField.text!,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]
        if parentId > 0 {
            param[Constants.KEY_PARENT_COMMENT_ID] = "\(parentId)"
        }
        //  PSUtil.showProgressHUD()
      //  self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doPostComment(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    PSUtil.hideProgressHUD()
                    DispatchQueue.main.async {
                        self.doPostCommentListFromServer()
                        self.commentField.text! = ""
                    }

                } else {
                    PSUtil.hideProgressHUD()
                    guard let msg = response[Constants.KEY_MESSAGE] as? String else {
                        PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
                        return
                    }
                    PSUtil.showAlertFromController(controller: self, withMessage: msg)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }

    // MARK: - for Delete Comment

    func doDeleteCommentFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_COMMENT_ID: self.selectedComment?.id,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doDeleteComment(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    DispatchQueue.main.async {
                        self.doPostCommentListFromServer()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }

    // MARK: - for Like List

    func doFollowUserFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: self.selectedComment?.userID,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doFollowUser(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    DispatchQueue.main.async {
                        self.doPostCommentListFromServer()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }

    // MARK: - for block User Add

    func doBlockUserFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: self.selectedComment?.userID ?? strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doBlockedFriend(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    DispatchQueue.main.async {
                        let msg = response[Constants.KEY_MESSAGE] as? String
                        Toast(text: msg).show()
                        self.doPostCommentListFromServer()
                    }

                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
}

extension AllCommentHomeVC: UIGestureRecognizerDelegate {
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
//    }
}

extension AllCommentHomeVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allComments.count // self.post?.comments?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        let comment = self.allComments[indexPath.row]

        cell.configure(commentClass: comment)
        cell.delegate = self

        return cell
    }
}

extension AllCommentHomeVC: CommentDelegate {
    func hideReplyTapped(comment: CommentClass, shouldHide: Bool) {
        if shouldHide == true {
            unCollapsedCommentIDs.append(comment.comment.id ?? 0)
        } else {
            if let index = unCollapsedCommentIDs.firstIndex(of: comment.comment.id ?? 0) {
                unCollapsedCommentIDs.remove(at: index)
            }
        }
        processComment(comments: commentsFromServer)
        commentTable.reloadData()
    }

    func commentActionTapped(comment: Comment) {
        self.selectedComment = comment

        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // alert.view.tintColor = UIColor.black
        if  comment.userID == LoggedInUser.shared.user?.id {
            alert.addAction(UIAlertAction(title: "Delete Comment", style: .default, handler: { _ in
                self.doDeleteCommentFromServer()

            }))
        } else {
//        alert.addAction(UIAlertAction(title: "Follow User", style: .default, handler: { _ in
//            self.doFollowUserFromServer()
//        }))
        

//        alert.addAction(UIAlertAction(title: "Report Comment", style: .default, handler: { _ in
//            PSUtil.showAlertFromController(controller: self, withMessage: "Report Comment to Here")
//
//        }))

        alert.addAction(UIAlertAction(title: "Block User", style: .default, handler: { _ in
            // PSUtil.showAlertFromController(controller: self, withMessage: "Block User to Here")
            self.doBlockUserFromServer()
        }))
        }

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in

            print("User click Cancel button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }

    func replyTapped(comment: Comment) {
        self.selectedComment = comment
        let color = UIColor.red
        let placeholder = self.commentField.placeholder ?? "" // There should be a placeholder set in storyboard or elsewhere string or pass empty
        self.commentField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: color])
        // let chilArr =  dict[Constants.KEY_CHILDREN] as? [Any] ?? [Any]()
        self.selectedComment = comment
        self.commentField.text = self.selectedComment?.user?.fullName() ?? "" + " "
        self.commentField.becomeFirstResponder()
    }
}

extension AllCommentHomeVC: CurrentUserViewDelegate {
    func moreOptionsTapped(alert: UIAlertController) {
        present(alert, animated: true, completion: {
            print("completion block")
        })
    }

    func treasureOptionsTapped(post: Post) {}
    func showAlert(with message: String) {}

    func postOption(post: Post, optionType: OptionType) {
        switch optionType {
        case .delete:
            deletePost(post: post)
        case .edit:
            break
        case .share:
            sharePost(post: post)
        case .turnComment:
            self.turnOffComments(post: post)
        case .follow:
            followPost(post: post)
        case .report:
            reportPost(post: post)
        case .block:
            blockUser(user: post.user)
        }
    }

    func profileImageTapped(post: Post) {
        let otherStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
        objMyProfile?.selectedUserId = "\(post.userID ?? 0)"
        navigationController?.pushViewController(objMyProfile!, animated: true)
    }

    func scrollToBottom() {
        DispatchQueue.main.async {
            if self.allComments.isEmpty == false {
                let indexPath = IndexPath(row: self.allComments.count, section: 0)
                self.commentTable.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
}

extension AllCommentHomeVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let keyboardHeight = KeyboardSize.height()
        self.bottomConstraint.constant = 332
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            // self.scrollToBottom()
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.bottomConstraint.constant = 0
    }
}

import UIKit

public class KeyboardSize {
    private static var sharedInstance: KeyboardSize?
    private static var measuredSize = CGRect.zero

    private var addedWindow: UIWindow
    private var textfield = UITextField()

    private var keyboardHeightKnownCallback: () -> Void = {}
    private var simulatorTimeout: Timer?

    public class func setup(_ callback: @escaping () -> Void) {
        guard self.measuredSize == CGRect.zero, self.sharedInstance == nil else {
            return
        }

        self.sharedInstance = KeyboardSize()
        self.sharedInstance?.keyboardHeightKnownCallback = callback
    }

    private init() {
        self.addedWindow = UIWindow(frame: UIScreen.main.bounds)
        self.addedWindow.rootViewController = UIViewController()
        self.addedWindow.addSubview(self.textfield)

        self.observeKeyboardNotifications()
        self.observeKeyboard()
    }

    public class func height() -> CGFloat {
        print("MS = \(self.measuredSize.height)")
        self.measuredSize = KeyboardSize.measuredSize
        print("MS = \(self.measuredSize.height)")
        return self.measuredSize.height
    }

    private func observeKeyboardNotifications() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(self.keyboardChange), name: UIResponder.keyboardDidShowNotification, object: nil)
    }

    private func observeKeyboard() {
        let currentWindow = UIApplication.shared.keyWindow

        self.addedWindow.makeKeyAndVisible()
        self.textfield.becomeFirstResponder()

        currentWindow?.makeKeyAndVisible()

        self.setupTimeoutForSimulator()
    }

    @objc private func keyboardChange(_ notification: Notification) {
        self.textfield.resignFirstResponder()
        self.textfield.removeFromSuperview()

        guard KeyboardSize.measuredSize == CGRect.zero, let info = notification.userInfo,
              let value = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else { return }

        self.saveKeyboardSize(value.cgRectValue)
    }

    private func saveKeyboardSize(_ size: CGRect) {
        self.cancelSimulatorTimeout()
        KeyboardSize.measuredSize = size
        self.keyboardHeightKnownCallback()

        KeyboardSize.sharedInstance = nil
    }

    private func setupTimeoutForSimulator() {
        #if targetEnvironment(simulator)
        let timeout = 2.0
        self.simulatorTimeout = Timer.scheduledTimer(withTimeInterval: timeout, repeats: false, block: { _ in
            print(" KeyboardSize")
            print(" .keyboardDidShowNotification did not arrive after \(timeout) seconds.")
            print(" Please check \"Toogle Software Keyboard\" on the simulator (or press cmd+k in the simulator) and relauch your app.")
            print(" A keyboard height of 0 will be used by default.")
            self.saveKeyboardSize(CGRect.zero)
        })
        #endif
    }

    private func cancelSimulatorTimeout() {
        self.simulatorTimeout?.invalidate()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

class CommentClass {
    var comment: Comment
    var isChild: Bool
    var isOpen: Bool = false

    public init(comment: Comment, isChild: Bool, isOpen: Bool) {
        self.comment = comment
        self.isChild = isChild
        self.isOpen = isOpen
    }
}
