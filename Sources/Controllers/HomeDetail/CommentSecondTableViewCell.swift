//
//  CommentSecondTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 27/01/21.
//

import UIKit

class CommentSecondTableViewCell: UITableViewCell {
    @IBOutlet weak var createCommentDateLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var replyBtn: UIButton!
    @IBOutlet weak var commentselectionBtn: UIButton!
    @IBOutlet weak var commentUserProfileImg: UIImageView!
    var delegate: CommentDelegate?
    var comment: Comment?
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(comment: Comment?) {
        guard let comment = comment else { return }
        self.comment = comment
        commentLbl.text = comment.comment
        userNameLbl.text = comment.user?.fullName()
        commentUserProfileImg.kf.setImage(with: comment.user?.profilePhotoURL(), placeholder: UIImage(named: "placeholder.png"))
        createCommentDateLbl.text = comment.createdAt?.toDate()?.timeAgoSinceDate()
//        replyBtn.addTarget(self, action: #selector(replyTapped(_:)), for: UIControl.Event.touchUpInside)
//        commentselectionBtn.addTarget(self, action: #selector(commentselectionBtnAction(_:)), for: UIControl.Event.touchUpInside)
    }

    @objc func replyTapped(_ sender: UIButton) {
        guard let comment = comment else { return }
        delegate?.replyTapped(comment: comment)
    }

    @objc func commentselectionBtnAction(_ sender: UIButton) {
        guard let comment = comment else { return }
        delegate?.commentActionTapped(comment: comment)
    }
}
