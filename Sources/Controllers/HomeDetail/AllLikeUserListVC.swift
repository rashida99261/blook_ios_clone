//
//  AllLikeUserListVC.swift
//  bloook
//
//  Created by Tech Astha on 08/02/21.
//

import UIKit

class AllLikeUserListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    @IBOutlet weak var likeListTable: UITableView!
    //var progressView : AcuantProgressView!
    var strPostId = String()
    var postDataArr = Array<Any>()
    @IBOutlet weak var topStatusView: UIView!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var followerBtn: UIButton!
    @IBOutlet weak var followingBtn: UIButton!
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true
        likeListTable.delegate = self
        likeListTable.dataSource = self
        
        self.allBtn.setTitleColor(UIColor.black, for: .normal)
        self.followerBtn.setTitleColor(UIColor.lightGray, for: .normal)
        self.followingBtn.setTitleColor(UIColor.lightGray, for: .normal)
        
            
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        doLikeListFromServer()
    }
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    //MARK:- for Like List
    func doLikeListFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_POST_ID:strPostId,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

      //  PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doAllLikeUserList(param , completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    self.postDataArr = dict[Constants.KEY_ALLLIKES] as? [Any] ?? [Any]()
                    print(self.postDataArr.count)
                    self.likeListTable.reloadData()
                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return postDataArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LikeListTableViewCell
        let dict = self.postDataArr[indexPath.row] as? NSDictionary ?? NSDictionary()
        let userDic =  dict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
        print(userDic)
        cell.userNameLbl.text = "\(userDic.value(forKey: Constants.KEY_NAME) ?? "")"
        let strDate = "\(dict.value(forKey: Constants.KEY_CREATE_DATE) ?? "")"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateCreate = dateFormatter.date(from: strDate)
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: (dateCreate)!) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        cell.userDateLbl.text = myStringafd
        let strUserProfileImg = "\(userDic.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            cell.userProfileImg.image = UIImage (named: "user")
        }else{
            cell.userProfileImg.sd_setImage(with: URL(string: strUserProfileImg), placeholderImage: UIImage(named: "placeholder.png"))
        }
        
        return cell
    }
    @IBAction func allBtnAction(_ sender: Any) {
        self.allBtn.setTitleColor(UIColor.black, for: .normal)
        self.followerBtn.setTitleColor(UIColor.lightGray, for: .normal)
        self.followingBtn.setTitleColor(UIColor.lightGray, for: .normal)
    }
    
    @IBAction func followerBtnAction(_ sender: Any) {
        self.allBtn.setTitleColor(UIColor.lightGray, for: .normal)
        self.followerBtn.setTitleColor(UIColor.black, for: .normal)
        self.followingBtn.setTitleColor(UIColor.lightGray, for: .normal)
    }
    @IBAction func followingBtnAction(_ sender: Any) {
        self.allBtn.setTitleColor(UIColor.lightGray, for: .normal)
        self.followerBtn.setTitleColor(UIColor.lightGray, for: .normal)
        self.followingBtn.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
         //   //self.progressView.messageView.text = text
           // self.progressView.startAnimation()
            //self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = true
            self.loadGif.animate()
        }
        }
    
    
   private func hideProgressView(){
        DispatchQueue.main.async {
           // self.progressView.stopAnimation()
           // self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
} 
//extension AllLikeUserListVC:UIGestureRecognizerDelegate {
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
//    }
//}
