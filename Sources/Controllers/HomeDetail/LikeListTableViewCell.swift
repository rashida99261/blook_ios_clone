//
//  LikeListTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 08/02/21.
//

import UIKit

class LikeListTableViewCell: UITableViewCell {

    @IBOutlet weak var userDateLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userProfileImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
