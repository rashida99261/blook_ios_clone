//
//  BounceViewController.swift
//  Bloook
//
//  Created by Suresh Varma on 10/08/21.
//

import Foundation
import InfiniteLayout
import UIKit

protocol BounceVCDelegate {
    func dismissBounceVC(withAction action: Bool)
}

class BounceViewController: BaseViewController {
    @IBOutlet var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }

    @IBOutlet var pageControl: UIPageControl!

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var continueButton: UIButton!
    @IBOutlet var gameView: UIView!
    @IBOutlet var introView: UIView!
    @IBOutlet var informationView: UIView!
    @IBOutlet var selectedUserView: UIView!
    @IBOutlet var selectedUserTitleLabel: UILabel!
    @IBOutlet var selectedUserDescriptionLabel: UILabel!
    @IBOutlet var selectedUserName: UILabel!
    @IBOutlet var selectedFollowerCount: UILabel!
    @IBOutlet var selectedUSerImageView: UIImageView!
    @IBOutlet var collectionView: InfiniteCollectionView!
    var delegate: BounceVCDelegate?
    

    var gameTimer: Timer?
    var carousalTimer: Timer?

    var usersList: [User]?
    var slides = BounceSlideView()
    var infoSlides: [BounceSlideView] = []
    var bouncedUser: User?
    var cellsize = 0.0

    var newOffsetX: CGFloat = 1.0

    init() {
        super.init(
            nibName: String(describing: BounceViewController.self),
            bundle: Bundle(for: BounceViewController.self)
        )
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        initialSetup()
        fakeGameData()
        BounceUserCollectionCell.register(for: collectionView)
    }

    // MARK: - Private

    private func fakeGameData() {
        titleLabel.text = "Bounce"
        descriptionLabel.text = SavedSetting.shared.setting?.bounce ?? "-once you press play the button the randomizer will begin and will automatically stop after."
        descriptionLabel.addLineSpacing(spacing: 4)
    }

    private func initialSetup() {
        slides = createSlides()
        slides.frame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
        introView.addSubview(slides)
        infoSlides = createInfoSlides()

        setupSlideScrollView(slides: infoSlides)

        pageControl.numberOfPages = infoSlides.count
        pageControl.currentPage = 0
        pageControl.superview?.bringSubviewToFront(pageControl)
        gameView.isHidden = true
        continueButton.isHidden = true
        selectedUserView.isHidden = true
    }

    private func createSlides() -> BounceSlideView {
        let slide1: BounceSlideView = Bundle.main.loadNibNamed("BounceSlideView", owner: self, options: nil)?.first as! BounceSlideView
        slide1.imageView.image = UIImage(named: "bounceIcon")
        slide1.titleLabel.text = "On Fire Achieved"
        slide1.descriptionLabel.text = SavedSetting.shared.setting?.onFireArchived ?? "-Congrats with your streak, you have now activated the “On Fire” bonus feature."
        slide1.descriptionLabel.addLineSpacing(spacing: 4)
        return slide1
    }

    private func createInfoSlides() -> [BounceSlideView] {
        let slide1: BounceSlideView = Bundle.main.loadNibNamed("BounceSlideView", owner: self, options: nil)?.first as! BounceSlideView
        slide1.titleLabel.text = "Bounce Onboarding"
        slide1.descriptionLabel.text = SavedSetting.shared.setting?.bounceOnboarding ?? "-To take advantage of the Bounce feature you will need to opt in. even after opting in you can still choose to post or let this bonus expire but if you opt out you will not be able to come back until your next streak."
        slide1.descriptionLabel.addLineSpacing(spacing: 4)
        return [slide1]
    }

    // MARK: - IBActions

    @IBAction func autoscroll() {
        if gameTimer != nil {
            gameTimer?.invalidate()
            gameTimer = nil
            carousalTimer?.invalidate()
            carousalTimer = nil
            playButton.setImage(UIImage(named: "playGame"), for: .normal)
            playButton.isEnabled = false
            scrollAutomatically()
            return
        }
        playButton.setImage(UIImage(named: "stopGame"), for: .normal)
        playButton.setImage(UIImage(named: "stopGame"), for: .highlighted)
        carousalTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(fireOne), userInfo: nil, repeats: true)
        gameTimer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(autoscroll), userInfo: nil, repeats: false)
    }

    @IBAction func dismiss() {
        delegate?.dismissBounceVC(withAction: false)
    }

    @IBAction func bounceAccepted() {
        let defaults = UserDefaults.standard
        defaults.set(bouncedUser?.id, forKey: Constants.Bounce.bounceUserId)
        defaults.set(true, forKey: Constants.Bounce.isBounceShown)
        defaults.synchronize()
        delegate?.dismissBounceVC(withAction: false)
    }
    
    @IBAction func bounceDeclined() {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: Constants.Bounce.isBounceShown)
        defaults.synchronize()
        delegate?.dismissBounceVC(withAction: false)
    }
    
    @IBAction func continueTapped() {
        
        introView.isHidden = true
        informationView.isHidden = true
        gameView.isHidden = true
        continueButton.isHidden = true
        selectedUserView.isHidden = false
        //delegate?.dismissBounceVC(withAction: true)
    }

    @IBAction func playGameTapped() {
        // Part 3
        introView.isHidden = true
        informationView.isHidden = true
        gameView.isHidden = false
        //continueButton.isHidden = false
        collectionView.reloadData()
    }

    @IBAction func continueToInfoTapped() {
        // Part 2
        introView.isHidden = true
        informationView.isHidden = false
        gameView.isHidden = true
        continueButton.isHidden = true
        if usersList == nil {
            fetchUser()
        }
    }

    // MARK: - Timers

    @objc func fireOne() {
        startScrolling()
    }

    func startScrolling() {
        let initailPoint = CGPoint(x: newOffsetX, y: 0)

        if __CGPointEqualToPoint(initailPoint, collectionView.contentOffset) {
            if newOffsetX < collectionView.contentSize.width {
                newOffsetX += 10 // CGFloat(self.cellsize)
            }
            if newOffsetX > collectionView.contentSize.width - collectionView.frame.size.width {
                newOffsetX = 0
            }

            collectionView.contentOffset = CGPoint(x: newOffsetX, y: 0)

        } else {
            newOffsetX = collectionView.contentOffset.x
        }
    }
    
    func configureSelectedUser(user: User?) {
        bouncedUser = user
        let strUserProfileImg = user?.profilePhoto
        
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            selectedUSerImageView.image = UIImage(named: "user")
        } else {
            let url = URL(string: strUserProfileImg ?? "")
            selectedUSerImageView.kf.setImage(with: url, placeholder: UIImage(named: "user"))
        }
        selectedUserName.text = user?.fullName() ?? "Anonymous"
        selectedFollowerCount.text = "\(user?.followerCount ?? 0)"
        
        
        selectedUserTitleLabel.text = "Congratulations!"
        selectedUserDescriptionLabel.text = "Your next post will go out to your newly adjusted follower count. This bonus will be only apply to one post."
        
    }
}

extension BounceViewController: UIScrollViewDelegate {
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
//        pageControl.currentPage = index
//    }

    func setupSlideScrollView(slides: [BounceSlideView]) {
        guard let view = scrollView.superview else { return }
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: scrollView.frame.width * CGFloat(slides.count), height: 0)
        scrollView.isPagingEnabled = true

        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }

    func scrollAutomatically() {
        let randomInt = Int.random(in: 0 ..< (usersList?.count ?? 0))
        let indexPath = IndexPath(row: randomInt, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0, execute: {
            let selectedCell = self.collectionView.cellForItem(at: indexPath) as? BounceUserCollectionCell
            self.configureSelectedUser(user: selectedCell?.selectedUser)
           })
        continueButton.isHidden = false
        return
    }
}

extension BounceViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (usersList?.count ?? 0) > 0 ? 5000 : 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // print("\n\(indexPath.row  % (usersList?.count ?? 0))\n")
        let user = usersList?[indexPath.row % (usersList?.count ?? 0)]

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BounceUserCollectionCellIdentifier", for: indexPath as IndexPath) as! BounceUserCollectionCell
        cell.profileImageView.cornerRadius = cell.profileImageView.frame.size.height / 2
        cell.configure(user: user)
        cellsize = Double(cell.frame.size.width + 20.0)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 0.0, bottom: 10.0, right: 0.0)
    }

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let itemsPerRow: CGFloat = 2.45
//        let hardCodedPadding: CGFloat = 10
//        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
//        return CGSize(width: itemWidth, height: 175)
//    }
}

extension BounceViewController {
    func fetchUser() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"

        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        PSWebServiceAPI.getBounceList(param, completion: { response, error in

            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.usersList = response?.users
                    DispatchQueue.main.async { [self] in
                        collectionView.reloadData()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
}
