//
//  BounceUserCollectionCell.swift
//  Bloook
//
//  Created by Suresh Varma on 24/08/21.
//

import UIKit
class BounceUserCollectionCell: UICollectionViewCell {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    var selectedUser: User?
    override func prepareForReuse() {
        profileImageView.kf.cancelDownloadTask() // first, cancel currenct download task
        profileImageView.kf.setImage(with: URL(string: ""), placeholder: UIImage(named: "user")) // second, prevent kingfisher from setting previous image
        profileImageView.image = UIImage(named: "user")
        profileImageView.cornerRadius = profileImageView.frame.size.height / 2
    }
    
    func configure (user: User?) {
        selectedUser = user
        let strUserProfileImg = user?.profilePhoto
        
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            profileImageView.image = UIImage(named: "user")
        } else {
            let url = URL(string: strUserProfileImg ?? "")
            profileImageView.kf.setImage(with: url, placeholder: UIImage(named: "user"))
        }
        userNameLabel.text = user?.fullName() ?? "Unknown"
        followersLabel.text = "\(user?.followerCount ?? 0)"
        let defult = UserDefaults.standard
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        //let results = user?.followers?.filter { $0.fromUserIDString == strUserId }
        profileImageView.cornerRadius = profileImageView.frame.size.height / 2
    }
    
    
}
