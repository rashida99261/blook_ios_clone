//
//  InviteFriendVC.swift
//  bloook
//
//  Created by Tech Astha on 07/01/21.
//

import UIKit

class InviteFriendVC: UIViewController {
    @IBOutlet weak var topStatusView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    @IBAction func selectContactBtnAction(_ sender: Any) {
        let objUserInviteFriend = self.storyboard?.instantiateViewController(withIdentifier: "UserInviteFriendList") as? UserInviteFriendList
        self.navigationController?.pushViewController(objUserInviteFriend!, animated: true)
    }
    
    @IBAction func continueBtnAction(_ sender: Any) {
        let objUserInviteFriend = self.storyboard?.instantiateViewController(withIdentifier: "UserInviteFriendList") as? UserInviteFriendList
        self.navigationController?.pushViewController(objUserInviteFriend!, animated: true)
    }
    @IBAction func skipBtnAction(_ sender: Any) {
        let otherStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
        let objTabBar = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as? TabView
        self.navigationController?.pushViewController(objTabBar!, animated: true)
    }
    
}


