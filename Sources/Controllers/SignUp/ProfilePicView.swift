//
//  ProfilePicView.swift
//  blook
//
//  Created by Tech Astha on 25/12/20.
//

import UIKit

class ProfilePicView: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var strUserName = String()
    var strPassword = String()
    var strCnfPassword = String()
    var strFirstName = String()
    var strLastName = String()
    var strMobileNo = String()
    var strEmail = String()
    var strPin = String()
    var strUserId = String()
    
    var strToken = String()
    @IBOutlet weak var profilePicImg: UIImageView!
    var imageDataOne = Data()
    var imagePicker = UIImagePickerController()
    //var progressView : AcuantProgressView!
    @IBOutlet weak var topStatusView: UIView!
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        doLoging()
    }
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    func doLoging() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult =  UserDefaults.standard

        let strUserEmail = "\(defult .object(forKey: Constants.KEY_EMAIL) ?? "")"
        let strPassword = "\(defult .object(forKey: Constants.KEY_PASSWORD) ?? "")"
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN) as! String
        let param = [Constants.KEY_USERNAME :strUserEmail,
                     Constants.KEY_PASSWORD :strPassword,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken)",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

       // PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doLogin(param , completion: { (response) in
            PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict =  response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    self.strToken =  ("\(dict.value(forKey: Constants.token) ?? "")" as? String)!
                    self.strUserId = "\(dict.value(forKey: Constants.KEY_ID) ?? "")"
                    self.strToken = "Bearer " + "\(self.strToken)"
                    UserDefaults.standard.set(self.strToken, forKey: Constants.token)
//                    let otherStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
//                    let objTabBar = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as? TabView
//                    self.navigationController?.pushViewController(objTabBar!, animated: true)

                }else{
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    if msg == nil {
                        
                    }else{
                      PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
                }
            }else{

                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continueBtnAction(_ sender: Any) {

        
        self.doUpdateProfileImg()
    }
   // for Update Profile Image
    func doUpdateProfileImg() {

       guard PSUtil.reachable() else
       {
           PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
           return
       }
       let defult =  UserDefaults.standard
   
       let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
       let imageData = profilePicImg.image?.jpegData(compressionQuality: 0.50)
       let strDeviceToken = "\(defult .object(forKey: Constants.DEVICETOKEN) ?? "")"
       let params = [Constants.KEY_MYUSER_ID : strUserId,
                      Constants.DEVICE_TYPE : "I",
                      Constants.KEY_DEVICE_ID : "\(strDeviceToken)"
           ] as [String : Any]

       PSWebServiceAPI.doProfilePicture(endUrl: PROFILEPICTURE, profileImg: imageData, parameter: params, onCompletion: { (response) in
           PSUtil.hideProgressHUD()
           if response["Error"] == nil {

               let Status = response[Constants.KEY_SUCCESS] as? NSInteger
               if Status == 200 {

                   let responseDic = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                print(responseDic)
                   let msg = response[Constants.KEY_MESSAGE] as? String
                DispatchQueue.main.async {
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong, andHandler: {(alert:UIAlertAction!) in
                   })
                }
                let objInviteFriend = self.storyboard?.instantiateViewController(withIdentifier: "InviteFriendVC") as? InviteFriendVC
                self.navigationController?.pushViewController(objInviteFriend!, animated: true)
//                let otherStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
//                let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
//                self.navigationController?.pushViewController(objTabView, animated: true)
                 }else{
                   let msg = response[Constants.KEY_MESSAGE] as? String
                    if msg == nil {
                        
                    }else{
                      PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
               }
           }else{

               PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)

           }
       })
    }
    
    @IBAction func addProfilePicBtnAction(_ sender: Any) {
        self .openImagePickerController()
    }
    func openImagePickerController() {
        
        let alrt = UIAlertController(title: "Select Source Type", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .camera
            
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        let photoAction = UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .photoLibrary
            
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        })
        alrt.addAction(cameraAction)
        alrt.addAction(photoAction)
        alrt.addAction(cancelAction)
        self.present(alrt, animated: true, completion: nil)
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let imageOne = info[UIImagePickerController.InfoKey.originalImage]as?UIImage else {
            return
        }
        
        guard let image = info[UIImagePickerController.InfoKey.editedImage]
            as? UIImage else {
                return
        }
        
        imageDataOne = imageOne.jpegData(compressionQuality: 0.50)!
        profilePicImg.image = image
        profilePicImg.cornerRadius = 50
        //user image service
        dismiss(animated:true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
      {
        
          picker .dismiss(animated: true, completion: nil)
      }
    
    @IBAction func takeAPhotoBtnAction(_ sender: Any) {
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = .camera
        
        self.present(self.imagePicker, animated: true, completion: nil)
        
    }
    @IBAction func selectFromLibraryBtnAction(_ sender: Any) {
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = .photoLibrary
        
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    @IBAction func skipBtnAction(_ sender: Any) {
        let objInviteFriend = self.storyboard?.instantiateViewController(withIdentifier: "InviteFriendVC") as? InviteFriendVC
        self.navigationController?.pushViewController(objInviteFriend!, animated: true)
//        let otherStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
//        let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
//
//        self.navigationController?.pushViewController(objTabView, animated: true)
    }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
           // self.progressView.startAnimation()
            //self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
    private func hideProgressView(){
        DispatchQueue.main.async {
           // self.progressView.stopAnimation()
            //self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
}
