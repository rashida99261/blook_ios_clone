//
//  UserInviteFriendList.swift
//  bloook
//
//  Created by Tech Astha on 27/05/21.
//

import Contacts
import ContactsUI
import Foundation
import Toaster
import UIKit

struct FetchedContact1 {
    var firstName: String
    var lastName: String
    var telephone: String
}

class UserInviteFriendList: UIViewController, UITableViewDelegate, UITableViewDataSource {
   // var progressView: AcuantProgressView!
    @IBOutlet var searchView: UIView!
    var selectedDetailArray = [String]()
    var selectedDetailNameArray = [String]()
    @IBOutlet var profileInviteSuccessView: UIView!
    var allContacts = [FetchedContact1]()
    var filteredContacts = [FetchedContact1]()
    @IBOutlet var userInviteFriendTable: UITableView!
    let selectedContactArr = NSMutableArray()
    var contactArr = [Any]()
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true
        profileInviteSuccessView.isHidden = true
        fetchContacts()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        fetchContacts()
    }
    
    override func viewDidLayoutSubviews() {
        searchView.layer.masksToBounds = true
        searchView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    func fetchContacts() {
        self.allContacts.removeAll()
        self.filteredContacts.removeAll()
        // 1.
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            if let error = error {
                print("failed to request access", error)
                return
            }
            if granted {
                // 2.
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                do {
                    // 3.
                    try store.enumerateContacts(with: request, usingBlock: { contact, _ in
                        let fetchedContacts = FetchedContact1(firstName: contact.givenName, lastName: contact.familyName, telephone: contact.phoneNumbers.first?.value.stringValue ?? "")
                        self.allContacts.append(fetchedContacts)
                        self.filteredContacts.append(fetchedContacts)
                    })
                } catch {
                    print("Failed to enumerate contact", error)
                }
            } else {
                print("access denied")
            }
        }
    }

    func getContactFromCNContact() -> [CNContact] {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactGivenNameKey,
            CNContactMiddleNameKey,
            CNContactFamilyNameKey,
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
        ] as [Any]

        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }

        var results: [CNContact] = []

        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)

            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)

            } catch {
                print("Error fetching results for container")
            }
        }

        return results
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredContacts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UserInviteFriendTableViewCell
        cell.selectionStyle = .none
       
        cell.contactUserNameLbl?.text = filteredContacts[indexPath.row].firstName + " " + filteredContacts[indexPath.row].lastName
        cell.phoneNoLbl?.text = filteredContacts[indexPath.row].telephone
     
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath)! as! UserInviteFriendTableViewCell
        
        let dictType = filteredContacts[indexPath.row] as? NSDictionary ?? NSDictionary()
        print(dictType)
        selectedContactArr.removeAllObjects()
        let strKeyId = filteredContacts[indexPath.row].telephone
        let strKeyFirstName = filteredContacts[indexPath.row].firstName
        let strKeyLastName = filteredContacts[indexPath.row].lastName
        let strName = strKeyFirstName + strKeyLastName
        if selectedDetailArray.contains(strKeyId) {
            cell.selectInviteBtn.backgroundColor = UIColor.white
            cell.selectInviteBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
            if let indexOf = selectedDetailNameArray.firstIndex(of: strName) {
                selectedDetailNameArray.remove(at: indexOf)
            }
            if let indexOf = selectedDetailArray.firstIndex(of: strKeyId) {
                selectedDetailArray.remove(at: indexOf)
            }
        } else {
            cell.selectInviteBtn.backgroundColor = UIColor(red: 114.0/255.0, green: 146.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            cell.selectInviteBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            selectedDetailNameArray.append(strName)
            selectedDetailArray.append(strKeyId)
        }
    }

    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func sendBtnAction(_ sender: Any) {
        doInviteFriendSendFromServer()
    }
   
    // MARK: - for invite send

    func doInviteFriendSendFromServer() {
        if selectedDetailArray.isEmpty {
            Toast(text: "Please select some friends to invite").show()
            return
        }
        
        var theJSONOtherText = String()
        var str = String()
        str = "["
        
        for index in 0 ... selectedDetailArray.count-1 {
            str += "{"
            str += "\"name\":\" " + selectedDetailArray[index] + "\",\"mobile\":\"" + selectedDetailNameArray[index] + "\""
            str += "}"
            if selectedDetailArray.count-1 != index {
                str += ","
            }
        }
        str += "]"
        theJSONOtherText = str
        print(theJSONOtherText)
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.KEY_FRIEND_DATA: theJSONOtherText,
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        // PSUtil.showProgressHUD()
       // progressView = AcuantProgressView(frame: view.frame, center: view.center)
        showProgressView(text: "Loading...")
        PSWebServiceAPI.doInviteFriendSend(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let msg = response[Constants.KEY_MESSAGE] as? String
                   
                    // let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    // DispatchQueue.main.async {
                    // PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    self.profileInviteSuccessView.isHidden = false
                    //  }
                    //  self.navigationController?.popViewController(animated: true)
                    
                    let otherStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
                
                    self.navigationController?.pushViewController(objTabView, animated: true)
                    
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }

    @IBAction func doneBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    private func showProgressView(text: String = "") {
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
            //self.progressView.startAnimation()
           // self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
    private func hideProgressView() {
        DispatchQueue.main.async {
            //self.progressView.stopAnimation()
           // self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
}


extension UserInviteFriendList: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let previousText:NSString = textField.text! as NSString
        let updatedText = previousText.replacingCharacters(in: range, with: string)
        getHintsFromTextField(text: updatedText)
        return true
    }

    @objc func getHintsFromTextField(text: String) {
        let updatedText = text
        if !updatedText.isEmpty {
            filteredContacts = allContacts.filter {
                ($0.firstName.lowercased()).hasPrefix(updatedText.lowercased()) ||
                ($0.lastName.lowercased()).hasPrefix(updatedText.lowercased())
            }
        } else {
            filteredContacts = allContacts
        }
        userInviteFriendTable.reloadData()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
