//
//  OtpVC.swift
//  blook
//
//  Created by Tech Astha on 24/12/20.
//

import UIKit
import SVPinView

class OtpVC: UIViewController {

    @IBOutlet weak var mobileNoField: UILabel!
    @IBOutlet weak var pinView: SVPinView!
    var strUserName = String()
    var strPassword = String()
    var strCnfPassword = String()
    var strFirstName = String()
    var strLastName = String()
    var strMobileNo = String()
    var strMobileOtp = String()
    var strEmail = String()
    var strPin = String()
    var isNavigatedDirectly = false
    //var progressView : AcuantProgressView!
    @IBOutlet weak var topStatusView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true
        
        let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.foregroundColor : UIColor.darkGray]

            let attrs2 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor : UIColor.black]

            let attributedString1 = NSMutableAttributedString(string:"Please enter the code sent to: ", attributes:attrs1)

            let attributedString2 = NSMutableAttributedString(string:strMobileNo, attributes:attrs2)

            attributedString1.append(attributedString2)
            self.mobileNoField.attributedText = attributedString1
        
        //Please enter the code sent to: 1-123-867-5309
        pinView.didFinishCallback = didFinishEnteringPin(pin:)
        
       // cancelButton.isHidden = isNavigatedDirectly
    }
    
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    func didFinishEnteringPin(pin:String) {
        print ("\(pin)")
       // showAlert(title: "Success", message: "The Pin entered is \(pin)")
        //pinView.clearPin()
        self.strPin = "\(pin)"
    }
    // MARK: Helper Functions
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continueBtnAction(_ sender: Any) {
    
     //   pinView.didFinishCallback = didFinishEnteringPin(pin:)
        pinView.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
            self.strPin = "\(pin)"
        }
        if strPin == "" {
            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter otp.")
            return;
        }else{
            doVerifyOtpFromServer()
        }
       
    }
    
    //for verify Otp
    func doVerifyOtpFromServer() {
        print(strPin)
        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let param = [Constants.KEY_MOBILE:strMobileNo,
                     Constants.KEY_OTP_CODE:strPin,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken)",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

        //PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doVerifyOtp(param , completion: { (response) in
            PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                     print(response)
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    DispatchQueue.main.async {
                        PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
                    UserDefaults.standard.set(true, forKey: Constants.ISLOGIN)
                    let objProfilePic = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePicView") as! ProfilePicView
                    self.navigationController?.pushViewController(objProfilePic, animated: true)


                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    @IBAction func resendBtnAction(_ sender: Any) {
        doResendOtpFromServer()
    }
    //for resend Otp
    func doResendOtpFromServer() {
        print(strPin)
        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let strToken = "1111"
        let param = [Constants.KEY_MOBILE:strMobileNo,
                     Constants.KEY_DEVICE_ID : "\(strToken)",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

        //PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doResendOtp(param , completion: { (response) in
            PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    DispatchQueue.main.async {
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
                 }else {

                     PSUtil.hideProgressHUD()
                     let msg = response[Constants.KEY_MESSAGE] as? String
                     PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
           // self.progressView.startAnimation()
            //self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
    private func hideProgressView(){
        DispatchQueue.main.async {
           // self.progressView.stopAnimation()
            //self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
}
