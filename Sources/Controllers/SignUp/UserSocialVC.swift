//
//  UserSocialVC.swift
//  blook
//
//  Created by Tech Astha on 24/12/20.
//

import UIKit


class UserSocialVC: UIViewController {

    @IBOutlet weak var mobileNoField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var topStatusView: UIView!
    
   // @IBOutlet weak var mobileNoField: MDCOutlinedTextField!
   // @IBOutlet weak var emailField: MDCOutlinedTextField!
    var strUserName = String()
    var strPassword = String()
    var strCnfPassword = String()
    var strFirstName = String()
    var strLastName = String()
    
    @IBOutlet weak var viewLoader: UIView!
    @IBOutlet weak var loadGif: loaderxib!

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        mobileNoField.setOutlineColor(Constants.colorConstants.CLR_BLUE_THEME, for: .editing)
//        emailField.setOutlineColor(Constants.colorConstants.CLR_BLUE_THEME, for: .editing)
//        mobileNoField.label.text = "Mobile number"
//        emailField.label.text = "Email"
    }
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }

    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continueBtnAction(_ sender: Any) {
        if (mobileNoField.text?.count)! < 1 {

             PSUtil.showAlertFromController(controller: self, withMessage: "Please enter MobileNo.")
             return;
        }else if (emailField.text?.count)! < 1 {
            
            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter email.")
            return;

        }else if (Constants.isValidEmail(testStr:emailField.text!) == false){

            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter valid email.")
            return;

        }else{
//        let objOtp = self.storyboard?.instantiateViewController(withIdentifier: "OtpVC") as? OtpVC
//            objOtp?.strFirstName = strFirstName
//            objOtp?.strLastName = strLastName
//            objOtp?.strUserName = strUserName
//            objOtp?.strPassword = strPassword
//            objOtp?.strCnfPassword = strCnfPassword
//            objOtp?.strMobileNo = mobileNoField.text!
//            objOtp?.strEmail = emailField.text!
//        self.navigationController?.pushViewController(objOtp!, animated: true)
            RegisterFromServer()
        }
    }
    
    //for register
    func RegisterFromServer() {
//        let pageNo = (UserDefaults.standard.value(forKey: Constants.Signup.pageNo) as? String) ?? "0"
//        if  pageNo == "3" {
//            let mobileNo = (UserDefaults.standard.value(forKey: Constants.Signup.mobileNo) as? String) ?? "0"
//            let objOtp = self.storyboard?.instantiateViewController(withIdentifier: "OtpVC") as? OtpVC
//            objOtp?.strMobileNo = mobileNo
//            self.navigationController?.pushViewController(objOtp!, animated: true)
//                return
//        }
        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let param = [Constants.KEY_FIRSTNAME:strFirstName,
                     Constants.KEY_LASTNAME:strLastName,
                     Constants.KEY_USERNAME:strUserName,
                     Constants.KEY_EMAIL:emailField.text!,
                     Constants.KEY_PASSWORD:strPassword,
                     Constants.KEY_PASSWORD_CONFIRMATION:strCnfPassword,
                     Constants.KEY_MOBILE:mobileNoField.text!,
                     Constants.KEY_NO:"3",
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken)",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

      //  PSUtil.showProgressHUD()
        
        
        self.showProgressView()
        PSWebServiceAPI.doRegister(param , completion: { (response) in
           PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict =  response[Constants.KEY_POSTDATA] as! NSDictionary
                    let strUserId = "\(dict.value(forKey: Constants.KEY_ID) ?? "")"
                    let strUserEmail = "\(dict.value(forKey: Constants.KEY_EMAIL) ?? "")"
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    DispatchQueue.main.async {
                        PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
                    let defult = UserDefaults.standard
                    defult.set(strUserId, forKey: Constants.KEY_ID)
                    defult.set(strUserEmail, forKey: Constants.KEY_EMAIL)
                    defult.set(self.strPassword, forKey: Constants.KEY_PASSWORD)
                    let objOtp = self.storyboard?.instantiateViewController(withIdentifier: "OtpVC") as? OtpVC
                    objOtp?.strMobileNo = self.mobileNoField.text!
                    self.navigationController?.pushViewController(objOtp!, animated: true)
                    self.saveDataLocally()

                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    private func saveDataLocally() {
        UserDefaults.standard.set(strFirstName, forKey: Constants.Signup.firstName)
        UserDefaults.standard.set(strLastName, forKey: Constants.Signup.lastName)
        UserDefaults.standard.set(strUserName, forKey: Constants.Signup.userName)
        UserDefaults.standard.set(emailField.text, forKey: Constants.Signup.emailAddress)
        UserDefaults.standard.set(strPassword, forKey: Constants.Signup.password)
        UserDefaults.standard.set(mobileNoField.text, forKey: Constants.Signup.mobileNo)
        UserDefaults.standard.set("3", forKey: Constants.Signup.pageNo)
        
    }
    
    private func showProgressView(){
        DispatchQueue.main.async {
           // //self.progressView.messageView.text = text
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
    private func hideProgressView(){
        DispatchQueue.main.async {
            self.viewLoader.isHidden = true
        }
    }
}
