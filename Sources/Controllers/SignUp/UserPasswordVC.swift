//
//  UserPasswordVC.swift
//  blook
//
//  Created by Tech Astha on 24/12/20.
//

import UIKit


class UserPasswordVC: UIViewController,UIGestureRecognizerDelegate {
//
//    @IBOutlet weak var userNameField: MDCOutlinedTextField!
//    @IBOutlet weak var passwordField: MDCOutlinedTextField!
//    @IBOutlet weak var cnfPasswordField: MDCOutlinedTextField!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var cnfPasswordField: UITextField!
    @IBOutlet weak var topStatusView: UIView!
    
    var strFirstName = String()
    var strLastName = String()
    
    @IBOutlet weak var viewLoader: UIView!
    @IBOutlet weak var loadGif: loaderxib!

    override func viewDidLoad() {
        super.viewDidLoad()
//        userNameField.setOutlineColor(Constants.colorConstants.CLR_BLUE_THEME, for: .editing)
//        passwordField.setOutlineColor(Constants.colorConstants.CLR_BLUE_THEME, for: .editing)
//        cnfPasswordField.setOutlineColor(Constants.colorConstants.CLR_BLUE_THEME, for: .editing)
//        userNameField.label.text = "Username"
//        passwordField.label.text = "Password"
//        cnfPasswordField.label.text = "Confirm password"
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    @IBAction func continueBtnAction(_ sender: Any) {
        if (userNameField.text?.count)! < 1 {

                PSUtil.showAlertFromController(controller: self, withMessage: "Please enter User name.")
                return;

         }else if (passwordField.text?.count)! < 1 {

            PSUtil.showAlertFromController(controller: self, withMessage: "Please select password.")
            return;

        }else if (cnfPasswordField.text?.count)! < 1 {

            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter confrim password.")
            return;

        }else if (passwordField.text != cnfPasswordField.text){

            PSUtil.showAlertFromController(controller: self, withMessage: "Password is not matched with confirm password." )
            return;

        }else if (passwordField.text?.count)! < 6 {

            PSUtil.showAlertFromController(controller: self, withMessage: "Password must be 6 character.")
            return;

        }else{
            RegisterFromServer()
            
        }
    }
    
    //for register
    func RegisterFromServer() {
//        let pageNo = (UserDefaults.standard.value(forKey: Constants.Signup.pageNo) as? String) ?? "0"
//        if pageNo != "1" {
//            let objUserSocial = self.storyboard?.instantiateViewController(withIdentifier: "UserSocialVC") as? UserSocialVC
//           
//            objUserSocial?.strFirstName = (UserDefaults.standard.value(forKey: Constants.Signup.firstName) as? String) ?? "0"
//            objUserSocial?.strLastName = (UserDefaults.standard.value(forKey: Constants.Signup.lastName) as? String) ?? "0"
//            objUserSocial?.strUserName = (UserDefaults.standard.value(forKey: Constants.Signup.userName) as? String) ?? "0"
//            objUserSocial?.strPassword = (UserDefaults.standard.value(forKey: Constants.Signup.password) as? String) ?? "0"
//            objUserSocial?.strCnfPassword = (UserDefaults.standard.value(forKey: Constants.Signup.password) as? String) ?? "0"
//            self.navigationController?.pushViewController(objUserSocial!, animated: true)
//            return
//        }
        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let param = [Constants.KEY_FIRSTNAME:strFirstName,
                     Constants.KEY_LASTNAME:strLastName,
                     Constants.KEY_USERNAME:userNameField.text!,
                     Constants.KEY_EMAIL:"",
                     Constants.KEY_PASSWORD:passwordField.text!,
                     Constants.KEY_PASSWORD_CONFIRMATION:cnfPasswordField.text!,
                     Constants.KEY_MOBILE:"",
                     Constants.KEY_NO:"2",
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken)",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

      //  PSUtil.showProgressHUD()
        
        self.showProgressView()
        PSWebServiceAPI.doRegister(param , completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
        
                    //let msg = response[Constants.KEY_MESSAGE] as? String
                   // PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    
                    let objUserSocial = self.storyboard?.instantiateViewController(withIdentifier: "UserSocialVC") as? UserSocialVC
                   
                    objUserSocial?.strFirstName = self.strFirstName
                    objUserSocial?.strLastName = self.strLastName
                    objUserSocial?.strUserName = self.userNameField.text!
                    objUserSocial?.strPassword = self.passwordField.text!
                    objUserSocial?.strCnfPassword = self.cnfPasswordField.text!
                    self.navigationController?.pushViewController(objUserSocial!, animated: true)
//
//                    let otherStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
//                    let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
//                    self.navigationController?.pushViewController(objTabView, animated: true)
                    self.saveDataLocally()
                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    private func saveDataLocally() {
        UserDefaults.standard.set(strFirstName, forKey: Constants.Signup.firstName)
        UserDefaults.standard.set(strLastName, forKey: Constants.Signup.lastName)
        UserDefaults.standard.set(userNameField.text, forKey: Constants.Signup.userName)
        UserDefaults.standard.set(nil, forKey: Constants.Signup.emailAddress)
        UserDefaults.standard.set(passwordField.text, forKey: Constants.Signup.password)
        UserDefaults.standard.set(nil, forKey: Constants.Signup.mobileNo)
        UserDefaults.standard.set("2", forKey: Constants.Signup.pageNo)
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showPAssword() {
        passwordField.isSecureTextEntry = false
        cnfPasswordField.isSecureTextEntry = !cnfPasswordField.isSecureTextEntry
    }
    
    private func showProgressView(){
        DispatchQueue.main.async {
           // //self.progressView.messageView.text = text
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
    private func hideProgressView(){
        DispatchQueue.main.async {
            self.viewLoader.isHidden = true
        }
    }


}
