//
//  UserInviteFriendTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 27/05/21.
//

import UIKit

class UserInviteFriendTableViewCell: UITableViewCell {
    @IBOutlet weak var contactUserNameLbl: UILabel!
    @IBOutlet weak var phoneNoLbl: UILabel!
    @IBOutlet weak var selectInviteBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
