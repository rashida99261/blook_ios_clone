//
//  TabView.swift
//  blook
//
//  Created by Tech Astha on 25/12/20.
//

import UIKit

class TabView: UITabBarController,UITabBarControllerDelegate {
    let roundedView = UIView(frame: .zero)
    var middleBtn = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.unselectedItemTintColor = UIColor.black
        tabBar.layer.cornerRadius = 20
        tabBar.layer.masksToBounds = true
        tabBar.isTranslucent = true
        tabBar.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        self.delegate = self
      //  self.setupMiddleButton()
        let myTabBarItem3 = (self.tabBar.items?[2])! as UITabBarItem
        myTabBarItem3.image = UIImage(named: "plus with circle")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem3.selectedImage = UIImage(named: "plus with circle")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
              myTabBarItem3.title = ""
              myTabBarItem3.imageInsets = UIEdgeInsets(top: -4, left: 10, bottom: -14, right:10)
        UITabBar.appearance().tintColor = UIColor(red: 114/255.0, green: 146/255.0, blue: 255/255.0, alpha: 1.0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tabBar.frame.size.height = 95
        tabBar.frame.origin.y = view.frame.height - 95
    }

    func setupMiddleButton() {

        self.middleBtn.frame = CGRect(x: (self.view.bounds.width / 2)-28, y: -28, width: 26, height: 26)
        self.middleBtn.setImage(UIImage(named: "plus with circle"), for: .normal)
        self.middleBtn.backgroundColor = UIColor.black
        self.middleBtn.cornerRadius = 28
        self.tabBar.addSubview(self.middleBtn)
        self.view.layoutIfNeeded()
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
           
        if viewController.isKind(of: ViewController.self) {
            return false
        }
        return true
    }

}

