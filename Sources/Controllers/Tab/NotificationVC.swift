//
//  NotificationVC.swift
//  bloook
//
//  Created by Tech Astha on 05/01/21.
//

import UIKit

class NotificationVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var notificationTable: UITableView!
    @IBOutlet var topStatusView: UIView!
    @IBOutlet var countLabel: UILabel!
  
    var allNotifications: [NotificationModel]?
    override func viewDidLoad() {
        super.viewDidLoad()
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getNotificaitonsFromServer()
    }

//    override func viewDidLayoutSubviews() {
//
//    }
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allNotifications?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NotificationTableViewCell
        cell.selectionStyle = .none
        let notif = allNotifications?[indexPath.row]
        cell.configure(notif: notif)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("tapped")
        guard let notif = allNotifications?[indexPath.row] else { return }
        switch notif.type {
        case .post:
            print("Post")
            let otherStoryboard = UIStoryboard(name: "Posts", bundle: nil)
            let postsViewController = otherStoryboard.instantiateViewController(withIdentifier: "PostsViewController") as? PostsViewController
            postsViewController?.selectedPostId = notif.routeId
            self.navigationController?.pushViewController(postsViewController!, animated: true)
        case .wallet:
            let accountStoryboard = UIStoryboard(name: "SettingStoryboard", bundle: nil)
            let objTotalCoinList = accountStoryboard.instantiateViewController(withIdentifier: "TotalCoinListVC") as? TotalCoinListVC
            navigationController?.pushViewController(objTotalCoinList!, animated: true)
        case .chat:
            print("chat")
        case .profile:
            let otherStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
            objMyProfile?.selectedUserId = "\(notif.userID ?? 0)"
            navigationController?.pushViewController(objMyProfile!, animated: true)
        case .invite:
            print("invite")
            //self.tabBarController?.tabBar.isHidden = true
            let accountStoryboard =  UIStoryboard(name: "SettingStoryboard", bundle: nil)
            let objInviteFriend = accountStoryboard.instantiateViewController(withIdentifier: "ProfileInviteFriend") as? ProfileInviteFriend
            self.navigationController?.pushViewController(objInviteFriend!, animated: true)
        case .bounce:
            let otherStoryboard = UIStoryboard(name: "Posts", bundle: nil)
            let objTopPhoto = otherStoryboard.instantiateViewController(withIdentifier: "PostsViewController") as? PostsViewController
            self.navigationController?.pushViewController(objTopPhoto!, animated: false)
            objTopPhoto?.showBounce()
        case .none:
            print("None")
            let accountStoryboard =  UIStoryboard(name: "SettingStoryboard", bundle: nil)
            let objInviteFriend = accountStoryboard.instantiateViewController(withIdentifier: "ProfileInviteFriend") as? ProfileInviteFriend
            self.navigationController?.pushViewController(objInviteFriend!, animated: true)
        }
        markSingleRead(notif: notif)
    }
}

extension NotificationVC {
    func getNotificaitonsFromServer(silent: Bool = false) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_PAGE: "1",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I",
                     Constants.KEY_LIMIT: 50] as [String: Any]

        //  PSUtil.showProgressHUD()
        if !silent {
        //progressView = AcuantProgressView(frame: view.frame, center: view.center)
        showProgressView(text: "Loading...")
        }
        PSWebServiceAPI.getAllNotifications(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.statusCode == 200 {
                    self.allNotifications = response?.notifications
                    let unreadNotification = self.allNotifications?.filter { $0.isRead == 0 }
                    self.countLabel.text = "New Notifications (\(unreadNotification?.count ?? 0))"
                    DispatchQueue.main.async {
                        self.notificationTable.reloadData()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }

    func markSingleRead(notif: NotificationModel) {
        if notif.isRead == 1 { return }
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "notification_id": "\(notif.notificationID ?? 0)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        PSWebServiceAPI.markSingleReadNotifications(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.statusCode == 200 {
                   // self.getNotificaitonsFromServer(silent: true)
                    DispatchQueue.main.async {
                        self.notificationTable.reloadData()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func markAllRead() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "isRead": 1,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        PSWebServiceAPI.markReadAllNotifications(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.statusCode == 200 {
                    self.getNotificaitonsFromServer()
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {}
        })
    }
}
