//
//  BlogCollectionReusableView.swift
//  bloook
//
//  Created by Tech Astha on 12/02/21.
//

import UIKit

class BlogCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var nameProfileLbl: UILabel!
    @IBOutlet weak var createDateLbl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    static let reuseIdentifier = String(describing: BlogCollectionReusableView.self)
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
