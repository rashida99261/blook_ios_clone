//
//  CollectionReusableView.swift
//  bloook
//
//  Created by Tech Astha on 03/02/21.
//

import UIKit
import GPVideoPlayer
class CollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var imageTabBtn: UIButton!
    @IBOutlet weak var imgBackgroundView: UIView!
    @IBOutlet weak var blingPopupImg: UIImageView!
    @IBOutlet weak var bligPopupBtn: UIButton!
    @IBOutlet weak var otherUserProfileBtn: UIButton!
    @IBOutlet weak var imageHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var descriptionHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var playVideo: UIButton!
    @IBOutlet weak var imgeView: UIView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var nameProfileLbl: UILabel!
    @IBOutlet weak var createDateLbl: UILabel!
    static let reuseIdentifier = String(describing: CollectionReusableView.self)
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var roundView: UIView!
    var videoURL :URL?
    @IBOutlet weak var imgHeightConsraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        let colorTop =  UIColor(red: 114.0/255.0, green: 146.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
//        let colorBottom = UIColor(red: 177.0/255.0, green: 178.0/255.0, blue: 254.0/255.0, alpha: 1.0).cgColor
//
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = [colorTop, colorBottom]
//        gradientLayer.locations = [0.0, 1.0]
//        gradientLayer.frame = imgBackgroundView.bounds
//        imgBackgroundView.layer.insertSublayer(gradientLayer, at:0)
        
        // Initialization code
    }
    override func prepareForReuse() {
        imageView.image = nil
        imageView.layer.sublayers = nil
    }
    
}
