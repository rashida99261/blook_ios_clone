//
//  videoCollectionViewCell.swift
//  bloook
//
//  Created by Tech Astha on 24/02/21.
//

import AVFoundation
import Kingfisher
import UIKit

class VideoCollectionViewCell: UICollectionViewCell {
    var videoURL: URL?
    @IBOutlet var postVideoImg: UIImageView!
    @IBOutlet var lblVideoTime: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        postVideoImg.kf.cancelDownloadTask()
        postVideoImg.kf.setImage(with: URL(string: ""))
        postVideoImg.image = UIImage(named: "postPlaceholder")
    }

    func configure(post: Post?) {
        lblVideoTime.textColor = UIColor.white
        let strThumnailImg = post?.thumbnail ?? ""
        let placeholderImage = UIImage(named: "postPlaceholder")
        postVideoImg.kf.setImage(with: URL(string: strThumnailImg), placeholder: placeholderImage)
        lblVideoTime.text = post?.videoDuration()
    }
}
