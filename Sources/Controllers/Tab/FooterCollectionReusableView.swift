//
//  FooterCollectionReusableView.swift
//  bloook
//
//  Created by Tech Astha on 03/02/21.
//

import UIKit

class FooterCollectionReusableView: UICollectionReusableView {
    static let reuseIdentifier = String(describing: FooterCollectionReusableView.self)
    @IBOutlet weak var roundedBorderView: UIView!
    @IBOutlet weak var addCommentLabel: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentSpaceView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
//        roundedBorderView.roundCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: 40)
//        roundedBorderView.layer.borderWidth = 1
//        roundedBorderView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1).cgColor
//        let colorTop =  UIColor(red: 114.0/255.0, green: 146.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
//        let colorBottom = UIColor(red: 177.0/255.0, green: 178.0/255.0, blue: 254.0/255.0, alpha: 1.0).cgColor
//
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = [colorTop, colorBottom]
//        gradientLayer.locations = [0.0, 1.0]
//        gradientLayer.frame = commentSpaceView.bounds
//        commentSpaceView.layer.insertSublayer(gradientLayer, at:0)
    }
    
}
private extension UIView {
    
    func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
        if #available(iOS 11, *) {
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = corners
        } else {
            var cornerMask = UIRectCorner()
            if corners.contains(.layerMinXMinYCorner) {
                cornerMask.insert(.topLeft)
            }
            if corners.contains(.layerMaxXMinYCorner) {
                cornerMask.insert(.topRight)
            }
            if corners.contains(.layerMinXMaxYCorner) {
                cornerMask.insert(.bottomLeft)
            }
            if corners.contains(.layerMaxXMaxYCorner) {
                cornerMask.insert(.bottomRight)
            }
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: cornerMask,
                                    cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
}
