//
//  PeopleSearchCollectionViewCell.swift
//  bloook
//
//  Created by Tech Astha on 14/06/21.
//

import UIKit
protocol PeopleSearchCollectionViewCellDelegate {
    func followTapped(userId: String, cell: PeopleSearchCollectionViewCell)
}
class PeopleSearchCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    var delegate: PeopleSearchCollectionViewCellDelegate?
    var selecteduserId: String?
    
    func configure (searchUser: SearchUser?) {
        let strUserProfileImg = searchUser?.user?.profilePhoto
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            profileImageView.image = UIImage(named: "user")
        } else {
            let url = URL(string: strUserProfileImg ?? "")
            profileImageView.kf.setImage(with: url, placeholder: UIImage(named: "user"))
        }
        userNameLabel.text = searchUser?.user?.fullName() ?? "Unknown"
        followersLabel.text = "\(searchUser?.user?.followersCount ?? 0)" + " Followers"
        let defult = UserDefaults.standard
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let results = searchUser?.user?.followers?.filter { $0.fromUserIDInt?.stringValue() == strUserId }
        if !(results?.isEmpty ?? true) {
            self.followButton.setTitle("Unfollow", for: .normal)
        } else {
            self.followButton.setTitle("Follow", for: .normal)
        }
        selecteduserId = "\(searchUser?.user?.id ?? 0)"
    }
    
    @IBAction func followTapped() {
        delegate?.followTapped(userId: selecteduserId ?? "", cell: self)
    }
    func updateFollowStatus(_ status: String) {
        self.followButton.setTitle(status, for: .normal)
    }
    
}
