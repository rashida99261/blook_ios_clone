//
//  NotificationTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 05/03/21.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        profileImage.kf.cancelDownloadTask() // first, cancel currenct download task
        profileImage.kf.setImage(with: URL(string: ""), placeholder: UIImage(named: "user")) // second, prevent kingfisher from setting previous image
        profileImage.image = UIImage(named: "user")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(notif: NotificationModel?) {
        guard let notif = notif else { return }
        titleLabel.text = (notif.datumDescription ?? "Notification")
        if notif.isRead == 0 {
            titleLabel.font = UIFont(name: "Roboto-Bold", size: 14.0)
        } else {
            titleLabel.font = UIFont(name: "Roboto-Regular", size: 14.0)
        }
        dateLabel.text = notif.dateTime ?? "Just now"

        switch notif.type {
        case .post:
            setImage(image: notif.image)
        case .wallet:
            profileImage.image = UIImage(named: "coin")
        case .chat:
            setImage(image: notif.image)
        case .profile:
            setImage(image: notif.image)
        case .invite:
            print("invite")
        case .bounce:
            profileImage.image = UIImage(named: "bounceNotif")
        case .none:
            setImage(image: notif.image)
        }
    }

    private func setImage(image: String?) {
        let strUserProfileImg = image
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            profileImage.image = UIImage(named: "user")
        } else {
            let url = URL(string: strUserProfileImg ?? "")
            profileImage.kf.setImage(with: url, placeholder: UIImage(named: "user"))
        }
    }
}
