//
//  UserUploadPostVC.swift
//  bloook
//
//  Created by Tech Astha on 06/01/21.
//

import UIKit

import AVFoundation
import AVKit
import MediaPlayer
import MobileCoreServices
import Photos
import SDWebImage
import Toaster
import YPImagePicker

class UserUploadPostVC: BaseViewController {
    @IBOutlet var postDescriptionView: UITextView!
    var descriptionTextViewPlaceHolder = "Tap to type..."
    var config = YPImagePickerConfiguration()
    var picker = YPImagePicker()
    var selectedItems = [YPMediaItem]()
    var strImage = UIImage()
    var playerLayer = AVPlayerLayer()
    var strVideoImg = String()
    var strBtnId = String()
    var strPhotoVideoSelectionId = String()
    var strVisibilitySelection = String()
    var strSelectedType = String()
    
    @IBOutlet var cancelBounceView: UIView!
    @IBOutlet var followSelectionBoostView: UIView!
    @IBOutlet var userProfileImg: UIImageView!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var roundView: UIView!
    @IBOutlet var editView: UIView!
    @IBOutlet var visibilityLbl: UILabel!
    @IBOutlet var visibilitySelectionLbl: UILabel!
    @IBOutlet var visibilityTypeLbl: UILabel!
    @IBOutlet var postBtn: UIButton!
    @IBOutlet var progressBar: UIProgressView!
    let selectedImageV = UIImageView()
    @IBOutlet var imgPicPost: UIImageView!
    @IBOutlet var postBoostBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(strImage)
        progressBar.isHidden = true
        //  editView.isHidden = true
        cancelBounceView.isHidden = true
        imgPicPost.isHidden = true
        editView.isHidden = true
        let imageSaveData = strImage.jpegData(compressionQuality: 1.0) as NSData?
        let base64String = imageSaveData?.base64EncodedString()
        if base64String == nil {
            imgPicPost.image = UIImage(named: "")
        } else {
            imgPicPost.image = strImage
        }
        config.showsCropGridOverlay = false

        let strUserName = "\(defult.object(forKey: "ForUserName") ?? "")"
        userNameLbl.text = strUserName
        let strProfilePhoto = "\(defult.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
        userProfileImg.sd_setImage(with: URL(string: strProfilePhoto), placeholderImage: UIImage(named: "placeholder.png"))
        
        print(strVideoImg)
        print(strPhotoVideoSelectionId)
       
        print(descriptionTextViewPlaceHolder)
       
        // Set
        
        postDescriptionView.text = descriptionTextViewPlaceHolder
        
        postDescriptionView.textColor = UIColor.lightGray
        postDescriptionView.delegate = self
        let defult = UserDefaults.standard
        strVisibilitySelection = "\(defult.object(forKey: "forSelectVisibility") ?? "Public")"
        let strVisibilityType = "\(defult.object(forKey: "forVisibilityType") ?? "")"
        if strVisibilitySelection == "" || strVisibilityType == "" {
            visibilityLbl.text = "Public"
        } else {
            visibilityLbl.text = strVisibilitySelection
        }
        if strPhotoVideoSelectionId == "0" {
            // postBtn.setTitle("Upload", for: UIControl.State.normal)
            // postBtn.setTitleColor(UIColor(red: 5.0/255.0, green: 64.0/255.0, blue: 163.0/255.0, alpha: 1.0), for: UIControl.State.normal)
            imgPicPost.isHidden = false
            editView.isHidden = false
            if defult.value(forKey: Constants.Bounce.bounceUserId) != nil {
                followSelectionBoostView.isHidden = true // bounce on
            } else {
                followSelectionBoostView.isHidden = true
            }
            visibilitySelectionLbl.text = strVisibilitySelection
            visibilityTypeLbl.text = strVisibilityType
        } else {
            // editView.isHidden = false
            //  postBtn.setTitle("Post", for: UIControl.State.normal)
            imgPicPost.isHidden = true
            editView.isHidden = true
            followSelectionBoostView.isHidden = true
        }
       
        // let imageData:NSData = strImage.pngData()! as NSData
        if strVideoImg == "" {
        } else {
            if strBtnId == "1" {
                let url = URL(string: strVideoImg)
                getThumbnailImageFromVideoUrl(url: url!) { thumbImage in
                    self.imgPicPost.image = thumbImage
                    var currentAsset: AVAsset?
                    currentAsset = AVAsset(url: url!)
                    let playerItem = AVPlayerItem(asset: currentAsset!)
                    let player = AVPlayer(playerItem: playerItem)
                  
                    // 3.Create AVPlayerLayer object
                    if self.playerLayer.player == nil {
                        self.playerLayer = AVPlayerLayer(player: player)
                        self.playerLayer.frame = self.imgPicPost.bounds // bounds of the view in which AVPlayer should be displayed
                        self.playerLayer.videoGravity = .resizeAspect
                        self.imgPicPost.layer.addSublayer(self.playerLayer)
                  
                    } else {
                        self.playerLayer.player = player
                    }
                    // 5.Play Video
                    player.play()
                }
            } else {
                // imgPicPost.image = strImage
            }
        }
  
        // selectedImageV.sd_setImage(with: URL(string: strImage), placeholderImage: UIImage(named: "placeholder.png"))
    }
    
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {
        playerLayer.player?.pause()
    }
    
    override func viewDidLayoutSubviews() {
        roundView.layer.masksToBounds = true
        roundView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?) -> Void)) {
        DispatchQueue.global().async { // 1
            let asset = AVAsset(url: url) // 2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) // 3
            avAssetImageGenerator.appliesPreferredTrackTransform = true // 4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) // 5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) // 6
                let thumbImage = UIImage(cgImage: cgThumbImage) // 7
                DispatchQueue.main.async { // 8
                    completion(thumbImage) // 9
                }
            } catch {
                print(error.localizedDescription) // 10
                DispatchQueue.main.async {
                    completion(nil) // 11
                }
            }
        }
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
   
    @IBAction func endBunceBtnAction(_ sender: Any) {
        let otherStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
        navigationController?.pushViewController(objTabView, animated: true)
    }

    @IBAction func keepBounceBtnAction(_ sender: Any) {
        let otherStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
        navigationController?.pushViewController(objTabView, animated: true)
    }

    @IBAction func closeBounceBtnAction(_ sender: Any) {
        let otherStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
        navigationController?.pushViewController(objTabView, animated: true)
    }
    
    @IBAction func postBtnAction(_ sender: Any) {
        if strPhotoVideoSelectionId == "0" {
            doUploadPost()
        } else {
            print(postDescriptionView.text ?? "")
            let defaults = UserDefaults.standard
            defaults.set(postDescriptionView.text, forKey: "forSaveDescriptionData")
            defaults.synchronize()
            tabBarController?.tabBar.isHidden = true
            let ImgStoryboard = UIStoryboard(name: "ImgStoryboard", bundle: nil)
            let objUploadImageGallery = ImgStoryboard.instantiateViewController(withIdentifier: "UploadImageGalleryVC") as! UploadImageGalleryVC
            navigationController?.pushViewController(objUploadImageGallery, animated: true)
        }
    }

    @IBAction func postBoostBtnAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        let ImgStoryboard = UIStoryboard(name: "ImgStoryboard", bundle: nil)
        let objPostVisibility = ImgStoryboard.instantiateViewController(withIdentifier: "PostVisibilityVC") as! PostVisibilityVC
        navigationController?.pushViewController(objPostVisibility, animated: true)
    }
    
    // for Update Profile Image
    func doUploadPost() {
        let defult = UserDefaults.standard
        var keyName = ""
        strVisibilitySelection = "\(defult.object(forKey: "forSelectVisibility") ?? "Public")"
        if strVisibilitySelection == "Follower Boost" {
            keyName = "follower"
        } else if strVisibilitySelection == "Public" {
            keyName = "public"
        } else if strVisibilitySelection == "Follower" {
            keyName = "follower"
        } else if strVisibilitySelection == "Close Friends" {
            keyName = "close_friends"
            
        } else {
            keyName = "public"
            strVisibilitySelection = "Public"
        }
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        if postDescriptionView.text == descriptionTextViewPlaceHolder {
            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter Description")
            return
        }
        if strSelectedType == "video" {
            var imageData = Data()
            self.progressBar.progress = 0.0
            let url = URL(string: strVideoImg)
            do {
                imageData = try Data(contentsOf: url!)
            } catch {
                print("Excption")
            }
            let strDeviceToken = "\(defult.object(forKey: Constants.DEVICETOKEN) ?? "")"
            var params = [Constants.KEY_MYUSER_ID: strUserId,
                          Constants.DEVICE_TYPE: "I",
                          Constants.KEY_DESCRIPTION: postDescriptionView.text!,
                          Constants.KEY_TYPE: strSelectedType,
                          "visibility": strVisibilitySelection,
                          Constants.KEY_DEVICE_ID: "\(strDeviceToken)"] as [String: Any]
            if defult.value(forKey: Constants.Bounce.bounceUserId) != nil {
                params["bounce_user"] = defult.value(forKey: Constants.Bounce.bounceUserId)
            }
            progressBar.isHidden = false
            showProgressView(text: "Uploading...")
            PSWebServiceAPI.doUploadPostVideo(endUrl: UPLOADPOST, videoData: imageData, parameter: params, onCompletion: {[weak self] response in
                guard let self = self else { return}
                
                self.progressBar.isHidden = true
                if response["Error"] == nil {
                    self.hideProgressView()
                    let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                    if Status == 200 {
                        DispatchQueue.main.async {
                            PSUtil.hideProgressHUD()
                            UserDefaults.standard.set("", forKey: "forSelectVisibility")
                            let responseDic = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                            print(responseDic)
                            // let msg = response[Constants.KEY_MESSAGE] as? String
                            let otherStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
                            let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
                            self.navigationController?.pushViewController(objTabView, animated: true)
                        }
                    } else {
                        let msg = response[Constants.KEY_MESSAGE] as? String
                        self.hideProgressView()
                        PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
                } else {
                    self.hideProgressView()
                    PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
                }
            }, onError: {_ in 
                self.hideProgressView()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
                self.progressBar.isHidden = true
            }, currentProgress: { completedFraction in
                self.progressBar.progress = Float(completedFraction)
            }
            )
            
        } else if strSelectedType == "image" {
            let imageData = imgPicPost.image?.jpegData(compressionQuality: 0.50)
            let strDeviceToken = "\(defult.object(forKey: Constants.DEVICETOKEN) ?? "")"
            var params = [Constants.KEY_MYUSER_ID: strUserId,
                          Constants.DEVICE_TYPE: "I",
                          Constants.KEY_DESCRIPTION: postDescriptionView.text!,
                          Constants.KEY_TYPE: strSelectedType,
                          "visibility": strVisibilitySelection,
                          Constants.KEY_DEVICE_ID: "\(strDeviceToken)"] as [String: Any]
            
            if defult.value(forKey: Constants.Bounce.bounceUserId) != nil {
                params["bounce_user"] = defult.value(forKey: Constants.Bounce.bounceUserId)
            }
            print(params)
            showProgressView(text: "Uploading...")
            PSWebServiceAPI.doUploadPost(endUrl: UPLOADPOST, profileImg: imageData, parameter: params, onCompletion: { response in
                PSUtil.hideProgressHUD()
                if response["Error"] == nil {
                    let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                    if Status == 200 {
                        UserDefaults.standard.set("", forKey: "forSelectVisibility")
                        let responseDic = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                        print(responseDic)
                        let msg = response[Constants.KEY_MESSAGE] as? String
                        DispatchQueue.main.async { [self] in
                            Toast(text: msg).show()
                            closeBounceBtnAction(self)
                        }
                    } else {
                        let msg = response[Constants.KEY_MESSAGE] as? String
                        PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
                } else {
                    PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
                }
            })
        } else {}
    }
}

extension UserUploadPostVC {
    /* Gives a resolution for the video by URL */
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}

// YPImagePickerDelegate
extension UserUploadPostVC: YPImagePickerDelegate {
    func imagePickerHasNoItemsInLibrary(_ picker: YPImagePicker) {}

    func noPhotos() {}

    func shouldAddToSelection(indexPath: IndexPath, numSelections: Int) -> Bool {
        return true // indexPath.row != 2
    }
}

extension UserUploadPostVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if postDescriptionView.textColor == UIColor.lightGray {
            postDescriptionView.text = nil
            postDescriptionView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if postDescriptionView.text.isEmpty {
            postDescriptionView.text = descriptionTextViewPlaceHolder
            postDescriptionView.textColor = UIColor.lightGray
        }
    }
}
