//
//  SearchListVC.swift
//  bloook
//
//  Created by Tech Astha on 12/01/21.
//

import AVFoundation
import AVKit
import CoreSpotlight
import Gallery
import UIKit

class SearchListVC: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate {
    @IBOutlet var peopleSearchCollectionView: UICollectionView!
    @IBOutlet var multipleTabView: UIView!
    @IBOutlet var popularView: UIView!
    @IBOutlet var popularBtn: UIButton!
    @IBOutlet var peopleBtn: UIButton!
    @IBOutlet var tagBtn: UIButton!
    @IBOutlet var searchField: UITextField!
    @IBOutlet var tableView: UITableView!
    var seachModel: SearchModel?
    var searchTimer: Timer?

    var selectedType = "popular"
    override func viewDidLoad() {
        super.viewDidLoad()
        multipleTabView.round(corners: [.bottomLeft, .topRight], radius: 40)
        popularBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        hideKeyboardWhenTappedAround()
        tableView.register(UINib(nibName: PostsTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: PostsTableViewCell.reuseIdentifier)
        tableView.estimatedSectionHeaderHeight = 60
    }

    override func viewDidLayoutSubviews() {
        multipleTabView.layer.masksToBounds = true
        multipleTabView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }

    override func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tap)
    }

    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)

        let editor = VideoEditor()
        editor.edit(video: video) { (_: Video?, tempPath: URL?) in
            DispatchQueue.main.async {
                if let tempPath = tempPath {
                    let controller = AVPlayerViewController()
                    controller.player = AVPlayer(url: tempPath)

                    self.present(controller, animated: true, completion: nil)
                }
            }
        }
    }

    @IBAction func popularBtnAction(_ sender: Any) {
        popularBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        peopleBtn.setTitleColor(UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0), for: UIControl.State.normal)
        tagBtn.setTitleColor(UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0), for: UIControl.State.normal)
        selectedType = "popular"
        fetchUser(search: searchField.text ?? "")
        tableView.superview?.isHidden = false
        peopleSearchCollectionView.superview?.isHidden = true
    }

    @IBAction func peopleBtnAction(_ sender: Any) {
        popularBtn.setTitleColor(UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0), for: UIControl.State.normal)
        peopleBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        tagBtn.setTitleColor(UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0), for: UIControl.State.normal)
        selectedType = "people"
        fetchUser(search: searchField.text ?? "")
        tableView.superview?.isHidden = true
        peopleSearchCollectionView.superview?.isHidden = false
    }

    @IBAction func tagBtnAction(_ sender: Any) {
        popularBtn.setTitleColor(UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0), for: UIControl.State.normal)
        peopleBtn.setTitleColor(UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0), for: UIControl.State.normal)
        tagBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        selectedType = "tag"
        fetchUser(search: searchField.text ?? "")
        tableView.superview?.isHidden = false
        peopleSearchCollectionView.superview?.isHidden = true
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return seachModel?.searchUsers?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let user = seachModel?.searchUsers?[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! PeopleSearchCollectionViewCell
        cell.delegate = self
        cell.configure(searchUser: user)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 0.0, bottom: 10.0, right: 0.0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow: CGFloat = 2
        let hardCodedPadding: CGFloat = 10
        let itemWidth = (peopleSearchCollectionView.bounds.width/itemsPerRow) - hardCodedPadding
        return CGSize(width: itemWidth, height: 225)
    }
}

extension SearchListVC {
    func fetchUser(search: String) {
        if search.count < 2 {
            return
        }
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"

        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TYPE: selectedType,
                     Constants.KEY_SEARCH: search,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        PSWebServiceAPI.fetchSearchedUser(param, completion: { response, error in

            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.seachModel = response?.searchModel
                    DispatchQueue.main.async { [self] in
                        if selectedType == "popular" || selectedType == "tag" {
                            tableView.reloadData()
                        } else {
                            peopleSearchCollectionView.reloadData()
                        }
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
}

extension SearchListVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(getHintsFromTextField),
            object: textField)
        perform(
            #selector(getHintsFromTextField),
            with: textField,
            afterDelay: 1)
        return true
    }

    @objc func getHintsFromTextField(textField: UITextField) {
        let updatedText = textField.text ?? ""
        if !updatedText.isEmpty {
            fetchUser(search: updatedText)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SearchListVC: PeopleSearchCollectionViewCellDelegate {
    func followTapped(userId: String, cell: PeopleSearchCollectionViewCell) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: userId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
       // progressView = AcuantProgressView(frame: view.frame, center: view.center)
        showProgressView(text: "Loading...")
        PSWebServiceAPI.doFollowUser(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let message = response[Constants.KEY_MESSAGE] as? String
                    if message == "User unfollowed successfully" {
                        cell.updateFollowStatus("Follow")
                    } else {
                        cell.updateFollowStatus("Unfollow")
                    }
                    // self.fetchUser(search: self.searchField.text ?? "")
                } else {
                    PSUtil.hideProgressHUD()
                    guard let msg = response[Constants.KEY_MESSAGE] as? String else {
                        PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
                        return
                    }
                    PSUtil.showAlertFromController(controller: self, withMessage: msg)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
}

extension SearchListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostsTableViewCell.reuseIdentifier, for: indexPath) as! PostsTableViewCell
        let post = seachModel?.searchPosts?[indexPath.section].posts
        cell.configure(post: post)
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 60
        }
        return 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let uiview = UIView()
        uiview.backgroundColor = .clear
        return uiview
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return seachModel?.searchPosts?.count ?? 0
    }
}

extension SearchListVC: PostsDelegate {
    func showAlert(message: String) {
        PSUtil.showAlertFromController(controller: self, withMessage: message)
    }

    func moreOptionsTapped(alert: UIAlertController) {
        present(alert, animated: true, completion: {
            print("completion block")
        })
    }

    func treasureTapped(post: Post?) {}

    func imageTapped(post: Post?) {
        let strUserImg = post?.filename ?? ""
        let url = URL(string: strUserImg)
        if post?.type == .video {
            playVideo(url: url!)
            return
        }
//        tabBarController?.tabBar.isHidden = true
//        view.addSubview(imageDetailView)
//        imageDetailView.isHidden = false
//
//        imageScrollView.setup()
//        imageScrollView.imageContentMode = .aspectFit
//        imageScrollView.initialOffset = .center
//
//        KingfisherManager.shared.retrieveImage(with: url!,
//                                               options: nil,
//                                               progressBlock: nil) { [self] (result) -> () in
//            switch result {
//            case .success(let value):
//                imageScrollView.display(image: value.image)
//            case .failure(let error):
//                print("Error: \(error)")
//            }
//        }
//
//        let strUserDesc = post?.dDescription
//        let strProfilePhoto = post?.user?.profilePhoto
//
//        nameLabel.text = post?.user?.fullName()
//
//        descriptionLabel.text = strUserDesc
//
//        if strProfilePhoto == "" || strProfilePhoto == "nil" {
//            profileImage.sd_setImage(with: URL(string: "img9"), placeholderImage: UIImage(named: "placeholder.png"))
//        } else {
//            profileImage.kf.setImage(with: URL(string: strProfilePhoto!), placeholder: UIImage(named: "placeholder.png"))
//        }
//        dateLabel.text = post?.createdAt?.dateWithoutTimeComponent()
    }

    func profileImageTapped(post: Post?) {
        let otherStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
        objMyProfile?.selectedUserId = "\(post?.userID ?? 0)"
        navigationController?.pushViewController(objMyProfile!, animated: true)
    }

    @IBAction func cancelBtnAction(_ sender: Any) {
//        imageDetailView.removeFromSuperview()
//        tabBarController?.tabBar.isHidden = false
    }

    func postOption(post: Post, option: OptionType) {
        switch option {
        case .delete:
            deletePost(post: post)
        case .edit:
            break
        case .share:
            break
        case .turnComment:
            turnOffComments(post: post)
        case .follow:
            followPost(post: post)
        case .report:
            reportPost(post: post)
        case .block:
            blockUser(user: post.user)
        }
    }
}

extension SearchListVC: LikeSectionDelegate {
    func shareButtonTapped(image: UIImage?, post: String?) {
        if image != nil {
            let shareItems = [image!, post!] as [Any]

            let avc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)

            present(avc, animated: true)
        } else {
            let shareItems = [post!]

            let avc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)

            present(avc, animated: true)
        }
    }

    func openCommentsTapped(postId: String) {
        tabBarController?.tabBar.isHidden = true
        let homeDetailStoryboard = UIStoryboard(name: "HomeDetailStoryboard", bundle: nil)
        let objAllCommentHome = homeDetailStoryboard.instantiateViewController(withIdentifier: "AllCommentHomeVC") as? AllCommentHomeVC
        objAllCommentHome?.selectedPostId = postId
        navigationController?.pushViewController(objAllCommentHome!, animated: true)
    }

    func likeButtonTapped() {
        fetchUser(search: searchField.text ?? "")
    }

    func openLikeButtonTapped(postId: String) {
        let homeDetailStoryboard = UIStoryboard(name: "HomeDetailStoryboard", bundle: nil)
        let objAllLikeUserList = homeDetailStoryboard.instantiateViewController(withIdentifier: "AllLikeUserListVC") as? AllLikeUserListVC
        objAllLikeUserList?.strPostId = postId
        tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(objAllLikeUserList!, animated: true)
    }

    func playVideo(url: URL) {
        let player = AVPlayer(url: url)

        let vc = AVPlayerViewController()
        vc.player = player

        present(vc, animated: true) { vc.player?.play() }
    }
}
