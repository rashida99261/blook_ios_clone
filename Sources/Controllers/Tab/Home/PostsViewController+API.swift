//
//  PostsViewController+API.swift
//  Bloook
//
//  Created by Suresh Varma on 03/08/21.
//

import Foundation
import Toaster

extension PostsViewController {
    func deletePost(post: Post) {
        guard PSUtil.reachable() else {
            showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.deletePost(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.startFreshLoading(clearOld: true, makeAPI: true)
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    self.showAlert(message: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func sharePost(post: Post) {
        guard PSUtil.reachable() else {
            showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.sharePost(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.startFreshLoading(clearOld: true, makeAPI: true)
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    self.showAlert(message: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }

    func followPost(post: Post) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: post.userID ?? strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doFollowUser(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    self.getPostsFromServer(silentLoad: true)
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
    
    func turnOffComments(post: Post) {
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     "comment_status": post.commentStatus == 0 ? "1" : "0",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.updateCommentSetting(param, completion: { response in
            
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    PSUtil.hideProgressHUD()
                    DispatchQueue.main.async {
//                        self.doPostCommentListFromServer()
//                        self.commentField.text! = ""
                        self.getPostsFromServer(silentLoad: true)
                    }
                   
                } else {
                    PSUtil.hideProgressHUD()
                    guard let msg = response[Constants.KEY_MESSAGE] as? String else {
                        self.showAlert(message: PSAPI.SomeThingWrong)
                        return
                    }
                    self.showAlert(message: msg)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: PSAPI.SomeThingWrong)
            }
        })
    }
    
    func getUserProfile() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_PROFILE_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        // self.showProgressView(text: "Loading...")
        PSWebServiceAPI.getUserProfile(param, completion: { response, error in
            if error == nil {
                if response?.isValid() == true {
                    guard let user = response?.user else { return }
                    
                    DispatchQueue.main.async {
                        defult.setValue(user.bounceStatus?.boolValue(), forKey: Constants.Bounce.isBounceOn)
                        if user.bounceStatus?.boolValue() == true {
                            self.showBounce()
                        }
                    }
                }
            }
        })
    }

    func blockUser(user: User?) {
        guard let user = user else { return }
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: "\(user.id)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doBlockedFriend(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                
                if Status == 200 {
                    DispatchQueue.main.async {
                        self.startFreshLoading(clearOld: true, makeAPI: true)
                    }

                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
    
    func reportPost(post: Post) {
        guard PSUtil.reachable() else {
            showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.reportPost(param, completion: { [self] response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    Toast(text: response?.message).show()
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    self.showAlert(message: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func savePost(post: Post) {
        guard PSUtil.reachable() else {
            showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     Constants.KEY_POST_USERId: post.userID ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Saving...")
        PSWebServiceAPI.savePost(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    Toast(text: response?.message).show()
                    self.getSavedPostList()
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    self.showAlert(message: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func getSavedPostList() {
        guard PSUtil.reachable() else {
            showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        PSWebServiceAPI.savedPostList(param, completion: { _, _ in
        })
    }
    
    func getSettings() {
        guard PSUtil.reachable() else {
            showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        PSWebServiceAPI.getSettings(param, completion: { _, _ in
        })
    }
    
    func getPostDetail() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"

        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: self.selectedPostId ?? "",
                     Constants.KEY_PAGE: "200",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        PSWebServiceAPI.getPostDetail(param, completion: { response, error in

            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.singlePost = response?.post
                    guard let post = self.singlePost else {
                        PSUtil.hideProgressHUD()
                        PSUtil.showAlertFromController(controller: self, withMessage: "Seems the post is deleted.")
                        return
                        
                    }
                    self.posts = [post]
                    DispatchQueue.main.async { [self] in
                        self.tableView.reloadData()
                        self.pausePlayeVideos()
                    }
                
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
}

extension SearchListVC {
    func deletePost(post: Post) {
        guard PSUtil.reachable() else {
            showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.deletePost(param, completion: { [self] response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.fetchUser(search: searchField.text ?? "")
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    self.showAlert(message: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }

    func reportPost(post: Post) {
        guard PSUtil.reachable() else {
            showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.reportPost(param, completion: { [self] response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.fetchUser(search: searchField.text ?? "")
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    self.showAlert(message: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func sharePost(post: Post) {
        guard PSUtil.reachable() else {
            showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.sharePost(param, completion: { [self] response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.fetchUser(search: searchField.text ?? "")
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    self.showAlert(message: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }

    func followPost(post: Post) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: post.userID ?? strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doFollowUser(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
    
    func turnOffComments(post: Post) {
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     "comment_status": post.commentStatus == 0 ? "1" : "0",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.updateCommentSetting(param, completion: { response in
            
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    PSUtil.hideProgressHUD()
                    DispatchQueue.main.async {
//                        self.doPostCommentListFromServer()
//                        self.commentField.text! = ""
                    }
                   
                } else {
                    PSUtil.hideProgressHUD()
                    guard let msg = response[Constants.KEY_MESSAGE] as? String else {
                        self.showAlert(message: PSAPI.SomeThingWrong)
                        return
                    }
                    self.showAlert(message: msg)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: PSAPI.SomeThingWrong)
            }
        })
    }

    func blockUser(user: User?) {
        guard let user = user else { return }
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: "\(user.id)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doBlockedFriend(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                
                if Status == 200 {
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    DispatchQueue.main.async { [self] in
                        Toast(text: msg).show()
                    }

                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
}

extension AllCommentHomeVC {
    func showAlert(message: String) {
        PSUtil.showAlertFromController(controller: self, withMessage: message)
    }
    
    func deletePost(post: Post) {
        guard PSUtil.reachable() else {
            self.showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.deletePost(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    Toast(text: response?.message).show()
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    self.showAlert(message: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func sharePost(post: Post) {
        guard PSUtil.reachable() else {
            self.showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.sharePost(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    Toast(text: response?.message).show()
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    self.showAlert(message: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }

    func followPost(post: Post) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: post.userID ?? strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doFollowUser(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                let msg = response[Constants.KEY_MESSAGE] as? String
                if Status == 200 {
                    Toast(text: msg).show()
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
    
    func turnOffComments(post: Post) {
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     "comment_status": post.commentStatus == 0 ? "1" : "0",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.updateCommentSetting(param, completion: { response in
            
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    DispatchQueue.main.async {
//                        self.doPostCommentListFromServer()
//                        self.commentField.text! = ""
                        Toast(text: msg).show()
                    }
                   
                } else {
                    PSUtil.hideProgressHUD()
                    guard let msg = response[Constants.KEY_MESSAGE] as? String else {
                        self.showAlert(message: PSAPI.SomeThingWrong)
                        return
                    }
                    self.showAlert(message: msg)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: PSAPI.SomeThingWrong)
            }
        })
    }
    
    func getUserProfile() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_PROFILE_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.getUserProfile(param, completion: { response, error in
            if error == nil {
                if response?.isValid() == true {
                    guard let user = response?.user else { return }
                    
                    DispatchQueue.main.async {
                        defult.setValue(user.bounceStatus?.boolValue(), forKey: Constants.Bounce.isBounceOn)
                        if user.bounceStatus?.boolValue() == true {
                            // self.showBounce()
                        }
                    }
                }
            }
        })
    }

    func blockUser(user: User?) {
        guard let user = user else { return }
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: "\(user.id)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doBlockedFriend(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                
                if Status == 200 {
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    DispatchQueue.main.async {
                        Toast(text: msg).show()
                    }

                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
    
    func reportPost(post: Post) {
        guard PSUtil.reachable() else {
            self.showAlert(message: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: post.id ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.reportPost(param, completion: { [self] response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    Toast(text: response?.message).show()
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    self.showAlert(message: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                self.showAlert(message: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
}
