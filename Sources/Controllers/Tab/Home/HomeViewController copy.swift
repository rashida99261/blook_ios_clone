//
//  HomeViewController.swift
//  Bloook
//
//  Created by Suresh Varma on 05/07/21.
//

import Foundation
import smooth_feed
import UIKit

class HomeViewController: UIViewController {
    var progressView: AcuantProgressView!
    @IBOutlet var collectionView: UICollectionView!
    var posts: [Post]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progressView = AcuantProgressView(frame: view.frame, center: view.center)
        collectionView.register(UINib(nibName: LikeCollectionViewCell.reuseIdentifier, bundle: nil),
                                forCellWithReuseIdentifier: LikeCollectionViewCell.reuseIdentifier)
        collectionView.register(UINib(nibName: PostsCollectionReusableView.reuseIdentifier, bundle: nil),
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: PostsCollectionReusableView.reuseIdentifier)
        collectionView.register(UINib(nibName: FooterCollectionReusableView.reuseIdentifier, bundle: nil),
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                                withReuseIdentifier: FooterCollectionReusableView.reuseIdentifier)
        guard let homeCollectionView = collectionView else { return }
        homeCollectionView.alwaysBounceVertical = true
        homeCollectionView.dataSource = self
        
        let layout = SmoothFeedCollectionViewLayout()
        layout.estimatedItemHeight = 50
        layout.itemsInSectionMaxHeight = UIScreen.main.bounds.width + 190
        layout.footerHeight = 60
        layout.headerHeight = 5
        homeCollectionView.collectionViewLayout = layout
        homeCollectionView.dataSource = self
        homeCollectionView.delegate = self

        homeCollectionView.stickyHeader.view = UIView()
        homeCollectionView.stickyHeader.height = 0 // 265
        homeCollectionView.stickyHeader.minimumHeight = 120
//        let layout = UICollectionViewFlowLayout()
//
//        layout.headerReferenceSize = CGSize(width: collectionView.frame.size.width,
//                                            height: 120)
//        layout.footerReferenceSize = CGSize(width: collectionView.frame.size.width,
//                                            height: 60)
//        layout.estimatedItemSize = CGSize(width: collectionView.frame.size.width,
//                                          height: 50)
//        homeCollectionView.collectionViewLayout = layout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getPostsFromServer()
    }

    func getPostsFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_PAGE: "0",
                     Constants.KEY_LIMIT: "2000",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        progressView = AcuantProgressView(frame: view.frame, center: view.center)

        PSWebServiceAPI.getAllPosts(param, completion: { response, error in
            PSUtil.hideProgressHUD()
            if error == nil {
                guard let posts = response?.posts else { return }
                self.posts = posts
                self.collectionView.reloadData()
            } else {
                PSUtil.showAlertFromController(controller: self, withMessage: error.debugDescription)
            }
        })
    }

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//         return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.height)
//    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        if let headerView = collectionView.visibleSupplementaryViews(ofKind: UICollectionView.elementKindSectionHeader).first as? PostsCollectionReusableView {
//                    // Layout to get the right dimensions
//                    headerView.layoutIfNeeded()
//
//                    // Automagically get the right height
//            //let height = 120.0
//
//                    // return the correct size
//                    return CGSize(width: collectionView.frame.width, height: 120)
//                }
//
//                // You need this because this delegate method will run at least
//                // once before the header is available for sizing.
//                // Returning zero will stop the delegate from trying to get a supplementary view
//                return CGSize(width: 1, height: 1)
//            }
//

    
    func collectionView1(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let post = posts?[section]
        if post?.type == .image {
            print("Bloggggggg")
            return CGSize(width: collectionView.frame.width, height: 465)
        }
        return CGSize(width: collectionView.frame.width, height: 120+60)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return posts?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = LikeCollectionViewCell.reuseIdentifier
        let post = posts?[indexPath.section]

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! LikeCollectionViewCell
        
        cell.allTimeView.isHidden = true
        // cell.allTimeView.isHidden = true
        let strLikeId = post?.userLike
        let strLikeCount = post?.likesCount
        cell.viewsLabel.text = strLikeCount

        if strLikeId == "0" {
            cell.likeImg.image = UIImage(named: "heartBlack")
            
        } else {
            cell.likeImg.image = UIImage(named: "like")
        }
        cell.likeCountBtn.tag = indexPath.section
//            cell.likeCountBtn.addTarget(self, action: #selector(likeCountListBtnAction(_:)), for: UIControl.Event.touchUpInside)
//            cell.likeButton.tag = indexPath.section
//            cell.likeButton.addTarget(self, action: #selector(likeBtnAction(_:)), for: UIControl.Event.touchUpInside)
//            cell.commentButton.tag = indexPath.section
//            cell.commentButton.addTarget(self, action: #selector(commentBtnAction(_:)), for: UIControl.Event.touchUpInside)
//            cell.shareButton.tag = indexPath.section
//            cell.shareButton.addTarget(self, action: #selector(shareBtnAction(_:)), for: UIControl.Event.touchUpInside)
//            cell.allTimeBtn.tag = indexPath.section
//            cell.allTimeBtn.addTarget(self, action: #selector(allTimeBtnAction(_:)), for: UIControl.Event.touchUpInside)
//            cell.allLifeTimeBtn.tag = indexPath.section
//            cell.allLifeTimeBtn.addTarget(self, action: #selector(allLifeTimeBtnAction(_:)), for: UIControl.Event.touchUpInside)
//
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView
    {
        var reuseIdentifier: String
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            reuseIdentifier = PostsCollectionReusableView.reuseIdentifier
        case UICollectionView.elementKindSectionFooter:
            reuseIdentifier = FooterCollectionReusableView.reuseIdentifier
        default:
            fatalError("unexpected kind")
        }
        let supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                                withReuseIdentifier: reuseIdentifier,
                                                                                for: indexPath)
        
        let commentBtn = supplementaryView as? FooterCollectionReusableView
        
        commentBtn?.roundedBorderView.layer.masksToBounds = true
        commentBtn?.roundedBorderView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        
        let post = posts?[indexPath.section]
        let layout = collectionView.collectionViewLayout as! SmoothFeedCollectionViewLayout
        if layout.headerHeight == 5 {
            
        }
        
        let postsCollectionReusableView = supplementaryView as? PostsCollectionReusableView
        postsCollectionReusableView?.configure(post: post)
        return supplementaryView
    }
}
