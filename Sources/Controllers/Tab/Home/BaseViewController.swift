//
//  BaseViewController.swift
//  Bloook
//
//  Created by Suresh Varma on 08/07/21.
//

import UIKit
import Pulsator
import Toaster




class BaseViewController: UIViewController {
   // var progressView: AcuantProgressView!
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //progressView = AcuantProgressView(frame: view.frame, center: view.center)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func showProgressView(text: String = "") {
       
      DispatchQueue.main.async {
//         //   //self.progressView.messageView.text = text
//            self.progressView.startAnimation()
//            self.view.addSubview(self.progressView)
          
//
//
      }
    }

    func hideProgressView() {
        
        DispatchQueue.main.async {
        
           //self.progressView.stopAnimation()
            //self.progressView.removeFromSuperview()
        }
    }

    func setupNavBar() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
           self.navigationController!.navigationBar.shadowImage = UIImage()
           self.navigationController!.navigationBar.isTranslucent = true
    }

//    func startPulse(imageView: UIImageView) {
//        DispatchQueue.main.async {
//            let pulsator = Pulsator()
//
//            pulsator.numPulse = 5
//            pulsator.radius = imageView.frame.size.width * 0.75
//            pulsator.backgroundColor = UIColor(red: 163/255.0, green: 139/255.0, blue: 212/255.0, alpha: 1).cgColor
//            imageView.layer.superlayer?.insertSublayer(pulsator, below: imageView.layer)
//            pulsator.position = imageView.layer.position
//            pulsator.start()
//        }
//    }
    
    func hideKeyboardWhenTappedAround() {
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            tap.cancelsTouchesInView = false
            view.addGestureRecognizer(tap)
        }
        
        @objc func dismissKeyboard() {
            view.endEditing(true)
        }
    
    private func imageView(imageName: String) -> UIImageView {
        let logo = UIImage(named: imageName)
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = logo
        logoImageView.widthAnchor.constraint(equalToConstant: 35).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 35).isActive = true
        return logoImageView
    }

    func barImageView(imageName: String) -> UIBarButtonItem {
        return UIBarButtonItem(customView: imageView(imageName: imageName))
    }

    func barButton(imageName: String, selector: Selector) -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: imageName), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        button.widthAnchor.constraint(equalToConstant: 35).isActive = true
        button.heightAnchor.constraint(equalToConstant: 35).isActive = true
        button.addTarget(self, action: selector, for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
}


