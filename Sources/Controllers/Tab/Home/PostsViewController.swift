//
//  PostsViewController.swift
//  Bloook
//
//  Created by Suresh Varma on 05/07/21.
//

import AVFoundation
import AVKit

import Foundation
import gooey_cell
import ImageScrollView
import Kingfisher
import smooth_feed
import Toaster
import UIKit
import VariousViewsEffects
import Gifu

class PostsViewController: BaseViewController, UITabBarControllerDelegate {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var roundView: RoundedView!
    var allPosts = [Post]()
    var followingPosts = [Post]()
    var posts: [Post]?
    var singlePost: Post?
    var selectedPostId: Int?
    var stopLoading = false
    var currentPage: Int = 1
    var isLoadingList: Bool = false
    var topTitle = "Post"
    @IBOutlet var footerView: UIView!
    @IBOutlet var leftButton: UIButton!
    @IBOutlet var rightButton: UIButton!
    @IBOutlet var homeTitleButton: UIButton!
    @IBOutlet var followingTitleButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var followerView: UIView!
    
  

    // TODO: Move to imageDetail
    @IBOutlet var imageDetailView: UIView!
    @IBOutlet var imageScrollView: ImageScrollView!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    var bottomSheetVC: BRQBottomSheetViewController?

    var postTimer: Timer?
    var treasureTimer: Timer?
    var fullyVisibleCell: PostsTableViewCell?
    var selectedTab = 0
    var isBounceOn = false
    var postFetchingInProgress = false
    var periodicObserver: Any?
    var playerController: ASVideoPlayerController?
    var videoLayer = AVPlayerLayer()
    var videoURL: String? {
        didSet {
            if let videoURL = videoURL {
                ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL)
            }
            videoLayer.isHidden = videoURL == nil
        }
    }

    var player: AVPlayer?
    var playerItem: AVPlayerItem?
    @IBOutlet var playPauseButton: UIButton!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var playbackSlider: UISlider!
    @IBOutlet var sliderHeightConstraint: NSLayoutConstraint!
    @IBOutlet var backButton: UIButton!
    var selectedPostType: PostType?
    var isSelectedVideoMute = true
    
    
    @IBOutlet weak var imgGradient: UIImageView!
    @IBOutlet var leaderBoardGif: GIFImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.bringSubviewToFront(imageDetailView)
        imageDetailView.isHidden = true
        
        leaderBoardGif.animate(withGIFNamed: "loaderBoard", animationBlock:  {
      })
        
        tableView.register(UINib(nibName: PostsTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: PostsTableViewCell.reuseIdentifier)
        tableView.estimatedSectionHeaderHeight = 60
        tabBarController?.delegate = self
        if singlePost == nil, selectedPostId == nil {
            getPostsFromServer(silentLoad: false)
            getSavedPostList()
            getSettings()
            setSelectedTab()
        } else {
            if singlePost != nil {
                selectedPostId = singlePost?.id
            }
            tableView.reloadData()
          //  showProgressView()
            self.imgGradient.isHidden = true
            self.viewLoader.isHidden = false
            self.loadGif.animate()
            
            getPostDetail()
        }
        followerView.backgroundColor = .clear
        clearDataLocally()
        NotificationCenter.default.addObserver(self, selector: #selector(appEnteredFromBackground), name: UIApplication.willEnterForegroundNotification, object: nil)

        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(singleTapGestureRecognizer(_:)))
        singleTapGesture.numberOfTapsRequired = 1
        imageScrollView.addGestureRecognizer(singleTapGesture)
       
        let pauseImage = UIImage(named: "pauseIcon")
        playPauseButton.setImage(pauseImage, for: .normal)
        let playImage = UIImage(named: "playIcon")
        playPauseButton.setImage(playImage, for: .selected)
                
    }

    
    @objc func singleTapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        singleTappedNotification()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    func configureSlider() {
        let image: UIImage? = UIImage(named: "sliderCircle")
        playbackSlider.setThumbImage(image, for: .normal)
        playbackSlider.setThumbImage(image, for: .highlighted)
        playPauseButton.isSelected = false
        guard let player = ASVideoPlayerController.sharedVideoPlayer.actualPlayer else { return }
        guard let playerItem = ASVideoPlayerController.sharedVideoPlayer.actualPlayerItem else { return }
        self.player = player
        self.playerItem = playerItem
        playbackSlider.minimumValue = 0

        playbackSlider.addTarget(self, action: #selector(playbackSliderValueChanged(_:)), for: .valueChanged)

        let duration: CMTime = playerItem.asset.duration
        let seconds: Float64 = CMTimeGetSeconds(duration)

        let duration1: CMTime = playerItem.currentTime()
        let seconds1: Float64 = CMTimeGetSeconds(duration1)
        lblDuration.text = stringFromTimeInterval(interval: seconds - seconds1)
        print("****\(seconds) == \(seconds1)")
        playbackSlider.maximumValue = Float(seconds)
        playbackSlider.isContinuous = true

        periodicObserver = player.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (_) -> Void in
            print("****\(self.player!.currentItem?.status)")
            if self.player!.currentItem?.status == .readyToPlay {
                let time: Float64 = CMTimeGetSeconds(self.player!.currentTime())
                self.playbackSlider.value = Float(time)

                self.lblDuration.text = self.stringFromTimeInterval(interval: seconds - time)
                print("****ST\(seconds) == \(time)")
            }

            let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
            if playbackLikelyToKeepUp == false {
                print("IsBuffering")
                self.playPauseButton.isEnabled = false
            } else {
                // stop the activity indicator
                print("Buffering completed")
                self.playPauseButton.isEnabled = true
            }
        }
    }

    @objc func playbackSliderValueChanged(_ playbackSlider: UISlider) {
        let seconds = Int64(playbackSlider.value)
        let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)

        player!.seek(to: targetTime)

        if player!.rate == 0 {
            player?.play()
        }
    }

    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval/60) % 60
        let hours = (interval/3600)
        if hours >= 1 {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pausePlayeVideos()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ASVideoPlayerController.sharedVideoPlayer.playPauseVideo(forcePlay: false)
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
        if LoggedInUser.shared.user == nil {
            getUserProfile()
        }
        if singlePost == nil, selectedPostId == nil {
            followerView.isHidden = false
            titleLabel.isHidden = true
        } else {
            if singlePost != nil {
                posts = [singlePost!]
            }

            tableView.reloadData()
            setTitle(title: topTitle)

            followerView.isHidden = true
            titleLabel.isHidden = false
            leftButton.setImage(UIImage(named: "left-arrow"), for: .normal)
            leftButton.removeTarget(nil, action: nil, for: .allEvents)
            leftButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
            rightButton.isHidden = true
        }
    }

    private func clearDataLocally() {
        UserDefaults.standard.set(nil, forKey: Constants.Signup.firstName)
        UserDefaults.standard.set(nil, forKey: Constants.Signup.lastName)
        UserDefaults.standard.set(nil, forKey: Constants.Signup.userName)
        UserDefaults.standard.set(nil, forKey: Constants.Signup.emailAddress)
        UserDefaults.standard.set(nil, forKey: Constants.Signup.password)
        UserDefaults.standard.set(nil, forKey: Constants.Signup.mobileNo)
        UserDefaults.standard.set(nil, forKey: Constants.Signup.pageNo)
    }

    func setSelectedTab() {
        if selectedTab == 0 {
            homeTitleButton.titleLabel?.font = UIFont(name: "Roboto-Bold", size: 18)
            followingTitleButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16)
            posts = allPosts
            tableView.reloadData()
            if posts?.isEmpty == false {
                _ = scrollTableToTop()
            }
        } else {
            homeTitleButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16)
            followingTitleButton.titleLabel?.font = UIFont(name: "Roboto-Bold", size: 18)
            posts = followingPosts
            if followingPosts.isEmpty == true {
                //showProgressView()
                self.imgGradient.isHidden = true
                self.viewLoader.isHidden = false
                self.loadGif.animate()

                startFreshLoading(clearOld: false)
                getPostsFromServer(scrollToTop: true)
            } else {
                if posts?.isEmpty == false {
                    _ = scrollTableToTop()
                }
            }
        }
        tableView.reloadData()
        if (posts?.count ?? 0) == 0 {
            //showProgressView()
            self.imgGradient.isHidden = true
            self.viewLoader.isHidden = false
            self.loadGif.animate()

            startFreshLoading(clearOld: false, makeAPI: true)
        }
    }

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            if scrollTableToTop() {
                startFreshLoading()
                getPostsFromServer(scrollToTop: true, silentLoad: false)
            }
        }
    }

    private func scrollTableToTop() -> Bool {
        tableView.reloadData()
        guard tableView.numberOfSections > 0, tableView.numberOfRows(inSection: 0) > 0 else {
            startFreshLoading()
            getPostsFromServer(scrollToTop: true)
            return false
        }

        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        return true
    }

    func setTitle(title: String) {
        titleLabel.text = title
    }

    func getPostsFromServer(scrollToTop: Bool = false, silentLoad: Bool = false) {
        if selectedPostId != nil {
            getPostDetail()
            return
        }
        if postFetchingInProgress == true { return }
        postFetchingInProgress = true
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }

        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        var param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_PAGE: currentPage,
                     Constants.KEY_LIMIT: "10",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I",
                     "toggle_status": selectedTab] as [String: Any]
        if selectedPostId != nil {
            param["post_id"] = "\(selectedPostId ?? 0)"
        }

        PSWebServiceAPI.getAllPosts(param, completion: { response, error in
            DispatchQueue.main.async {
                self.postFetchingInProgress = false
                //self.hideProgressView()
                self.imgGradient.isHidden = false
                self.viewLoader.isHidden = true

                if error != nil {
                    self.checkForAuthorization(message: error?.localizedDescription)
                } else {
                    if response?.status == 200 {
                        guard let posts = response?.posts else { return }
                        if self.selectedTab == 0 {
                            self.allPosts.append(contentsOf: posts)
                            self.posts = self.allPosts
                        } else {
                            self.followingPosts.append(contentsOf: posts)
                            self.posts = self.followingPosts
                        }
                        if self.selectedPostId != nil {
                            self.singlePost = posts.first
                        }

                        if self.posts?.isEmpty == true {
                            if self.selectedTab == 1 {
                                self.selectedTab = 0
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                    self.setSelectedTab()
                                }

                                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.notFollowing)
                            }
                            return
                        }
                        DispatchQueue.main.async { [self] in
                            if silentLoad == false {
                                self.tableView.reloadData()
                                self.tableView.tableFooterView = posts.count < 10 ? nil : self.self.footerView
                                self.stopLoading = posts.count < 10 ? true : false
                                if scrollToTop == true {
                                    _ = self.scrollTableToTop()
//                                    if self.tableView.numberOfRows(inSection: 0) != 0 {
//                                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
//                                            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
//                                        }
//                                    }
                                }
                            } else {
                                for item in self.tableView.indexPathsForVisibleRows! {
                                    let cell = self.tableView.cellForRow(at: item) as! PostsTableViewCell
                                    if posts.count > item.section {
                                        let post = posts[item.section]
                                        cell.configure(post: post)
                                    } else {
                                        self.tableView.reloadData()
                                        break
                                    }
                                }
                            }
                            self.pausePlayeVideos()
                        }

                    } else {
                        self.checkForAuthorization(message: response?.message)
                    }
                }
            }
        })
    }

    func updatePostViewed(post: Post?, cell: PostsTableViewCell) {
        guard let post = post else { return }
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_USERId: "\(post.user?.id ?? 0)",
                     Constants.KEY_POST_ID: "\(post.id ?? 0)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        PSWebServiceAPI.updatePostView(param, completion: { response in
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    cell.updatePostCount(count: dict["count"] as! Int)
                    self.getPostsFromServer(silentLoad: true)
                }
            }
        })
    }

    private func checkForAuthorization(message: String?) {
        if message == "Unauthorized" {
            tabBarController?.tabBar.isHidden = true

            PSUtil.showAlertFromController(controller: self, withMessage: message ?? "", andHandler: { _ in
                UserDefaults.standard.set(false, forKey: Constants.ISLOGIN)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
                self.navigationController?.pushViewController(objVC!, animated: true)
            })
        }
    }

    @IBAction func menuGalleryBtnAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        let TopStoryboard = UIStoryboard(name: "TopStoryboard", bundle: nil)
        let objTopScreen = TopStoryboard.instantiateViewController(withIdentifier: "TopScreenVC") as! TopScreenVC
        navigationController?.pushViewController(objTopScreen, animated: true)
    }

    @IBAction func messageBtnAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        let msgStoryboard = UIStoryboard(name: "MessageStoryboard", bundle: nil)
        let objUMessage = msgStoryboard.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        navigationController?.pushViewController(objUMessage, animated: true)
    }

    @IBAction func playPauseTapped(_ sender: Any) {
        guard let sender = sender as? UIButton else { return }
        sender.isSelected = !sender.isSelected
        ASVideoPlayerController.sharedVideoPlayer.playPauseVideo()
    }

    @IBAction func homeBtnAction(_ sender: Any) {
        selectedTab = 0
        setSelectedTab()
    }

    @IBAction func followerBtnAction(_ sender: Any) {
        selectedTab = 1
        setSelectedTab()
    }

    @objc func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
}

extension PostsViewController: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostsTableViewCell.reuseIdentifier, for: indexPath) as! PostsTableViewCell
        let post = posts?[indexPath.section]
        cell.configure(post: post)
        cell.delegate = self
        cell.footerDelegate = self
        cell.gooeyCellDelegate = self
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.red
        cell.selectedBackgroundView = backgroundView
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 60
        }
        return 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let uiview = UIView()
        uiview.backgroundColor = .clear
        return uiview
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return posts?.count ?? 0
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let videoCell = cell as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
            ASVideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
        }
    }

//    func tableView(_ tableView: UITableView,
//                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
//    {
//        let action = UIContextualAction(style: .normal,
//                                        title: "Save") { [weak self] _, _, completionHandler in
//            self?.handleSaveAction(indexPath: indexPath)
//            completionHandler(true)
//        }
//        action.image = UIImage(named: "save")
//        action.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.0)
//        return UISwipeActionsConfiguration(actions: [action])
//    }
//
//    func tableView(_ tableView: UITableView,
//                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
//    {
//        let archive = UIContextualAction(style: .normal,
//                                         title: "Pop") { [weak self] _, _, completionHandler in
//            self?.handlePopAction(indexPath: indexPath)
//            completionHandler(true)
//        }
//        archive.image = UIImage(named: "delete-cross")
//        archive.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.0)
//
//        let configuration = UISwipeActionsConfiguration(actions: [archive])
//
//        return configuration
//    }
//
//    private func handleSaveAction(indexPath: IndexPath) {
//        guard let cell = tableView.cellForRow(at: indexPath) as? PostsTableViewCell else { return }
//        guard let post = cell.post else { return }
//        let savedPosts = SavedPosts.shared.posts
//        let checkSPost = savedPosts?.filter { $0.postId == post.id }
//        if checkSPost?.isEmpty ?? false {
//            showScreenshotEffect(cell)
//            savePost(post: post)
//
//        } else {
//            // savePost(post: post)
//            Toast(text: "Post Saved Already!").show()
//        }
//    }
//
//    private func handlePopAction(indexPath: IndexPath) {
//        guard let cell = tableView.cellForRow(at: indexPath) as? PostsTableViewCell else { return }
//        removeCell(cell)
//    }

    func tableView(_ tableView: UITableView,
                   editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle
    {
        return .none
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if postTimer != nil {
            postTimer?.invalidate()
            postTimer = nil
        }

        if singlePost == nil, selectedPostId == nil {
            if (scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height, !postFetchingInProgress {
                currentPage += 1
                if stopLoading == false {
                    getPostsFromServer()
                }
            } else if scrollView.contentOffset.y < -50, !postFetchingInProgress {
                startFreshLoading()
                getPostsFromServer()
            }
        }
    }

    func startFreshLoading(clearOld: Bool = true, makeAPI: Bool = false) {
        tableView.tableFooterView = nil
        if clearOld == true {
            allPosts.removeAll()
            followingPosts.removeAll()
        }
        currentPage = 1
        if makeAPI {
            getPostsFromServer()
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if singlePost == nil, selectedPostId == nil {
            checkForVisibleCells()
        }
        pausePlayeVideos()
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        checkForVisibleCells()
        if !decelerate {
            pausePlayeVideos()
        }
    }

    func pausePlayeVideos() {
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: tableView)
    }

    @objc func appEnteredFromBackground() {
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: tableView, appEnteredFromBackground: true)
    }

    private func checkForVisibleCells() {
        if postTimer != nil {
            postTimer?.invalidate()
            postTimer = nil
        }
        for item in tableView.indexPathsForVisibleRows! {
            if tableView.bounds.contains(tableView.rectForRow(at: item)) {
                fullyVisibleCell = tableView.cellForRow(at: item) as? PostsTableViewCell
                postTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(postViewed), userInfo: nil, repeats: false)
            }
        }
    }

    @objc func postViewed() {
        guard let post = fullyVisibleCell?.post else { return }
        if fullyVisibleCell?.isAlreadyViewed == true { return }
        if post.alreadyViewed?.boolValue() == true { return }
        updatePostViewed(post: post, cell: fullyVisibleCell!)
        fullyVisibleCell?.enableTreasure()

        treasureTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(backToNormal), userInfo: nil, repeats: false)
    }

    @objc func backToNormal() {
        if treasureTimer != nil {
            fullyVisibleCell?.enableTreasure(enable: false)
            treasureTimer?.invalidate()
            treasureTimer = nil
        }
    }
}

extension PostsViewController: LikeSectionDelegate, ASAutoPlayVideoLayerContainer {
    func shareButtonTapped(image: UIImage?, post: String?) {
        if image != nil {
            let shareItems = [image!, post!] as [Any]

            let avc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)

            present(avc, animated: true)
        } else {
            let shareItems = [post!]

            let avc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)

            present(avc, animated: true)
        }
    }

    func openCommentsTapped(postId: String) {
        tabBarController?.tabBar.isHidden = true
        let homeDetailStoryboard = UIStoryboard(name: "HomeDetailStoryboard", bundle: nil)
        let objAllCommentHome = homeDetailStoryboard.instantiateViewController(withIdentifier: "AllCommentHomeVC") as? AllCommentHomeVC
        objAllCommentHome?.selectedPostId = postId
        navigationController?.pushViewController(objAllCommentHome!, animated: true)
    }

    func likeButtonTapped() {
        getPostsFromServer(silentLoad: true)
    }

    func openLikeButtonTapped(postId: String) {
        let homeDetailStoryboard = UIStoryboard(name: "HomeDetailStoryboard", bundle: nil)
        let objAllLikeUserList = homeDetailStoryboard.instantiateViewController(withIdentifier: "AllLikeUserListVC") as? AllLikeUserListVC
        objAllLikeUserList?.strPostId = postId
        tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(objAllLikeUserList!, animated: true)
    }

    func playVideo() {
        // let player = AVPlayer(url: url)

        let vc = AVPlayerViewController()
        vc.player = ASVideoPlayerController.sharedVideoPlayer.actualPlayer

        present(vc, animated: true) { vc.player?.play() }
    }

    override func viewDidLayoutSubviews() {
        let horizontalMargin: CGFloat = 0
        let width: CGFloat = view.bounds.size.width - horizontalMargin * 2
        let height: CGFloat = view.bounds.size.height - horizontalMargin * 2
        videoLayer.frame = CGRect(x: 0, y: 0, width: width, height: height)

    }

    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = view.convert(imageScrollView.frame, from: imageScrollView)
        guard let videoFrame = videoFrameInParentSuperView,
              let superViewFrame = view.superview?.frame
        else {
            return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.height
    }
}

extension PostsViewController: PostsDelegate {
    func showAlert(message: String) {
        PSUtil.showAlertFromController(controller: self, withMessage: message)
    }

    func moreOptionsTapped(alert: UIAlertController) {
        present(alert, animated: true, completion: {
            print("completion block")
        })
    }

    func treasureTapped(post: Post?) {
        let myViewController = TreasureViewController()
        let bottomSheetViewModel = BRQBottomSheetViewModel(
            cornerRadius: 20,
            animationTransitionDuration: 0.3,
            backgroundColor: UIColor.black.withAlphaComponent(0.5)
        )

        bottomSheetVC = BRQBottomSheetViewController(
            viewModel: bottomSheetViewModel,
            childViewController: myViewController
        )

        myViewController.delegate = self
        presentBottomSheet(bottomSheetVC!, completion: nil)
    }

    func showBounce() {
        let defaults = UserDefaults.standard
        if (defaults.value(forKey: Constants.Bounce.isBounceShown) ?? false) as! Bool == true { return }
        let myViewController = BounceViewController()
        let bottomSheetViewModel = BRQBottomSheetViewModel(
            cornerRadius: 20,
            animationTransitionDuration: 0.3,
            backgroundColor: UIColor.black.withAlphaComponent(0.5)
        )

        bottomSheetVC = BRQBottomSheetViewController(
            viewModel: bottomSheetViewModel,
            childViewController: myViewController
        )

        myViewController.delegate = self
        presentBottomSheet(bottomSheetVC!, completion: nil)
    }

    func imageTapped(post: Post?) {
        let strUserImg = post?.filename ?? ""
        let url = URL(string: strUserImg)
        tabBarController?.tabBar.isHidden = true
        imageDetailView.isHidden = false
        selectedPostType = post?.type
        let image = UIImage(named: "left-arrow")
        backButton.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = UIColor.black
        if post?.type == .video {
            backButton.tintColor = UIColor.white
            sliderHeightConstraint.constant = 70.0
            self.isSelectedVideoMute = ASVideoPlayerController.sharedVideoPlayer.isMute()
            ASVideoPlayerController.sharedVideoPlayer.mute(shouldMute: false)
            videoURL = strUserImg
            videoLayer.backgroundColor = UIColor.black.cgColor
            videoLayer.videoGravity = AVLayerVideoGravity.resizeAspect
            imageScrollView.layer.addSublayer(videoLayer)
            let strFileListImg = post?.filename ?? ""
            if strFileListImg.isValidString() {
                videoURL = strFileListImg
            }
            imageScrollView.backgroundColor = .black
            imageScrollView.setup()
            imageScrollView.imageContentMode = .aspectFit
            imageScrollView.initialOffset = .center
            imageScrollView.display(image: UIImage())

            ASVideoPlayerController.sharedVideoPlayer.playVideo(withLayer: videoLayer, url: strFileListImg)
            configureSlider()
//            playVideo()
//            return
        } else {
            imageScrollView.backgroundColor = .white
            sliderHeightConstraint.constant = 0.0
            videoLayer.removeFromSuperlayer()
            imageScrollView.setup()
            imageScrollView.imageContentMode = .aspectFit
            imageScrollView.initialOffset = .center

            KingfisherManager.shared.retrieveImage(with: url!,
                                                   options: nil,
                                                   progressBlock: nil) { [self] (result) -> Void in
                switch result {
                case .success(let value):
                    imageScrollView.display(image: value.image)
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
        }

        let strUserDesc = post?.dDescription
        let strProfilePhoto = post?.user?.profilePhoto

        nameLabel.text = post?.user?.fullName()

        descriptionLabel.text = strUserDesc

        if strProfilePhoto == "" || strProfilePhoto == "nil" {
            profileImage.sd_setImage(with: URL(string: "img9"), placeholderImage: UIImage(named: "placeholder.png"))
        } else {
            profileImage.kf.setImage(with: URL(string: strProfilePhoto!), placeholder: UIImage(named: "placeholder.png"))
        }
        dateLabel.text = post?.createdAt?.dateWithoutTimeComponent()
    }

    func profileImageTapped(post: Post?) {
        let otherStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
        objMyProfile?.selectedUserId = "\(post?.userID ?? 0)"
        navigationController?.pushViewController(objMyProfile!, animated: true)
    }

    @IBAction func backBtnAction(_ sender: Any) {
        imageDetailView.isHidden = true // removeFromSuperview()
        tabBarController?.tabBar.isHidden = false
        if selectedPostType == .video {
            ASVideoPlayerController.sharedVideoPlayer.playPauseVideo(forcePlay: true)
            guard let periodicObserver = periodicObserver else { return }
            player?.removeTimeObserver(periodicObserver)
            ASVideoPlayerController.sharedVideoPlayer.mute(shouldMute: isSelectedVideoMute)
        }
    }

    func postOption(post: Post, option: OptionType) {
        switch option {
        case .delete:
            deletePost(post: post)
        case .edit:
            break
        case .share:
            sharePost(post: post)
        case .turnComment:
            turnOffComments(post: post)
        case .follow:
            followPost(post: post)
        case .report:
            reportPost(post: post)
        case .block:
            blockUser(user: post.user)
        }
    }
}

extension PostsViewController: TreasureVCDelegate, BounceVCDelegate {
    func dismissVC() {
        bottomSheetVC?.dismissViewController()
        let accountStoryboard = UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objTotalCoinList = accountStoryboard.instantiateViewController(withIdentifier: "TotalCoinListVC") as? TotalCoinListVC
        navigationController?.pushViewController(objTotalCoinList!, animated: true)
    }

    func dismissBounceVC(withAction action: Bool) {
        bottomSheetVC?.dismissViewController()
        if action {}
    }
}

extension PostsViewController: GooeyCellDelegate {
    func gooeyCellActionConfig(for cell: UITableViewCell, direction: GooeyEffect.Direction) -> GooeyEffectTableViewCell.ActionConfig? {
        var color = #colorLiteral(red: 0.4470588235, green: 0.5725490196, blue: 1, alpha: 1)
        color = .clear
        var image = direction == .toLeft ? #imageLiteral(resourceName: "image_cross") : #imageLiteral(resourceName: "image_mark")
        image = UIImage()
        let isCellDeletingAction = direction == .toLeft

        var tempImage = UIImage(named: "postPlaceholder")
        guard let cell = cell as? PostsTableViewCell else {
            let effectConfig = GooeyEffect.Config(color: color, image: tempImage)
            let actionConfig = GooeyEffectTableViewCell.ActionConfig(effectConfig: effectConfig,
                                                                     isCellDeletingAction: isCellDeletingAction)
            return actionConfig
            
        }
        
        if cell.post?.type == .video {
            
            tempImage = UIImage(named: "blankPost")
            
        } else {
            tempImage = cell.contentView.makeScreenshot()
        }
        
        
        let effectConfig = GooeyEffect.Config(color: color, image: tempImage)

        let actionConfig = GooeyEffectTableViewCell.ActionConfig(effectConfig: effectConfig,
                                                                 isCellDeletingAction: isCellDeletingAction)
        return actionConfig
    }

    func gooeyCellActionTriggered(for cell: UITableViewCell, direction: GooeyEffect.Direction) {
        guard let postWalaCell = cell as? PostsTableViewCell else { return }
        guard let post = postWalaCell.post else { return }
        switch direction {
        case .toLeft:
            removeCell(cell)
        case .toRight:
            let savedPosts = SavedPosts.shared.posts
            let checkSPost = savedPosts?.filter { $0.postId == post.id }
            if checkSPost?.isEmpty ?? false {
                showScreenshotEffect(cell)
                savePost(post: post)

            } else {
                // savePost(post: post)
                Toast(text: "Post Saved Already!").show()
            }
        }
    }

    private func removeCell(_ cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        // cell.transform = CGAffineTransform(scaleX: -1, y: 1)

        cell.contentView.explode()
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            cell.contentView.isHidden = false
            cell.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.tableView.beginUpdates()
            self.posts?.remove(at: indexPath.section)
            self.tableView.deleteSections([indexPath.section], with: .none)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.tableView.endUpdates()
            }
        }
    }

    private func showScreenshotEffect(_ cell: UITableViewCell) {
        guard let cell = cell as? PostsTableViewCell else { return }
        var imgViewTemp = UIImageView(image: UIImage(named: "postPlaceholder"))
        if cell.post?.type == .video {
            imgViewTemp = UIImageView(image: UIImage(named: "blankPost"))
        } else {
            imgViewTemp = UIImageView(image: cell.contentView.makeScreenshot())
        }
        animation(tempView: imgViewTemp)
    }

    func animation(tempView: UIView) {
        tempView.center = view.center
        guard let window = UIApplication.shared.keyWindow else { return }
        window.addSubview(tempView)
        UIView.animate(withDuration: 0.5,
                       animations: {
                           tempView.animationZoom(scaleX: 1.5, y: 1.5)
                       }, completion: { _ in
                           tempView.animationZoom(scaleX: 1.0, y: 1.0)

                           UIView.animate(withDuration: 0.5, animations: {
                               tempView.animationZoom(scaleX: 0.2, y: 0.2)
                               tempView.animationRoted(angle: CGFloat(Double.pi))

                               tempView.frame.origin.x = window.frame.size.width - 100
                               tempView.frame.origin.y = window.frame.size.height - 140

                           }, completion: { _ in
                               tempView.removeFromSuperview()
                           })
                       })
    }
}

extension UIView {
    func animationZoom(scaleX: CGFloat, y: CGFloat) {
        transform = CGAffineTransform(scaleX: scaleX, y: y)
    }

    func animationRoted(angle: CGFloat) {
        transform = transform.rotated(by: angle)
    }
}

extension UIView {
    func makeScreenshot() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { context in
            self.layer.render(in: context.cgContext)
        }
    }
}

extension PostsViewController {
    func singleTappedNotification() {
        playPauseTapped(playPauseButton)
    }
}
