//
//  LikeCollectionViewCell.swift
//  bloook
//
//  Created by Tech Astha on 03/02/21.
//

import UIKit

class LikeCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = String(describing: LikeCollectionViewCell.self)
   
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var likeImg: UIImageView!
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var likeCountBtn: UIButton!
    
    @IBOutlet weak var allTimeView: UIView!
    @IBOutlet weak var allTimeBtn: UIButton!
    @IBOutlet weak var allLifeTimeBtn: UIButton!
    func setup(with views: Int) {
        viewsLabel.text = "\(views)"

    }
    override func awakeFromNib() {
        super.awakeFromNib()
       // likeButton.addTarget(self, action: #selector(likeBtnAction(_:)), for: UIControl.Event.touchUpInside)
     //   commentButton.addTarget(self, action: #selector(commentBtnAction(_:)), for: UIControl.Event.touchUpInside)
        
        
//        likeView.layer.cornerRadius = 18
//        likeView.layer.borderWidth = 1
//        likeView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1).cgColor
//
//        commentButton.layer.cornerRadius = 18
//        commentButton.layer.borderWidth = 1
//        commentButton.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1).cgColor
//
//        shareButton.layer.cornerRadius = 18
//        shareButton.layer.borderWidth = 1
//        shareButton.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1).cgColor
    }
    


}
