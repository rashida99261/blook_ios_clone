//
//  MyProfileVC.swift
//  bloook
//
//  Created by Tech Astha on 16/02/21.
//

import AVKit
import Kingfisher
import Pulsator
import UIKit
import Gifu

class MyProfileVC: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UINavigationControllerDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet var roundView: UIView!
    @IBOutlet var postCollectionView: UICollectionView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var profileImg: UIImageView!
    @IBOutlet var postCountLbl: UILabel!
    @IBOutlet var viewsCountLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var followerCountLbl: UILabel!
    var selectedUserId: String?
    var playerLayer = AVPlayerLayer()
    @IBOutlet var onFireView: UIView!
    @IBOutlet var otherProfileView: UIView!
    @IBOutlet var cancelButtonView: UIView!
    @IBOutlet var followButton: UIButton!
    @IBOutlet var statisticsView: UIView!
    @IBOutlet var hamburgerRightView: UIView!
    
    @IBOutlet var onFireGif: GIFImageView!

    @IBOutlet var galaryMenuImage: UIImageView!
    @IBOutlet var listMenuImage: UIImageView!
    var pulsator: Pulsator?
    var user: User?
    var allPosts: [Post]?
    var posts: [Post]?
    var profileImagePosts: [Post]?
    var profileBlogPosts: [Post]?
    var isSavedOn: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        

        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"

        if self.selectedUserId != nil {
            self.cancelButtonView.isHidden = false
            self.otherProfileView.isHidden = strUserId == self.selectedUserId
            self.statisticsView.isHidden = strUserId != self.selectedUserId
            self.hamburgerRightView.isHidden = strUserId != self.selectedUserId
            self.getUserProfileFromServer()
        } else {
            self.user = LoggedInUser.shared.user
            self.fillUserData()
        }
        if self.pulsator == nil {
            self.pulsator = Pulsator()
        }
        self.tableView.isHidden = true
        self.tableView.register(UINib(nibName: BlogTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: BlogTableViewCell.reuseIdentifier)
        
        guard let url = Bundle.main.url(forResource: "Temp", withExtension: "gif") else {return}
        self.onFireGif.animate(withGIFURL: url)
//        #if DEBUG
//
//        profileImg.startPulse(pulsator: pulsator!)
//        #endif
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
//        user =  LoggedInUser.shared.user
//        fillUserData()
        updateTable()
        self.setBounce()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        if self.selectedUserId == nil {
//            self.user = LoggedInUser.shared.user
//            self.fillUserData()
//        }
    }

    override func viewDidLayoutSubviews() {
        self.roundView.layer.masksToBounds = true
        self.roundView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }

    @IBAction func gallaryTapped() {
        self.postCollectionView.isHidden = false
        self.tableView.isHidden = true
        let templateImage = self.galaryMenuImage.image?.withRenderingMode(.alwaysTemplate)
        self.galaryMenuImage.image = templateImage
        self.galaryMenuImage.tintColor = UIColor.black

        let templateImage2 = self.listMenuImage.image?.withRenderingMode(.alwaysTemplate)
        self.listMenuImage.image = templateImage2
        self.listMenuImage.tintColor = UIColor.gray
    }

    @IBAction func listTapped() {
        self.postCollectionView.isHidden = true
        self.tableView.isHidden = false

        let templateImage = self.galaryMenuImage.image?.withRenderingMode(.alwaysTemplate)
        self.galaryMenuImage.image = templateImage
        self.galaryMenuImage.tintColor = UIColor.gray

        let templateImage2 = self.listMenuImage.image?.withRenderingMode(.alwaysTemplate)
        self.listMenuImage.image = templateImage2
        self.listMenuImage.tintColor = UIColor.black
    }

    // MARK: - for Profile Data

    func getUserProfileFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_PROFILE_ID: self.selectedUserId ?? strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]
        self.allPosts?.removeAll()
        self.posts?.removeAll()
        self.profileImagePosts?.removeAll()
        self.profileBlogPosts?.removeAll()

        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.getUserProfile(param, completion: { response, error in
            self.hideProgressView()
            if error == nil {
                if response?.isValid() == true {
                    guard let user = response?.user else { return }
                    self.user = user
                    self.fillUserData()

                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }

    private func fillUserData() {
        guard let user = self.user else {
            self.getUserProfileFromServer()
            return
        }
        DispatchQueue.main.async {
            self.titleLbl.text = user.titleText()
            self.nameLbl.text = user.fullName()
            let strUserProfileImg = user.profilePhoto
            self.postCountLbl.text = "\(user.postsCount ?? 0)"
            self.followerCountLbl.text = "\(user.followersCount ?? 0)"
            if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
                self.profileImg.image = UIImage(named: "user")
            } else {
                self.profileImg.kf.setImage(with: user.profilePhotoURL(), placeholder: UIImage(named: "placeholder.png"))
            }

            self.setBounce()
            self.allPosts = user.posts?.filter { $0.type?.rawValue == "image" || $0.type?.rawValue == "video" }
            self.posts = user.posts?.filter { $0.type?.rawValue == "blog" }
            self.postCollectionView.reloadData()
            self.tableView.reloadData()
            guard let followers = user.followers else { return }
            let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            let results = followers.filter { $0.fromUserID?.userId.stringValue() == strUserId }
            if !results.isEmpty {
                self.followButton.setTitle("Unfollow", for: .normal)
            }
        }
    }

    func doFollowUserFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: self.selectedUserId ?? strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doFollowUser(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let message = response[Constants.KEY_MESSAGE] as? String
                    if message == "User unfollowed successfully" {
                        self.followButton.setTitle("Follow", for: .normal)
                    } else {
                        self.followButton.setTitle("Unfollow", for: .normal)
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    guard let msg = response[Constants.KEY_MESSAGE] as? String else {
                        PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
                        return
                    }
                    PSUtil.showAlertFromController(controller: self, withMessage: msg)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }

    private func setBounce() {
        
        onFireGif.animate(withGIFNamed: "onFire", animationBlock:  {
        })
        
        self.onFireView.isHidden = self.user?.bounceStatus?.boolValue() == false
        
        if self.user?.bounceStatus?.boolValue() == true {
            if self.pulsator == nil {
                self.pulsator = Pulsator()
            }
            self.profileImg.startPulse(pulsator: self.pulsator!)
        }
    }

    @IBAction func editAccountBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        let accountStoryboard = UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objEditProfile = accountStoryboard.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC
        self.navigationController?.pushViewController(objEditProfile!, animated: true)
    }

    @IBAction func savedPostTapped(_ sender: Any) {
        guard let button = sender as? UIButton else { return }
        self.isSavedOn = !self.isSavedOn
        button.backgroundColor = self.isSavedOn ? #colorLiteral(red: 0.4470588235, green: 0.5725490196, blue: 1, alpha: 1) : .clear
        button.setTitleColor(self.isSavedOn ? .white : .black, for: .normal)
        updateTable()
    }

    private func updateTable() {
        if self.isSavedOn {
            let savedPosts = SavedPosts.shared.posts
            if let allSavedPosts = savedPosts?.filter { $0.post?.type?.rawValue == "image" || $0.post?.type?.rawValue == "video" } {
                self.allPosts?.removeAll()
                for i in allSavedPosts {
                    if let post = i.post {
                        allPosts?.append(post)
                    }
                }
            }

            if let allSavedBlogs = savedPosts?.filter { $0.post?.type?.rawValue == "blog" } {
                self.posts?.removeAll()

                for i in allSavedBlogs {
                    if let post = i.post {
                        posts?.append(post)
                    }
                }
            }

        } else {
            self.allPosts = self.user?.posts?.filter { $0.type?.rawValue == "image" || $0.type?.rawValue == "video" }
            self.posts = self.user?.posts?.filter { $0.type?.rawValue == "blog" }
        }

        self.postCollectionView.reloadData()
        self.tableView.reloadData()
    }
    @IBAction func followBtnAction(_ sender: Any) {
        self.doFollowUserFromServer()
    }

    @IBAction func messageButtonTapped(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        let msgStoryboard = UIStoryboard(name: "MessageStoryboard", bundle: nil)
        let objChatList = msgStoryboard.instantiateViewController(withIdentifier: "ChatListVC") as! ChatListVC
        objChatList.selectedUser = self.user
        self.navigationController?.pushViewController(objChatList, animated: true)
    }

    @IBAction func accountSettingBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        let accountStoryboard = UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objAccountSettings = accountStoryboard.instantiateViewController(withIdentifier: "AccountSettingsVC") as? AccountSettingsVC
        self.navigationController?.pushViewController(objAccountSettings!, animated: true)
    }

    @IBAction func followListBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        let MyFollowStoryboard = UIStoryboard(name: "MyFollowStoryboard", bundle: nil)
        let objMyFollowList = MyFollowStoryboard.instantiateViewController(withIdentifier: "MyFollowListVC") as? MyFollowListVC
        self.navigationController?.pushViewController(objMyFollowList!, animated: true)
    }

    @IBAction func statisticsBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        let myStatisticsStoryboard = UIStoryboard(name: "StatisticsStoryboard", bundle: nil)
        let objMyStatistics = myStatisticsStoryboard.instantiateViewController(withIdentifier: "StatisticsVC") as? StatisticsVC
        objMyStatistics?.user = self.user
        self.navigationController?.pushViewController(objMyStatistics!, animated: true)
    }

    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.allPosts?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let post = allPosts?[indexPath.row] else { return UICollectionViewCell() }

        if post.type == .video {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCell", for: indexPath as IndexPath) as! VideoCollectionViewCell
            cell.configure(post: post)

            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath as IndexPath) as! imageCollectionViewCell
            let strFileListImg = post.thumbnail ?? ""
            if strFileListImg.isValidString() {
                cell.postImg.kf.setImage(with: URL(string: strFileListImg), placeholder: UIImage(named: "placeholder.png"))
            }
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let post = allPosts?[indexPath.row] else { return CGSize(width: 0, height: 0) }

        if post.type == .video {
            let itemsPerRow: CGFloat = 3
            let hardCodedPadding: CGFloat = 2
            let itemWidth = (self.postCollectionView.bounds.width / itemsPerRow) - hardCodedPadding
            return CGSize(width: itemWidth, height: itemWidth)
        } else {
            let itemsPerRow: CGFloat = 3
            let hardCodedPadding: CGFloat = 2
            let itemWidth = (self.postCollectionView.bounds.width / itemsPerRow) - hardCodedPadding
            return CGSize(width: itemWidth, height: itemWidth)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var post = self.allPosts?[indexPath.row]
        let otherStoryboard = UIStoryboard(name: "Posts", bundle: nil)
        let objTopPhoto = otherStoryboard.instantiateViewController(withIdentifier: "PostsViewController") as? PostsViewController
        if post?.user == nil {
            post?.user = self.user
        }
        objTopPhoto?.singlePost = post
        self.navigationController?.pushViewController(objTopPhoto!, animated: true)
    }

    public func imageFromVideo(url: URL, at time: TimeInterval, completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global(qos: .background).async {
            let asset = AVURLAsset(url: url)

            let assetIG = AVAssetImageGenerator(asset: asset)
            assetIG.appliesPreferredTrackTransform = true
            assetIG.apertureMode = AVAssetImageGenerator.ApertureMode.encodedPixels

            let cmTime = CMTime(seconds: time, preferredTimescale: 60)
            let thumbnailImageRef: CGImage
            do {
                thumbnailImageRef = try assetIG.copyCGImage(at: cmTime, actualTime: nil)
            } catch {
                print("Error: \(error)")
                return completion(nil)
            }

            DispatchQueue.main.async {
                completion(UIImage(cgImage: thumbnailImageRef))
            }
        }
    }

    @objc func playeVideo(overTheView sender: UIButton?) {
        guard let photoView = sender?.superview as? VideoCollectionViewCell else { return }
        // photoView.playVideo.isHidden = true
        guard let url = photoView.videoURL else { return }
        var currentAsset: AVAsset?
        currentAsset = AVAsset(url: url)
        let playerItem = AVPlayerItem(asset: currentAsset!)
        let player = AVPlayer(playerItem: playerItem)
        // 3.Create AVPlayerLayer object
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = photoView.postVideoImg.bounds // bounds of the view in which AVPlayer should be displayed
        self.playerLayer.videoGravity = .resizeAspect
        photoView.postVideoImg.image = nil
        photoView.postVideoImg.layer.addSublayer(self.playerLayer)

        // 5. Play Video
        player.play()
        sender?.isHidden = true
        // photoView.playVideo.isHidden = false
    }
}

extension MyProfileVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BlogTableViewCell.reuseIdentifier, for: indexPath) as! BlogTableViewCell
        var post = self.posts?[indexPath.section]
        post?.user = self.user
        cell.configure(post: post)
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let uiview = UIView()
        uiview.backgroundColor = .clear
        return uiview
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.posts?.count ?? 0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = self.posts?[indexPath.row]
        let otherStoryboard = UIStoryboard(name: "Posts", bundle: nil)
        let objTopPhoto = otherStoryboard.instantiateViewController(withIdentifier: "PostsViewController") as? PostsViewController
        objTopPhoto?.singlePost = post
        self.navigationController?.pushViewController(objTopPhoto!, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MyProfileVC: BlogTableViewDelegate {
    func profileImageTapped(post: Post?) {
        let otherStoryboard = UIStoryboard(name: "Posts", bundle: nil)
        let objTopPhoto = otherStoryboard.instantiateViewController(withIdentifier: "PostsViewController") as? PostsViewController
        objTopPhoto?.singlePost = post
        self.navigationController?.pushViewController(objTopPhoto!, animated: true)
    }
}
