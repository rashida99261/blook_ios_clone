//
//  LoginVC.swift
//  blook
//
//  Created by Tech Astha on 24/12/20.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AuthenticationServices
import FirebaseMessaging

import SwiftKeychainWrapper
class LoginVC: UIViewController {
    var strFbToken = String()
    var strToken = String()
    var strProviderType = String()
    var strFirstName = String()
    var strLastName = String()
    var strEmail = String()
    var strAppleId = String()
    var strAppleEmail = String()
   
    @IBOutlet weak var topStatusView: UIView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var appleLoginStackView: UIStackView!
    @IBOutlet weak var versionNumberLabel: UILabel!
    
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
//    @IBOutlet weak var emailField: MDCOutlinedTextField!
//    @IBOutlet weak var passwordField: MDCOutlinedTextField!
//    var emailController = MDCOutlinedTextField()
    //var progressView : AcuantProgressView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        emailField.setOutlineColor(Constants.colorConstants.CLR_BLUE_THEME, for: .editing)
//        passwordField.setOutlineColor(Constants.colorConstants.CLR_BLUE_THEME, for: .editing)
//        emailField.label.text = "Email / Phone / Username"
//        passwordField.label.text = "Password"
        #if DEBUG
        emailField.text = "test@blook.com"
        passwordField.text = "123456"
        #endif
        if #available(iOS 13.0, *) {
            setupAppleProviderLoginView()
        }
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        versionNumberLabel.text = "v \(version ?? "0")(\(build ?? "0"))"
        self.viewLoader.isHidden = true
    }
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    @IBAction func continueBtnAction(_ sender: Any) {
        if (emailField.text?.count)! < 1 {
            
            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter Email / Username")
            return;
            
       }
//        else if (Constants.isValidEmail(testStr:emailField.text!) == false){
//
//            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter valid email.")
//            return;
//
//        }
       else if (passwordField.text?.count)! < 1 {
            
            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter password.")
            return;
            
        }else{
            
//            let objProfilePic = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePicView") as! ProfilePicView
//            self.navigationController?.pushViewController(objProfilePic, animated: true)
            
//            let otherStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
//            let objTabView = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as! TabView
//            self.navigationController?.pushViewController(objTabView, animated: true)
            
            doLoing()
        }
    }
    
    func doLoing() {
    
        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        LoggedInUser.shared.user = nil
        let defult =  UserDefaults.standard
        let strDeviceToken = "\(defult .object(forKey: Constants.DEVICETOKEN) ?? "")"
        let strFBDeviceToken = "\(defult .object(forKey: Constants.FIREBASEDEVICETOKEN) ?? "")"
        defult.setValue(false, forKey: Constants.Bounce.isBounceShown)
        defult.setValue(false, forKey: Constants.Bounce.isBounceOn)
        defult.setValue(nil, forKey: Constants.Bounce.bounceUserId)
        let param = [Constants.KEY_USERNAME :emailField.text!,
                     Constants.KEY_PASSWORD :passwordField.text!,
                     "firebase_token": strFBDeviceToken,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken)",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]
        
       // PSUtil.showProgressHUD()
        print(param)
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doLogin(param , completion: { (response) in
            PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict =  response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    var strToken =  "\(dict.value(forKey: "access_token") ?? "")"
                    let strUserId = "\(dict.value(forKey: Constants.KEY_ID) ?? "")"
                    let strFirstName = "\(dict.value(forKey: Constants.KEY_FIRSTNAME) ?? "")"
                    let strLastName = "\(dict.value(forKey: Constants.KEY_LASTNAME) ?? "")"
                    let strUserName = strFirstName + " " + strLastName
                    strToken = "Bearer " + strToken
                    let strProfilePhoto = "\(dict.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
                    UserDefaults.standard.set(strProfilePhoto, forKey: Constants.KEY_PROFILE_PHOTO)
                    UserDefaults.standard.set(strUserName, forKey: "ForUserName")
                    UserDefaults.standard.set(strToken, forKey: "access_token")
                    UserDefaults.standard.set(strUserId, forKey: Constants.KEY_ID)
                    UserDefaults.standard.set(self.passwordField.text, forKey: "forPassword")
                    UserDefaults.standard.set(true, forKey: Constants.ISLOGIN)
//
//                    let objProfilePic = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePicView") as! ProfilePicView
//                    self.navigationController?.pushViewController(objProfilePic, animated: true)
                    
                    let otherStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
                    let objTabBar = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as? TabView
                    self.navigationController?.pushViewController(objTabBar!, animated: true)
                    
                 }else{
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    if msg == nil {
                        
                    }else{
                      PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    @IBAction func signUpBtnAction(_ sender: Any) {
        let objUserNameRegister = self.storyboard?.instantiateViewController(withIdentifier: "UserNameRegisterVC") as? UserNameRegisterVC
        self.navigationController?.pushViewController(objUserNameRegister!, animated: true)
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        let objViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
        self.navigationController?.pushViewController(objViewController!, animated: false)
       // self.navigationController?.popViewController(animated: true)
    }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
            //self.progressView.startAnimation()
            //self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
    private func hideProgressView(){
        DispatchQueue.main.async {
           // self.progressView.stopAnimation()
            //self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
    
    //MARK: - Apple Login Methods
    /// - Tag: add_appleid_button
    func setupAppleProviderLoginView() {
        strProviderType = "Apple"
        if #available(iOS 13.0, *) {
            let authorizationButton = ASAuthorizationAppleIDButton()
            authorizationButton.addTarget(self, action: #selector(handleAuthorizationAppleIDButtonPress), for: .touchUpInside)
            self.appleLoginStackView.addArrangedSubview(authorizationButton)
        }
    }
    /// - Tag: perform_appleid_request
    @objc
    func handleAuthorizationAppleIDButtonPress() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
    }
    
    // - Tag: perform_appleid_password_request
    /// Prompts the user if an existing iCloud Keychain credential or Apple ID credential is found.
    func performExistingAccountSetupFlows() {
        if #available(iOS 13.0, *) {
            // Prepare requests for both Apple ID and password providers.
            let requests = [ASAuthorizationAppleIDProvider().createRequest(),
                            ASAuthorizationPasswordProvider().createRequest()]
            
            // Create an authorization controller with the given requests.
            let authorizationController = ASAuthorizationController(authorizationRequests: requests)
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
    }
    
    @IBAction func facebookBtnAction(_ sender: Any) {
        strProviderType = "Facebook"
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email", "public_profile"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                print(fbloginresult)
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            strFbToken = AccessToken.current!.tokenString
            self.strToken = strFbToken as String
           // AccessToken.current?.t
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, birthday, gender"]).start(completion: { (connection, result, error) -> Void in
                if (error == nil){
                    
                }
                let dictionary = result as! [String: AnyObject]
                print(dictionary)
                let loginEmailStr = dictionary["email"] as! NSString
                self.strEmail = loginEmailStr as String
                self.strFirstName = dictionary["first_name"] as! String
                self.strLastName = dictionary["last_name"] as! String
                
                print(loginEmailStr)
                if let imageURL = ((dictionary["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                    let imageProfStr = "\(imageURL)" as NSString
                    // let self.imageProfStr = "\(imageURL)" as NSString
                    print(imageProfStr)
                }
                /// self.loginPassStr = ""
                self.doSocialLoginFromServer()
              }
            )
          }
     }
    //MARK:- Social Login
    func doSocialLoginFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let param = [Constants.KEY_FIRSTNAME:self.strFirstName,
                     Constants.KEY_LASTNAME:self.strLastName,
                     Constants.KEY_EMAIL:strEmail,
                     Constants.KEY_PROVIDER_TYPE:strProviderType,
                     Constants.KEY_PROVIDER_TOKEN:strToken,
                     Constants.KEY_DEVICE_ID : strDeviceToken ?? "",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

      //  PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doSocialLogin(param , completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    
                    let dict =  response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    var strToken =  "\(dict.value(forKey: "access_token") ?? "")"
                    let strUserId = "\(dict.value(forKey: Constants.KEY_ID) ?? "")"
                    let strProfilePhoto = "\(dict.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
                    strToken = "Bearer " + strToken
                    UserDefaults.standard.set(strToken, forKey: "access_token")
                    UserDefaults.standard.set(strUserId, forKey: Constants.KEY_ID)
                    //UserDefaults.standard.set(self.passwordField.text, forKey: "forPassword")
                    UserDefaults.standard.set(true, forKey: Constants.ISLOGIN)
                    let strFirstName = "\(dict.value(forKey: Constants.KEY_FIRSTNAME) ?? "")"
                    let strLastName = "\(dict.value(forKey: Constants.KEY_LASTNAME) ?? "")"
                    let strUserName = strFirstName + " " + strLastName
                    UserDefaults.standard.set(strProfilePhoto, forKey: Constants.KEY_PROFILE_PHOTO)
                    UserDefaults.standard.set(strUserName, forKey: "ForUserName")
                    let otherStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
                    let objTabBar = otherStoryboard.instantiateViewController(withIdentifier: "TabView") as? TabView
                    self.navigationController?.pushViewController(objTabBar!, animated: true)
                    
                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    @IBAction func forgotPasswordBtnAction(_ sender: Any) {
        let objForgotPassword = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as? ForgotPasswordVC
        self.navigationController?.pushViewController(objForgotPassword!, animated: true)
    }
}
@available(iOS 13.0, *)
extension LoginVC :ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            let userIdentifier = appleIDCredential.user
            // Create an account in your system.
            self.saveUserInKeychain(userIdentifier)
            
        // Create an account in your system.
            var first_name = "\(appleIDCredential.fullName?.givenName ?? "")"
            var last_name = "\(appleIDCredential.fullName?.familyName ?? "")"
            strAppleEmail = "\(appleIDCredential.email ?? "")"
            if let tokenData = appleIDCredential.identityToken {
                strAppleId = String(data: tokenData, encoding: .utf8) ?? ""
            }
           
            if first_name != "" {
                
                KeychainWrapper.standard.set(first_name, forKey: Constants.KEY_FIRSTNAME)
                KeychainWrapper.standard.set(last_name, forKey: Constants.KEY_LASTNAME)
                KeychainWrapper.standard.set(strAppleEmail, forKey: Constants.KEY_EMAIL)
                
            }else{
                
                first_name = KeychainWrapper.standard.string(forKey: Constants.KEY_FIRSTNAME) ?? ""
                last_name =  KeychainWrapper.standard.string(forKey: Constants.KEY_LASTNAME) ?? ""
                strAppleEmail = KeychainWrapper.standard.string(forKey: Constants.KEY_EMAIL) ?? ""
            }
            
            print(strAppleId)
            print(strAppleEmail)
            strToken = strAppleId
            strEmail = strAppleEmail
            strFirstName = first_name
            strLastName = last_name
           // PSUtil.showAlertFromController(controller: self, withMessage: "This is apple Idwhere UserID is \(userIdentifier) and fullName is \(fullName) and email is \(strAppleId)")
           self.doSocialLoginFromServer()
            
        case let passwordCredential as ASPasswordCredential:
        // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            PSUtil.showAlertFromController(controller: self, withMessage: "This is password and username is \(username) and password is \(password)")
            
          default:
            break
        }
        
    }
    private func saveUserInKeychain(_ userIdentifier: String) {
        do {
            try KeychainItem(service: "com.AbsalonUser.App", account: "userIdentifier").saveItem(userIdentifier)
        } catch {
            print("Unable to save userIdentifier to keychain.")
        }
    }
    func showResultViewController(userIdentifier: String, fullName: PersonNameComponents?, email: String?) {
             
                 
    }
  
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        
    }
}
@available(iOS 13.0, *)
extension LoginVC: ASAuthorizationControllerPresentationContextProviding {
    /// - Tag: provide_presentation_anchor
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
