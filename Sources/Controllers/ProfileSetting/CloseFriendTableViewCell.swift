//
//  CloseFriendTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 19/02/21.
//

import UIKit

class CloseFriendTableViewCell: UITableViewCell {

    @IBOutlet weak var AddFriend: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var addView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
