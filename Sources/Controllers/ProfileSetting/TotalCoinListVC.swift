//
//  TotalCoinListVC.swift
//  bloook
//
//  Created by Tech Astha on 24/03/21.
//

import UIKit

class TotalCoinListVC: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var coinCollectionView: UICollectionView!
    @IBOutlet weak var dailyBtn: UIButton!
    @IBOutlet weak var annualBtn: UIButton!
    @IBOutlet weak var lifetimeBtn: UIButton!
    @IBOutlet weak var roundView: UIView!
    var coinArray: [Coin]?
    var type = "daily"
    override func viewDidLoad() {
        super.viewDidLoad()
        coinCollectionView.showsHorizontalScrollIndicator = false
        coinCollectionView.showsVerticalScrollIndicator = false
        dailyBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        getCoinsFromServer()
    }
    override func viewDidLayoutSubviews() {
        roundView.layer.masksToBounds = true
        roundView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    @IBAction func dailyBtnAction(_ sender: Any) {
        self.dailyBtn.setTitleColor(UIColor.black, for: .normal)
        self.annualBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        type = "daily"
        getCoinsFromServer()
    }
    @IBAction func annualBtnAction(_ sender: Any) {
        self.dailyBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.annualBtn.setTitleColor(UIColor.black, for: .normal)
        self.lifetimeBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        type = "annual"
        getCoinsFromServer()
    }
    @IBAction func lifeTimeBtnAction(_ sender: Any) {
        self.dailyBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.annualBtn.setTitleColor(Constants.colorConstants.CLR_DARKGRAY_THEME, for: .normal)
        self.lifetimeBtn.setTitleColor(UIColor.black, for: .normal)
        type = "lifetime"
        getCoinsFromServer()
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return coinArray?.count ?? 0
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       // let dict = self.pendingArr[indexPath.row] as? NSDictionary ?? NSDictionary()
        guard let coin = coinArray?[indexPath.row] else { return UICollectionViewCell() }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: coin.type == .platinum ? "Cell" : "DiamondCell", for: indexPath as IndexPath) as! TotalCoinCollectionViewCell
        cell.configure(coin: coin)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10.0, left: 0.0, bottom: 10.0, right: 0.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 2
        let hardCodedPadding: CGFloat = 10
        let itemWidth = (self.coinCollectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = (self.coinCollectionView.bounds.height / itemsPerRow) - hardCodedPadding
        return CGSize(width: itemWidth, height: 200)
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension TotalCoinListVC {
    func getCoinsFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_PAGE: "200",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I",
                     "type" : type] as [String: Any]

        //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.getCoinList(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.coinArray = response?.coins
                    
                    DispatchQueue.main.async {
                        self.coinCollectionView.reloadData()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
}
