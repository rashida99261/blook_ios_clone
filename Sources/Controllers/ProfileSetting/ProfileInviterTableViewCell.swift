//
//  ProfileInviterTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 23/03/21.
//

import UIKit

class ProfileInviterTableViewCell: UITableViewCell {
    @IBOutlet weak var contactUserNameLbl: UILabel!
    @IBOutlet weak var phoneNoLbl: UILabel!
    @IBOutlet weak var selectInviteBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectInviteBtn.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
