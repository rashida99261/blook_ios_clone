//
//  EditProfileVC.swift
//  bloook
//
//  Created by Tech Astha on 16/02/21.
//

import UIKit
import Kingfisher

class EditProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var roundView: UIView!
   // var progressView : AcuantProgressView!
    var imagePicker = UIImagePickerController()
    var imageDataOne = Data()
    @IBOutlet weak var profilePicImg: UIImageView!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var titleField: UITextField!
    
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true

        imagePicker.delegate = self
        doFollowUserListFromServer()
    }
    override func viewDidLayoutSubviews() {
        roundView.layer.masksToBounds = true
        roundView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    //MARK:- for Profile Data
    func doFollowUserListFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_PROFILE_ID:strUserId,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

       //PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doProfileUser(param, completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    self.firstNameField.text = "\(dict.value(forKey: Constants.KEY_FIRSTNAME) ?? "")"
                    //self.lastNameField.text = "\(dict.value(forKey: Constants.KEY_LASTNAME) ?? "")"
                    let strUserProfileImg = "\(dict.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
                    var uname = "\(dict.value(forKey: Constants.KEY_USERNAME) ?? "")"
                    if uname == "<null>" {
                        uname = ""
                    }
                    
                    self.userNameField.text = "\(uname)"
                    self.emailField.text = "\(dict.value(forKey: Constants.KEY_EMAIL) ?? "")"
                    var rmobile = "\(dict.value(forKey: Constants.KEY_MOBILE) ?? "")"
                    if rmobile == "<null>" {
                        rmobile = ""
                    }
                                         
                    self.phoneField.text = "\(rmobile)"
                    self.titleField.text = "\(dict.value(forKey: Constants.KEY_TITLE) ?? "")"
                    if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
                        self.profilePicImg.image = UIImage (named: "user")
                    }else{
                        self.profilePicImg.sd_setImage(with: URL(string: strUserProfileImg), placeholderImage: UIImage(named: "placeholder.png"))
                    }
                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    @IBAction func changeProfilePicBtnAction(_ sender: Any) {
        self .openImagePickerController()
    }
    func openImagePickerController() {
        
        let alrt = UIAlertController(title: "Select Source Type", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .camera
            
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        let photoAction = UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .photoLibrary
            
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        })
        alrt.addAction(cameraAction)
        alrt.addAction(photoAction)
        alrt.addAction(cancelAction)
        self.present(alrt, animated: true, completion: nil)
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let imageOne = info[UIImagePickerController.InfoKey.originalImage]as?UIImage else {
            return
        }
        
        guard let image = info[UIImagePickerController.InfoKey.editedImage]
            as? UIImage else {
                return
        }
        
        imageDataOne = imageOne.jpegData(compressionQuality: 0.50)!
        profilePicImg.image = image
        profilePicImg.cornerRadius = 40
        //user image service
        dismiss(animated:true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
      {
        
          picker .dismiss(animated: true, completion: nil)
      }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
            //self.progressView.startAnimation()
            //self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
   private func hideProgressView(){
        DispatchQueue.main.async {
            //self.progressView.stopAnimation()
           // self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
    @IBAction func doneBtnAction(_ sender: Any) {
        doUpdateProfileFromServer()
    }
    
    //MARK:- for Update Profile
    func doUpdateProfileFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let imageData = profilePicImg.image?.jpegData(compressionQuality: 0.50)
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_FIRSTNAME:firstNameField.text!,
                     Constants.KEY_LASTNAME:"",
                     Constants.KEY_TITLE:titleField.text!,
                     Constants.KEY_USERNAME:userNameField.text!,
                     Constants.KEY_EMAIL:emailField.text!,
                     Constants.KEY_MOBILE:phoneField.text!,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

       //PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doProfileUserUpdate(endUrl:PROFILE_USER_UPDATE, imageData: imageData, parameter: param, onCompletion: { (response) in
        //PSWebServiceAPI.doProfileUserUpdate(param, completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    
//                    profilePicImg.image
//                    let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
//                    print(dict)
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    //
                   DispatchQueue.main.async {
                   // self.doFollowUserListFromServer()
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                   }
                    self.tabBarController?.tabBar.isHidden = false
                    let otherStoryboard =  UIStoryboard(name: "Profile", bundle: nil)
                    let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
                    self.navigationController?.pushViewController(objMyProfile!, animated: true)
                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
}
