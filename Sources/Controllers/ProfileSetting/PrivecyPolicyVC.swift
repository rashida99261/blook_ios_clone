//
//  PrivecyPolicyVC.swift
//  bloook
//
//  Created by Tech Astha on 24/03/21.
//

import UIKit
import WebKit

class PrivecyPolicyVC: UIViewController {
    @IBOutlet weak var privecyWebView: WKWebView!
    @IBOutlet weak var roundView: UIView!
    
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
   // var progressView : AcuantProgressView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true
        doPrivacyPolicyFromServer()
    }
    override func viewDidLayoutSubviews() {
        roundView.layer.masksToBounds = true
        roundView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    //MARK:- for Faq
    func doPrivacyPolicyFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

       //PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doPrivacyPolicy(param, completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    print(dict)
                    let link = URL(string:"https://www.lipsum.com")!
                    let request = URLRequest(url: link)
                    self.privecyWebView.load(request)
                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
           // self.progressView.startAnimation()
           // self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
   private func hideProgressView(){
        DispatchQueue.main.async {
            //self.progressView.stopAnimation()
            //self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
}
