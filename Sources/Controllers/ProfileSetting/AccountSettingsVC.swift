//
//  AccountSettingsVC.swift
//  bloook
//
//  Created by Tech Astha on 15/02/21.
//

import UIKit

class AccountSettingsVC: UIViewController {

    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var emailFieldView: UIView!
    @IBOutlet weak var phoneNoFieldView: UIView!
    @IBOutlet weak var passwordFieldView: UIView!
    @IBOutlet weak var editAccountInfoBtn: UIButton!
    @IBOutlet weak var switchImg: UIImageView!
    @IBOutlet weak var passwordUpdateField: UITextField!
    @IBOutlet weak var phoneNoUpdateField: UITextField!
    @IBOutlet weak var emailUpdateField: UITextField!
//    @IBOutlet weak var emailView: UIView!
//    @IBOutlet weak var phoneNoView: UIView!
//    @IBOutlet weak var passwordView: UIView!
   // var progressView : AcuantProgressView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
    
    
    var strPassword = String()
    var strBounceSwitch = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        let defult = UserDefaults.standard
//        emailView.isHidden = true
//        phoneNoView.isHidden = true
//        passwordView.isHidden = true
        strPassword = "\(defult .object(forKey:"forPassword") ?? "")"
        emailField.isUserInteractionEnabled = false
        phoneField.isUserInteractionEnabled = false
        passwordField.isUserInteractionEnabled = false
        self.viewLoader.isHidden = true
        
        
        
        
//        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkActionEditViewHide))
//              self.emailView.addGestureRecognizer(gesture)
//        let gesture1 = UITapGestureRecognizer(target: self, action:  #selector(self.checkActionEditPhoneNoViewHide))
//              self.phoneNoView.addGestureRecognizer(gesture1)
//        let gesture2 = UITapGestureRecognizer(target: self, action:  #selector(self.checkActionEditPasswordViewHide))
//              self.passwordView.addGestureRecognizer(gesture2)
        doFollowUserListFromServer()
    }
//    @objc func checkActionEditViewHide(sender : UITapGestureRecognizer) {
//        self.emailView.isHidden = true
//    }
//    @objc func checkActionEditPhoneNoViewHide(sender : UITapGestureRecognizer) {
//        self.phoneNoView.isHidden = true
//    }
//    @objc func checkActionEditPasswordViewHide(sender : UITapGestureRecognizer) {
//        self.passwordView.isHidden = true
//    }
    
    override func viewDidLayoutSubviews() {
        roundView.layer.masksToBounds = true
        roundView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    //MARK:- for Profile Data
    func doFollowUserListFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_PROFILE_ID:strUserId,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

       //PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doProfileUser(param, completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    self.emailField.text = "\(dict.value(forKey: Constants.KEY_EMAIL) ?? "")"
                    self.phoneField.text = "\(dict.value(forKey: Constants.KEY_MOBILE) ?? "")"
                    let strPassword = "\(defult.object(forKey: "forPassword") ?? "")"
                    self.passwordField.text = strPassword
                   // self.emailUpdateField.text = "\(dict.value(forKey: Constants.KEY_EMAIL) ?? "")"
                   // self.phoneNoUpdateField.text = "\(dict.value(forKey: Constants.KEY_MOBILE) ?? "")"
                    self.strBounceSwitch = "\(dict.value(forKey: Constants.KEY_BOUNCE_STATUS) ?? "")"
                    if self.strBounceSwitch == "1"{
                        self.switchImg.image = UIImage(named: "switchon")
                       
                    }else{
                        self.switchImg.image = UIImage(named: "switchOff")
                    }
                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                   // PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }

    @IBAction func logoutBtnAction(_ sender: Any) {
       self.tabBarController?.tabBar.isHidden = true
       UserDefaults.standard.set(false, forKey: Constants.ISLOGIN)
       let storyboard =  UIStoryboard(name: "Main", bundle: nil)
       let objVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
       self.navigationController?.pushViewController(objVC!, animated: true)
    }
    @IBAction func closeFriendBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        let accountStoryboard =  UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objCloseFriend = accountStoryboard.instantiateViewController(withIdentifier: "CloseFriendVC") as? CloseFriendVC
        self.navigationController?.pushViewController(objCloseFriend!, animated: true)
    }
    @IBAction func blockedUserBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        let accountStoryboard =  UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objBlockedUser = accountStoryboard.instantiateViewController(withIdentifier: "BlockedUserVC") as? BlockedUserVC
        self.navigationController?.pushViewController(objBlockedUser!, animated: true)
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
//    @IBAction func editEmailBtnAction(_ sender: Any) {
//        //transitionFlipFromTop
//        UIView.transition(with: emailView,
//                                 duration: 0.5,
//                                 options: [.transitionFlipFromBottom],
//                                 animations: {
//
//                                   self.emailView.isHidden = true
//               },
//                                 completion: nil)
//        emailView.isHidden = false
//       // passwordField.text = strPassword
//      //  emailField.isUserInteractionEnabled = true
//
//    }
//    @IBAction func emailPhoneBtnAction(_ sender: Any) {
//        UIView.transition(with: phoneNoView,
//                                 duration: 0.5,
//                                 options: [.transitionFlipFromBottom],
//                                 animations: {
//
//                                   self.phoneNoView.isHidden = true
//               },
//                                 completion: nil)
//        phoneNoView.isHidden = false
//       // passwordField.text = strPassword
//       // phoneField.isUserInteractionEnabled = true
//
//    }
//    @IBAction func passwordBtnAction(_ sender: Any) {
//        UIView.transition(with: passwordView,
//                                 duration: 0.5,
//                                 options: [.transitionFlipFromBottom],
//                                 animations: {
//
//                                   self.passwordView.isHidden = true
//               },
//                                 completion: nil)
//        passwordView.isHidden = false
//       // passwordField.isUserInteractionEnabled = true
//    }
    private func showProgressView(text:String = "") {
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
           // self.progressView.startAnimation()
            //self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
            
            
            
            
        }
    }
   private func hideProgressView(){
        DispatchQueue.main.async {
            self.viewLoader.isHidden = true
            //self.progressView.stopAnimation()
            //self.progressView.removeFromSuperview()
        }
    }
    
    @IBAction func updateEditBtnAction(_ sender: Any) {
        
        doUpdateProfileSettingFromServer()
    }
    
    @IBAction func updatePhoneNoBtnAction(_ sender: Any) {
        doUpdateProfileSettingFromServer()
    }
    @IBAction func updatePasswordBtnAction(_ sender: Any) {
        //passwordUpdateField.text = ""
        strPassword = passwordUpdateField.text!
        doUpdateProfileSettingFromServer()
    }
    
    //MARK:- for Update Profile
    func doUpdateProfileSettingFromServer() {

        
        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_EMAIL:emailField.text!,
                     Constants.KEY_MOBILE:phoneField.text!,
                     Constants.KEY_PASSWORD:strPassword,
                     Constants.KEY_BOUNCE_STATUS:strBounceSwitch,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

       //PSUtil.showProgressHUD()
      //  self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doProfileSettingUpdate(param, completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
//                    let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
//                    print(dict)
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    DispatchQueue.main.async {
                       // self.emailView.isHidden = true
                        //self.phoneNoView.isHidden = true
                       // self.passwordView.isHidden = true
                        self.doFollowUserListFromServer()
                    }
                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    @IBAction func switchBtnAction(_ sender: Any) {
        if self.strBounceSwitch == "1"{
            self.switchImg.image = UIImage(named: "switchon")
            strBounceSwitch = "0"
        }else{
            self.switchImg.image = UIImage(named: "switchOff")
            strBounceSwitch = "1"
        }
        doUpdateProfileSettingFromServer()
    }
    @IBAction func feedBackBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        let accountStoryboard =  UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objFeedback = accountStoryboard.instantiateViewController(withIdentifier: "FeedbackVC") as? FeedbackVC
        self.navigationController?.pushViewController(objFeedback!, animated: true)
    }
    @IBAction func inviteFriendBtnAction(_ sender: Any) {
        
        self.tabBarController?.tabBar.isHidden = true
        let accountStoryboard =  UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objInviteFriend = accountStoryboard.instantiateViewController(withIdentifier: "ProfileInviteFriend") as? ProfileInviteFriend
        self.navigationController?.pushViewController(objInviteFriend!, animated: true)
    }
    @IBAction func privecyPolicyBtnAction(_ sender: Any) {
        
        let accountStoryboard =  UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objPrivecyPolicy = accountStoryboard.instantiateViewController(withIdentifier: "PrivecyPolicyVC") as? PrivecyPolicyVC
        self.navigationController?.pushViewController(objPrivecyPolicy!, animated: true)
    }
    @IBAction func faqBtnAction(_ sender: Any) {
        let accountStoryboard =  UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objFaq = accountStoryboard.instantiateViewController(withIdentifier: "FaqVC") as? FaqVC
        self.navigationController?.pushViewController(objFaq!, animated: true)
    }
    @IBAction func defaultVisibilityBtnAction(_ sender: Any) {
        let accountStoryboard =  UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objDefaultVisibility = accountStoryboard.instantiateViewController(withIdentifier: "DefaultVisibilityVC") as? DefaultVisibilityVC
        self.navigationController?.pushViewController(objDefaultVisibility!, animated: true)
    }
    @IBAction func editAccountInfoBtnAction(_ sender: Any) {
        emailFieldView.borderColor = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        emailFieldView.borderWidth = 1
        emailFieldView.cornerRadius = 20
        emailField.setLeftPaddingPoints(10)
        emailField.isUserInteractionEnabled = true
        phoneNoFieldView.borderColor = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        phoneNoFieldView.borderWidth = 1
        phoneNoFieldView.cornerRadius = 20
        phoneField.setLeftPaddingPoints(10)
        phoneField.isUserInteractionEnabled = true
        passwordFieldView.borderColor = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        passwordFieldView.borderWidth = 1
        passwordFieldView.cornerRadius = 20
        passwordField.setLeftPaddingPoints(10)
        passwordField.isUserInteractionEnabled = true
        editAccountInfoBtn.setTitle("Done", for: UIControl.State.normal)
        emailField.text = emailField.text!
        phoneField.text = phoneField.text!
        strPassword = passwordField.text!
       // doUpdateProfileSettingFromServer()
        
    }
}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
