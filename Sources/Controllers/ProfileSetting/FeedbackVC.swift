//
//  FeedbackVC.swift
//  bloook
//
//  Created by Tech Astha on 23/03/21.
//

import UIKit

class FeedbackVC: UIViewController {
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    @IBOutlet weak var labelThree: UILabel!
    @IBOutlet weak var labelFour: UILabel!
    @IBOutlet weak var labelFive: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var btnFiveRate: UIButton!
    @IBOutlet weak var btnFourRate: UIButton!
    @IBOutlet weak var btnThreeRate: UIButton!
    @IBOutlet weak var btnTwoRate: UIButton!
    @IBOutlet weak var btnOneRate: UIButton!
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
   // var progressView : AcuantProgressView!
    var strRate = String()
    @IBOutlet weak var roundView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true

    }
    override func viewDidLayoutSubviews() {
        roundView.layer.masksToBounds = true
        roundView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    @IBAction func submitBtnAction(_ sender: Any) {
        
        if strRate == "" {
            
            PSUtil.showAlertFromController(controller: self, withMessage: "Please select Rate")
            return;
            
       }else if (descriptionTextView.text?.count)! < 1 {
        
            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter description.")
        return;
        
    }else{
        
        doFeedbackCreateFromServer()
     }
    }
    
    //MARK:- for Feedback Create
    func doFeedbackCreateFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_RATING:strRate,
                     Constants.KEY_DESCRIPTION:descriptionTextView.text!,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

       //PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doFeedbackCreate(param, completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let msg = response[Constants.KEY_MESSAGE] as? String
                   
                   // let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    DispatchQueue.main.async {
                        PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
                    self.navigationController?.popViewController(animated: true)
                 }else{

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
              }
            }else{
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    @IBAction func backBtnAction(_ sender: Any) {
       
        self.navigationController?.popViewController(animated: true)
    }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
            //self.progressView.startAnimation()
           // self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
   private func hideProgressView(){
        DispatchQueue.main.async {
            //self.progressView.stopAnimation()
            //self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
    @IBAction func btnOneRateAction(_ sender: Any) {
        labelOne.backgroundColor = UIColor(red: 114/255.0, green: 146/255.0, blue: 255/255.0, alpha: 1)
        labelTwo.backgroundColor = UIColor.white
        labelThree.backgroundColor = UIColor.white
        labelFour.backgroundColor = UIColor.white
        labelFive.backgroundColor = UIColor.white
        labelOne.textColor = UIColor.white
        labelTwo.textColor = UIColor.black
        labelThree.textColor = UIColor.black
        labelFour.textColor = UIColor.black
        labelFive.textColor = UIColor.black
        strRate = labelOne.text!
    }
    @IBAction func btnTwoRateAction(_ sender: Any) {
        labelOne.backgroundColor = UIColor.white
        labelTwo.backgroundColor = UIColor(red: 114/255.0, green: 146/255.0, blue: 255/255.0, alpha: 1)
        labelThree.backgroundColor = UIColor.white
        labelFour.backgroundColor = UIColor.white
        labelFive.backgroundColor = UIColor.white
        labelOne.textColor = UIColor.black
        labelTwo.textColor = UIColor.white
        labelThree.textColor = UIColor.black
        labelFour.textColor = UIColor.black
        labelFive.textColor = UIColor.black
        strRate = labelTwo.text!
    }
    @IBAction func btnThreeRateAction(_ sender: Any) {
        labelOne.backgroundColor = UIColor.white
        labelTwo.backgroundColor = UIColor.white
        labelThree.backgroundColor = UIColor(red: 114/255.0, green: 146/255.0, blue: 255/255.0, alpha: 1)
        labelFour.backgroundColor = UIColor.white
        labelFive.backgroundColor = UIColor.white
        labelOne.textColor = UIColor.black
        labelTwo.textColor = UIColor.black
        labelThree.textColor = UIColor.white
        labelFour.textColor = UIColor.black
        labelFive.textColor = UIColor.black
        strRate = labelThree.text!
    }
    @IBAction func btnFourRateAction(_ sender: Any) {
        labelOne.backgroundColor = UIColor.white
        labelTwo.backgroundColor = UIColor.white
        labelThree.backgroundColor = UIColor.white
        labelFour.backgroundColor = UIColor(red: 114/255.0, green: 146/255.0, blue: 255/255.0, alpha: 1)
        labelFive.backgroundColor = UIColor.white
        labelOne.textColor = UIColor.black
        labelTwo.textColor = UIColor.black
        labelThree.textColor = UIColor.black
        labelFour.textColor = UIColor.white
        labelFive.textColor = UIColor.black
        strRate = labelFour.text!
    }
    @IBAction func btnFiveRateAction(_ sender: Any) {
        labelOne.backgroundColor = UIColor.white
        labelTwo.backgroundColor = UIColor.white
        labelThree.backgroundColor = UIColor.white
        labelFour.backgroundColor = UIColor.white
        labelFive.backgroundColor = UIColor(red: 114/255.0, green: 146/255.0, blue: 255/255.0, alpha: 1)
        labelOne.textColor = UIColor.black
        labelTwo.textColor = UIColor.black
        labelThree.textColor = UIColor.black
        labelFour.textColor = UIColor.black
        labelFive.textColor = UIColor.white
        strRate = labelFive.text!
    }
}
