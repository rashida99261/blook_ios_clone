//
//  TotalCoinCollectionViewCell.swift
//  bloook
//
//  Created by Tech Astha on 24/03/21.
//

import UIKit

class TotalCoinCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var coinLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
   
    func configure(coin: Coin) {
        coinLabel.text = (coin.coin ?? "0") + " Coins"
        dateLabel.text = coin.date ?? "Today"
        image.image = UIImage(named: coin.type == .platinum ? "coin" : "diamond")
    }
}
