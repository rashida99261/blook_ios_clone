//
//  DefaultVisibilityVC.swift
//  bloook
//
//  Created by Tech Astha on 30/04/21.
//

import UIKit

class DefaultVisibilityVC: UIViewController {
    @IBOutlet weak var followerBoostRadioImg: UIImageView!
    @IBOutlet weak var publicRadioImg: UIImageView!
    @IBOutlet weak var followerRadioImg: UIImageView!
    @IBOutlet weak var closeFriendRadioImg: UIImageView!
    @IBOutlet weak var topView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        topView.layer.masksToBounds = true
        topView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func followerBoostBtnAction(_ sender: Any) {
        followerBoostRadioImg.image = UIImage(named:"radiobutton2")
        publicRadioImg.image = UIImage(named:"radiobutton1")
        followerRadioImg.image = UIImage(named:"radiobutton1")
        closeFriendRadioImg.image = UIImage(named:"radiobutton1")
        //strVisibilitySelection = "Follower Boost"
    }
    @IBAction func publicBtnAction(_ sender: Any) {
        followerBoostRadioImg.image = UIImage(named:"radiobutton1")
        publicRadioImg.image = UIImage(named:"radiobutton2")
        followerRadioImg.image = UIImage(named:"radiobutton1")
        closeFriendRadioImg.image = UIImage(named:"radiobutton1")
      //  strVisibilitySelection = "Public"
    }
    @IBAction func flowersBtnAction(_ sender: Any) {
        followerBoostRadioImg.image = UIImage(named:"radiobutton1")
        publicRadioImg.image = UIImage(named:"radiobutton1")
        followerRadioImg.image = UIImage(named:"radiobutton2")
        closeFriendRadioImg.image = UIImage(named:"radiobutton1")
       // strVisibilitySelection = "Followes"
    }
    @IBAction func closeFriendBtnAction(_ sender: Any) {
        followerBoostRadioImg.image = UIImage(named:"radiobutton1")
        publicRadioImg.image = UIImage(named:"radiobutton1")
        followerRadioImg.image = UIImage(named:"radiobutton1")
        closeFriendRadioImg.image = UIImage(named:"radiobutton2")
       // strVisibilitySelection = "Close Friends"
    }
}
