//
//  BlockedUserVC.swift
//  bloook
//
//  Created by Tech Astha on 19/02/21.
//

import UIKit

class BlockedUserVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
   // var progressView : AcuantProgressView!
    @IBOutlet weak var blockTableList: UITableView!
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
    var followingArray = Array<Any>()
    var blockFriendArray = Array<Any>()
    @IBOutlet weak var searchView: UIView!
    var strBlockUsersId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true
        blockTableList.delegate = self
        blockTableList.dataSource = self
        doBlockedUserListFromServer()
    }
    override func viewDidLayoutSubviews() {
        searchView.layer.masksToBounds = true
        searchView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    //MARK:- for Blocked Friend List
    func doBlockedUserListFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_CLOSE_FRIEND_ID:strUserId,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

      //  PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doBlockedFriendList(param , completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    print(dict)
                    self.followingArray = dict[Constants.KEY_FOLLOWING] as? [Any] ?? [Any]()
                    self.blockFriendArray = dict[Constants.KEY_BLOCKED_FRIEND] as? [Any] ?? [Any]()
                    self.blockTableList.reloadData()

                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return  self.followingArray.count
        }else{
            return self.blockFriendArray.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BlockedTableViewCell
        cell.selectionStyle = .none
        if indexPath.section == 0{
            let dict = self.followingArray[indexPath.row] as? NSDictionary ?? NSDictionary()
           let toUserDic = dict[Constants.KEY_TO_USER_ID] as? NSDictionary ?? NSDictionary()
            cell.userNameLbl.text = "\(toUserDic.value(forKey: Constants.KEY_NAME) ?? "")"
            let strUserProfileImg = "\(toUserDic.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
            if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
                cell.userImg.image = UIImage (named: "user")
            }else{
                cell.userImg.sd_setImage(with: URL(string: strUserProfileImg), placeholderImage: UIImage(named: "placeholder.png"))
          }
            cell.blockBtn.setTitle("Block", for: .normal)
        }else{
            let dict = self.blockFriendArray[indexPath.row] as? NSDictionary ?? NSDictionary()
            let toUserDic = dict[Constants.KEY_TO_USER_ID] as? NSDictionary ?? NSDictionary()
            cell.userNameLbl.text = "\(toUserDic.value(forKey: Constants.KEY_NAME) ?? "")"
            let strUserProfileImg = "\(toUserDic.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
            if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
                cell.userImg.image = UIImage (named: "user")
            }else{
                cell.userImg.sd_setImage(with: URL(string: strUserProfileImg), placeholderImage: UIImage(named: "placeholder.png"))
          }
            cell.blockBtn.setTitle("Unblock", for: .normal)
        }
        
        cell.blockBtn.tag = indexPath.row
        cell.blockBtn.addTarget(self, action: #selector(blockUserBtnAction(_:)), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == blockTableList && section == 1 {
            return 40
        }
        return  0.01
      }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == blockTableList && section == 0 {
            return 0.1
        }
        return  0.1
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //if incidentsMyArr.isE
        if  section == 1 {
            let theView = UIView(frame: CGRect(x: 8, y: 0, width: tableView.frame.width, height: 40))
            let theTitleLabel = UILabel(frame: CGRect(x: 8, y: 0, width: theView.frame.width, height: 40))
            theView.backgroundColor = UIColor.clear

            theTitleLabel.text = "Blocked Users " + "(" + "\(blockFriendArray.count)" + ")"

            theTitleLabel.textColor = UIColor.black
            theTitleLabel.font = UIFont.systemFont(ofSize: 14)
            theTitleLabel.textAlignment = .left
            theView.addSubview(theTitleLabel)
            return theView
        }
        return nil
    }
    
    @objc func blockUserBtnAction( _ sender :UIButton){
        let buttonRow = sender.tag
        let strTitle = sender.titleLabel?.text!
        
       if strTitle == "Block"{
            let dict = self.followingArray[buttonRow] as? NSDictionary
            let toUserDic = dict?[Constants.KEY_TO_USER_ID] as? NSDictionary ?? NSDictionary()
            strBlockUsersId = "\(toUserDic.value(forKey: Constants.KEY_Id) ?? "")"
            
         }else{
            let dict = blockFriendArray[buttonRow] as? NSDictionary
            let toUserDic = dict?[Constants.KEY_TO_USER_ID] as? NSDictionary ?? NSDictionary()
            strBlockUsersId = "\(toUserDic.value(forKey: Constants.KEY_Id) ?? "")"
           
        }
         doBlockUserFromServer()
     }
    
    //MARK:- for block User Add
    func doBlockUserFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_TO_USER_ID:strBlockUsersId,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

      //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doBlockedFriend(param , completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    DispatchQueue.main.async {
                    self.doBlockedUserListFromServer()
                    }

                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
           // self.progressView.startAnimation()
           // self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
   private func hideProgressView(){
        DispatchQueue.main.async {
            self.viewLoader.isHidden = true
           // self.progressView.stopAnimation()
           // self.progressView.removeFromSuperview()
        }
    }
}
