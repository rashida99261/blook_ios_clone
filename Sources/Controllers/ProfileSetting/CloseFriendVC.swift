//
//  CloseFriendVC.swift
//  bloook
//
//  Created by Tech Astha on 19/02/21.
//

import UIKit

class CloseFriendVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var searchField: UITextField!
   // var progressView : AcuantProgressView!
    @IBOutlet weak var closeFriendTable: UITableView!
    var followingArray = Array<Any>()
   // var closeFriendArray = Array<Any>()
    var closeFriendArray  = NSArray()
    var filtered  = NSArray()
    var strBtnId = String()
    var strFriendId = String()
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true
        closeFriendTable.delegate = self
        closeFriendTable.dataSource = self
       
        doCloseFriendListFromServer()
    }
    override func viewDidLayoutSubviews() {
        searchView.layer.masksToBounds = true
        searchView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    //MARK:- for Close Friend List
    func doCloseFriendListFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_CLOSE_FRIEND_ID:strUserId,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

      //  PSUtil.showProgressHUD()
        //self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doCloseFriendList(param , completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    print(dict)
                    self.followingArray = dict[Constants.KEY_FOLLOWING] as? [Any] ?? [Any]()
                    self.closeFriendArray = dict[Constants.KEY_CLOSE_FRIEND] as? NSArray ?? NSArray()
                    self.filtered = self.closeFriendArray
                    self.closeFriendTable.reloadData()

                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                   // PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return  self.followingArray.count
        }else{
            return self.filtered.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
       
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CloseFriendTableViewCell
            cell.selectionStyle = .none
            let dict = self.followingArray[indexPath.row] as? NSDictionary ?? NSDictionary()
           let toUserDic = dict[Constants.KEY_TO_USER_ID] as? NSDictionary ?? NSDictionary()
            cell.userNameLbl.text = "\(toUserDic.value(forKey: Constants.KEY_NAME) ?? "")"
            let strUserProfileImg = "\(toUserDic.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
            if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
                cell.userImg.image = UIImage (named: "user")
            }else{
                cell.userImg.sd_setImage(with: URL(string: strUserProfileImg), placeholderImage: UIImage(named: "placeholder.png"))
          }
           // strBtnId = "0"
            cell.addView.cornerRadius = 18
            //cell.addView.round(corners: [.bottomRight .bottomRight .topLeft .topRight], cornerRadius: 40)
            cell.AddFriend.setTitle("Add", for: .normal)
            cell.AddFriend.tag = indexPath.row
            cell.AddFriend.addTarget(self, action: #selector(AddFriendBtnAction(_:)), for: UIControl.Event.touchUpInside)
            return cell
        }else {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CloseFriendTableViewCell
            cell1.selectionStyle = .none
            let dict = self.filtered[indexPath.row] as? NSDictionary ?? NSDictionary()
            let toUserDic = dict[Constants.KEY_TO_USER_ID] as? NSDictionary ?? NSDictionary()
            cell1.userNameLbl.text = "\(toUserDic.value(forKey: Constants.KEY_NAME) ?? "")"
            let strUserProfileImg = "\(toUserDic.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
            if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
                cell1.userImg.image = UIImage (named: "user")
            }else{
                cell1.userImg.sd_setImage(with: URL(string: strUserProfileImg), placeholderImage: UIImage(named: "placeholder.png"))
          }
            //strBtnId = "1"
            cell1.addView.cornerRadius = 18
            cell1.AddFriend.setTitle("Remove", for: .normal)
            cell1.AddFriend.tag = indexPath.row
            cell1.AddFriend.addTarget(self, action: #selector(AddFriendBtnAction(_:)), for: UIControl.Event.touchUpInside)
            return cell1
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == closeFriendTable && section == 1 {
            return 40
        }
        return  0.01
     }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == closeFriendTable && section == 0 {
            return 0.1
        }
        return  0.1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //if incidentsMyArr.isE
        if  section == 1 {
            let theView = UIView(frame: CGRect(x: 8, y: 0, width: tableView.frame.width - 8, height: 40))
            let theTitleLabel = UILabel(frame: CGRect(x: 8, y: 0, width: theView.frame.width, height: 40))
            theView.backgroundColor = UIColor.clear

            theTitleLabel.text = "Close Friends" + " (" + "\(closeFriendArray.count)" + ")"
            theTitleLabel.textColor = UIColor.black
            theTitleLabel.font = UIFont.systemFont(ofSize: 14)
            theTitleLabel.textAlignment = .left
            theView.addSubview(theTitleLabel)
            return theView
        }
        return nil
    }
    
    @objc func AddFriendBtnAction( _ sender :UIButton){
        let buttonRow = sender.tag
        let strTitle = sender.titleLabel?.text!
        
       if strTitle == "Add"{
            let dict = self.followingArray[buttonRow] as? NSDictionary
            let toUserDic = dict?[Constants.KEY_TO_USER_ID] as? NSDictionary ?? NSDictionary()
            strFriendId = "\(toUserDic.value(forKey: Constants.KEY_Id) ?? "")"
            
         }else{
            let dict = filtered[buttonRow] as? NSDictionary
            let toUserDic = dict?[Constants.KEY_TO_USER_ID] as? NSDictionary ?? NSDictionary()
            strFriendId = "\(toUserDic.value(forKey: Constants.KEY_Id) ?? "")"
           
        }
        doAddFriendFromServer()
     }
    
    //MARK:- for Close Friend List
    func doAddFriendFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_TO_USER_ID:strFriendId,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

      //  PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doCloseFriend(param , completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    DispatchQueue.main.async {
                    self.doCloseFriendListFromServer()
                    }

                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    @IBAction func searchArrowBtnAction(_ sender: Any) {
   //     guard let textField = sender as? MDCOutlinedTextField else { return }
        var searchString = searchField.text ?? ""
        searchString = searchString.trimmingCharacters(in: .whitespacesAndNewlines)
        self.filterCloseList(forSearchText: searchString)
        
    }
    func filterCloseList(forSearchText searchText :String) {
        if searchText.isEmpty {
            filtered = closeFriendArray
        } else {
            let thePredicate = NSPredicate(format: "%K CONTAINS[cd] %@", "name", searchText)
            let theResultArray = closeFriendArray.filtered(using: thePredicate)
            filtered = theResultArray as NSArray
        }
        self.closeFriendTable.reloadData()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
           // self.progressView.startAnimation()
           // self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
            
        }
    }
    
   private func hideProgressView(){
        DispatchQueue.main.async {
            //self.progressView.stopAnimation()
            //self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
}
