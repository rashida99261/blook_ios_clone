//
//  BlockedTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 19/02/21.
//

import UIKit

class BlockedTableViewCell: UITableViewCell {
   
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var blockBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
