//
//  AchievementsTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 22/02/21.
//

import UIKit

class AchievementsTableViewCell: UITableViewCell {

    @IBOutlet weak var UserImgView: UIImageView!
    @IBOutlet weak var viewPostBtn: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var pointsLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
