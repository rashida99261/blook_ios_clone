//
//  StatisticsVC.swift
//  bloook
//
//  Created by Tech Astha on 19/02/21.
//

import Pulsator
import UIKit

class StatisticsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var dailyLbl: UILabel!
    @IBOutlet var totalCoinLabel: UILabel!
    @IBOutlet var currentStreakLabel: UILabel!
    @IBOutlet var recordStreakLabel: UILabel!
    @IBOutlet var totalFollowersLabel: UILabel!
    @IBOutlet var annualLbl: UILabel!
    @IBOutlet var lifetimeLbl: UILabel!
    @IBOutlet var statisticsListTable: UITableView!
    
    @IBOutlet var topStatusView: UIView!
    @IBOutlet var userProfileImg: UIImageView!
    @IBOutlet var userNameLbl: UILabel!
    
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
    
    var user: User!
    //var progressView: AcuantProgressView!
    var statisticsArr = [Any]()
    var strType = String()
    var pulsator: Pulsator?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true
        strType = "daily"
        
        // bounceImageView.isHidden = true//(user.bounceStatus == 0)
        
        userNameLbl.text = user?.fullName()
        userProfileImg.kf.setImage(with: user?.profilePhotoURL(), placeholder: UIImage(named: "placeholder.png"))
        // bounceImageView.isHidden = user.bounceStatus?.boolValue() == false
        totalFollowersLabel.text = "+\(user.followersCount ?? 0)"
        doStatisticsFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBounce()
    }

    private func setBounce() {
        if user?.bounceStatus?.boolValue() == true {
            if pulsator == nil {
                pulsator = Pulsator()
            }
            userProfileImg.startPulse(pulsator: pulsator!)
        }
    }

    // MARK: - for All Follow List

    func doStatisticsFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.KEY_TYPE: "annual",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        //progressView = AcuantProgressView(frame: view.frame, center: view.center)
        showProgressView(text: "Loading...")
        PSWebServiceAPI.doStatistics(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    guard let d = response[Constants.KEY_POSTDATA] as? [String: Any] else { return }
                    self.statisticsArr = d["points"] as? [Any] ?? [Any]()
                    DispatchQueue.main.async {
                        self.statisticsListTable.reloadData()
                        self.currentStreakLabel.text = d["current_streak"] as? String
                        self.recordStreakLabel.text = d["total_streak"] as? String
                        self.totalCoinLabel.text = d["total_coins"] as? String
                    }
                   
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
    
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }

    @IBAction func dailyBtnAction(_ sender: Any) {
        dailyLbl.textColor = UIColor.black
        annualLbl.textColor = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        lifetimeLbl.textColor = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        strType = "daily"
        doStatisticsFromServer()
    }

    @IBAction func annualBtnAction(_ sender: Any) {
        dailyLbl.textColor = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        annualLbl.textColor = UIColor.black
        lifetimeLbl.textColor = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        strType = "annual"
        doStatisticsFromServer()
    }

    @IBAction func lifetimeBtnAction(_ sender: Any) {
        dailyLbl.textColor = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        annualLbl.textColor = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        lifetimeLbl.textColor = UIColor.black
        strType = "lifetime"
        doStatisticsFromServer()
    }

    @IBAction func backBtnAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statisticsArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dict = statisticsArr[indexPath.row] as? NSDictionary ?? NSDictionary()
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! StatisticsTableViewCell
        cell.selectionStyle = .none
        
        cell.statisticsTypeLbl?.text = "\(dict.value(forKey: Constants.KEY_EARN_TYPE) ?? "")".capitalized
        if "\(dict.value(forKey: Constants.KEY_EARN_TYPE) ?? "")".lowercased() == "quantity" {
            cell.statisticsDescriptionLbl?.text = "your post achievements"
        } else if "\(dict.value(forKey: Constants.KEY_EARN_TYPE) ?? "")".lowercased() == "post" {
            cell.statisticsDescriptionLbl?.text = "your posts"
        } else {
        cell.statisticsDescriptionLbl?.text = "your post \(dict.value(forKey: Constants.KEY_EARN_TYPE) ?? "")s"
        }
        cell.pointLbl?.text = "\(dict.value(forKey: Constants.KEY_TOTAL_POINTS) ?? "")"
        cell.viewPostBtn.tag = indexPath.row
        cell.viewPostBtn.addTarget(self, action: #selector(viewPostBtnAction(_:)), for: UIControl.Event.touchUpInside)
        cell.staticsGraphBtn.tag = indexPath.row
        cell.staticsGraphBtn.addTarget(self, action: #selector(staticsGraphBtnAction(_:)), for: UIControl.Event.touchUpInside)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    @objc func staticsGraphBtnAction(_ sender: UIButton) {
        let buttonRow = sender.tag
        print(buttonRow)
        let myStatisticsStoryboard = UIStoryboard(name: "StatisticsStoryboard", bundle: nil)
        let objStatisticsGraph = myStatisticsStoryboard.instantiateViewController(withIdentifier: "StatisticsGraphVC") as? StatisticsGraphVC
        navigationController?.pushViewController(objStatisticsGraph!, animated: true)
    }

    @objc func viewPostBtnAction(_ sender: UIButton) {
        let buttonRow = sender.tag
        print(buttonRow)
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // alert.view.tintColor = UIColor.black
        
        alert.addAction(UIAlertAction(title: "Share", style: .default, handler: { _ in
            PSUtil.showAlertFromController(controller: self, withMessage: "Share to Here")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Hide", style: .default, handler: { _ in
            PSUtil.showAlertFromController(controller: self, withMessage: "Hide to Here")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
         
            print("User click Cancel button")
        }))
        
        present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @IBAction func achievementsBtnAction(_ sender: Any) {
        let myStatisticsStoryboard = UIStoryboard(name: "StatisticsStoryboard", bundle: nil)
        let objAchievements = myStatisticsStoryboard.instantiateViewController(withIdentifier: "AchievementsVC") as? AchievementsVC
        navigationController?.pushViewController(objAchievements!, animated: true)
    }
    
    @IBAction func totalCoinBtnAction(_ sender: Any) {
        let accountStoryboard = UIStoryboard(name: "SettingStoryboard", bundle: nil)
        let objTotalCoinList = accountStoryboard.instantiateViewController(withIdentifier: "TotalCoinListVC") as? TotalCoinListVC
        navigationController?.pushViewController(objTotalCoinList!, animated: true)
    }

    private func showProgressView(text: String = "") {
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
            //self.progressView.startAnimation()
            //self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
    private func hideProgressView() {
        DispatchQueue.main.async {
          //  self.progressView.stopAnimation()
            //self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
}
