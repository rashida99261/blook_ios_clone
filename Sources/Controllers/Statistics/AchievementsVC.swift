//
//  AchievementsVC.swift
//  bloook
//
//  Created by Tech Astha on 22/02/21.
//

import Kingfisher
import UIKit

class AchievementsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var multipleView: UIView!
    @IBOutlet var dailyLbl: UILabel!
    @IBOutlet var annualLbl: UILabel!
    @IBOutlet var lifetimeLbl: UILabel!
    @IBOutlet var dailyBtn: UIButton!
    @IBOutlet var annualBtn: UIButton!
    @IBOutlet var lifetimeBtn: UIButton!
    @IBOutlet var achievementsTable: UITableView!
    
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
    var strType = String()
   // var progressView: AcuantProgressView!
    var achievementsArr: [Post]?
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true
        
        let path = UIBezierPath(roundedRect: multipleView.bounds,
                                byRoundingCorners: [.bottomRight, .bottomLeft],
                                cornerRadii: CGSize(width: 20, height: 20))

        let maskLayer = CAShapeLayer()

        maskLayer.path = path.cgPath
        multipleView.layer.mask = maskLayer
        
        dailyLbl.textColor = UIColor.black
        annualLbl.textColor = UIColor.lightGray
        lifetimeLbl.textColor = UIColor.lightGray
        // achievementsTable.reloadData()
        strType = "daily"
        doUserAchievementsFromServer()
    }
    
    // MARK: - for User Achievements

    func doUserAchievementsFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TYPE: strType,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        // PSUtil.showProgressHUD()
       // progressView = AcuantProgressView(frame: view.frame, center: view.center)
        showProgressView(text: "Loading...")
        PSWebServiceAPI.getAchievements(param, completion: { response, error in
            DispatchQueue.main.async {
                self.hideProgressView()
                if error == nil {
                    if response?.status == 200 {
                        self.achievementsArr = response?.posts
                        self.achievementsTable.reloadData()
                    } else {
                        PSUtil.hideProgressHUD()
                        let msg = response?.message
                        PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
                }
            }
        })
//        PSWebServiceAPI.doUserAchievements(param, completion: { response in
//            // PSUtil.hideProgressHUD()
//            self.hideProgressView()
//            if response["Error"] == nil {
//                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
//                if Status == 200 {
//                    self.achievementsArr = response[Constants.KEY_POSTDATA] as? [Any] ?? [Any]()
//                    // let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
//                    print(self.achievementsArr)
//                    self.achievementsTable.reloadData()
//                } else {
//                    PSUtil.hideProgressHUD()
//                    let msg = response[Constants.KEY_MESSAGE] as? String
//                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
//                }
//            } else {
//                PSUtil.hideProgressHUD()
//                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
//            }
//        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return achievementsArr?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let post = achievementsArr?[indexPath.row] else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AchievementsTableViewCell
        cell.selectionStyle = .none
        let strName = post.dDescription
        cell.nameLbl.text = strName
        
        cell.dateLbl.text = post.createdAt?.dateWithoutTimeComponent()
     
        let strUserProfileImg = post.thumbnail ?? post.filename
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            cell.UserImgView.image = UIImage(named: "user")
        } else {
            let url = URL(string: strUserProfileImg ?? "")
            cell.UserImgView.kf.setImage(with: url, placeholder: UIImage(named: "user"))
        }
        
        let point = post.sharePoints?.first
        cell.pointsLbl.text = "\(point?.totalPoints?.formatPoints() ?? "0")  points"
        cell.viewPostBtn.tag = indexPath.row
        cell.viewPostBtn.addTarget(self, action: #selector(viewPostBtnAction(_:)), for: UIControl.Event.touchUpInside)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let post = self.achievementsArr?[indexPath.row] else { return }
        let otherStoryboard = UIStoryboard(name: "Posts", bundle: nil)
        let objPostsViewController = otherStoryboard.instantiateViewController(withIdentifier: "PostsViewController") as? PostsViewController
        objPostsViewController?.singlePost = post
        
        self.navigationController?.pushViewController(objPostsViewController!, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    @objc func viewPostBtnAction(_ sender: UIButton) {
        let buttonRow = sender.tag
        print(buttonRow)
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
      
        alert.addAction(UIAlertAction(title: "View Post", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            guard let post = self.achievementsArr?[buttonRow] else { return }
            let otherStoryboard = UIStoryboard(name: "Posts", bundle: nil)
            let objPostsViewController = otherStoryboard.instantiateViewController(withIdentifier: "PostsViewController") as? PostsViewController
            objPostsViewController?.singlePost = post
            self.navigationController?.pushViewController(objPostsViewController!, animated: true)
         
        }))
        
        alert.addAction(UIAlertAction(title: "Share Achievement", style: .default, handler: { _ in
            guard let post = self.achievementsArr?[buttonRow] else { return }
            self.loadThumbnailFromCache(urlString: post.filename, post: post.dDescription)
            
        }))
        
//        alert.addAction(UIAlertAction(title: "Hide", style: .default, handler: { _ in
//            PSUtil.showAlertFromController(controller: self, withMessage: "Hide to Here")
//
//        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
         
            print("User click Cancel button")
        }))
        
        present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    private func loadThumbnailFromCache(urlString: String?, post: String?) {
        guard let url = URL(string: urlString ?? "") else {
            let shareItems = [post ?? "-"]

            let avc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)

            present(avc, animated: true)
            return
        }
        KingfisherManager.shared.retrieveImage(with: url,
                                               options: nil,
                                               progressBlock: nil) { [self] (result) -> Void in
            switch result {
                case .success(let value):
                    DispatchQueue.main.async {
                        if let img2 = UIImage(named: "watermark") {
                            let img = value.image
                            let width = img.size.width
                            let height = img.size.height
                            let rect = CGRect(x: 0, y: 0, width: width, height: height)

                            UIGraphicsBeginImageContextWithOptions(img.size, true, 0)
                            let context = UIGraphicsGetCurrentContext()

                            context!.setFillColor(UIColor.white.cgColor)
                            context!.fill(rect)

                            img.draw(in: rect, blendMode: .normal, alpha: 1)
                            let x = img.size.width - 220
                            let y = img.size.height - 286
                            img2.draw(in:CGRect(x: x > 0 ? x : 0,
                                                y: y > 0 ? y : 0,
                                                width: 200,
                                                height: 266)
                                      , blendMode: .normal, alpha: 0.3)

                            let result = UIGraphicsGetImageFromCurrentImageContext()
                            UIGraphicsEndImageContext()

                            if result != nil {
                                let shareItems = [result!, post ?? "-"] as [Any]

                                let avc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)

                                present(avc, animated: true)
                            } else {
                                let shareItems = [post ?? "-"]

                                let avc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)

                                present(avc, animated: true)
                            }

                        }
                        
                    }

                case .failure(let error):
                    print("Error: \(error)")
            }
        }
    }
    
    @IBAction func dailyBtnAction(_ sender: Any) {
        dailyLbl.textColor = UIColor.black
        annualLbl.textColor = UIColor.lightGray
        lifetimeLbl.textColor = UIColor.lightGray
        strType = "daily"
        doUserAchievementsFromServer()
        // achievementsTable.reloadData()
    }

    @IBAction func annualBtnAction(_ sender: Any) {
        dailyLbl.textColor = UIColor.lightGray
        annualLbl.textColor = UIColor.black
        lifetimeLbl.textColor = UIColor.lightGray
        strType = "annual"
        doUserAchievementsFromServer()
        //  achievementsTable.reloadData()
    }

    @IBAction func lifetimeBtnAction(_ sender: Any) {
        dailyLbl.textColor = UIColor.lightGray
        annualLbl.textColor = UIColor.lightGray
        lifetimeLbl.textColor = UIColor.black
        strType = "lifetime"
        doUserAchievementsFromServer()
        // achievementsTable.reloadData()
    }

    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    private func showProgressView(text: String = "") {
        DispatchQueue.main.async {
           // //self.progressView.messageView.text = text
            //self.progressView.startAnimation()
            //self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
    private func hideProgressView() {
        DispatchQueue.main.async {
           // self.progressView.stopAnimation()
           // self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
            
        }
    }
}

extension UIView {
    // func round(corners: UIRectCorner, cornerRadius: Double) {
//
//    let size = CGSize(width: cornerRadius, height: cornerRadius)
//    let bezierPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size)
//    let shapeLayer = CAShapeLayer()
//    shapeLayer.frame = self.bounds
//    shapeLayer.path = bezierPath.cgPath
//    self.layer.mask = shapeLayer
//    }
 
    func round(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
