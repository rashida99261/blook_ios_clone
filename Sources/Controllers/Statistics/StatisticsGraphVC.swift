//
//  StatisticsGraphVC.swift
//  bloook
//
//  Created by Tech Astha on 08/03/21.
//

import UIKit

class StatisticsGraphVC: UIViewController {
    @IBOutlet weak var dailyLbl: UILabel!
    @IBOutlet weak var annualLbl: UILabel!
    @IBOutlet weak var lifetimeLbl: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var topStatusView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        dateView.roundCorners(corners: [.topLeft, .topRight], radius: 40)
    }
    @IBAction func dailyBtnAction(_ sender: Any) {
        dailyLbl.textColor = UIColor.black
        annualLbl.textColor = UIColor.lightGray
        lifetimeLbl.textColor = UIColor.lightGray
        
       
    }
    @IBAction func annualBtnAction(_ sender: Any) {
        dailyLbl.textColor = UIColor.lightGray
        annualLbl.textColor = UIColor.black
        lifetimeLbl.textColor = UIColor.lightGray
       
    }
    @IBAction func lifetimeBtnAction(_ sender: Any) {
        dailyLbl.textColor = UIColor.lightGray
        annualLbl.textColor = UIColor.lightGray
        lifetimeLbl.textColor = UIColor.black
        
    }
    @IBAction func backBtnAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension UIView {
    func roundCorner(corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11, *) {
            self.clipsToBounds = true
            self.layer.cornerRadius = radius
            var masked = CACornerMask()
            if corners.contains(.topLeft) { masked.insert(.layerMinXMinYCorner) }
            if corners.contains(.topRight) { masked.insert(.layerMaxXMinYCorner) }
            if corners.contains(.bottomLeft) { masked.insert(.layerMinXMaxYCorner) }
            if corners.contains(.bottomRight) { masked.insert(.layerMaxXMaxYCorner) }
            self.layer.maskedCorners = masked
        }
        else {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
    }
}
