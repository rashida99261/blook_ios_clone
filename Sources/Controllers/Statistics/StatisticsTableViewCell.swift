//
//  StatisticsTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 22/02/21.
//

import UIKit

class StatisticsTableViewCell: UITableViewCell {
    @IBOutlet weak var viewPostBtn: UIButton!
    @IBOutlet weak var staticsGraphBtn: UIButton!
    @IBOutlet weak var statisticsTypeLbl: UILabel!
    @IBOutlet weak var statisticsDescriptionLbl: UILabel!
    @IBOutlet weak var pointLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
