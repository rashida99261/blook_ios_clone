//
//  ChatSettingsVC.swift
//  bloook
//
//  Created by Tech Astha on 19/03/21.
//

import Toaster
import UIKit

class ChatSettingsVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var roundView: UIView!
    @IBOutlet var topStatusView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var notificationImageView: UIImageView!
    @IBOutlet var muteSwitch: UISwitch!
    var members: [User]?
    var groupModel: GroupModel?
    var group: Group?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        getGroupMembers()
        group = groupModel?.group
        muteSwitch.isOn = ((groupModel?.isArchive ?? 0) != 0)
        if groupModel?.isArchive == 1 {
            notificationImageView.image = UIImage(named: "silence")
        } else {
            notificationImageView.image = UIImage(named: "notification")
        }
    }

//    override func viewDidLayoutSubviews() {
//        roundView.layer.masksToBounds = true
//        roundView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
//    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getGroupMembers()
    }
    
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ChatSettingTableViewCell
        cell.selectionStyle = .none
        cell.delegate = self
        guard let user = members?[indexPath.row] else { return UITableViewCell() }
        let adminUsers = members?.filter { $0.isAdmin == 1 }
        let isMyGroup = adminUsers?.filter { $0.id == LoggedInUser.shared.user?.id }
        cell.configure(user: user, isAdminUser: (isMyGroup?.count ?? 0).boolValue())
        return cell
    }

    @IBAction func backBtnAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        navigationController?.popViewController(animated: true)
    }

    @IBAction func inviteButtonTapped() {
        let msgStoryboard = UIStoryboard(name: "MessageStoryboard", bundle: nil)
        let objNewConversation = msgStoryboard.instantiateViewController(withIdentifier: "NewConversationVC") as! CreateGroupChatVC
        objNewConversation.addedMembers = members
        objNewConversation.selectedGroupId = group?.groupId
        navigationController?.pushViewController(objNewConversation, animated: true)
    }
  
    @IBAction func muteButtonTapped(_ sender: Any) {
        guard let sender = sender as? UISwitch else { return }
        if sender.isOn == true {
            notificationImageView.image = UIImage(named: "silence")
        } else {
            notificationImageView.image = UIImage(named: "notification")
        }
        muteGroup()
    }

    @IBAction func leaveButtonTapped() {
        leaveGroup()
    }

    @IBAction func deleteButtonTapped() {
        deleteConversation()
    }
}

extension ChatSettingsVC {
    func getGroupMembers() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "group_id": "\(group?.groupId ?? 0)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        PSWebServiceAPI.getGroupMembers(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.members = response?.users
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func muteGroup() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "group_id": "\(group?.groupId ?? 0)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        PSWebServiceAPI.groupArchive(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func leaveGroup() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "group_id": "\(group?.groupId ?? 0)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        PSWebServiceAPI.memberLeft(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }

    func deleteConversation() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "group_id": "\(group?.groupId ?? 0)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        PSWebServiceAPI.deleteConversation(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func makeAdmin(user: User?, makeAdmin: Bool = true) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "group_id": "\(group?.groupId ?? 0)",
                     "member_id": "\(user?.id ?? 0)",
                     "is_admin": makeAdmin ? "1" : "0",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        PSWebServiceAPI.makeAdmin(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    let msg = response?.message
                    DispatchQueue.main.async { [self] in
                        self.getGroupMembers()
                        Toast(text: msg).show()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func removeUser(user: User?, makeAdmin: Bool = true) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "group_id": "\(group?.groupId ?? 0)",
                     "member_id": "\(user?.id ?? 0)",
                     "is_admin": makeAdmin ? "1" : "0",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        PSWebServiceAPI.removeMember(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    let msg = response?.message
                    DispatchQueue.main.async { [self] in
                        self.getGroupMembers()
                        Toast(text: msg).show()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
}

extension ChatSettingsVC: ChatSettingTableViewCellDelegate {
    func moreButtonTapped(user: User) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // alert.view.tintColor = UIColor.black
        if user.isAdmin?.boolValue() == false {
            alert.addAction(UIAlertAction(title: "Make Admin", style: .default, handler: { _ in
                self.makeAdmin(user: user)
             
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Remove Admin", style: .default, handler: { _ in
                self.makeAdmin(user: user, makeAdmin: false)
             
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Remove User", style: .default, handler: { _ in
            self.removeUser(user: user)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
         
            print("User click Cancel button")
        }))
        
        present(alert, animated: true, completion: {
            print("completion block")
        })
    }
}
