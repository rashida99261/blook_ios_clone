//
//  ISChatTableViewCell.swift
//  GreenTowTruckDriver
//
//  Created by mac on 3/13/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ISChatTableViewCell: UITableViewCell {

    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(message: Message?, user: User?) {
        guard let user = user else { return }
        self.messageLbl.text = message?.message ?? ""
        self.timeLbl.text = message?.createdAt?.toDate(plain: true)?.timeAgoSinceDate() ?? "now"
        let strUserProfileImg = user.profilePhoto
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            self.profileImg.image = UIImage(named: "user")
        } else {
            self.profileImg.kf.setImage(with: user.profilePhotoURL(), placeholder: UIImage(named: "placeholder.png"))
        }
    }
}
