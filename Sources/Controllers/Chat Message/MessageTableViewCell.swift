//
//  MessageTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 25/02/21.
//

import UIKit

protocol MessageTableViewCellDelegate {
    func moreButtonTapped(for user: User)
    func moreButtonTapped(for groupModel: GroupModel?)
}
class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!

    
    var delegate: MessageTableViewCellDelegate?
    var group: GroupModel?
    
    @IBOutlet weak var selectActionBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }


    override func prepareForReuse() {
        profileImg.kf.cancelDownloadTask() // first, cancel currenct download task
        profileImg.kf.setImage(with: URL(string: ""), placeholder: UIImage(named: "postPlaceholder")) // second, prevent kingfisher from setting previous image
        profileImg.image = UIImage(named: "postPlaceholder")
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

     }
        
    func configure(group: GroupModel?) {
        self.group = group
        if group?.group != nil {
            nameLbl?.text = group?.group?.groupName ?? "Unknown Group"
        } else {
            nameLbl?.text = group?.user?.fullName() ?? "Anonymous"
            let strUserProfileImg = group?.user?.profilePhoto
            if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
                profileImg.image = UIImage(named: "postPlaceholder")
            } else {
                let url = URL(string: strUserProfileImg ?? "")
                profileImg.kf.setImage(with: url, placeholder: UIImage(named: "user"))
            }

        }
        messageLbl.text = group?.message ?? "A group is created, Tap to start chatting"
        timeLbl.text = group?.updatedAt?.toDate(plain: true)?.timeSinceDate()
        
    }
    @IBAction func moreOptionsTapped() {
        if group?.group != nil {
            delegate?.moreButtonTapped(for: group)
        } else {
            delegate?.moreButtonTapped(for: (group?.user)!)
        }
    }
    
  }
