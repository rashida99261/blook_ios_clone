//
//  NewConversationVC.swift
//  bloook
//
//  Created by Tech Astha on 26/02/21.
//

import SwiftUI
import UIKit

class CreateGroupChatVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var userCountLbl: UILabel!
    @IBOutlet var conversationTable: UITableView!
    @IBOutlet var topStatusView: UIView!
    @IBOutlet var groupNameTextField: UITextField!
    @IBOutlet var textFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet var createButton: UIButton!
    
   

    var addedMembers: [User]?
    var selectedGroupId: Int?

    var usersList: [User]?
    var selectedUser = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // conversationTable.reloadData()
        getUserListFromServer()
        // textFieldHeightConstraint.constant = (selectedGroupId != nil) ? 0 : 100
        textFieldHeightConstraint.constant = 0
        createButton.setTitle((selectedGroupId == nil) ? "Chat" : "Add", for: .normal)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        groupNameTextField.text = ""
        selectedUser.removeAll()
        conversationTable.reloadData()
    }

    // MARK: - for All Follow List

    func getUserListFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        //progressView = AcuantProgressView(frame: view.frame, center: view.center)
        showProgressView(text: "Loading...")
        PSWebServiceAPI.getUserList(param, completion: { response, error in

            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.usersList = response?.users
                    DispatchQueue.main.async { [self] in
                        self.userCountLbl.text = "Friends " + "(" + "\(self.usersList?.count ?? 0)" + ")"
                        conversationTable.reloadData()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }

    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersList?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let user = usersList?[indexPath.row] else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GroupChatUserTableViewCell
        cell.delegate = self
        cell.selectionStyle = .none
        let isAdded = addedMembers?.first(where: { $0.id == user.id })
        if isAdded != nil {
            cell.configure(user: user, hideButton: true)
        } else {
            cell.configure(user: user)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let contentView = UIView()

        return contentView
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    @IBAction func backBtnAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        navigationController?.popViewController(animated: true)
    }

    @IBAction func createBtnAction(_ sender: Any) {
        if groupNameTextField.text?.isEmpty == true, addedMembers?.isEmpty == true, selectedUser.count > 1 {
            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter Groupname")
            return
        }
        if selectedUser.isEmpty == true {
            PSUtil.showAlertFromController(controller: self, withMessage: "Please select some users")
            return
        }
        if selectedGroupId == nil {
            print("GrouName = \(groupNameTextField.text) Selected user = \(selectedUser.count)")
            if selectedUser.count == 1 {
                //navigationController?.popViewController(animated: false)
                let msgStoryboard = UIStoryboard(name: "MessageStoryboard", bundle: nil)
                let objChatList = msgStoryboard.instantiateViewController(withIdentifier: "ChatListVC") as! ChatListVC
                objChatList.selectedUser = selectedUser.last
                objChatList.group = nil
                var vcArray = self.navigationController?.viewControllers
                vcArray!.removeLast()
                vcArray!.append(objChatList)
                self.navigationController?.setViewControllers(vcArray!, animated: false)
            } else {
                createGroup()
            }
        } else {
            addMembers()
        }
    }
}

extension CreateGroupChatVC: NewConversationTableViewCellDelegate {
    func chatButtonTapped(user: User, shouldAdd: Bool) {
        groupNameTextField.resignFirstResponder()
        if shouldAdd {
            selectedUser.append(user)
        } else {
            selectedUser.remove(at: selectedUser.firstIndex { $0.id == user.id } ?? 0)
        }
        if selectedGroupId == nil {
            textFieldHeightConstraint.constant = (selectedUser.count < 2) ? 0 : 100
        }
    }
}

extension CreateGroupChatVC {
    func createGroup() {
        groupNameTextField.resignFirstResponder()
        guard let groupName = groupNameTextField.text, !groupName.isEmpty else {
            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter Groupname")
            return
        }
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        var csv = ""
        for i in selectedUser {
            csv = csv + "\(i.id)"
            csv = csv + ","
        }

        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "group_name": "\(groupName)",
                     "members": "\(csv.dropLast() ?? "")",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        showProgressView(text: "Loading...")
        PSWebServiceAPI.createGroup(param, completion: { response, error in

            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    DispatchQueue.main.async {
                        let array = response?.groups
                        let id = array?.first?.group?.groupId ?? array?.first?.id
                        let msgStoryboard = UIStoryboard(name: "MessageStoryboard", bundle: nil)
                        let objChatList = msgStoryboard.instantiateViewController(withIdentifier: "ChatListVC") as! ChatListVC
                        objChatList.group = array?.first?.group ?? Group(groupId: id ?? 0, groupName: self.groupNameTextField.text)
                        var vcArray = self.navigationController?.viewControllers
                                        vcArray!.removeLast()
                                        vcArray!.append(objChatList)
                                        self.navigationController?.setViewControllers(vcArray!, animated: false)
                        //self.navigationController?.pushViewController(objChatList, animated: true)
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }

    func addMembers() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        var csv = ""
        for i in selectedUser {
            csv = csv + "\(i.id)"
            csv = csv + ","
        }

        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "group_id": "\(selectedGroupId ?? 0)",
                     "member_id": "\(csv.dropLast() ?? "")",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
        showProgressView(text: "Loading...")
        PSWebServiceAPI.addMember(param, completion: { response, error in

            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    DispatchQueue.main.async {
                        // self.navigationController?.popToViewController(ofClass: ChatListVC.self)
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
}
