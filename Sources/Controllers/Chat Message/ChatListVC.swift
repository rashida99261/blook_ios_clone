//
//  ChatListVC.swift
//  bloook
//
//  Created by Tech Astha on 19/03/21.
//

import SocketIO
import UIKit
import IQKeyboardManagerSwift

class ChatListVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var chatMessageTable: UITableView!
    var selectedUser: User?
    var group: Group?
    var messages: [Message]?
    var members: [User]?
    @IBOutlet var textField: UITextField!
    @IBOutlet var myProfileImg: UIImageView!
    @IBOutlet var partnerProfileImg: UIImageView!
    @IBOutlet var groupName: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var manager: SocketManager?
    var socket: SocketIOClient?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        if group != nil {
            groupName.text = group?.groupName
            getGroupMembers()
        } else {
            groupName.text = selectedUser?.fullName()
        }
        fetchMEssage()
        configureSocket()
        var strUserProfileImg = selectedUser?.profilePhoto
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            partnerProfileImg.image = UIImage(named: "user")
        } else {
            partnerProfileImg.kf.setImage(with: selectedUser?.profilePhotoURL(), placeholder: UIImage(named: "placeholder.png"))
        }
        strUserProfileImg = LoggedInUser.shared.user?.profilePhoto
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            myProfileImg.image = UIImage(named: "user")
        } else {
            myProfileImg.kf.setImage(with: LoggedInUser.shared.user?.profilePhotoURL(), placeholder: UIImage(named: "placeholder.png"))
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (messages?.count ?? 0) + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ISCustomCell-MyChatDateCell", for: indexPath) as! ISChatTableViewCell
            cell.selectionStyle = .none
            return cell
        }
        let message = messages?[indexPath.row - 1]
        let defult = UserDefaults.standard
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        if strUserId == "\(message?.userID ?? 0)" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ISCustomCell-MyChatCell", for: indexPath) as! ISChatTableViewCell
            cell.selectionStyle = .none
            cell.configure(message: message, user: message?.user ?? selectedUser)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ISCustomCell-OtherChatCell", for: indexPath) as! ISChatTableViewCell
            cell.selectionStyle = .none
            cell.configure(message: message, user: message?.user ?? selectedUser)
        
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func chatSettingBtnAction(_ sender: Any) {
        let msgStoryboard = UIStoryboard(name: "MessageStoryboard", bundle: nil)
        let objChatSettings = msgStoryboard.instantiateViewController(withIdentifier: "ChatSettingsVC") as! ChatSettingsVC
        navigationController?.pushViewController(objChatSettings, animated: true)
    }

    @IBAction func backBtnAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        socketOff()
        socket?.disconnect()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendChatBtnAction(_ sender: Any) {
        if (textField.text?.count)! < 1 {
            PSUtil.showAlertFromController(controller: self, withMessage: "Please enter message")
            return
        } else {
            sendMessage()
        }
    }
    
    override func hideKeyboardWhenTappedAround() {
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            tap.cancelsTouchesInView = false
            chatMessageTable.addGestureRecognizer(tap)
        }
        
    
}

extension ChatListVC {
    func fetchMEssage() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        var param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        if (group?.groupId ?? 0) > 0 {
            param["group_id"] = "\(group?.groupId ?? 0)"
            getGroupMessage(param: param)
        } else {
            param["partner_id"] = "\(selectedUser?.id ?? 0)"
            getGroupMessage(param: param)
        }
    }
    
    func getGroupMembers() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "group_id": "\(group?.groupId ?? 0)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        PSWebServiceAPI.getGroupMembers(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.members = response?.users
                    
                    DispatchQueue.main.async {
                        self.partnerProfileImg.kf.setImage(with: self.members?.first?.profilePhotoURL(), placeholder: UIImage(named: "placeholder.png"))
                        if (self.members?.count ?? 0) > 2 {
                            self.myProfileImg.kf.setImage(with: self.members?.last?.profilePhotoURL(), placeholder: UIImage(named: "placeholder.png"))
                        }
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }

    private func getGroupMessage(param: [String: Any]) {
        PSWebServiceAPI.getGroupMessage(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.messages = response?.messages
                    
                    DispatchQueue.main.async {
                        self.chatMessageTable.reloadData()
                        
                        self.scrollToBottom()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func sendMessage() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        var param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_MESSAGE_FULL: textField.text ?? "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]
        if (group?.groupId ?? 0) > 0 {
            param["group_id"] = "\(group?.groupId ?? 0)"
            // sendGroupMessage(param: param)
        } else {
            param[Constants.KEY_PARTNER_ID] = "\(selectedUser?.id ?? 0)"
            // sendSingleMessage(param: param)
        }
        sendGroupMessage(param: param)
    }

    private func sendSingleMessage(param: [String: Any]) {
        var param = param
        PSWebServiceAPI.sendMessage(param, completion: { response, error in
            if error == nil {
                if response?.status == 200 {
                    param["msgID"] = "\(response?.singleMessage?.id ?? 0)"
                    param["updated_at"] = "\(response?.singleMessage?.updatedAt ?? "")"
                    param["created_at"] = "\(response?.singleMessage?.createdAt ?? "")"
                    self.socket?.emit("messageNotify", param)
                    self.textField.text = ""
                    DispatchQueue.main.async {
                        self.chatMessageTable.reloadData()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    private func sendGroupMessage(param: [String: Any]) {
        var param = param
        PSWebServiceAPI.sendGroupMessage(param, completion: { response, error in
            if error == nil {
                if response?.status == 200 {
                    param["msgID"] = "\(response?.singleMessage?.id ?? 0)"
                    param["updated_at"] = "\(response?.singleMessage?.updatedAt ?? "")"
                    param["created_at"] = "\(response?.singleMessage?.createdAt ?? "")"
                    param["profilePhoto"] = LoggedInUser.shared.user?.profilePhoto
                    self.socket?.emit("messageNotify", param)
                    self.textField.text = ""
                    DispatchQueue.main.async {
                        self.chatMessageTable.reloadData()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
}

extension ChatListVC {
    func configureSocket() {
        manager = SocketManager(socketURL: URL(string: "http://dev.bloook.com:8080")!, config: [.log(true), .compress])
        socket = manager?.defaultSocket
        guard let socket = socket else { return }
        socket.on(clientEvent: .connect) { _, _ in
            print("socket connected")
        }
        socketOn()

        socket.connect()
    }
    
    private func socketOn() {
        guard let socket = socket else { return }
        let defult = UserDefaults.standard
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        
        if selectedUser != nil {
            let singleMessage = "messageNotify" + "\(selectedUser?.id ?? 0)" + strUserId
            socket.on(singleMessage) { data, _ in
                guard let temp = data[0] as? NSDictionary else { return }
                self.processReceivedMessage(message: temp)
            }
        } else {
            let groupMessage = "messageNotify" + "\(group?.groupId ?? 0)"
            socket.on(groupMessage) { data, _ in
                guard let temp = data[0] as? NSDictionary else { return }
                self.processReceivedMessage(message: temp)
            }
        }
        socket.on("messageNotify") { data, _ in
            print(data)
        }
//        socket.on("newMessageNotification" + strUserId) { data, _ in
//            guard let temp = data[0] as? NSDictionary else { return }
//            self.processReceivedMessage(message: temp)
//        }
        
//        socket.on(receivedMessage) { data, _ in
//            guard let temp = data[0] as? NSDictionary else { return }
//            self.processReceivedMessage(message: temp)
//        }
//
//        socket.on("newMessageNotificationundefined") { data, _ in
//            guard let temp = data[0] as? NSDictionary else { return }
//            self.processReceivedMessage(message: temp)
//        }
    }

    private func socketOff() {
        guard let socket = socket else { return }
        let defult = UserDefaults.standard
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let receivedMessage = "messageNotify" + strUserId + "\(selectedUser?.id ?? 0)"
        let sentMessage = "messageNotify" + "\(selectedUser?.id ?? 0)" + strUserId
        socket.on("messageNotify") { data, _ in
            print(data)
        }
        socket.off("newMessageNotification" + strUserId)
        
        socket.off(sentMessage)

        socket.off(receivedMessage)
        
        socket.off("newMessageNotificationundefined")
    }

    private func processReceivedMessage(message: NSDictionary?) {
        let defult = UserDefaults.standard
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        guard let temp = message else { return }
        let id = temp["msgID"] as? String
        let userId = temp["user_id"] as? String
        let partnerId = temp["partner_id"] as? String
        let message = temp["message"] as? String
        let updatedAt = temp["updated_at"] as? String
        let createdAt = temp["created_at"] as? String
        let profilePhoto = temp["profilePhoto"] as? String
        var messagedUSer = LoggedInUser.shared.user
        messagedUSer?.profilePhoto = profilePhoto
        let messageObject = Message(id: Int(id ?? ""), userID: Int(userId ?? ""), partnerID: Int(partnerId ?? ""), message: message, createdAt: createdAt, updatedAt: updatedAt, deletedAt: nil, user: messagedUSer)
        if messages == nil {
            messages = [Message]()
        }
        messages?.append(messageObject)
        DispatchQueue.main.async {
            self.chatMessageTable.reloadData()
            self.scrollToBottom()
        }
        
        if socket?.status == .connected {
            if strUserId == temp["partner_id"] as? String {
                let strM = temp["msgID"] as? String
                socket?.emit("readMessage", strM!)
            } else if strUserId == temp["group_id"] as? String {
                let strM = temp["msgID"] as? String
                socket?.emit("readMessage", strM!)
            }
        }
    }
    
    func scrollToBottom() {
        DispatchQueue.main.async {
            if self.messages?.isEmpty == false {
                let indexPath = IndexPath(row: self.messages?.count ?? 1, section: 0)
                self.chatMessageTable.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
}

extension ChatListVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        bottomConstraint.constant = 332
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.scrollToBottom()
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        bottomConstraint.constant = 0
    }
    
}
