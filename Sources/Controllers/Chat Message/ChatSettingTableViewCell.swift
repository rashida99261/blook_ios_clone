//
//  ChatSettingTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 19/03/21.
//

import UIKit
protocol ChatSettingTableViewCellDelegate {
    func moreButtonTapped(user: User)
}

class ChatSettingTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var moreView: UIView!
    var delegate: ChatSettingTableViewCellDelegate?
    var user: User?
    
    @IBOutlet weak var adminCreateActionBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(user: User, isAdminUser: Bool){
        self.user = user
        userNameLabel.text = user.fullName() + (((user.isAdmin ?? 0) != 0) ? " (Admin)" : " ")
        let isAdminUser = isAdminUser
        moreView.isHidden = !isAdminUser
        let strUserProfileImg = user.profilePhoto
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            self.profileImage.image = UIImage(named: "user")

        } else {
            self.profileImage.sd_setImage(with: URL(string: strUserProfileImg!), placeholderImage: UIImage(named: "placeholder.png"))
        }
    }
    
    @IBAction func moreOptionsTapped(){
        guard let user = user else { return }
        delegate?.moreButtonTapped(user: user)
    }
}
