//
//  NewConversationTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 26/02/21.
//

import UIKit
protocol NewConversationTableViewCellDelegate {
    func chatButtonTapped(user: User, shouldAdd: Bool)
}
    
class GroupChatUserTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var button: UIButton!
    var delegate: NewConversationTableViewCellDelegate?
    var user: User?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        button.isSelected = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func configure(user: User?, hideButton: Bool = false) {
        self.user = user
        userNameLbl?.text = user?.fullName() ?? "Anonymous"
        let strUserProfileImg = user?.profilePhoto
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            userImage.image = UIImage(named: "user")
        } else {
            let url = URL(string: strUserProfileImg ?? "")
            userImage.kf.setImage(with: url, placeholder: UIImage(named: "user"))
        }
        button.isHidden = hideButton
        button.superview?.isHidden = hideButton
        
    }
    
    @IBAction func chatButtonTapped() {
        button.isSelected = !button.isSelected
        guard let user = user else { return }
        delegate?.chatButtonTapped(user: user, shouldAdd: button.isSelected)
    }
}
