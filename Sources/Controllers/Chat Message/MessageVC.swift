//
//  MessageVC.swift
//  bloook
//
//  Created by Tech Astha on 25/02/21.
//

import UIKit
import Toaster

class MessageVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var messageTable: UITableView!
    @IBOutlet var unreadBtn: UIButton!
    @IBOutlet var allMessageBtn: UIButton!
    
    
    var usersList: [User]?
    var groupList: [GroupModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        unreadBtn.setTitleColor(.black, for: UIControl.State.normal)
        allMessageBtn.setTitleColor(.lightGray, for: UIControl.State.normal)
        // getChatUserListFromServer()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getChatGroupListFromServer()
    }
    
    @IBAction func unReadBtnAction(_ sender: UIButton) {
        unreadBtn.setTitleColor(.black, for: UIControl.State.normal)
        allMessageBtn.setTitleColor(.lightGray, for: UIControl.State.normal)
    }

    @IBAction func allMessageBtnAction(_ sender: UIButton) {
        unreadBtn.setTitleColor(.lightGray, for: UIControl.State.normal)
        allMessageBtn.setTitleColor(.black, for: UIControl.State.normal)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ((usersList?.count ?? 0) + (groupList?.count ?? 0))
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MessageTableViewCell
        cell.delegate = self
        if indexPath.row < usersList?.count ?? 0 {
            let user = usersList?[indexPath.row]
            // cell.configure(user: user)
        } else {
            let group = groupList?[indexPath.row - (usersList?.count ?? 0)]
            cell.configure(group: group)
        }
        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tabBarController?.tabBar.isHidden = true
        let cell = tableView.cellForRow(at: indexPath) as? MessageTableViewCell
        let msgStoryboard = UIStoryboard(name: "MessageStoryboard", bundle: nil)
        let objChatList = msgStoryboard.instantiateViewController(withIdentifier: "ChatListVC") as! ChatListVC
        
        if cell?.group?.group == nil {
            objChatList.selectedUser = cell?.group?.user
            objChatList.group = nil
        } else {
            objChatList.selectedUser = nil
            objChatList.group = cell?.group?.group
        }
        navigationController?.pushViewController(objChatList, animated: true)
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        navigationController?.popViewController(animated: true)
    }

    @objc func selectActionBtn(_ sender: UIButton) {
        let buttonRow = sender.tag
        print(buttonRow)
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // alert.view.tintColor = UIColor.black0
      
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { _ in
            PSUtil.showAlertFromController(controller: self, withMessage: "Delete Selection to Here")
         
        }))
        
        alert.addAction(UIAlertAction(title: "Block User", style: .default, handler: { _ in
           // blockUser(user: user)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
         
            print("User click Cancel button")
        }))
        
        present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @IBAction func plusConversationBtnAction(_ sender: Any) {
        let msgStoryboard = UIStoryboard(name: "MessageStoryboard", bundle: nil)
        let objNewConversation = msgStoryboard.instantiateViewController(withIdentifier: "NewConversationVC") as! CreateGroupChatVC
        navigationController?.pushViewController(objNewConversation, animated: true)
    }

    func getChatUserListFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        PSWebServiceAPI.getChatUserList(param, completion: { response, error in

            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.usersList = response?.users
                    DispatchQueue.main.async { [self] in
                        // self.userCountLbl.text = "Friends " + "(" + "\(self.usersList?.count ?? 0)" + ")"
                        messageTable.reloadData()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
    
    func getChatGroupListFromServer(silent: Bool = false) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        if !silent {
            showProgressView(text: "Loading...")
        }
        PSWebServiceAPI.getGroupList(param, completion: { response, error in

            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    self.groupList = response?.groups
                    DispatchQueue.main.async { [self] in
                        // self.userCountLbl.text = "Friends " + "(" + "\(self.usersList?.count ?? 0)" + ")"
                        messageTable.reloadData()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
}

extension MessageVC: MessageTableViewCellDelegate {
    func moreButtonTapped(for user: User) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // alert.view.tintColor = UIColor.black
              
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { _ in
            self.deleteConversation(user: user)
        }))
                
        alert.addAction(UIAlertAction(title: "Block User", style: .default, handler: { _ in
            self.blockUser(user: user)
                    
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                 
            print("User click Cancel button")
        }))
                
        present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func moreButtonTapped(for group: GroupModel?) {
        let msgStoryboard = UIStoryboard(name: "MessageStoryboard", bundle: nil)
        let objChatSettings = msgStoryboard.instantiateViewController(withIdentifier: "ChatSettingsVC") as! ChatSettingsVC
        objChatSettings.groupModel = group
        navigationController?.pushViewController(objChatSettings, animated: true)
    }
}


extension MessageVC {
    func blockUser(user: User?) {
        guard let user = user else { return }
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_TO_USER_ID: "\(user.id)" ,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        //  PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text: "Loading...")
        PSWebServiceAPI.doBlockedFriend(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                
                if Status == 200 {
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    DispatchQueue.main.async { [self] in
                        self.getChatGroupListFromServer()
                        Toast(text: msg).show()
                    }

                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
    
    func deleteConversation(user: User?) {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     "partner_id": "\(user?.id ?? 0)",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        showProgressView(text: "Loading...")
        PSWebServiceAPI.deleteConversation(param, completion: { response, error in
            
            self.hideProgressView()
            if error == nil {
                if response?.status == 200 {
                    let msg = response?.message
                    DispatchQueue.main.async {
                        //self.navigationController?.popViewController(animated: true)
                        Toast(text: msg).show()
                    }
                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response?.message
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: error?.localizedDescription ?? PSAPI.SomeThingWrong)
            }
        })
    }
}
