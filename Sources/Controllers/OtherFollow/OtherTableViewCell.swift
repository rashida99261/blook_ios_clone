//
//  OtherTableViewCell.swift
//  bloook
//
//  Created by Tech Astha on 17/02/21.
//

import UIKit

class OtherTableViewCell: UITableViewCell {
    @IBOutlet weak var userProfileImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var followClickBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
