//
//  OtherFollowListVC.swift
//  bloook
//
//  Created by Tech Astha on 16/02/21.
//

import UIKit

class OtherFollowListVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var strFollowUserId = String()
    
    @IBOutlet weak var followTableView: UITableView!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var followingBtn: UIButton!
    @IBOutlet weak var followCountLbl: UILabel!
    
    @IBOutlet weak var viewLoader:UIView!
    @IBOutlet weak var loadGif:loaderxib!
    
   // var progressView : AcuantProgressView!
    var followArray = Array<Any>()
    var followingArray = Array<Any>()
    var strBtnId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoader.isHidden = true
        print(strFollowUserId)
        followBtn.setTitleColor(.black, for: UIControl.State.normal)
        followingBtn.setTitleColor(.lightGray, for: UIControl.State.normal)
        doFollowUserListFromServer()
    }
    //MARK:- for All Follow List
    func doFollowUserListFromServer() {

        guard PSUtil.reachable() else
        {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        let param = [Constants.KEY_MYUSER_ID:strUserId,
                     Constants.KEY_FOLLOW_ID:strFollowUserId,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

       //PSUtil.showProgressHUD()
       // self.progressView = AcuantProgressView(frame: self.view.frame, center: self.view.center)
        self.showProgressView(text:  "Loading...")
        PSWebServiceAPI.doFollowUserList(param, completion: { (response) in
            //PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let dict = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    self.followArray = dict[Constants.KEY_FOLLOWERS] as? [Any] ?? [Any]()
                    self.followingArray = dict[Constants.KEY_FOLLOWING] as? [Any] ?? [Any]()
                    if self.followArray.isEmpty {
                        self.followCountLbl.text = "Followers (0)"
                    }else{
                        self.followCountLbl.text = "Followers" + " (" + "\(self.followArray.count)" + ")"
                    }
//                    print(self.postDataArr.count)
                    self.followTableView.reloadData()
                 }else {

                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            }else{
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
     }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if strBtnId == "0" || strBtnId == ""{
            return followArray.count
        }else{
            return followingArray.count
       }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var dict = NSDictionary()
        var toUserDic = NSDictionary()
        if strBtnId == "0" || strBtnId == ""{
          
            dict = self.followArray[indexPath.row] as? NSDictionary ?? NSDictionary()
            toUserDic = dict[Constants.KEY_FROM_USER_ID] as? NSDictionary ?? NSDictionary()
        }else{
            dict = self.followingArray[indexPath.row] as? NSDictionary ?? NSDictionary()
            toUserDic = dict[Constants.KEY_TO_USER_ID] as? NSDictionary ?? NSDictionary()
        }
      
       let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! OtherTableViewCell
            cell.selectionStyle = .none
            cell.userNameLbl.text = "\(toUserDic.value(forKey: Constants.KEY_NAME) ?? "")"
            let strUserProfileImg = "\(toUserDic.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
            if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
                cell.userProfileImg.image = UIImage (named: "user")
            }else{
                cell.userProfileImg.sd_setImage(with: URL(string: strUserProfileImg), placeholderImage: UIImage(named: "placeholder.png"))
            }
        cell.followClickBtn.tag = indexPath.row
        cell.followClickBtn.addTarget(self, action: #selector(followBlockBtnAction(_:)), for: UIControl.Event.touchUpInside)
       return cell
    }
    @objc func followBlockBtnAction( _ sender :UIButton){
        let buttonRow = sender.tag
        print(buttonRow)
        
        let alert = UIAlertController(title:nil, message: nil, preferredStyle: .actionSheet)
        //alert.view.tintColor = UIColor.black
      
        alert.addAction(UIAlertAction(title: "Follow", style: .default, handler: { (_) in
            PSUtil.showAlertFromController(controller: self, withMessage: "Follow User Selection to Here")
         
        }))
        
        alert.addAction(UIAlertAction(title: "Block/Remove", style: .default, handler: { (_) in
            PSUtil.showAlertFromController(controller: self, withMessage: "Block follow User to Here")
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
         
            print("User click Cancel button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
        strBtnId = "0"
        if followArray.isEmpty{
            followCountLbl.text = "Followers (0)"
        }else{
        followCountLbl.text = "Followers" + " (" + "\(followArray.count)" + ")"
        }
        followTableView.reloadData()
     }

    @IBAction func followBtnAction(_ sender: Any) {
        followBtn.setTitleColor(.black, for: UIControl.State.normal)
        followingBtn.setTitleColor(.lightGray, for: UIControl.State.normal)
        strBtnId = "0"
        if followArray.isEmpty{
            followCountLbl.text = "Followers (0)"
        }else{
            followCountLbl.text = "Followers" + " (" + "\(followArray.count)" + ")"
        }
        followTableView.reloadData()
    }
    @IBAction func followingBtnAction(_ sender: Any) {
        followBtn.setTitleColor(.lightGray, for: UIControl.State.normal)
        followingBtn.setTitleColor(.black, for: UIControl.State.normal)
        strBtnId = "1"
        
        if followingArray.isEmpty{
        followCountLbl.text = "Following (0)"
        }else{
        followCountLbl.text = "Following" + " (" + "\(followingArray.count)" + ")"
        }
        followTableView.reloadData()
        
    }
    private func showProgressView(text:String = ""){
        DispatchQueue.main.async {
            //self.progressView.messageView.text = text
           // self.progressView.startAnimation()
           // self.view.addSubview(self.progressView)
            self.viewLoader.isHidden = false
            self.loadGif.animate()
        }
    }
    
   private func hideProgressView(){
        DispatchQueue.main.async {
           // self.progressView.stopAnimation()
           // self.progressView.removeFromSuperview()
            self.viewLoader.isHidden = true
        }
    }
}
