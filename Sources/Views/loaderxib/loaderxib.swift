//
//  loaderxib.swift
//  Bloook
//
//  Created by Rashida on 29/03/22.
//

import UIKit
import Gifu

class loaderxib: UIView {
    
    @IBOutlet weak var viewMain : UIView!
    //@IBOutlet weak var viewAnimate : AnimatableCircleView!
    @IBOutlet weak var loadIcon : GIFImageView!

    let nibName = "loaderxib"
    var contentView: UIView?
    
    override public init(frame: CGRect)
    {
        super.init(frame: frame)

        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])

        contentView = view
        view.frame = self.bounds
        
       

    }


  
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])

        contentView = view
        view.frame = self.bounds
        
        
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func animate() {
        loadIcon.animate(withGIFNamed: "loader_icon", animationBlock:  {
      })
    }
    

}
