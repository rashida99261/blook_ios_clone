//
//  PostsSectionHeaderView.swift
//  Bloook
//
//  Created by Suresh Varma on 08/07/21.
//

import UIKit

import AVFoundation
import Kingfisher

/*
class PostsSectionHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var imageHeightConstant: NSLayoutConstraint!
    var delegate: PostsDelegate?
    @IBOutlet weak var currentUserView: CurrentUserView!
//    @IBOutlet weak var imageContainerView: UIView!
    static let reuseIdentifier = String(describing: PostsSectionHeaderView.self)
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var roundView: UIView!
    var videoURL: URL?

    @IBOutlet weak var playButton: UIButton!
    var post: Post?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.kf.cancelDownloadTask()
        imageView.kf.setImage(with: URL(string: ""))
        imageView.image = UIImage(named: "postPlaceholder")
        currentUserView.prepareForReuse()
    }

    func configure(post: Post?) {
        self.post = post
        tintColor = .clear
        backgroundView = nil
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTabBtnAction(_:)))
        imageView.addGestureRecognizer(tapGesture)
        currentUserView.configure(post: post)
        currentUserView.delegate = self
        if post?.type == .image {
            showImage(for: post)
        } else if post?.type == .video {
            showVideo(for: post)
        } else {
            showBlog()
        }
    }

    private func showVideo(for post: Post?) {
        playButton.isHidden = false
        let strFileListImg = post?.filename ?? ""
        if strFileListImg.isValidString() {
            imageHeightConstant.constant = imageView.frame.size.width

            loadThumbnailFromCache(urlString: strFileListImg)
        }
    }

    private func loadThumbnailFromCache(urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        KingfisherManager.shared.retrieveImage(with: url,
                                               options: nil,
                                               progressBlock: nil) { [self] (result) -> Void in
            switch result {
                case .success(let value):
                    self.imageView.image = value.image
                case .failure(let error):
                    print("Error: \(error)")
                    self.getThumbnailImageFromVideoUrl(url: url) { _ in
                    }
            }
        }
    }

    private func showImage(for post: Post?) {
        playButton.isHidden = true
        let strFileListImg = post?.filename ?? ""
        if strFileListImg.isValidString() {
            let placeholderImage = UIImage(named: "postPlaceholder")
            imageView.kf.setImage(with: URL(string: strFileListImg), placeholder: placeholderImage)
        }
        imageHeightConstant.constant = imageView.frame.size.width
    }

    private func showBlog() {
        playButton.isHidden = true
        imageHeightConstant.constant = 0
    }
}

extension PostsSectionHeaderView {
    @IBAction func imageTabBtnAction(_ sender: UITapGestureRecognizer) {
        if imageView.image == UIImage(named: "postPlaceholder"), post?.type == .image {
            return
        }
        delegate?.imageTapped(post: post)
    }

    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?) -> Void)) {
        DispatchQueue.global().async { // 1
            let asset = AVAsset(url: url) // 2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) // 3
            avAssetImageGenerator.appliesPreferredTrackTransform = true // 4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) // 5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) // 6
                let thumbNailImage = UIImage(cgImage: cgThumbImage) // 7
                DispatchQueue.main.async { [self] in // 8
                    completion(thumbNailImage) // 9
                    ImageCache.default.store(thumbNailImage, forKey: url.absoluteString)
                    self.loadThumbnailFromCache(urlString: url.absoluteString)
                }
            } catch {
                print(error.localizedDescription) // 10
                DispatchQueue.main.async {
                    completion(nil) // 11
                }
            }
        }
    }
}

extension PostsSectionHeaderView: UserViewDelegate {
    func moreOptionsTapped(alert: UIAlertController) {
        delegate?.moreOptionsTapped(alert: alert)
    }
    
    func treasureOptionsTapped(post: Post) {
        delegate?.treasureTapped(post: post)
    }
    
    func profileImageTapped(post: Post) {
        delegate?.profileImageTapped(post: post)
    }
}
*/
