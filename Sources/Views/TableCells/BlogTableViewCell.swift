//
//  PostTableViewCell.swift
//  Bloook
//
//  Created by Suresh Varma on 18/07/21.
//

import AVFoundation
import Kingfisher
import UIKit
protocol BlogTableViewDelegate {
    func profileImageTapped(post: Post?)
}
class BlogTableViewCell: UITableViewCell {
    @IBOutlet var currentUserView: CurrentUserView!
    static let reuseIdentifier = String(describing: BlogTableViewCell.self)
    var post: Post?
    var delegate: BlogTableViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        currentUserView.prepareForReuse()
    }


    func configure(post: Post?) {
        self.post = post
        tintColor = .clear
        backgroundView = nil
        currentUserView.hideOptions = true
        currentUserView.configure(post: post)
        currentUserView.delegate = self
    }

}

extension BlogTableViewCell: CurrentUserViewDelegate {
    func moreOptionsTapped(alert: UIAlertController) {
        
    }
    
    func treasureOptionsTapped(post: Post) {
        
    }
    
    func profileImageTapped(post: Post) {
        delegate?.profileImageTapped(post: post)
    }
    
    func showAlert(with message: String) {
        
    }
    
    func postOption(post: Post, optionType: OptionType) {
        
    }
    
    
}
