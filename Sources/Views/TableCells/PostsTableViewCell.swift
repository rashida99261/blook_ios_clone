//
//  PostTableViewCell.swift
//  Bloook
//
//  Created by Suresh Varma on 18/07/21.
//

import AVFoundation
import gooey_cell
import Kingfisher
import UIKit

protocol PostsDelegate {
    func imageTapped(post: Post?)
    func moreOptionsTapped(alert: UIAlertController)
    func treasureTapped(post: Post?)
    func profileImageTapped(post: Post?)
    func showAlert(message: String)
    func postOption(post: Post, option: OptionType)
}

class PostsTableViewCell: GooeyEffectTableViewCell, ASAutoPlayVideoLayerContainer {
    @IBOutlet var imageHeightConstant: NSLayoutConstraint!
    var delegate: PostsDelegate?
    var footerDelegate: LikeSectionDelegate?
    @IBOutlet var currentUserView: CurrentUserView!
    static let reuseIdentifier = String(describing: PostsTableViewCell.self)
    @IBOutlet var postImageView: UIImageView!
    @IBOutlet var roundView: UIView!
    @IBOutlet var footerView: PostsSectionFooterView!

    @IBOutlet var playButton: UIButton!

    @IBOutlet var muteButton: UIButton!
    var post: Post?
    var isAlreadyViewed: Bool = false
    
    var playerController: ASVideoPlayerController?
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    var videoURL: String? {
        didSet {
            if let videoURL = videoURL {
                ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL)
            }
            videoLayer.isHidden = videoURL == nil
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let inset: CGFloat = 10
        muteButton.contentEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
        muteButton.isHidden = true
    }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        postImageView.kf.cancelDownloadTask()
        postImageView.kf.setImage(with: URL(string: ""))
        postImageView.image = UIImage(named: "postPlaceholder")
        currentUserView.prepareForReuse()
        enableTreasure(enable: false)
        isAlreadyViewed = false
        videoURL = nil
        videoLayer.removeFromSuperlayer()
    }

    func enableTreasure(enable: Bool = true) {
        currentUserView.disableTreasure(disable: !enable)
    }

    func configure(post: Post?) {
        self.post = post
        tintColor = .clear
        backgroundView = nil
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTabBtnAction(_:)))
        postImageView.addGestureRecognizer(tapGesture)
        currentUserView.configure(post: post)
        currentUserView.delegate = self
        footerView.configure(post: post)
        footerView.delegate = self
//
        if post?.type == .image {
            showImage(for: post)
        } else if post?.type == .video {
            showVideo(for: post)
        } else {
            showBlog()
        }
    }

    func updatePostCount(count: Int) {
        isAlreadyViewed = true
        footerView.updateViewCount(count: count)
    }

    private func showVideo(for post: Post?) {
        let image = UIImage(named: ASVideoPlayerController.sharedVideoPlayer.isMute() ? "mutePlayer" : "unmutePlayer")
        muteButton.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
        muteButton.tintColor = UIColor.white
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        postImageView.layer.addSublayer(videoLayer)
        
        playButton.isHidden = true
        muteButton.isHidden = false
        imageHeightConstant.constant = 300
        let strFileListImg = post?.filename ?? ""
        let strThumnailImg = post?.thumbnail ?? ""
        if strFileListImg.isValidString() {
            self.videoURL = strFileListImg
            imageHeightConstant.constant = 300//postImageView.frame.size.width
            let placeholderImage = UIImage(named: "postPlaceholder")
            postImageView.kf.setImage(with: URL(string: strThumnailImg), placeholder: placeholderImage)
        }
    }

    private func showImage(for post: Post?) {
        videoLayer.removeFromSuperlayer()
        playButton.isHidden = true
        muteButton.isHidden = true
        let strFileListImg = post?.thumbnail ?? ""
        let fileName = post?.filename ?? ""
        var placeholderImage = UIImage(named: "postPlaceholder")
        if strFileListImg.isValidString() {
            postImageView.kf.setImage(with: URL(string: strFileListImg), placeholder: placeholderImage)
        } else {
            if fileName.isValidString() {
                self.postImageView.kf.setImage(with: URL(string: fileName), placeholder: placeholderImage)
            }
        }
        

        

        guard let url = URL(string: strFileListImg) else {
            return
        }
        KingfisherManager.shared.retrieveImage(with: url,
                                               options: nil,
                                               progressBlock: nil) { (result) -> Void in
            switch result {
                case .success(let value):
                    placeholderImage = value.image
                    if fileName.isValidString() {
                        self.postImageView.kf.setImage(with: URL(string: fileName), placeholder: placeholderImage)
                    }
                case .failure(let error):
                    print("Error: \(error)")
            }
        }
        

        imageHeightConstant.constant = 300//postImageView.frame.size.width
    }

    private func showBlog() {
        playButton.isHidden = true
        muteButton.isHidden = true
        imageHeightConstant.constant = 0
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let horizontalMargin: CGFloat = 16
        let width: CGFloat = bounds.size.width - horizontalMargin * 2
        let height: CGFloat = 300//(width * 0.9).rounded(.up)
        videoLayer.frame = CGRect(x: 0, y: 0, width: width, height: height)
    }
    
    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(postImageView.frame, from:postImageView)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
             return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.height
    }
}

extension PostsTableViewCell {
    @IBAction func imageTabBtnAction(_ sender: UITapGestureRecognizer) {
        if postImageView.image == UIImage(named: "postPlaceholder"), post?.type == .image {
            return
        }
        delegate?.imageTapped(post: post)
    }

    @IBAction func muteButtonTapped(_ sender: UIButton) {
        ASVideoPlayerController.sharedVideoPlayer.muteUnmute()
        let image = UIImage(named: ASVideoPlayerController.sharedVideoPlayer.isMute() ? "mutePlayer" : "unmutePlayer")
        muteButton.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
        muteButton.tintColor = UIColor.white
        print("Mute tapped")
    }
}

extension PostsTableViewCell: CurrentUserViewDelegate {
    func postOption(post: Post, optionType: OptionType) {
        delegate?.postOption(post: post, option: optionType)
    }

    func showAlert(with message: String) {
        delegate?.showAlert(message: message)
    }

    func moreOptionsTapped(alert: UIAlertController) {
        delegate?.moreOptionsTapped(alert: alert)
    }

    func treasureOptionsTapped(post: Post) {
        delegate?.treasureTapped(post: post)
    }

    func profileImageTapped(post: Post) {
        delegate?.profileImageTapped(post: post)
    }
}

extension PostsTableViewCell: LikeSectionDelegate {
    func likeButtonTapped() {
        footerDelegate?.likeButtonTapped()
    }

    func openLikeButtonTapped(postId: String) {
        footerDelegate?.openLikeButtonTapped(postId: postId)
    }

    func openCommentsTapped(postId: String) {
        footerDelegate?.openCommentsTapped(postId: postId)
    }

    func shareButtonTapped(image: UIImage?, post: String?) {
        footerDelegate?.shareButtonTapped(image: image, post: post)
    }
}
