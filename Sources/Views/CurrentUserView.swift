//
//  CurrentUserView.swift
//  Bloook
//
//  Created by Suresh Varma on 14/07/21.
//

import Pulsator
import UIKit
import Gifu

enum OptionType: String {
    case delete = "Delete"
    case edit = "Edit"
    case share = "Share"
    case turnComment = "Turn off Comment"
    case follow = "Follow User"
    case report = "Report"
    case block = "Block User"
}

protocol CurrentUserViewDelegate {
    func moreOptionsTapped(alert: UIAlertController)
    func treasureOptionsTapped(post: Post)
    func profileImageTapped(post: Post)
    func showAlert(with message: String)
    
    func postOption(post: Post, optionType: OptionType)
}

class CurrentUserView: UIView {
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var profileImg: UIImageView!
    @IBOutlet var nameProfileLbl: UILabel!
    @IBOutlet var createDateLbl: UILabel!
    @IBOutlet var moreButton: UIButton!
    @IBOutlet var treasureButton: UIButton!
    @IBOutlet var sharedViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var shareLabel: UILabel!
    @IBOutlet var moneyBagGif: GIFImageView!
    @IBOutlet var treasureView: UIView!
    
    var delegate: CurrentUserViewDelegate?
    var pulsator: Pulsator?
    var post: Post?
    var hideOptions = false
    override func awakeFromNib() {
        super.awakeFromNib()
        disableTreasureButton()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped(_:)))
        profileImg.addGestureRecognizer(tapGesture)
        sharedViewHeightConstraint.constant = 0
        
        moneyBagGif.animate(withGIFNamed: "money_bag", animationBlock:  {
        })
    }

    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return Bundle.main.loadNibNamed("CurrentUserView", owner: nil, options: nil)?.first
    }

    func prepareForReuse() {
        profileImg.kf.cancelDownloadTask() // first, cancel currenct download task
        profileImg.kf.setImage(with: URL(string: ""), placeholder: UIImage(named: "user")) // second, prevent kingfisher from setting previous image
        profileImg.image = UIImage(named: "user")
        pulsator?.stop()
    }

    func configure(post: Post?, showtreasureIcon: Bool = true) {
        moreButton.isHidden = hideOptions
        treasureButton.isHidden = !showtreasureIcon
        treasureView.isHidden = !showtreasureIcon
        
        self.post = post
        let strUserProfileImg = post?.user?.profilePhoto
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            profileImg.image = UIImage(named: "user")
        } else {
            let url = URL(string: strUserProfileImg ?? "")
            profileImg.kf.setImage(with: url, placeholder: UIImage(named: "user"))
        }
        nameProfileLbl.text = post?.user?.fullName() ?? "Anonymous"
        createDateLbl.text = post?.createdAt?.dateWithoutTimeComponent()
        descriptionLbl.text = post?.dDescription ?? "-"
        if post?.user?.bounceStatus?.boolValue() == true {
            if pulsator == nil {
                pulsator = Pulsator()
            }
            profileImg.startPulse(pulsator: pulsator!)
        }
        if post?.shares != nil {
            sharedViewHeightConstraint.constant = 30
            shareLabel.text = "\(post?.shares?.first?.user?.fullName() ?? "Anonymous") Reshared"
        } else {
            sharedViewHeightConstraint.constant =  0
        }
    }
    
    func disableTreasure(disable: Bool = true) {
        disableTreasureButton(disable: disable)
//        #if DEBUG
//            disableTreasureButton(disable: false)
//        if pulsator == nil  {
//            pulsator = Pulsator()
//        }
//            profileImg.startPulse(pulsator: pulsator!)
//        #endif
    }
    
    private func disableTreasureButton(disable: Bool = true) {
        let disabledColor = Color.clear
        let enabledColor = Color.blue
        treasureButton.isEnabled = !disable
        let image = UIImage(named: "treasure")
        treasureButton.setImage(image?.maskWithColor(color: disable ? disabledColor : enabledColor), for: .normal)
        treasureButton.backgroundColor = disable ? disabledColor : .white
        treasureButton.layer.borderWidth = 1
        treasureButton.layer.borderColor = disable ? disabledColor.cgColor : enabledColor.cgColor
    }
}

// Should not be the part of this view But :(
extension CurrentUserView {
    @IBAction func moreActionTapped() {
        var selfPost = false
        if post?.ownPost != nil {
            selfPost = post?.ownPost?.boolValue() ?? false
        } else {
            let defult = UserDefaults.standard
            let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
            selfPost = strUserId == "\(post?.userID ?? 0)"
        }
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if selfPost {
            alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { _ in
                self.delegate?.postOption(post: self.post!, optionType: .delete)
            }))
            if post?.isShared?.boolValue() == true {
                alert.addAction(UIAlertAction(title: "Edit Post", style: .default, handler: { _ in
                    self.delegate?.postOption(post: self.post!, optionType: .edit)
                }))
            }
        
            alert.addAction(UIAlertAction(title: "Share", style: .default, handler: { _ in
                self.delegate?.postOption(post: self.post!, optionType: .share)
        
            }))
            if post?.commentStatus?.boolValue() == true {
                alert.addAction(UIAlertAction(title: "Turn off Comment", style: .default, handler: { _ in
                    self.delegate?.postOption(post: self.post!, optionType: .turnComment)
                }))
            } else {
                alert.addAction(UIAlertAction(title: "Turn on Comment", style: .default, handler: { _ in
                    self.delegate?.postOption(post: self.post!, optionType: .turnComment)
                }))
            }
        
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
        
                print("User click Cancel button")
            }))
        } else {
            var title = "Follow User"
            if post?.isFollowed?.boolValue() == true {
                title = "Unfollow User"
            }
            alert.addAction(UIAlertAction(title: title, style: .default, handler: { _ in
                self.delegate?.postOption(post: self.post!, optionType: .follow)
            }))
    
            alert.addAction(UIAlertAction(title: "Share Post", style: .default, handler: { _ in
                self.delegate?.postOption(post: self.post!, optionType: .share)
            }))
    
            alert.addAction(UIAlertAction(title: "Report", style: .default, handler: { _ in
                self.delegate?.postOption(post: self.post!, optionType: .report)
    
            }))
    
            alert.addAction(UIAlertAction(title: "Block User", style: .default, handler: { _ in
                self.delegate?.postOption(post: self.post!, optionType: .block)
            }))
    
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
    
                print("User click Cancel button")
            }))
        }
        delegate?.moreOptionsTapped(alert: alert)
    }
    
    @IBAction func treasureButtonTapped() {
        delegate?.treasureOptionsTapped(post: post!)
    }
    
    @objc func profileImageTapped(_ sender: UITapGestureRecognizer?) {
        delegate?.profileImageTapped(post: post!)
    }
    
    @IBAction func profileImageTapped() {
        profileImageTapped(nil)
    }
}
