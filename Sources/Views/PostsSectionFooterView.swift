//
//  PostsSectionFooterView.swift
//  Bloook
//
//  Created by Suresh Varma on 08/07/21.
//

import Kingfisher
import UIKit
protocol LikeSectionDelegate {
    func likeButtonTapped()
    func openLikeButtonTapped(postId: String)
    func openCommentsTapped(postId: String)
    func shareButtonTapped(image: UIImage?, post: String?)
}

class PostsSectionFooterView: UIView {
    static let reuseIdentifier = String(describing: PostsSectionFooterView.self)

    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var allTimeView: UIView!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var dailyCountLabel: UILabel!
    @IBOutlet weak var annualCountLabel: UILabel!
    @IBOutlet weak var lifetimeCountLabel: UILabel!
    @IBOutlet weak var totalCountLabel: UILabel!
    
    @IBOutlet weak var viewsCountLabel: UILabel!
    var post: Post?
    var currentLikeStatus = "0"
    var currentLikesCount = 0
    var delegate: LikeSectionDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return Bundle.main.loadNibNamed("PostsSectionFooterView", owner: nil, options: nil)?.first
    }


    func configure(post: Post?) {
        self.post = post
        currentLikesCount = post?.likesCount ?? 0
        currentLikeStatus = "\(post?.userLike ?? 0)"
        updateLikeButtonStatus()

        commentCountLabel.text = "View " + "\(post?.commentsCount ?? 0)" + " " + "Comments"
        updateViewCount(count: post?.views ?? 0)
        dailyCountLabel.text = post?.points?.dailyPoints ?? "0"
        annualCountLabel.text = post?.points?.annualPoints ?? "0"
        lifetimeCountLabel.text = post?.points?.lifetimePoints ?? "0"
        totalCountLabel.text = post?.points?.totalPoints ?? "0"
    }
}

// Will be moved to Like View later on
extension PostsSectionFooterView {
    private func updateLikeButtonStatus() {
        likeImage.image = UIImage(named: currentLikeStatus == "0" ? "heartBlack" : "like")
        likeCountLabel.text = "\(currentLikesCount)"
    }

    @IBAction func likeBtnAction(_ sender: UIButton) {
        let strPostUserId = "\(post?.userID ?? 0)"
        let strPostId = post?.id ?? 0
        let expectedLikeId = currentLikeStatus == "0" ? "1" : "0"
        updateLikeButtonStatus()
        doPostLikeFromServer(postId: strPostId, likeId: expectedLikeId, postUserId: strPostUserId)
    }

    @IBAction func likeCountListBtnAction(_ sender: UIButton) {
        delegate?.openLikeButtonTapped(postId: String(post?.id ?? 0))
    }

    @IBAction func commentBtnAction(_ sender: UIButton) {
        delegate?.openCommentsTapped(postId: String(post?.id ?? 0))
    }

    @IBAction func shareBtnAction(_ sender: UIButton) {
        loadThumbnailFromCache(urlString: post?.filename ?? "")
    }

    private func loadThumbnailFromCache(urlString: String) {
        guard let url = URL(string: urlString) else {
            self.delegate?.shareButtonTapped(image: nil, post: post?.dDescription)
            return
        }
        KingfisherManager.shared.retrieveImage(with: url,
                                               options: nil,
                                               progressBlock: nil) { [self] (result) -> Void in
            switch result {
                case .success(let value):
                    DispatchQueue.main.async {
                        if let img2 = UIImage(named: "watermark") {
                            let img = value.image
                            let width = img.size.width
                            let height = img.size.height
                            let rect = CGRect(x: 0, y: 0, width: width, height: height)

                            UIGraphicsBeginImageContextWithOptions(img.size, true, 0)
                            let context = UIGraphicsGetCurrentContext()

                            context!.setFillColor(UIColor.white.cgColor)
                            context!.fill(rect)
                            let wwidth = 240.0
                            let wheight = 306.0
                            img.draw(in: rect, blendMode: .normal, alpha: 1)
                            let x = img.size.width - (wwidth + 20)
                            let y = img.size.height - (wheight + 20)
                            img2.draw(in:CGRect(x: x > 0 ? x : 0,
                                                y: y > 0 ? y : 0,
                                                width: wwidth,
                                                height: wheight)
                                      , blendMode: .normal, alpha: 0.3)

                            let result = UIGraphicsGetImageFromCurrentImageContext()
                            UIGraphicsEndImageContext()

                            self.delegate?.shareButtonTapped(image: result, post: post?.dDescription)

                        }
                        
                    }

                case .failure(let error):
                    print("Error: \(error)")
            }
        }
    }

    
    @IBAction func allTimeBtnAction(_ sender: UIButton) {
        allTimeView.isHidden = !allTimeView.isHidden
    }

    func doPostLikeFromServer(postId: Int, likeId: String, postUserId: String) {
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strMyUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"

        let param = [Constants.KEY_MYUSER_ID: strMyUserId,
                     Constants.KEY_POST_ID: postId,
                     Constants.KEY_LIKE: likeId,
                     Constants.KEY_POST_USERId: postUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        PSWebServiceAPI.doLikePost(param, completion: { response in
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    guard let msg = response["m"] as? String else {
                        self.updateLikeButtonStatus()
                        self.delegate?.likeButtonTapped()
                        return
                    }
                    if msg == "Post disliked successfully" {
                        self.currentLikeStatus = "0"
                        self.currentLikesCount -= 1

                    } else {
                        self.currentLikeStatus = "1"
                        self.currentLikesCount += 1
                    }
                }
            }
            DispatchQueue.main.async {
                self.updateLikeButtonStatus()
                self.delegate?.likeButtonTapped()
            }

        })
    }
    func updateViewCount(count: Int) {
        viewsCountLabel.text = "\(count)"
    }
}
