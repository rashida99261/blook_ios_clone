//
//  TopPhotoSelectionVC.swift
//  bloook
//
//  Created by Tech Astha on 25/06/21.
//

import UIKit
import AVKit
class TopPhotoSelectionVC: UIViewController {

    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var buttonPlaySecondBtn: UIButton!
    @IBOutlet weak var buttonPlay: UIButton!
    @IBOutlet weak var imageHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var heightImg: NSLayoutConstraint!
    @IBOutlet weak var lifetimeTijmeLbl: UILabel!
    @IBOutlet weak var annualLbl: UILabel!
    @IBOutlet weak var dailyRankLbl: UILabel!
    @IBOutlet weak var multipleView: UIView!
    @IBOutlet weak var viewCountLbl: UILabel!
    @IBOutlet weak var likeImg: UIImageView!
    @IBOutlet weak var likeCountLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var topStatusView: UIView!
    @IBOutlet weak var userPhotoImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userDateLbl: UILabel!
    @IBOutlet weak var userDescriptionLbl: UILabel!
    @IBOutlet weak var topSelectedImg: UIImageView!
    @IBOutlet weak var commentLbl: UILabel!
    
    @IBOutlet weak var likeBtn: UIButton!
    
    var strLikeId = String()
    var strPostUserId = String()
    var strPostId = String()
    var strMyUserId = String()
    var dict = NSDictionary()
    var postDict = NSDictionary()
    var strBtnFavId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonPlay.isHidden = true
        buttonPlaySecondBtn.isHidden = true
        postDict = dict[Constants.KEY_POSTS] as? NSDictionary ?? NSDictionary()
        print(dict)
        let UserDict = postDict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
        let strFileType = "\(postDict.value(forKey: Constants.KEY_TYPE) ?? "")"
        if strFileType == "blog" {
            TitleLbl.text = "Top Blog"
            imageHeightConstant.constant = 0
            let strUserDesc = postDict[Constants.KEY_DESCRIPTION] as? String
            let strLikeCount = postDict[Constants.KEY_LIKE_COUNT] as? String
            let strViewCount = postDict[Constants.KEY_VIEWS_COUNT] as? String
            let strProfilePhoto = UserDict[Constants.KEY_PROFILE_PHOTO] as? String
            let strUserName = UserDict[Constants.KEY_NAME] as? String
            let strDailyPoints = dict[Constants.KEY_DAILY_POINTS] as? String
            let strAnnualPoints = dict[Constants.KEY_ANNUAL_POINTS] as? String
            let strLifetimePoints = dict[Constants.KEY_LIFETIME_POINTS] as? String
           // let strCommentsCount = UserDict[Constants.KEY_COMMENT] as? String
            let strCommentCount = "\(postDict.value(forKey: Constants.KEY_ALL_COMMENTS_COUNT) ?? "")"
            commentLbl.text = "View " + strCommentCount + " " + "Comments"
            
            userDescriptionLbl.text = strUserDesc
            likeCountLbl.text = strLikeCount
            userNameLbl.text = strUserName
            viewCountLbl.text = strViewCount
            if strDailyPoints == "nil"{
                dailyRankLbl.text = "0"
            }else{
               dailyRankLbl.text = strDailyPoints
            }
            annualLbl.text = strAnnualPoints
            lifetimeTijmeLbl.text = strLifetimePoints
            if strProfilePhoto == ""{
                userPhotoImg.sd_setImage(with: URL(string: "img9"), placeholderImage: UIImage(named: "placeholder.png"))
            }else{
                userPhotoImg.sd_setImage(with: URL(string: strProfilePhoto!), placeholderImage: UIImage(named: "placeholder.png"))
            }
            let strUserCreateDate = "\(postDict.value(forKey: Constants.KEY_CREATE_DATE) ?? "")"
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let dateCreate = dateFormatter.date(from: strUserCreateDate)
            
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date / server String
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

            let myString = formatter.string(from: (dateCreate)!) // string purpose I add here
            // convert your string to date
            let yourDate = formatter.date(from: myString)
            //then again set the date format whhich type of output you need
            formatter.dateFormat = "dd-MMM-yyyy"
            // again convert your date to string
            let myStringafd = formatter.string(from: yourDate!)
            userDateLbl.text = myStringafd
        }else {
            TitleLbl.text = "Top Photos"
        let strPostPhoto = postDict[Constants.KEY_FILE_NAME] as? String
        let strUserDesc = postDict[Constants.KEY_DESCRIPTION] as? String
        let strLikeCount = postDict[Constants.KEY_LIKE_COUNT] as? String
        let strViewCount = postDict[Constants.KEY_VIEWS_COUNT] as? String
        let strProfilePhoto = UserDict[Constants.KEY_PROFILE_PHOTO] as? String
        let strUserName = UserDict[Constants.KEY_NAME] as? String
        let strDailyPoints = dict[Constants.KEY_DAILY_POINTS] as? String
        let strAnnualPoints = dict[Constants.KEY_ANNUAL_POINTS] as? String
        let strLifetimePoints = dict[Constants.KEY_LIFETIME_POINTS] as? String
       // let strCommentsCount = UserDict[Constants.KEY_COMMENT] as? String
        let strCommentCount = "\(postDict.value(forKey: Constants.KEY_ALL_COMMENTS_COUNT) ?? "")"
        commentLbl.text = "View " + strCommentCount + " " + "Comments"
        
        userDescriptionLbl.text = strUserDesc
        likeCountLbl.text = strLikeCount
        userNameLbl.text = strUserName
        viewCountLbl.text = strViewCount
        if strDailyPoints == "nil"{
            dailyRankLbl.text = "0"
        }else{
           dailyRankLbl.text = strDailyPoints
        }
        annualLbl.text = strAnnualPoints
        lifetimeTijmeLbl.text = strLifetimePoints
        
        if strPostPhoto == ""{
            topSelectedImg.sd_setImage(with: URL(string: "img10"), placeholderImage: UIImage(named: "placeholder.png"))
        }else{
            topSelectedImg.sd_setImage(with: URL(string: strPostPhoto!), placeholderImage: UIImage(named: "placeholder.png"))
        }
        if strProfilePhoto == ""{
            userPhotoImg.sd_setImage(with: URL(string: "img9"), placeholderImage: UIImage(named: "placeholder.png"))
        }else{
            userPhotoImg.sd_setImage(with: URL(string: strProfilePhoto!), placeholderImage: UIImage(named: "placeholder.png"))
        }
        let strUserCreateDate = "\(postDict.value(forKey: Constants.KEY_CREATE_DATE) ?? "")"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateCreate = dateFormatter.date(from: strUserCreateDate)
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: (dateCreate)!) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        userDateLbl.text = myStringafd
        }
     }
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        descriptionView.layer.masksToBounds = true
        descriptionView.round(corners: [.topLeft, .topRight], radius: 20)
        mainView.layer.masksToBounds = true
        mainView.round(corners: [.topLeft, .topRight], radius: 20)
        commentView.layer.masksToBounds = true
        commentView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        mainView.layer.masksToBounds = true
        mainView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    @IBAction func commentBtnAction(_ sender: Any) {
        //let dict = self.postDataArr[buttonRow] as? NSDictionary ?? NSDictionary()
        let postDict = dict[Constants.KEY_POSTS] as? NSDictionary ?? NSDictionary()
        let strPostId = "\(postDict.value(forKey: Constants.KEY_Id) ?? "")"
        let strPostUserId = "\(postDict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
        let UserDict = postDict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
        let strUserProfileImg = UserDict[Constants.KEY_PROFILE_PHOTO] as? String
        var strName = UserDict[Constants.KEY_NAME] as? String
        self.tabBarController?.tabBar.isHidden = true
        let homeDetailStoryboard =  UIStoryboard(name: "HomeDetailStoryboard", bundle: nil)
        let objAllCommentHome = homeDetailStoryboard.instantiateViewController(withIdentifier: "AllCommentHomeVC") as? AllCommentHomeVC
        objAllCommentHome?.selectedPostId = strPostId
        //TODO: Suresh, find the links
//        objAllCommentHome?.strPostUserId = strPostUserId
//        if strName == ""{
//            strName = ""
//            objAllCommentHome?.strUserName = "\(strName ?? "")"
//        }else{
//        objAllCommentHome?.strUserName = "\(strName ?? "")"
//        }
//        objAllCommentHome?.strProfileImg = strUserProfileImg!
        self.navigationController?.pushViewController(objAllCommentHome!, animated: true)
    }
    @IBAction func flagMultipleBtnAction(_ sender: Any) {
        multipleView.isHidden = false
    }
    @IBAction func allLifeTImeBtnAction(_ sender: Any) {
        multipleView.isHidden = true
    }
    @IBAction func profileBtnAction(_ sender: Any) {
        let postDict = dict[Constants.KEY_POSTS] as? NSDictionary ?? NSDictionary()
     //   let strPostUserId = "\(postDict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
        self.tabBarController?.tabBar.isHidden = false
       // UserDefaults.standard.set(strPostUserId, forKey: Constants.KEY_ID)
        let strId = "\(postDict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
        print(strId)
        let strUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        if strId == strUserId{
            self.tabBarController?.tabBar.isHidden = false
            UserDefaults.standard.set(strUserId, forKey: Constants.KEY_ID)
            let otherStoryboard =  UIStoryboard(name: "Profile", bundle: nil)
            let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
            self.navigationController?.pushViewController(objMyProfile!, animated: true)
        }else{
            self.tabBarController?.tabBar.isHidden = false
            let otherStoryboard =  UIStoryboard(name: "Storyboard", bundle: nil)
            let objProfile = otherStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC
            objProfile?.strFollowUserId = strId
            self.navigationController?.pushViewController(objProfile!, animated: true)
        }
        
     }
    @IBAction func buttonPlayAction(_ sender: Any) {
        let strPostPhoto = postDict[Constants.KEY_FILE_NAME] as? String
        if let url = URL(string: strPostPhoto!) {
            //2. Create AVPlayer object
            let asset = AVAsset(url: url)
            let playerItem = AVPlayerItem(asset: asset)
            let player = AVPlayer(playerItem: playerItem)
            
            //3. Create AVPlayerLayer object
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.videoView.bounds //bounds of the view in which AVPlayer should be displayed
            playerLayer.videoGravity = .resizeAspectFill
            
            //4. Add playerLayer to view's layer
            self.videoView.layer.addSublayer(playerLayer)
            
            //5. Play Video
            player.play()
        }
    }
    @objc func likeBtnAction( _ sender :UIButton){

        //let buttonRow = sender.tag
       //let dict = self.postDataArr[buttonRow] as? NSDictionary
        postDict = dict[Constants.KEY_POSTS] as? NSDictionary ?? NSDictionary()
        strLikeId = "\(postDict.value(forKey: Constants.KEY_USER_LIKE) ?? "")"
        strPostUserId = "\(postDict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
        strPostId = "\(postDict.value(forKey: Constants.KEY_Id) ?? "")"
        let userDic =  postDict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
        strMyUserId = "\(userDic.value(forKey: Constants.KEY_ID) ?? "")"
      //  print(strMyUserId)
      //  print(buttonRow)
        //    || strLikeId == "<null>"
      //  let theCell = homeCollectionView.cellForItem(at: IndexPath(item: 0, section: buttonRow)) as? LikeCollectionViewCell
        if strLikeId == "0"  {
            strBtnFavId = "1"
           likeImg.image = UIImage(named: "like")
            doPostLikeFromServer()
        }else{
            strBtnFavId = "0"
            likeImg.image = UIImage(named: "heartBlack")
        }
        doPostLikeFromServer()
    }
    
    //MARK:- for Like Post
    func doPostLikeFromServer() {

        let defult = UserDefaults.standard
        let strDeviceToken = defult .object(forKey: Constants.DEVICETOKEN)
        let strMyUserId = "\(defult .object(forKey: Constants.KEY_ID) ?? "")"
        print(strLikeId)
        let param = [Constants.KEY_MYUSER_ID:strMyUserId,
                     Constants.KEY_POST_ID:strPostId,
                     Constants.KEY_LIKE:strBtnFavId,
                     Constants.KEY_POST_USERId:strPostUserId,
                     Constants.KEY_DEVICE_ID : "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE : "I"] as [String : Any]

        PSWebServiceAPI.doLikePost(param , completion: { (response) in
            if response["Error"] == nil {

                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    /*  if msg == "Post disliked successfully"{

                        self.strLikeId = "0"

                    }else{
                        self.strLikeId = "1"

                    }
                    self.homeCollectionView.reloadData()
                    doAllListPostFromServer(offset: offset, limit: limit)
                    */
                  //  self.doAllListPostFromServer()

                 }
            }
        })
     }
}
