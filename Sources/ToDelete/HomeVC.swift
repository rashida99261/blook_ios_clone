//
//  HomeVC.swift
//  blook
//
//  Created by Tech Astha on 25/12/20.
//

import AVFoundation
import AVKit
import gooey_cell
import GPVideoPlayer
import ImageScrollView
import Kingfisher
import Photos
import QuartzCore
import SDWebImage
import smooth_feed
import Social
import StickyHeader
import UIKit
import ZoomImageView

class HomeVC: UIViewController, UITabBarDelegate, UITabBarControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    // MARK: - zoom screen post lbl

    @IBOutlet weak var postWiseLifetimeTijmeLbl: UILabel!
    @IBOutlet weak var postWiseannualLbl: UILabel!
    @IBOutlet weak var postWisedailyRankLbl: UILabel!
    @IBOutlet weak var postWiseviewCountLbl: UILabel!
    @IBOutlet weak var postWiseLikeCountLbl: UILabel!
    @IBOutlet weak var postWiseMultipleView: UIView!
    @IBOutlet weak var postWiseUserNameLbl: UILabel!
    @IBOutlet weak var postWiseUserDateLbl: UILabel!
    @IBOutlet weak var postWiseCommentLbl: UILabel!
    @IBOutlet weak var postWiseUserDescriptionLbl: UILabel!
    @IBOutlet weak var postWiseLikeImg: UIImageView!
    @IBOutlet weak var postWiseUserPhotoImg: UIImageView!
    @IBOutlet weak var postWiseTopSelectedImg: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var postWiseCommentView: UIView!
    @IBOutlet weak var postWiseDescriptionView: UIView!
    @IBOutlet weak var postWiseTopStatusView: UIView!
    @IBOutlet weak var postWiseLikeBtn: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var treasureHuntStartView: UIView!
    @IBOutlet weak var treasureHuntPlayView: UIView!
    @IBOutlet weak var fireArchievedView: UIView!
    @IBOutlet weak var bounceOnBoardingView: UIView!
    @IBOutlet weak var bounceMainView: UIView!
    @IBOutlet weak var bouncePlayView: UIView!
    
    @IBOutlet weak var playBtn: UIButton!
    
    @IBOutlet weak var homeCollectionHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var bounceCOllectionHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var cancelBtnAction: UIImageView!
    // @IBOutlet weak var zoomMainView: ZoomImageView!
    @IBOutlet weak var zoomMainView: UIView!
    @IBOutlet weak var coinCountLbl: UILabel!
    @IBOutlet weak var fireImg: UIImageView!
    var progressView: AcuantProgressView!
    var MenuImage = [Any]()
    var postDataArr = [Any]()
    var arrNumber = [Any]()
    
    var strPostId = String()
    var strPostUserId = String()
    var strBtnFavId = String()
    var strMyUserId = String()
    
    var strMultipleDay = String()
    @IBOutlet weak var bounceCollectionView: UICollectionView!
    @IBOutlet weak var playBounceCollectionView: UICollectionView!
    @IBOutlet weak var imageZoomView: UIImageView!
    @IBOutlet weak var coinPicker: UIPickerView!
    @IBOutlet weak var tableOne: UITableView!
    @IBOutlet weak var tableTwo: UITableView!
    @IBOutlet weak var tableThree: UITableView!
  
    var playerLayer = AVPlayerLayer()
    
    var scrollImg: UIScrollView!
    
    @IBOutlet weak var pickerOne: UIPickerView!
    @IBOutlet weak var pickerTwo: UIPickerView!
    @IBOutlet weak var pickerThree: UIPickerView!
    @IBOutlet weak var pickerFour: UIPickerView!
    @IBOutlet weak var coinPopupView: UIView!
    @IBOutlet weak var homeCollectionView: UICollectionView!
    @IBOutlet weak var viewColor: UIView!
    @IBOutlet weak var imageScrollView: ImageScrollView!
    var strLikeId = String()
    var strLikeCount = String()
    
    var limit = 10
    var offset = 0
    let pickerDataSize = 100_000
    private var scrollToTime = true
    private var groupNoToScroll = 12
    private var items = [Int]()
    
    var myT = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageScrollView.isHidden = true
        // scrollView.delegate = self
        //  scrollView.contentSize = homeCollectionView.frame.size
        // homeCollectionView.isScrollEnabled = false
//      homeCollectionView.showsHorizontalScrollIndicator = false
//      homeCollectionView.showsVerticalScrollIndicator = false
        //  self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * 4, height:2000)
        //  self.scrollView.delegate = self
        // self.pageControl.currentPage = 0
        fireImg.setImageColor(color: UIColor.white)
        zoomMainView.isHidden = true
        let vWidth = view.frame.width
        let vHeight = view.frame.height
        
//        scrollImg = UIScrollView()
//        scrollImg.delegate = self
//        scrollImg.frame = CGRect(x: 0, y: 0, width: vWidth, height: vHeight)
//        zoomMainView.addSubview(scrollImg)
       
        coinPicker?.dataSource = self
        coinPicker?.delegate = self
        
        let strUserId = "\(defult.object(forKey: Constants.ISLOGIN) ?? "")"
        bounceMainView.isHidden = true
        coinPopupView.isHidden = true
        let offset = String(self.offset)
        let limit = String(self.limit)
        items = [1, 1, 2, 2, 2, 3, 4, 4, 4, 4, 4, 5, 5, 6, 7, 7, 8, 8, 8, 9, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 13, 13, 14, 14, 14, 14, 15, 15, 16, 17, 17, 18, 18, 18, 19, 19, 19, 19, 20, 21, 22, 22, 23, 23, 23]
        MenuImage = ["img1", "img2", "img3", "img4", "img5", "img6", "img7"]
        arrNumber = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        
        homeCollectionView.register(UINib(nibName: CollectionReusableView.reuseIdentifier, bundle: nil),
                                    forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                    withReuseIdentifier: CollectionReusableView.reuseIdentifier)
        
        homeCollectionView.register(UINib(nibName: LikeCollectionViewCell.reuseIdentifier, bundle: nil),
                                    forCellWithReuseIdentifier: LikeCollectionViewCell.reuseIdentifier)
        homeCollectionView.register(UINib(nibName: FooterCollectionReusableView.reuseIdentifier, bundle: nil),
                                    forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                                    withReuseIdentifier: FooterCollectionReusableView.reuseIdentifier)
        
        homeCollectionView.alwaysBounceVertical = true
        homeCollectionView.dataSource = self
        let layout = SmoothFeedCollectionViewLayout()
        layout.estimatedItemHeight = 50
        layout.itemsInSectionMaxHeight = UIScreen.main.bounds.width + 190
        // layout.itemHeight = 50
        layout.footerHeight = 60
        layout.headerHeight = UIScreen.main.bounds.width + 120
        homeCollectionView.collectionViewLayout = layout
        homeCollectionView.dataSource = self
        homeCollectionView.delegate = self
        
        homeCollectionView.stickyHeader.view = headerView
        homeCollectionView.stickyHeader.height = 265
        homeCollectionView.stickyHeader.minimumHeight = 0
        
        pickerOne.selectRow(7, inComponent: 0, animated: false)
        pickerTwo.selectRow(7, inComponent: 0, animated: false)
        pickerThree.selectRow(7, inComponent: 0, animated: false)
        pickerFour.selectRow(7, inComponent: 0, animated: false)
        //  let nextItem = NSIndexPath(row: currentItem.row + 1, section: 0)
        startTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        doAllListPostFromServer()
    }
    
    override func viewDidLayoutSubviews() {
        roundView.layer.masksToBounds = true
        roundView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        //  homeCollectionView.reloadData()
    }

    func setGradientBackground() {
        let colorTop = UIColor(red: 114.0/255.0, green: 146.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 177.0/255.0, green: 178.0/255.0, blue: 254.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @objc func coinPopupActionView(sender: UITapGestureRecognizer) {
        tabBarController?.tabBar.isHidden = false
        coinPopupView.isHidden = true
        animateButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        playerLayer.player?.pause()
    }
    
    // func scrollViewDidScroll(_ scrollView: UIScrollView) {
    // This will be called every time the user scrolls the scroll view with their finger
    // so each time this is called, contentOffset should be different.

    // print(self.hom.contentOffset.y)

    // Additional workaround here.
//        bounceCOllectionHeightConstant.constant = 0
    // }
    
    // MARK: - for All List Post

    var canLoadMoreData = false
    // func doAllListPostFromServer(offset: String, limit: String) {
    func doAllListPostFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        print(strLikeId)
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_PAGE: "0",
                     Constants.KEY_LIMIT: "2000",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        // PSUtil.showProgressHUD()
        progressView = AcuantProgressView(frame: view.frame, center: view.center)
        //  self.showProgressView(text:  "Loading...")
        isLoadMoreInProgress = true
        PSWebServiceAPI.doAllPostList(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    self.postDataArr = response[Constants.KEY_POSTDATA] as? [Any] ?? [Any]()
                    PSUtil.hideProgressHUD()
                    self.homeCollectionView.reloadData()
                    
//                    self.homeCollectionHeightConstant.constant = self.homeCollectionView.contentSize.height + 50
//                    self.homeCollectionView.updateConstraints()
                    
                    // DispatchQueue.main.async {
                    // self.homeCollectionView.reloadData()
                    //  let refreshControl = UIRefreshControl()
                    //  refreshControl.addTarget(self, action: #selector(self.doSomething), for: .valueChanged)

                    // this is the replacement of implementing: "collectionView.addSubview(refreshControl)"
                    //  self.homeCollectionView.refreshControl = refreshControl
                        
                    //   }
                    PSUtil.hideProgressHUD()
                   
                } else {
                    self.isLoadMoreInProgress = true
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    // PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                self.isLoadMoreInProgress = false
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }

//    @objc func doSomething(refreshControl: UIRefreshControl) {
//        print("Hello World!")
//
//        refreshControl.endRefreshing()
//       // doAllListPostFromServer()
//    }
    private var shouldShowLoadingCell = false
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldShowLoadingCell else { return false }
        return indexPath.row == postDataArr.count
    }

    var isLoadMoreInProgress = false
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        // Load more should be enabled only if inbox list webservice call is made or data is present.
//
//        let distanceFromBottom : CGFloat = scrollView.contentSize.height - scrollView.contentOffset.y
//        if distanceFromBottom < scrollView.frame.size.height,self.canLoadMoreData == true {
//            print("I am in bottom now")
//            if self.isLoadMoreInProgress == false {
//                // Call The Service for load more
//
//                let offset = String(self.offset)
//                let limit = String(self.limit)
//                //doAllListPostFromServer(offset: offset, limit: limit)
//                doAllListPostFromServer()
//            }
//         }
//    }
//
    public func imageFromVideo(url: URL, at time: TimeInterval, completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global(qos: .background).async {
            let asset = AVURLAsset(url: url)

            let assetIG = AVAssetImageGenerator(asset: asset)
            assetIG.appliesPreferredTrackTransform = true
            assetIG.apertureMode = AVAssetImageGenerator.ApertureMode.encodedPixels

            let cmTime = CMTime(seconds: time, preferredTimescale: 60)
            let thumbnailImageRef: CGImage
            do {
                thumbnailImageRef = try assetIG.copyCGImage(at: cmTime, actualTime: nil)
            } catch {
                print("Error: \(error)")
                return completion(nil)
            }

            DispatchQueue.main.async {
                completion(UIImage(cgImage: thumbnailImageRef))
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // return items.count
        if collectionView == bounceCollectionView {
            return 20
        } else if collectionView == playBounceCollectionView {
            return 20
        } else {
            return postDataArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //  return items[section].count
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // let item = items[indexPath.section][indexPath.item]
        let reuseIdentifier = LikeCollectionViewCell.reuseIdentifier
        // let reuseIdentifier = LikeCollectionViewCell.reuseIdentifier
        if collectionView == bounceCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! bounceCollectionViewCell
            return cell
        } else if collectionView == playBounceCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! playFireBounceCollectionViewCell
            return cell
        } else {
            let dict = postDataArr[indexPath.section] as? NSDictionary ?? NSDictionary()
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! LikeCollectionViewCell
        
            cell.allTimeView.isHidden = true
            // cell.allTimeView.isHidden = true
            strLikeId = "\(dict.value(forKey: Constants.KEY_USER_LIKE) ?? "")"
            strLikeCount = "\(dict.value(forKey: Constants.KEY_LIKE_COUNT) ?? "")"
            cell.viewsLabel.text = strLikeCount

            if strLikeId == "0" {
                cell.likeImg.image = UIImage(named: "heartBlack")
            
            } else {
                cell.likeImg.image = UIImage(named: "like")
            }
            cell.likeCountBtn.tag = indexPath.section
            cell.likeCountBtn.addTarget(self, action: #selector(likeCountListBtnAction(_:)), for: UIControl.Event.touchUpInside)
            cell.likeButton.tag = indexPath.section
            cell.likeButton.addTarget(self, action: #selector(likeBtnAction(_:)), for: UIControl.Event.touchUpInside)
            cell.commentButton.tag = indexPath.section
            cell.commentButton.addTarget(self, action: #selector(commentBtnAction(_:)), for: UIControl.Event.touchUpInside)
            cell.shareButton.tag = indexPath.section
            cell.shareButton.addTarget(self, action: #selector(shareBtnAction(_:)), for: UIControl.Event.touchUpInside)
            cell.allTimeBtn.tag = indexPath.section
            cell.allTimeBtn.addTarget(self, action: #selector(allTimeBtnAction(_:)), for: UIControl.Event.touchUpInside)
            cell.allLifeTimeBtn.tag = indexPath.section
            cell.allLifeTimeBtn.addTarget(self, action: #selector(allLifeTimeBtnAction(_:)), for: UIControl.Event.touchUpInside)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == bounceCollectionView {
            return UIEdgeInsets(top: 0.0, left: 2.0, bottom: 0.0, right: 2.0)
        } else {
            return UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // if collectionView == bounceCollectionView{
//        let itemsPerRow:CGFloat = 3.5
//            let hardCodedPadding:CGFloat = 2
//            let itemWidth = (self.bounceCollectionView.bounds.width / itemsPerRow) - hardCodedPadding
       
        return CGSize(width: 150, height: 195)
       
        // }else{
            
        // }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        let dict = self.postDataArr[indexPath.section] as? NSDictionary ?? NSDictionary()
//        let strUserImg = "\(dict.value(forKey: Constants.KEY_FILE_NAME) ?? "")"
//
//         let newImageView = UIImageView()
//         let url = URL(string: strUserImg)
//         newImageView.kf.setImage(with: url)
//         newImageView.frame = UIScreen.main.bounds
//         newImageView.backgroundColor = .white
//         newImageView.contentMode = .scaleAspectFit
//         newImageView.isUserInteractionEnabled = true
//         let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
//         newImageView.addGestureRecognizer(tap)
//         self.view.addSubview(newImageView)
//         self.tabBarController?.tabBar.isHidden = true
//     }
    
    func startTimer() {
        let timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(scrollAutomatically), userInfo: nil, repeats: true)
    }

    @objc func scrollAutomatically(_ timer1: Timer) {
//        if let coll  = homeCollectionView {
//            for cell in coll.visibleCells {
//                let indexPath: IndexPath? = coll.indexPath(for: cell)
//                if ((indexPath?.row)! < postDataArr.count - 1){
//                    let indexPath1: IndexPath?
//                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
//
//                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
//                }
//                else{
//                    let indexPath1: IndexPath?
//                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
//                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
//                }
//            }
//        }
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    @objc func allTimeBtnAction(_ sender: UIButton) {
        let buttonRow = sender.tag
        let indexPath = IndexPath(item: 0, section: buttonRow)
        print(buttonRow)
        let reuseIdentifier = LikeCollectionViewCell.reuseIdentifier
        let cell = homeCollectionView?.cellForItem(at: indexPath) as? LikeCollectionViewCell
        // let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: buttonRow as IndexPath) as! LikeCollectionViewCell
        cell?.allTimeView.isHidden = false
        // strMultipleDay = "1"
        // homeCollectionView.reloadData()
    }

    @objc func allLifeTimeBtnAction(_ sender: UIButton) {
        let buttonRow = sender.tag
        print(buttonRow)
        let indexPath = IndexPath(item: 0, section: buttonRow)
        print(buttonRow)
        let reuseIdentifier = LikeCollectionViewCell.reuseIdentifier
        let cell = homeCollectionView?.cellForItem(at: indexPath) as? LikeCollectionViewCell
        // let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: buttonRow as IndexPath) as! LikeCollectionViewCell
        cell?.allTimeView.isHidden = true
        // strMultipleDay = "1"
        // homeCollectionView.reloadData()
    }
    
    @objc func shareBtnAction(_ sender: UIButton) {
        

        var strDealId = "10"
        let nsdata = strDealId.data(
            using: .utf8)
        strDealId = (nsdata?.base64EncodedString(options: []))!
        var message = "https://www.appysalons.com/shareddeallink.php?deal=\(strDealId)"
        message = "Check this amazing offer out...\n" + message
        
        let shareItems = [message]
        
        let avc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        
        present(avc, animated: true)
    }

    @objc func commentBtnAction(_ sender: UIButton) {
        playerLayer.player?.pause()
        let buttonRow = sender.tag
        print(buttonRow)
        
        let dict = postDataArr[buttonRow] as? NSDictionary ?? NSDictionary()
        strPostId = "\(dict.value(forKey: Constants.KEY_Id) ?? "")"
        strPostUserId = "\(dict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
        tabBarController?.tabBar.isHidden = true
        let userDic = dict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
        let strUserFirstName = "\(userDic.value(forKey: Constants.KEY_FIRSTNAME) ?? "")"
        let strUserLastName = "\(userDic.value(forKey: Constants.KEY_LASTNAME) ?? "")"
        let strName = strUserFirstName + " " + strUserLastName
        let strUserProfileImg = "\(userDic.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
        let homeDetailStoryboard = UIStoryboard(name: "HomeDetailStoryboard", bundle: nil)
        let objAllCommentHome = homeDetailStoryboard.instantiateViewController(withIdentifier: "AllCommentHomeVC") as? AllCommentHomeVC
//        objAllCommentHome?.strPostId = strPostId
//        objAllCommentHome?.strPostUserId = strPostUserId
//        objAllCommentHome?.strUserName = strName
//        objAllCommentHome?.strProfileImg = strUserProfileImg
        navigationController?.pushViewController(objAllCommentHome!, animated: true)
    }

    @objc func otherUserProfileBtnAction(_ sender: UIButton) {
        playerLayer.player?.pause()
        let buttonRow = sender.tag
        print(buttonRow)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let dict = postDataArr[buttonRow] as? NSDictionary ?? NSDictionary()
        let strFollowUserId = "\(dict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
        if strUserId == strFollowUserId {
            UserDefaults.standard.set(strUserId, forKey: Constants.KEY_ID)
            let otherStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
            navigationController?.pushViewController(objMyProfile!, animated: true)
        } else {
            let otherStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
            let objProfile = otherStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC
            objProfile?.strFollowUserId = strFollowUserId
            navigationController?.pushViewController(objProfile!, animated: true)
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView
    {
        //  var reuseIdentifier: String = ""
        var reuseIdentifier: String
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            reuseIdentifier = CollectionReusableView.reuseIdentifier
        case UICollectionView.elementKindSectionFooter:
            reuseIdentifier = FooterCollectionReusableView.reuseIdentifier
//        case UICollectionView.elementKindSectionHeader:
//            reuseIdentifier = LikeCollectionViewCell.reuseIdentifier
        
        default:
            fatalError("unexpected kind")
        }
        let supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                                withReuseIdentifier: reuseIdentifier,
                                                                                for: indexPath)
        
        let commentBtn = supplementaryView as? FooterCollectionReusableView
        
        commentBtn?.roundedBorderView.layer.masksToBounds = true
        commentBtn?.roundedBorderView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        
        let dict = postDataArr[indexPath.section] as? NSDictionary ?? NSDictionary()
        let strCommentCount = "\(dict.value(forKey: Constants.KEY_ALL_COMMENTS_COUNT) ?? "")"
        commentBtn?.addCommentLabel.text = "View " + strCommentCount + " " + "Comments"
        commentBtn?.commentButton.tag = indexPath.section
        commentBtn?.commentButton.addTarget(self, action: #selector(commentBtnAction(_:)), for: UIControl.Event.touchUpInside)
        
        let photo = supplementaryView as? CollectionReusableView
        
        // photo?.imageView.image = [#imageLiteral(resourceName: "img5"), #imageLiteral(resourceName: "img6")][indexPath.section % 2]
        
        photo?.otherUserProfileBtn.tag = indexPath.section
        photo?.otherUserProfileBtn.addTarget(self, action: #selector(otherUserProfileBtnAction(_:)), for: UIControl.Event.touchUpInside)
       
        strPostId = "\(dict.value(forKey: Constants.KEY_Id) ?? "")"
       
        let userDic = dict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
        photo?.descriptionLbl.text = "\(dict.value(forKey: Constants.KEY_DESCRIPTION) ?? "")"
     
        let strFileListType = "\(dict.value(forKey: Constants.KEY_TYPE) ?? "")"
        let cornerRadius: CGFloat = 20.0
        photo?.roundView.layer.masksToBounds = true
        photo?.roundView.round(corners: [.topLeft, .topRight], radius: 20)
        photo?.imgeView.layer.cornerRadius = cornerRadius
        photo?.imageView.layer.cornerRadius = cornerRadius
        photo?.imageView.clipsToBounds = true

        if strFileListType == "image" {
            // photo?.imgHeightConsraint.constant = 0
            // photo?.descriptionHeightConstant.constant = 0
            photo?.containerView.isHidden = true
            photo?.playVideo.isHidden = true
            photo?.imageTabBtn.isHidden = false
            let strFileListImg = "\(dict.value(forKey: Constants.KEY_FILE_NAME) ?? "")"
            if strFileListImg.isValidString() {
                let screenRect = photo?.imageView.bounds
                print(screenRect)
                let screenWidth = screenRect?.size.height
                photo?.imageView.frame.size.width = screenWidth!
                  
                photo?.imageView.sd_setImage(with: URL(string: strFileListImg), placeholderImage: UIImage(named: "placeholder.png"))
                let tapGesture = UITapGestureRecognizer(target: self, action: "imageTapped:")
                    
                photo?.imageTabBtn.tag = indexPath.section
                photo?.imageTabBtn.addTarget(self, action: #selector(imageTabBtnAction(_:)), for: UIControl.Event.touchUpInside)
            }
            playerLayer.player?.pause()
        } else if strFileListType == "video" {
            photo?.imageTabBtn.isHidden = true
            photo?.playVideo.isHidden = false
            let strFileListImg = "\(dict.value(forKey: Constants.KEY_FILE_NAME) ?? "")"
        } else {
            photo?.containerView.isHidden = true
            playerLayer.player?.pause()
            photo?.playVideo.isHidden = true
            photo?.imgHeightConsraint.constant = 0
                
            //  photo?.imgeView.isHidden = true
               
            // photo?.imageView.image = UIImage (named: "test")
        }
        // }
        
        let strUserProfileImg = "\(userDic.value(forKey: Constants.KEY_PROFILE_PHOTO) ?? "")"
        if strUserProfileImg == "/images/default.png" || strUserProfileImg == "" {
            photo?.profileImg.image = UIImage(named: "user")
        } else {
            let url = URL(string: strUserProfileImg)
            photo?.profileImg.kf.setImage(with: url)
        }
        
        photo?.bligPopupBtn.tag = indexPath.section
        photo?.bligPopupBtn.addTarget(self, action: #selector(bligPopupBtnAction(_:)), for: UIControl.Event.touchUpInside)
        
        let strUserFirstName = "\(userDic.value(forKey: Constants.KEY_FIRSTNAME) ?? "")"
        let strUserLastName = "\(userDic.value(forKey: Constants.KEY_LASTNAME) ?? "")"
        if strUserLastName == "<null>" {
            photo?.nameProfileLbl.text = strUserFirstName
        } else {
            photo?.nameProfileLbl.text = strUserFirstName + " " + strUserLastName
        }
        let strUserCreateDate = "\(dict.value(forKey: Constants.KEY_CREATE_DATE) ?? "")"
        print(strUserCreateDate)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateCreate = dateFormatter.date(from: strUserCreateDate)
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: (dateCreate)!) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        // then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)

        print(myStringafd)
        photo?.createDateLbl.text = myStringafd
        return supplementaryView
    }
    
    @objc func playeVideo(overTheView sender: UIButton?) {
        let buttonRow = sender?.tag
        let dict = postDataArr[buttonRow!] as? NSDictionary
        guard let photoView = sender?.superview as? CollectionReusableView else { return }
            
        photoView.playVideo.isHidden = false
        //        photoView.playVideo.isHidden = true
        guard let url = photoView.videoURL else { return }
        if let player = GPVideoPlayer.initialize(with: photoView.bounds) {
            player.isToShowPlaybackControls = true
                
            photoView.addSubview(player)
            let urlString: String = url.absoluteString
            let url1 = URL(string: urlString)!
            // let videoFilePath = Bundle.main.path(forResource: "video", ofType: "mp4")
            //  let url2 = URL(fileURLWithPath: videoFilePath!)
            
            player.loadVideos(with: [url1])
            player.isToShowPlaybackControls = true
            player.isMuted = true
            player.playVideo()
        }
    }

    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?) -> Void)) {
        DispatchQueue.global().async { // 1
            let asset = AVAsset(url: url) // 2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) // 3
            avAssetImageGenerator.appliesPreferredTrackTransform = true // 4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) // 5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) // 6
                let thumbImage = UIImage(cgImage: cgThumbImage) // 7
                DispatchQueue.main.async { // 8
                    completion(thumbImage) // 9
                }
            } catch {
                print(error.localizedDescription) // 10
                DispatchQueue.main.async {
                    completion(nil) // 11
                }
            }
        }
    }
    
    @objc func imageTabBtnAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Posts", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
      
        self.navigationController?.pushViewController(vc, animated: true)
        
        // if the tapped view is a UIImageView then set it to imageview
        playerLayer.player?.pause()
        zoomMainView.isHidden = false
        imageScrollView.isHidden = false
        tabBarController?.tabBar.isHidden = true
        let buttonRow = sender.tag
        let dict = postDataArr[buttonRow] as? NSDictionary
        let userDic = dict?[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
        let strUserImg = "\(dict?.value(forKey: Constants.KEY_FILE_NAME) ?? "")"
       
        imageScrollView.setup()
        imageScrollView.imageScrollViewDelegate = self
        imageScrollView.imageContentMode = .aspectFit
        imageScrollView.initialOffset = .center
        if let cachedImage = SDImageCache.shared().imageFromCache(forKey: strUserImg) {
            imageScrollView.display(image: cachedImage)
        } else {
            SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string: strUserImg), completed: { image, _, _, _ in
                self.imageScrollView.refresh()
                
                self.imageScrollView.display(image: image ?? UIImage())
            })
        }

        let strUserDesc = dict?[Constants.KEY_DESCRIPTION] as? String
        let strLikeCount = dict?[Constants.KEY_LIKE_COUNT] as? String
        let strViewCount = dict?[Constants.KEY_VIEWS_COUNT] as? String
        let strProfilePhoto = userDic[Constants.KEY_PROFILE_PHOTO] as? String
        let strDailyPoints = dict?[Constants.KEY_DAILY_POINTS] as? String
        let strAnnualPoints = dict?[Constants.KEY_ANNUAL_POINTS] as? String
        let strLifetimePoints = dict?[Constants.KEY_LIFETIME_POINTS] as? String
        // let strCommentsCount = UserDict[Constants.KEY_COMMENT] as? String
        let strCommentCount = "\(dict?.value(forKey: Constants.KEY_ALL_COMMENTS_COUNT) ?? "")"
        // postWiseCommentLbl.text = "View " + strCommentCount + " " + "Comments"
        
        let strUserFirstName = "\(userDic.value(forKey: Constants.KEY_FIRSTNAME) ?? "")"
        let strUserLastName = "\(userDic.value(forKey: Constants.KEY_LASTNAME) ?? "")"
        if strUserLastName == "<null>" {
            postWiseUserNameLbl.text = strUserFirstName
        } else {
            postWiseUserNameLbl.text = strUserFirstName + " " + strUserLastName
        }
        
        postWiseUserDescriptionLbl.text = strUserDesc
//        postWiseLikeCountLbl.text = strLikeCount
//        postWiseviewCountLbl.text = strViewCount
//        if strDailyPoints == "nil"{
//            postWisedailyRankLbl.text = "0"
//        }else{
//            postWisedailyRankLbl.text = strDailyPoints
//        }
//        postWiseannualLbl.text = strAnnualPoints
//        postWiseLifetimeTijmeLbl.text = strLifetimePoints
        
        if strProfilePhoto == "" || strProfilePhoto == "nil" {
            postWiseUserPhotoImg.sd_setImage(with: URL(string: "img9"), placeholderImage: UIImage(named: "placeholder.png"))
        } else {
            postWiseUserPhotoImg.sd_setImage(with: URL(string: strProfilePhoto!), placeholderImage: UIImage(named: "placeholder.png"))
        }
        let strUserCreateDate = "\(dict?.value(forKey: Constants.KEY_CREATE_DATE) ?? "")"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateCreate = dateFormatter.date(from: strUserCreateDate)
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: (dateCreate)!) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        // then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        postWiseUserDateLbl.text = myStringafd
    }

    @objc func pinch(sender: UIPinchGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            let currentScale = imageZoomView.frame.size.width/imageZoomView.bounds.size.width
            let newScale = currentScale * sender.scale
            let transform = CGAffineTransform(scaleX: newScale, y: newScale)
            imageZoomView.transform = transform
            // sender.view?.transform = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale))!
            sender.scale = 1.0
        }
    }

    @objc func doubleTapped() {
//        let currentScale = self.imageZoomView.frame.size.width / self.imageZoomView.bounds.size.width
        imageZoomView.contentMode = .scaleAspectFill
        imageZoomView.isUserInteractionEnabled = true
        
        //        let scale = min(scrollImg.zoomScale * 2, scrollImg.maximumZoomScale)
//
//            if scale != scrollImg.zoomScale { // zoom in
//                let point = recognizer.location(in: imageZoomView)
//
//
//                let scrollSize = scrollImg.frame.size
//                let size = CGSize(width: scrollSize.width / scrollImg.maximumZoomScale,
//                                  height: scrollSize.height / scrollImg.maximumZoomScale)
//                let origin = CGPoint(x: point.x - size.width / 2,
//                                     y: point.y - size.height / 2)
//                imageZoomView.zoom(to:CGRect(origin: origin, size: size), animated: true)
//            }
//            else if scrollImg.zoomScale == 1 { zoom out
//                scrollImg.zoom(to: zoomRectForScale(scale: scrollImg.maximumZoomScale, center: recognizer.location(in: imageZoomView)), animated: true)
//            }
    }

    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageZoomView.frame.size.height/scale
        zoomRect.size.width = imageZoomView.frame.size.width/scale
        let newCenter = scrollImg.convert(center, from: imageZoomView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width/2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height/2.0)
        return zoomRect
    }
    
    @objc func bligPopupBtnAction(_ sender: UIButton) {
        playerLayer.player?.pause()
        tabBarController?.tabBar.isHidden = false
        coinPopupView.isHidden = false
        treasureHuntStartView.isHidden = false
    }
    
    @objc func likeCountListBtnAction(_ sender: UIButton) {
        let buttonRow = sender.tag
        let dict = postDataArr[buttonRow] as? NSDictionary
        strPostId = "\(dict?.value(forKey: Constants.KEY_Id) ?? "")"
        
        let homeDetailStoryboard = UIStoryboard(name: "HomeDetailStoryboard", bundle: nil)
        let objAllLikeUserList = homeDetailStoryboard.instantiateViewController(withIdentifier: "AllLikeUserListVC") as? AllLikeUserListVC
        objAllLikeUserList?.strPostId = strPostId
        tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(objAllLikeUserList!, animated: true)
    }
    
    @objc func likeBtnAction(_ sender: UIButton) {
        let buttonRow = sender.tag
        let dict = postDataArr[buttonRow] as? NSDictionary
        strLikeId = "\(dict?.value(forKey: Constants.KEY_USER_LIKE) ?? "")"
        strPostUserId = "\(dict?.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
        strPostId = "\(dict?.value(forKey: Constants.KEY_Id) ?? "")"
        let userDic = dict?[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
        strMyUserId = "\(userDic.value(forKey: Constants.KEY_ID) ?? "")"
        print(strMyUserId)
        print(buttonRow)
        //    || strLikeId == "<null>"
        let theCell = homeCollectionView.cellForItem(at: IndexPath(item: 0, section: buttonRow)) as? LikeCollectionViewCell
        if strLikeId == "0" {
            strBtnFavId = "1"
            theCell?.likeImg.image = UIImage(named: "like")
            // doPostLikeFromServer()
        } else {
            strBtnFavId = "0"
            theCell?.likeImg.image = UIImage(named: "heartBlack")
        }
        doPostLikeFromServer()
    }
    
    // MARK: - for Like Post

    func doPostLikeFromServer() {
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strMyUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        print(strLikeId)
        let param = [Constants.KEY_MYUSER_ID: strMyUserId,
                     Constants.KEY_POST_ID: strPostId,
                     Constants.KEY_LIKE: strBtnFavId,
                     Constants.KEY_POST_USERId: strPostUserId,
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        PSWebServiceAPI.doLikePost(param, completion: { response in
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    /*  if msg == "Post disliked successfully"{

                         self.strLikeId = "0"

                     }else{
                         self.strLikeId = "1"

                     }
                     self.homeCollectionView.reloadData()
                     doAllListPostFromServer(offset: offset, limit: limit)
                     */
                    self.doAllListPostFromServer()
                }
            }
        })
    }
    
    @IBAction func menuGalleryBtnAction(_ sender: Any) {
        playerLayer.player?.pause()
        tabBarController?.tabBar.isHidden = true
        let TopStoryboard = UIStoryboard(name: "TopStoryboard", bundle: nil)
        let objTopScreen = TopStoryboard.instantiateViewController(withIdentifier: "TopScreenVC") as! TopScreenVC
        navigationController?.pushViewController(objTopScreen, animated: true)
    }

    @IBAction func messageBtnAction(_ sender: Any) {
        playerLayer.player?.pause()
        tabBarController?.tabBar.isHidden = true
        let msgStoryboard = UIStoryboard(name: "MessageStoryboard", bundle: nil)
        let objUMessage = msgStoryboard.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        navigationController?.pushViewController(objUMessage, animated: true)
    }

    @IBAction func playTreasureStartBtnAction(_ sender: Any) {
        treasureHuntStartView.isHidden = true
        treasureHuntPlayView.isHidden = false
    }

    @IBAction func skipTreasureStartBtnAction(_ sender: Any) {
        coinPopupView.isHidden = true
        view.isHidden = false
    }

    @IBAction func closeTreasureStartBtnAction(_ sender: Any) {
        treasureHuntPlayView.isHidden = true
        treasureHuntStartView.isHidden = false
    }

    @IBAction func testBounceBtnAction(_ sender: Any) {
        bounceMainView.isHidden = false
        fireArchievedView.isHidden = false
        bounceOnBoardingView.isHidden = true
    }

    @IBAction func fireContinueBtnAction(_ sender: Any) {
        bounceMainView.isHidden = false
        fireArchievedView.isHidden = true
        bounceOnBoardingView.isHidden = false
    }
    
    @IBAction func fireNotNowBtnAction(_ sender: Any) {
        bounceMainView.isHidden = true
        fireArchievedView.isHidden = true
    }

    @IBAction func acceptBounceBtnAction(_ sender: Any) {
        bouncePlayView.isHidden = false
        bounceOnBoardingView.isHidden = true
    }

    @IBAction func declineBounceBtnAction(_ sender: Any) {
        bouncePlayView.isHidden = true
        bounceOnBoardingView.isHidden = true
        fireArchievedView.isHidden = false
    }

    @IBAction func fireBouncePlayScrollBtnAction(_ sender: Any) {
        // self.playBounceCollectionView.scrollToItem(at:IndexPath(item: nextItem as IndexPath, section: 0), at: .right, animated: true)
        // let collectionView:UICollectionView;

        // get cell size
        //   let cellSize = CGSizeMake(self.view.frame.width, self.view.frame.height);

        // get current content Offset of the Collection view
        let contentOffset = playBounceCollectionView.contentOffset

        // scroll to next cell
        playBounceCollectionView.scrollRectToVisible(CGRect(x: contentOffset.x + playBounceCollectionView.frame.size.width, y: contentOffset.y, width: playBounceCollectionView.frame.size.width, height: playBounceCollectionView.frame.size.height), animated: true)
    }
    
    private func showProgressView(text: String = "") {
        DispatchQueue.main.async {
            self.progressView.messageView.text = text
            self.progressView.startAnimation()
            self.view.addSubview(self.progressView)
        }
    }

    private func hideProgressView() {
        DispatchQueue.main.async {
            self.progressView.stopAnimation()
            self.progressView.removeFromSuperview()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableOne {
            return arrNumber.count
        } else if tableView == tableTwo {
            return arrNumber.count
        } else {
            return arrNumber.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableOne {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CoinTableViewCell
            cell.selectionStyle = .none
            cell.textLabel?.text = arrNumber[indexPath.row] as? String
            cell.textLabel?.textAlignment = .center
            return cell
        } else if tableView == tableTwo {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOne", for: indexPath) as! CoinTableViewCell
            cell.selectionStyle = .none
            cell.textLabel?.text = arrNumber[indexPath.row] as? String
            cell.textLabel?.textAlignment = .center
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellTwo", for: indexPath) as! CoinTableViewCell
            cell.selectionStyle = .none
            cell.textLabel?.text = arrNumber[indexPath.row] as? String
            cell.textLabel?.textAlignment = .center
            return cell
        }
    }

    // MARK: - UITableViewDelegate

    private func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        // let lastRow = tableView.indexPathsForVisibleRows()?.last as? NSIndexPath
        let lastRow = tableThree.indexPathsForVisibleRows?.last
        if indexPath.row == lastRow?.row {
            if scrollToTime == true {
                let indexPath = NSIndexPath(row: groupNoToScroll, section: 0)
                tableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                scrollToTime = false
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let contentView = UIView()
        
        return contentView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var strOne = String()
        var strTwo = String()
        var strThree = String()
        if tableView == tableOne {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CoinTableViewCell
            cell.selectionStyle = .none
            strOne = cell.textLabel?.text! ?? ""
        
        } else if tableView == tableTwo {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOne", for: indexPath) as! CoinTableViewCell
            cell.selectionStyle = .none
            strTwo = cell.textLabel?.text! ?? ""
         
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellTwo", for: indexPath) as! CoinTableViewCell
            cell.selectionStyle = .none
            strThree = cell.textLabel?.text! ?? ""
        }
        coinCountLbl.text = strOne + strTwo + strThree
        // self.scrollToTop()
    }

    func animateButton() {
        // animate button
        var bounds = CGRect.zero
        let shrinkSize = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width - 15, height: bounds.size.height)
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.5,
                       usingSpringWithDamping: 0.1,
                       initialSpringVelocity: 5,
                       options: .curveLinear,
                       animations: { shrinkSize },
                       completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as! UILabel?
        if view == nil { // if no label there yet
            pickerLabel = UILabel()
            // color the label's background
            let hue = CGFloat(row)/CGFloat(arrNumber.count)
            // pickerLabel!.backgroundColor = UIColor(hue: hue, saturation: 5.0, brightness: 1.0, alpha: 1.0)
            // pickerLabel!.backgroundColor = UIColor(red: 244.0/255.0, green: 245.0/255.0, blue: 246.0/255.0, alpha: 1.0)
        }
        let titleData = arrNumber[row]
        let myTitle = NSAttributedString(string: titleData as! String, attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black])
        pickerLabel!.attributedText = myTitle
        pickerLabel!.textAlignment = .center
        //  pickerLabel?.backgroundColor = UIColor(red: 244.0/255.0, green: 245.0/255.0, blue: 246.0/255.0, alpha: 0.55)
   
        return pickerLabel!

        // return view!
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerOne {
            return arrNumber.count
        } else if pickerView == pickerTwo {
            return arrNumber.count
        } else if pickerView == pickerThree {
            return arrNumber.count
        } else {
            return arrNumber.count
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // nameDisplay.text = arrNumber[row]
        var strOne = String()
        var strTwo = String()
        var strThree = String()
        if pickerView == pickerOne {
            strOne = arrNumber[row] as! String
            print(strOne)
        } else if pickerView == pickerTwo {
            strTwo = arrNumber[row] as! String
            print(strTwo)
        } else if pickerView == pickerThree {
            strThree = arrNumber[row] as! String
            print(strThree)
        } else {
            strThree = arrNumber[row] as! String
            print(strThree)
        }
        let str = strOne + strTwo + strThree
        coinCountLbl.text = str
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerOne {
            return arrNumber[row] as? String
        } else if pickerView == pickerTwo {
            return arrNumber[row] as? String
        } else if pickerView == pickerThree {
            return arrNumber[row] as? String
        } else {
            return arrNumber[row] as? String
        }
    }

    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // do something with the resulting selected row
        // reset the picker to the middle of the long list
        let position = pickerDataSize/2
        coinPicker.selectRow(position, inComponent: 0, animated: false)
    }

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSize
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        zoomMainView.isHidden = true
        tabBarController?.tabBar.isHidden = false
    }
  
    // @IBAction func playBtnAction(_ sender: UIButton) {
    //   print(playBtn.currentImage)
//        if playBtn.currentImage == UIImage(named: "play") {
//            pickerReload()
//            playBtn.setImage(UIImage.init(named: "pause"), for: UIControl.State.selected)
//
//        }else if playBtn.currentImage == UIImage(named: "pause"){
//            playBtn.setImage(UIImage.init(named: "play"), for: UIControl.State.selected)
//        }

//        if sender.isSelected == true {
//                //sender.isSelected = false
//                print("not Selected")
//            playBtn.setImage(UIImage.init(named: "pause"), for: UIControl.State.normal)
//            }else {
//             //   sender.isSelected = true
//                print("Selected")
//                playBtn.setImage(UIImage.init(named: "play"), for: UIControl.State.normal)
//                pickerReload()
//            }
    // }
    
    @IBAction func playBtnAction(_ sender: Any) {
        // pickerOne.delegate = self
        // pickerOne.reloadAllComponents()
        // self.pickerOne.selectRow(0, inComponent: 0, animated: false)
        // pickerTwo.selectRow(0, inComponent: 0, animated: true)

        // myT = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(HomeVC.movePicker), userInfo: nil, repeats: true)
        var strBtnId = String()
        if strBtnId == "0" {
            playBtn.setImage(UIImage(named: "pause"), for: UIControl.State.normal)
            strBtnId = "1"
            // doPostLikeFromServer()
        } else {
            playBtn.setImage(UIImage(named: "play"), for: UIControl.State.normal)
            strBtnId = "0"
            pickerReload()
        }
    }

    func pickerReload() {
        pickerOne.selectRow(Int(arc4random()) % pickerView(pickerOne, numberOfRowsInComponent: 0), inComponent: 0, animated: true)
        pickerTwo.selectRow(Int(arc4random()) % pickerView(pickerTwo, numberOfRowsInComponent: 0), inComponent: 0, animated: true)
        pickerThree.selectRow(Int(arc4random()) % pickerView(pickerThree, numberOfRowsInComponent: 0), inComponent: 0, animated: true)
        pickerFour.selectRow(Int(arc4random()) % pickerView(pickerFour, numberOfRowsInComponent: 0), inComponent: 0, animated: true)
    }
    
    // MARK: - move picker

//     @objc func movePicker()  {
//
//
//       let position = Int(arc4random_uniform(0) + 1)
//
//
//       pickerOne.selectRow(position, inComponent: 0, animated: true)
//    pickerOne.showsSelectionIndicator = true
//
//       if position == 0 || position == 3 || position == 6 || position == 9  {
//
//           myT.invalidate()
//
//           let alert = UIAlertController(title: "You Won!!", message: "Congratulations!!!", preferredStyle: .alert)
//        let buttonOK = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
//           let playAgain = UIAlertAction(title: "Play Again!", style: .default, handler: { (action) in
//
//               self.myT = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(HomeVC.movePicker), userInfo: nil, repeats: true)
//           })
//
//           alert.addAction(buttonOK)
//           alert.addAction(playAgain)
//
//           present(alert, animated: true, completion: nil)
//
//
//
//       }
//
    //   }
    
    // MARK: - for Top Post

    func doStorePointsFromServer() {
        guard PSUtil.reachable() else {
            PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.NoInternet)
            return
        }
        let defult = UserDefaults.standard
        let strDeviceToken = defult.object(forKey: Constants.DEVICETOKEN)
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        // "image,video,blog" - post trype
        let param = [Constants.KEY_MYUSER_ID: strUserId,
                     Constants.KEY_POST_ID: "",
                     Constants.KEY_TYPE: "points",
                     Constants.KEY_NUMBER: "",
                     Constants.KEY_POST_TYPE: "",
                     Constants.KEY_POST_USERId: "",
                     Constants.KEY_DEVICE_ID: "\(strDeviceToken ?? "")",
                     Constants.DEVICE_TYPE: "I"] as [String: Any]

        // PSUtil.showProgressHUD()
        print(param)
        progressView = AcuantProgressView(frame: view.frame, center: view.center)
        showProgressView(text: "Loading...")
        PSWebServiceAPI.doStorePointList(param, completion: { response in
            // PSUtil.hideProgressHUD()
            self.hideProgressView()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    //    self.imagesArr = response[Constants.KEY_POSTDATA] as? [Any] ?? [Any]()

                } else {
                    PSUtil.hideProgressHUD()
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    PSUtil.showAlertFromController(controller: self, withMessage: msg ?? PSAPI.SomeThingWrong)
                }
            } else {
                PSUtil.hideProgressHUD()
                PSUtil.showAlertFromController(controller: self, withMessage: PSAPI.SomeThingWrong)
            }
        })
    }
}

extension HomeVC: GooeyCellDelegate {
    func gooeyCellActionConfig(for cell: UITableViewCell, direction: GooeyEffect.Direction) -> GooeyEffectTableViewCell.ActionConfig? {
        let color = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
        let image = direction == .toLeft ? #imageLiteral(resourceName: "image_cross") : #imageLiteral(resourceName: "image_mark")
        let isCellDeletingAction = direction == .toLeft
        let effectConfig = GooeyEffect.Config(color: color, image: image)
        let actionConfig = GooeyEffectTableViewCell.ActionConfig(effectConfig: effectConfig,
                                                                 isCellDeletingAction: isCellDeletingAction)
        return actionConfig
    }
    
    func gooeyCellActionTriggered(for cell: UITableViewCell, direction: GooeyEffect.Direction) {
        switch direction {
        case .toLeft: break
        // removeCell(cell)
        case .toRight:
            break
        }
    }
    
    /* private func removeCell(_ cell: UITableViewCell) {
         guard let indexPath = tableView.indexPath(for: cell) else { return }
         objects.remove(at: indexPath.row)
         tableView.beginUpdates()
         tableView.deleteRows(at: [indexPath], with: .fade)
         tableView.endUpdates()
     }
      */
}

extension UIImage {
    convenience init?(withContentsOfUrl url: URL) throws {
        let imageData = try Data(contentsOf: url)
    
        self.init(data: imageData)
    }
}

extension HomeVC: ImageScrollViewDelegate {
    func imageScrollViewDidChangeOrientation(imageScrollView: ImageScrollView) {
        print("Did change orientation")
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        print("scrollViewDidEndZooming at scale \(scale)")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll at offset \(scrollView.contentOffset)")
    }
}

