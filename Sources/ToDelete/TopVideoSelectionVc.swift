//
//  TopVideoSelectionVc.swift
//  bloook
//
//  Created by Tech Astha on 30/06/21.
//

import UIKit
import AVKit
import GPVideoPlayer
class TopVideoSelectionVc: UIViewController {
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var buttonPlaySecondBtn: UIButton!
    @IBOutlet weak var buttonPlay: UIButton!
    @IBOutlet weak var imageHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var heightImg: NSLayoutConstraint!
    @IBOutlet weak var lifetimeTijmeLbl: UILabel!
    @IBOutlet weak var annualLbl: UILabel!
    @IBOutlet weak var dailyRankLbl: UILabel!
    @IBOutlet weak var multipleView: UIView!
    @IBOutlet weak var viewCountLbl: UILabel!
    @IBOutlet weak var likeImg: UIImageView!
    @IBOutlet weak var likeCountLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var topStatusView: UIView!
    @IBOutlet weak var userPhotoImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userDateLbl: UILabel!
    @IBOutlet weak var userDescriptionLbl: UILabel!
    @IBOutlet weak var topSelectedImg: UIImageView!
    @IBOutlet weak var commentLbl: UILabel!
    
    @IBOutlet weak var likeBtn: UIButton!
    var dict = NSDictionary()
    var postDict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonPlay.isHidden = false
      //  buttonPlaySecondBtn.isHidden = false
        postDict = dict[Constants.KEY_POSTS] as? NSDictionary ?? NSDictionary()
        let UserDict = postDict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
        let strFileType = "\(postDict.value(forKey: Constants.KEY_TYPE) ?? "")"
        let strPostPhoto = postDict[Constants.KEY_FILE_NAME] as? String
        let strUserDesc = postDict[Constants.KEY_DESCRIPTION] as? String
        let strLikeCount = postDict[Constants.KEY_LIKE_COUNT] as? String
        let strViewCount = postDict[Constants.KEY_VIEWS_COUNT] as? String
        let strProfilePhoto = UserDict[Constants.KEY_PROFILE_PHOTO] as? String
        let strUserName = UserDict[Constants.KEY_NAME] as? String
        let strDailyPoints = dict[Constants.KEY_DAILY_POINTS] as? String
        let strAnnualPoints = dict[Constants.KEY_ANNUAL_POINTS] as? String
        let strLifetimePoints = dict[Constants.KEY_LIFETIME_POINTS] as? String
       // let strCommentsCount = UserDict[Constants.KEY_COMMENT] as? String
        let strCommentCount = "\(postDict.value(forKey: Constants.KEY_ALL_COMMENTS_COUNT) ?? "")"
        commentLbl.text = "View " + strCommentCount + " " + "Comments"
        
        userDescriptionLbl.text = strUserDesc
        likeCountLbl.text = strLikeCount
        userNameLbl.text = strUserName
        viewCountLbl.text = strViewCount
        if strDailyPoints == "nil"{
            dailyRankLbl.text = "0"
        }else{
           dailyRankLbl.text = strDailyPoints
        }
        annualLbl.text = strAnnualPoints
        lifetimeTijmeLbl.text = strLifetimePoints
        if strProfilePhoto == ""{
            userPhotoImg.sd_setImage(with: URL(string: "img9"), placeholderImage: UIImage(named: "placeholder.png"))
        }else{
            userPhotoImg.sd_setImage(with: URL(string: strProfilePhoto!), placeholderImage: UIImage(named: "placeholder.png"))
        }
        let strUserCreateDate = "\(postDict.value(forKey: Constants.KEY_CREATE_DATE) ?? "")"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateCreate = dateFormatter.date(from: strUserCreateDate)
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: (dateCreate)!) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        userDateLbl.text = myStringafd
        
        let url = URL(string: strPostPhoto!)
        var currentAsset: AVAsset?
        currentAsset = AVAsset(url: url!)
        
        let imageGenerator = AVAssetImageGenerator(asset: currentAsset!)
        imageGenerator.appliesPreferredTrackTransform = true
        let durationSeconds = CMTimeGetSeconds(currentAsset!.duration)
        let midpoint = CMTimeMakeWithSeconds(Float64(durationSeconds / 2.0), preferredTimescale: 600)

        let cgImage: CGImage
        do {
            cgImage = try imageGenerator.copyCGImage(at: midpoint, actualTime: nil)
            let uiImage = UIImage(cgImage: cgImage)
            topSelectedImg.image = uiImage
        } catch {
            print("Error: \(error)")
        }
       
    }
    override func viewDidLayoutSubviews() {
        topStatusView.layer.masksToBounds = true
        topStatusView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        descriptionView.layer.masksToBounds = true
        descriptionView.round(corners: [.topLeft, .topRight], radius: 20)
        mainView.layer.masksToBounds = true
        mainView.round(corners: [.topLeft, .topRight], radius: 20)
        commentView.layer.masksToBounds = true
        commentView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
        mainView.layer.masksToBounds = true
        mainView.round(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func likeBtnAction(_ sender: Any) {
    }
    @IBAction func commentBtnAction(_ sender: Any) {
        //let dict = self.postDataArr[buttonRow] as? NSDictionary ?? NSDictionary()
        let postDict = dict[Constants.KEY_POSTS] as? NSDictionary ?? NSDictionary()
        let strPostId = "\(postDict.value(forKey: Constants.KEY_Id) ?? "")"
        let strPostUserId = "\(postDict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
        self.tabBarController?.tabBar.isHidden = true
        let UserDict = postDict[Constants.KEY_USER] as? NSDictionary ?? NSDictionary()
        let strUserProfileImg = UserDict[Constants.KEY_PROFILE_PHOTO] as? String
        let strName = UserDict[Constants.KEY_NAME] as? String
        let homeDetailStoryboard =  UIStoryboard(name: "HomeDetailStoryboard", bundle: nil)
        let objAllCommentHome = homeDetailStoryboard.instantiateViewController(withIdentifier: "AllCommentHomeVC") as? AllCommentHomeVC
 //TODO: Suresh Find the missing link
        objAllCommentHome?.selectedPostId = strPostId
//        objAllCommentHome?.strPostUserId = strPostUserId
//        objAllCommentHome?.strUserName = strName!
//        objAllCommentHome?.strProfileImg = strUserProfileImg!
        self.navigationController?.pushViewController(objAllCommentHome!, animated: true)
    }
    @IBAction func flagMultipleBtnAction(_ sender: Any) {
        multipleView.isHidden = false
    }
    @IBAction func allLifeTImeBtnAction(_ sender: Any) {
        multipleView.isHidden = true
    }
    @IBAction func buttonPlayAction(_ sender: Any) {
        let strPostPhoto = postDict[Constants.KEY_FILE_NAME] as? String
        if let player = GPVideoPlayer.initialize(with: self.videoView.bounds) {
            player.isToShowPlaybackControls = true
            
            self.videoView.addSubview(player)
            
            let url1 = URL(string: strPostPhoto!)!
            //let videoFilePath = Bundle.main.path(forResource: "video", ofType: "mp4")
          //  let url2 = URL(fileURLWithPath: videoFilePath!)
        
            player.loadVideos(with: [url1])
            player.isToShowPlaybackControls = true
            player.isMuted = true
            player.playVideo()
        }
    }
    @IBAction func profileBtnAction(_ sender: Any) {
        let postDict = dict[Constants.KEY_POSTS] as? NSDictionary ?? NSDictionary()
        let strPostUserId = "\(postDict.value(forKey: Constants.KEY_MYUSER_ID) ?? "")"
        self.tabBarController?.tabBar.isHidden = false
        UserDefaults.standard.set(strPostUserId, forKey: Constants.KEY_ID)
        let otherStoryboard =  UIStoryboard(name: "Profile", bundle: nil)
        let objMyProfile = otherStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
        self.navigationController?.pushViewController(objMyProfile!, animated: true)
        
     }
}
