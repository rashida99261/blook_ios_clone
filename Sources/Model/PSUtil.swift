//
//  PSUtil.swift
//  ProjectStructure
//
//  Created by Nectarbits on 23/03/17.
//  Copyright © 2017 nectarbits. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

class PSUtil
{
    class func showAlertFromController(controller:UIViewController, withMessage message:String)
    {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: PSText.Label.Ok, style: .default, handler: nil))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    class func showAlertFromController(controller:UIViewController, withMessage message:String, andHandler handler:((UIAlertAction) -> Void)?)
    {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: PSText.Label.Ok, style: .default, handler: handler))
        
        controller.present(alert, animated: true, completion: nil)
        
      }
    
    //MARK: - SVProgressHUD Methods
    class func showProgressHUD()
    {
        SVProgressHUD.show(withStatus: "Loading...")
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    class func hideProgressHUD()
    {
        UIApplication.shared.endIgnoringInteractionEvents()
        SVProgressHUD.dismiss()
    }
    
    //Check Network
    class func reachable() -> Bool
    {
        let rechability = Reachability()!
        if rechability.isReachableViaWiFi || rechability.isReachableViaWWAN {
            return true
        }else{
            return false
        }
    }

}
