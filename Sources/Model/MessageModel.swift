//
//  MessageModel.swift
//  Bloook
//
//  Created by Suresh Varma on 10/10/21.
//

import Foundation

struct MessageBase: Codable {
    let status: Int
    let message: String
    var messages: [Message]?
    var singleMessage: Message?
    
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case messages = "d"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do { status = try container.decode(Int.self, forKey: .status) } catch { status = 0
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { message = try container.decode(String.self, forKey: .message) } catch { message = ""
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do {
            messages = try container.decode([Message]?.self, forKey: .messages)
            singleMessage = nil
        } catch {
            do {
                singleMessage = try container.decode(Message?.self, forKey: .messages)
            } catch {
                singleMessage = nil
                print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
            }
            messages = nil
        }
    }
        
}

// MARK: - D
struct Message: Codable {
    let id, userID, partnerID: Int?
        let message, createdAt, updatedAt: String?
        let deletedAt: String?
    let user: User?

    enum CodingKeys: String, CodingKey {
            case id
            case userID = "user_id"
            case partnerID = "partner_id"
            case message
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case deletedAt = "deleted_at"
            case user = "user"
        }
}
