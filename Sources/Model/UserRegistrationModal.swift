//
//  UserRegistrationModal.swift
//  blook
//
//  Created by Tech Astha on 01/01/21.
//

import UIKit

class UserRegistrationModal: NSObject {

    var firstName : String?
    var lastName : String?
    var userName : String?
    var emailAddress : String?
    var password : String?
    var confirmPassword : String?
    var id : String?
    var userTocken : String?
    var mobileNo : String?
    
    override init() {
        super.init()
    }
}
