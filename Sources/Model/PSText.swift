//
//  PSText.swift
//  ProjectStructure
//
//  Created by Nectarbits on 23/03/17.
//  Copyright © 2017 nectarbits. All rights reserved.
//

import Foundation

struct PSText
{
    struct Label
    {
        static let Ok          = "OK"
        static let Done        = "Done"
        static let Cancel      = "Cancel"
        static let fromCamera  = "Take Photo"
        static let fromLibrary = "From Albums"
    }
    
    struct Key
    {
        static let appName     = "bloook"
        static let appLanguage = "applanguage"
        static let appMessages = "appmessages"
        static let X_API_KEY = "X-API-KEY"
    }
}
