//
//  SettingModel.swift
//  Bloook
//
//  Created by Suresh Varma on 24/01/22.
//

import Foundation
struct SettingBase: Codable {
    let status: Int
    let message: String
    let setting: Setting?
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case setting = "d"
    }
}

// MARK: - D

struct Setting: Codable {
    let id: Int
    let onFireArchived, treasureHunt, treasureHuntOnboarding, bounce: String?
    let bounceOnboarding, createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case onFireArchived = "on_fire_archived"
        case treasureHunt = "treasure_hunt"
        case treasureHuntOnboarding = "treasure_hunt_onboarding"
        case bounce
        case bounceOnboarding = "bounce_onboarding"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

struct SavedSettingBase: Codable {
    let status: Int
    let message: String
    let setting: Setting?
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case setting = "d"
    }
}

struct SavedSetting {
  static var shared = SavedSetting()
    var setting: Setting?
  // useful properties and methods

  private init() {
    // now we can only create new instances within DataProvider
  }
}
