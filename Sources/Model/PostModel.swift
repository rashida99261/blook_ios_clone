//
//  PostModel.swift
//  Bloook
//
//  Created by Suresh Varma on 05/07/21.
//


import Foundation

// MARK: - Posts

struct PostsBase: Codable {
    let status: Int
    let message: String
    let posts: [Post]?
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case posts = "d"
    }
}

// MARK: - D
struct Post: Codable {
    let id: Int?
    let postId: Int?
    let userID: Int?
    let filename: String?
    let thumbnail: String?
    let dDescription: String?
    let access: String?
    let viewsCount: String?
    let privacy: String?
    let type: PostType?
    let status, createdAt, updatedAt: String?
    let deletedAt: String?
    let likesCount, commentsCount: Int?
    let userLike: Int?
    var user: User?
    let comments: [Comment]?
    let ownPost, alreadyViewed, views, bounce: Int?
    let points: Points?
    var sharePoints: [Points]?
    let commentStatus: Int?
    let isFollowed: String?
    let isShared: Int?
    let shares: [Post]?
    let duration: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case postId = "post_id"
        case filename
        case dDescription = "description"
        case access, views, privacy, type, status
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case likesCount = "likes_count"
        case commentsCount = "comments_count"
        case userLike, user, comments
        case ownPost = "own_post"
        case alreadyViewed = "already_viewed"
        case viewsCount = "views_count"
        case bounce = "bounce"
        case points
        case commentStatus = "comment_status"
        case isFollowed = "is_followed"
        case thumbnail = "thumbnail"
        case isShared = "is_shared"
        case shares = "shares"
        case duration = "duration"
        
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do { id = try container.decode(Int.self, forKey: .id) } catch { id = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { userID = try container.decode(Int?.self, forKey: .userID) } catch { userID = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        
        do { filename = try container.decode(String?.self, forKey: .filename) } catch { filename = "0"
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { thumbnail = try container.decode(String?.self, forKey: .thumbnail) } catch { thumbnail = nil
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { dDescription = try container.decode(String?.self, forKey: .dDescription) } catch { dDescription = "0"
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
            
        }
        do { access = try container.decode(String?.self, forKey: .access) } catch { access = "0"
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
            
        }
        do { viewsCount = try container.decode(String?.self, forKey: .viewsCount) } catch { viewsCount = "0"
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { privacy = try container.decode(String?.self, forKey: .privacy) } catch { privacy = "0"
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { type = try container.decode(PostType?.self, forKey: .type) } catch { type = nil
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { status = try container.decode(String?.self, forKey: .status) } catch { status = "0"
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { createdAt = try container.decode(String?.self, forKey: .createdAt) } catch { createdAt = "0"
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { updatedAt = try container.decode(String?.self, forKey: .updatedAt) } catch { updatedAt = "0"
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { deletedAt = try container.decode(String?.self, forKey: .deletedAt) } catch { deletedAt = "0"
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { likesCount = try container.decode(Int?.self, forKey: .likesCount) } catch { likesCount = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { commentsCount = try container.decode(Int?.self, forKey: .commentsCount) } catch { commentsCount = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { commentStatus = try container.decode(Int?.self, forKey: .commentStatus) } catch { commentStatus = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { isFollowed = try container.decode(String?.self, forKey: .isFollowed) } catch { isFollowed = "0"
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { userLike = try container.decode(Int?.self, forKey: .userLike) } catch { userLike = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { user = try container.decode(User?.self, forKey: .user) } catch { user = nil
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        
        do { comments = try container.decode([Comment]?.self, forKey: .comments) } catch { comments = nil
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { points = try container.decode(Points?.self, forKey: .points) } catch {
            do { sharePoints = try container.decode([Points]?.self, forKey: .points) } catch {
                sharePoints = nil
                //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
            }
            points = nil
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { ownPost = try container.decode(Int?.self, forKey: .ownPost) } catch { ownPost = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { alreadyViewed = try container.decode(Int?.self, forKey: .alreadyViewed) } catch { alreadyViewed = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { views = try container.decode(Int?.self, forKey: .views) } catch { views = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { bounce = try container.decode(Int?.self, forKey: .bounce) } catch { bounce = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { isShared = try container.decode(Int?.self, forKey: .isShared) } catch { isShared = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { shares = try container.decode([Post]?.self, forKey: .shares) } catch { shares = nil
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { postId = try container.decode(Int?.self, forKey: .postId) } catch { postId = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { duration = try container.decode(Int?.self, forKey: .duration) } catch {
            duration = 0
            //print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
    }
    func imageURL() -> URL {
        return URL(string: (filename ?? (user?.profilePhoto ?? "")))!
    }
    func thumbImageURL() -> URL {
        guard let thumbnail = thumbnail else {
            return imageURL()
        }

        return URL(string: thumbnail)!
    }
    func videoDuration() -> String {
        return (duration ?? 0).secondsToTime()
    }
}

enum Access: String, Codable {
    case accessPublic = "public"
}

enum PostType: String, Codable {
    case blog = "blog"
    case image = "image"
    case video = "video"
}


struct Points: Codable {
    let totalPoints: String?
    let dailyPoints: String?
    let annualPoints, lifetimePoints: String?

    enum CodingKeys: String, CodingKey {
        case totalPoints = "total_points"
        case dailyPoints = "daily_points"
        case annualPoints = "annual_points"
        case lifetimePoints = "lifetime_points"
    }
}

enum AnnualPointsUnion: Codable {
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(AnnualPointsUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for AnnualPointsUnion"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

enum DailyPoints: Codable {
    case integer(Int)
    case string(String)
    case null

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if container.decodeNil() {
            self = .null
            return
        }
        throw DecodingError.typeMismatch(DailyPoints.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for DailyPoints"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        case .null:
            try container.encodeNil()
        }
    }
}


struct SavedPosts {
  static var shared = SavedPosts()
    var posts: [SavedPost]?
  // useful properties and methods

  private init() {
    // now we can only create new instances within DataProvider
  }
}



struct PostsDetailBase: Codable {
    let status: Int
    let message: String
    let post: Post?
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case post = "d"
    }
}
