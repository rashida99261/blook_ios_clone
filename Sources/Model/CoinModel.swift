//
//  CoinModel.swift
//  Bloook
//
//  Created by Suresh Varma on 02/08/21.
//

import Foundation
struct CoinBase: Codable {
    let status: Int
    let message: String
    let coins: [Coin]?
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case coins = "d"
    }
}

// MARK: - D
struct Coin: Codable {
    let coin: String?
    let date: String?
    let type: CoinType?

    enum CodingKeys: String, CodingKey {
        case coin
        case date = "Date"
        case type = "type"
    }
}

enum CoinType: String, Codable {
    case platinum = "platinum"
    case diamond = "blue diamond"
}
