//
//  CommentsModel.swift
//  Bloook
//
//  Created by Suresh Varma on 12/07/21.
//

import Foundation

// MARK: - CommentsBase
struct CommentsBase: Codable {
    let status: Int
    let message: String
    let postDetail: PostDetail?
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case postDetail = "d"
    }
}

// MARK: - D
struct PostDetail: Codable {
    let post: Post?
    let ownPost: Int?

    enum CodingKeys: String, CodingKey {
        case post
        case ownPost = "own_post"
    }
}


// MARK: - Comment
struct Comment: Codable {
    let id: Int?
    let postID: Int?
    let parentCommentID: Int?
    var postUserID, userID: Int?
    let comment, createdAt: String?
    let updatedAt: String?
    let user: User?
    let children: [Comment]?

    enum CodingKeys: String, CodingKey {
        case id
        case postID = "post_id"
        case parentCommentID = "parent_comment_id"
        case postUserID = "post_user_id"
        case comment
        case userID = "user_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case user, children
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do { id = try container.decode(Int.self, forKey: .id) } catch { id = 0
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { userID = try container.decode(Int?.self, forKey: .userID) } catch { userID = 0
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { parentCommentID = try container.decode(Int?.self, forKey: .parentCommentID) } catch { parentCommentID = 0
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { postUserID = try container.decode(Int?.self, forKey: .postUserID) } catch { postUserID = 0
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { postID = try container.decode(Int?.self, forKey: .postID) } catch { postID = 0
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { comment = try container.decode(String?.self, forKey: .comment) } catch { comment = nil
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { createdAt = try container.decode(String?.self, forKey: .createdAt) } catch { createdAt = nil
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { updatedAt = try container.decode(String?.self, forKey: .updatedAt) } catch { updatedAt = nil
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { user = try container.decode(User?.self, forKey: .user) } catch { user = nil
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
        do { children = try container.decode([Comment]?.self, forKey: .children) } catch { children = nil
            print("ParseIssue = \(#file) : \(#function) : \(#line) : \(#column)")
        }
    }
}

