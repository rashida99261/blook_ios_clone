//
//  SearchModel.swift
//  Bloook
//
//  Created by Suresh Varma on 04/08/21.
//

import Foundation


struct SearchBase: Codable {
    let status: Int
    let message: String
    let searchModel: SearchModel?
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case searchModel = "d"
    }
}

struct SearchModel: Codable {
    let searchPosts: [SearchPost]?
    let searchUsers: [SearchUser]?
    enum CodingKeys: String, CodingKey {
        case searchPosts = "posts"
        case searchUsers = "users"
    }
}
    
struct SearchUser: Codable {
    let user: User?
}

struct SearchPost: Codable {
    let postID: Int?
    let totalPoints: String?
    let posts: Post?
    

    enum CodingKeys: String, CodingKey {
        case posts
        case postID = "post_id"
        case totalPoints = "total_points"
    }
}


struct GenericResponse: Codable {
    let status: Int
    let message: String
    let posts: String?
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case posts = "d"
    }
}
