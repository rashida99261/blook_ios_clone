//
//  NotificationModel.swift
//  Bloook
//
//  Created by Suresh Varma on 18/10/21.
//

import Foundation
struct NotificationBase: Codable {
    let status: Bool
    let statusCode: Int
    let message: String
    let notifications: [NotificationModel]?

    enum CodingKeys: String, CodingKey {
        case status
        case statusCode = "status_code"
        case message
        case notifications = "data"
    }
}

// MARK: - ShareDatum

struct NotificationModel: Codable {
    let notificationID: Int?
    let image: String?
    let name: String?
    let userID: Int?
    let routeId: Int?
    let type: NotificationType?
    let datumDescription, dateTime: String?
    let isRead: Int?
    let isRedirect: String?

    enum CodingKeys: String, CodingKey {
        case notificationID = "notification_id"
        case image, name
        case userID = "user_id"
        case routeId = "route_id"
        case datumDescription = "description"
        case dateTime = "date_time"
        case isRead = "is_read"
        case isRedirect = "is_redirect"
        case type = "type"
    }
    enum NotificationType: String, Codable {
        case wallet = "wallet"
        case profile = "profile"
        case post = "post"
        case invite = "invite"
        case bounce = "bounce"
        case chat = "chat"
    }
}

struct NotificationReadBase: Codable {
    let status: Bool
    let statusCode: Int
    let message: String
    let data: Bool

    enum CodingKeys: String, CodingKey {
        case status
        case statusCode = "status_code"
        case message
        case data = "data"
    }
}

