//
//  GroupModel.swift
//  Bloook
//
//  Created by Suresh Varma on 23/10/21.
//

import Foundation
struct GroupResponse: Codable {
    let status: Int
    let message: String
    let groups: [GroupModel]
    
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case groups = "d"
    }
}
struct GroupModel: Codable {
    let id, groupID, memberID: Int?
    let status: String?
    let createdBy: Int?
    let isArchive: Int?
    let createdAt, updatedAt: String?
    let group: Group?
    let message: String?
    let user: User?

    enum CodingKeys: String, CodingKey {
        case id
        case groupID = "group_id"
        case memberID = "member_id"
        case status
        case createdBy = "created_by"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isArchive = "is_archive"
        case group, message, user
    }
}
struct Group: Codable {
    let groupId: Int
    let groupName: String?
    enum CodingKeys: String, CodingKey {
        case groupId = "id"
        case groupName = "group_name"
    }
}
