//
//  UserModel.swift
//  Bloook
//
//  Created by Suresh Varma on 11/07/21.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let userBase = try? newJSONDecoder().decode(UserBase.self, from: jsonData)

import Foundation

// MARK: - UserBase

struct UserBase: Codable {
    let status: Int
    let message: String
    let user: User?
    var users: [User]?

    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case user = "d"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do { status = try container.decode(Int.self, forKey: .status) } catch { status = 0 }
        do { message = try container.decode(String.self, forKey: .message) } catch { message = "" }

        do {
            user = try container.decode(User?.self, forKey: .user)
            
        } catch {
            do {
                let allUsers = try container.decode([User]?.self, forKey: .user)
                user = nil
                users = allUsers

            } catch {
                user = nil
                users = nil
            }
        }
        do {
            users = try container.decode([User]?.self, forKey: .user)
        } catch { users = nil }
    }

    func isValid() -> Bool {
        return status == 200
    }
}

// MARK: - D

struct User: Codable {
    let id: Int
    let firstname: String?
    let lastname: String?
    let title, username, mobile, email: String?
    let emailVerifiedAt: String?
    let bounceStatus: Int?
    var profilePhoto: String?
    let providerType, providerToken: String?
    let createdAt, updatedAt: String?
    let deletedAt: String?
    let checkFollowUser, postsCount: Int?
    let followersCount: Int?
    let followerCount: Int?
    let followers: [Follower]?
    let posts: [Post]?
    let name: String?
    let isAdmin: Int?

    enum CodingKeys: String, CodingKey {
        case id, firstname, lastname, title, username, mobile, email
        case emailVerifiedAt = "email_verified_at"
        case bounceStatus = "bounce_status"
        case profilePhoto = "profile_photo"
        case providerType = "provider_type"
        case providerToken = "provider_token"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case isAdmin = "is_admin"
        case checkFollowUser = "check_follow_user"
        case postsCount = "posts_count"
        case followersCount = "followers_count"
        case followerCount = "follower_count"
        case followers, posts, name
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do { id = try container.decode(Int.self, forKey: .id) } catch { id = 0 }
        do { firstname = try container.decode(String?.self, forKey: .firstname) } catch { firstname = nil }
        do { lastname = try container.decode(String?.self, forKey: .lastname) } catch { lastname = nil }
        do { title = try container.decode(String?.self, forKey: .title) } catch { title = nil }
        do { username = try container.decode(String?.self, forKey: .username) } catch { username = nil }
        do { mobile = try container.decode(String?.self, forKey: .mobile) } catch { mobile = nil }
        do { email = try container.decode(String?.self, forKey: .email) } catch { email = nil }
        do { emailVerifiedAt = try container.decode(String?.self, forKey: .emailVerifiedAt) } catch { emailVerifiedAt = nil }
        do { bounceStatus = try container.decode(Int?.self, forKey: .bounceStatus) } catch { bounceStatus = 0 }
        do { profilePhoto = try container.decode(String?.self, forKey: .profilePhoto) } catch { profilePhoto = nil }
        do { providerType = try container.decode(String?.self, forKey: .providerType) } catch { providerType = nil }
        do { providerToken = try container.decode(String?.self, forKey: .providerToken) } catch { providerToken = nil }
        do { createdAt = try container.decode(String?.self, forKey: .createdAt) } catch { createdAt = nil }
        do { updatedAt = try container.decode(String?.self, forKey: .updatedAt) } catch { updatedAt = nil }
        do { deletedAt = try container.decode(String?.self, forKey: .deletedAt) } catch { deletedAt = nil }
        do { checkFollowUser = try container.decode(Int?.self, forKey: .checkFollowUser) } catch { checkFollowUser = 0 }
        do { postsCount = try container.decode(Int?.self, forKey: .postsCount) } catch { postsCount = 0 }
        do { isAdmin = try container.decode(Int?.self, forKey: .isAdmin) } catch { isAdmin = 0 }

        do { name = try container.decode(String?.self, forKey: .name) } catch { name = nil }

        do {
            followersCount = try container.decode(Int?.self, forKey: .followersCount)
        } catch {
                followersCount = 0
            
        }
        do {
            followerCount = try container.decode(Int?.self, forKey: .followerCount)
        } catch {
                followerCount = 0
        }
        do {
            followers = try container.decode([Follower]?.self, forKey: .followers)
        } catch {
            followers = nil
        }

        do {
            posts = try container.decode([Post]?.self, forKey: .posts)
        } catch {
            posts = nil
        }
    }

    func fullName() -> String {
        if firstname != nil {
            if lastname != nil {
                return (firstname ?? "") + " " + (lastname ?? "")
            }
            return firstname ?? "Unknown"
        } else if username != nil {
            return username ?? "Unknown"
        } else if name != nil {
            return name ?? "Unknown"
        }
        return "Unknown"
    }

    func titleText() -> String {
        if title == "" || title == "<null>" {
            return ""
        } else {
            return title ?? ""
        }
    }

    func profilePhotoURL() -> URL {
        return URL(string: profilePhoto ?? "")!
    }
}

// MARK: - Follower

struct Follower: Codable {
    let id: Int
    var fromUserID: FromUserID?
    let fromUserIDInt: Int?
    let createdAt: String?
    let toUserID: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case fromUserID = "from_user_id"
        case toUserID = "to_user_id"
        case createdAt = "created_at"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        toUserID = try container.decode(Int.self, forKey: .toUserID)
        createdAt = try container.decode(String.self, forKey: .createdAt)

        do {
            fromUserID = try container.decode(FromUserID.self, forKey: .fromUserID)
            fromUserIDInt = 0
        } catch {
            do {
                fromUserID = nil
                fromUserIDInt = try container.decode(Int.self, forKey: .fromUserID)
            } catch {
                fromUserIDInt = 0
                fromUserID = nil
            }
        }
    }
}

// MARK: - FromUserID

struct FromUserID: Codable {
    let userId: Int
    let name: String
    let profilePhoto: String

    enum CodingKeys: String, CodingKey {
        case userId = "id"
        case name
        case profilePhoto = "profile_photo"
    }
}


struct LoggedInUser {
  static var shared = LoggedInUser()
    var user: User?
  // useful properties and methods

  private init() {
    // now we can only create new instances within DataProvider
  }
}
