//
//  LBPostModel.swift
//  Bloook
//
//  Created by Suresh Varma on 19/07/21.
//

import Foundation
struct LeaderboardBase: Codable {
    let status: Int
    let message: String
    let lbPosts: [LBPost]?
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case lbPosts = "d"
    }
}

// MARK: - D
struct LBPost: Codable {
    let postID: Int?
    let totalPoints, dailyPoints, annualPoints: String?
    let lifetimePoints: String?
    let posts: Post?

    enum CodingKeys: String, CodingKey {
        case postID = "post_id"
        case totalPoints = "total_points"
        case dailyPoints = "daily_points"
        case annualPoints = "annual_points"
        case lifetimePoints = "lifetime_points"
        case posts
    }
}


struct SavedPostBase: Codable {
    let status: Int
    let message: String
    let savedPost: [SavedPost]?
    enum CodingKeys: String, CodingKey {
        case status = "sc"
        case message = "m"
        case savedPost = "d"
    }
}

// MARK: - D
struct SavedPost: Codable {
    let postId: Int?
    let id, userId: Int?
    let createdAt: String?
    let post: Post?

    enum CodingKeys: String, CodingKey {
        case postId = "post_id"
        case id = "id"
        case userId = "user_id"
        case createdAt = "created_at"
        case post = "posts"
    }
}
