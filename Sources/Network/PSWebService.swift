//
//  WebService.swift
//  ProjectStructure
//
//  Created by Nectarbits on 23/03/17.
//  Copyright © 2017 nectarbits. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

let PSWebServiceAPI: PSWebService = PSWebService.APIClient
var defult = UserDefaults.standard

let configuration = URLSessionConfiguration.default

class PSWebService: SessionManager
{
    let genericErrorText = PSAPI.SomeThingWrong
    var header = ["Accept":"application/json","Content-Type":"application/x-www-form-urlencoded"]
    var headerAfterLogin :[String :String] {
        return ["Authorization":"\(UserDefaults.standard.object(forKey: "access_token") ?? "")","Accept":"application/json","Content-Type":"application/x-www-form-urlencoded"]
    }
    //,"Content-Type":"application/x-www-form-urlencoded"
  
    static let APIClient: PSWebService =
    {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 180
        configuration.timeoutIntervalForRequest  = 180
        
        return PSWebService(configuration: configuration)
    }()
     
//    func removeAuthorizeToken()
//    {
//        header.removeValue(forKey: PSAPI.accessToken)
//    }
    
    func sendRequest(_ route: Router)-> DataRequest
    {
        let path = route.path.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
     
        if(headerAfterLogin.count == 0)
        {
            header = ["Accept":"application/json","Content-Type":"application/x-www-form-urlencoded"]
        }else{
            
            
            
            header = headerAfterLogin
            
        }
        
        var encoding: ParameterEncoding!
        
        if header["Content-Type"] != nil{
            if header["Content-Type"]! == "application/x-www-form-urlencoded"
            {
                encoding = URLEncoding.default
            }
            else
            {
                encoding = JSONEncoding.default
            }
        }else{
            encoding = JSONEncoding.default
        }
    
 
        if route.method == .get
        {
            return self.requestWithoutCache(path!, method: route.method, parameters: route.parameters, encoding: encoding, headers: header)

        }else{
         //print(header)
            return self.request(path!, method: route.method, parameters: route.parameters, encoding: encoding, headers: header)
        
        }
     }
  }


