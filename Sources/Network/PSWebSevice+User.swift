//
//  PSWebSevice+User.swift
//  GetItDoneUser
//
//  Created by ganesh on 10/15/18.
//  Copyright © 2018 Sumit Gajjar. All rights reserved.
//

import Alamofire
import CoreData
import CoreGraphics.CGImage
import CoreImage.CIImage
import Foundation
import SwiftyJSON
import UIKit.UIImage

extension PSWebService {
    // MARK: - Login

    func doLogin(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void) {
        self.sendRequest(.Login(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                 
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Social Login

    func doSocialLogin(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.SocialLogin(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                 
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Register

    func doRegister(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void) {
        self.sendRequest(.Register(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Verify Otp

    func doVerifyOtp(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.VerifyOtp(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Resend Otp

    func doResendOtp(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.ResendOtp(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Profile Pic

    func doProfilePicture(endUrl: String, profileImg: Data?, parameter: [String: Any]?, onCompletion: (([String: AnyObject]) -> Void)? = nil, onError: ((Error?) -> Void)? = nil)
    {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 300
        manager.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append(profileImg!, withName: Constants.KEY_PROFILE_PICTURE, fileName: "Image.png", mimeType: "image/png")
                
                for (key, value) in parameter! {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            },
            to: endUrl, method: .post, headers: headerAfterLogin, encodingCompletion: { encodingResult in
                print(self.headerAfterLogin)
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        let data = response.data!
                        var dictonary = [String: AnyObject]()
                        do {
                            dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                            onCompletion!(dictonary as [String: AnyObject])
                            
                        } catch let error as NSError {
                            onError?(error)
                        }
                        
                    }.uploadProgress { _ in // main queue by default
                    }
                    return
                    
                case .failure(let encodingError):
                    onError?(encodingError)
                }
            })
    }
    
    // MARK: - Profile Pic

    func doUploadPost(endUrl: String, profileImg: Data?, parameter: [String: Any]?, onCompletion: (([String: AnyObject]) -> Void)? = nil, onError: ((Error?) -> Void)? = nil)
    {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 300
        manager.upload(
            multipartFormData: { multipartFormData in
                // multipartFormData.append(profileImg!, withName: Constants.KEY_FILE_NAME, fileName: "upload.mov", mimeType: "video/mov")
                
                multipartFormData.append(profileImg!, withName: Constants.KEY_FILE_NAME, fileName: "Image.png", mimeType: "image/png")
                
                for (key, value) in parameter! {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            },
            to: endUrl, method: .post, headers: headerAfterLogin, encodingCompletion: { encodingResult in
                print(self.headerAfterLogin)
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        let data = response.data!
                        var dictonary = [String: AnyObject]()
                        do {
                            dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                            onCompletion!(dictonary as [String: AnyObject])
                            
                        } catch let error as NSError {
                            onError?(error)
                        }
                        
                    }.uploadProgress { _ in // main queue by default
                    }
                    return
                    
                case .failure(let encodingError):
                    onError?(encodingError)
                }
            })
    }
    
    // MARK: - Video upload

    func doUploadPostVideo(endUrl: String, videoData: Data?, parameter: [String: Any]?, onCompletion: (([String: AnyObject]) -> Void)? = nil, onError: ((Error?) -> Void)? = nil, currentProgress: ((Double) -> Void)? = nil)
    {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 300
        manager.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append(videoData!, withName: Constants.KEY_FILE_NAME, fileName: "upload.mp4", mimeType: "video/mp4")
                  
                for (key, value) in parameter! {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                  
            },
            to: endUrl, method: .post, headers: headerAfterLogin, encodingCompletion: { encodingResult in
                print(self.headerAfterLogin)
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                          
                        let data = response.data!
                        var dictonary = [String: AnyObject]()
                        do {
                            dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                            onCompletion!(dictonary as [String: AnyObject])
                              
                        } catch let error as NSError {
                            onError?(error)
                        }
                          
                    }.uploadProgress (closure: { (progress) in
                        currentProgress?(progress.fractionCompleted)
                    })
                    return
                      
                case .failure(let encodingError):
                    onError?(encodingError)
                }
            })
    }
    
    // MARK: - Blog upload
    
    func doUploadPostBlog(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.UploadPost(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
//    func doUploadPostBlog(_ parameter:[String: Any]?, completion:@escaping ([String:AnyObject]) -> Void)
//    {
//        self.sendRequest(.UploadPost(parameter!)).responseJSON { response in
//
//            switch response.result
//            {
//            case .success:
//                let data = response.data!
//                var dictonary = [String:AnyObject]()
//                do {
//                    dictonary =  (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject])!
//                    completion(dictonary as [String : AnyObject])
//
//                } catch let error as NSError {
//                    completion(["Error":error.localizedDescription as AnyObject])
//                }
//
//            case .failure(let error):
//
//                completion(["Error":error.localizedDescription as AnyObject])
//            }
//
//        }
//
//    }
    
    // MARK: - Like Post

    func doLikePost(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void) {
        self.sendRequest(.PostLike(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Comment List

    func doCommentList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.PostCommentList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - All Post List

    func getAllPosts(_ parameter: [String: Any]?, completion: @escaping (PostsBase?, Error?) -> Void)
    {
        self.sendRequest(.AllPostList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let postsBase = try? JSONDecoder().decode(PostsBase.self, from: jsonData)
                    if postsBase?.status == 200 {
                        completion(postsBase, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: postsBase?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: postsBase?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func doAllPostList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.AllPostList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Update View
    func updatePostView(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.PostViewed(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Store Coins
    func storeCoin(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.StoreCoins(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    func availableCoin(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.AvailableCoins(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    func getBounceList(_ parameter: [String: Any]?, completion: @escaping (UserBase?, Error?) -> Void)
    {
        self.sendRequest(.BounceUserList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(UserBase.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    // MARK: - All Like User List

    func doAllLikeUserList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.LikeList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - post Comment

    func doPostComment(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.PostComment(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Follow User

    func doFollowUser(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.FolloUser(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Follow User List

    func doFollowUserList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.FollowUSerList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Profile User Data

    func getUserProfile(_ parameter: [String: Any]?, completion: @escaping (UserBase?, Error?) -> Void)
    {
        self.sendRequest(.ProfileUser(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(UserBase.self, from: jsonData)
                    if baseModel?.status == 200 {
                        let defult = UserDefaults.standard
                        let strMyUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
                        if strMyUserId == "\(baseModel?.user?.id ?? 0)" {
                        LoggedInUser.shared.user = baseModel?.user
                    }
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func doProfileUser(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.ProfileUser(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Profile Update
    
    func doProfileUserUpdate(endUrl: String, imageData: Data?, parameter: [String: Any]?, onCompletion: (([String: AnyObject]) -> Void)? = nil, onError: ((Error?) -> Void)? = nil) {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 300
        manager.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append(imageData!, withName: Constants.KEY_PROFILE_PHOTO, fileName: "Image.png", mimeType: "image/png")
                
                for (key, value) in parameter! {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            },
            to: endUrl, method: .post, headers: header, encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        let data = response.data!
                        var dictonary = [String: AnyObject]()
                        do {
                            dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                            onCompletion!(dictonary as [String: AnyObject])
                            
                        } catch let error as NSError {
                            onError?(error)
                        }
                        
                    }.uploadProgress { _ in // main queue by default
                    }
                    return
                    
                case .failure(let encodingError):
                    onError?(encodingError)
                }
            })
    }
    
//    func doProfileUserUpdate(_ parameter:[String: Any]?, completion:@escaping ([String:AnyObject]) -> Void)
//    {
//        self.sendRequest(.ProfileUserUpdate(parameter!)).responseJSON { response in
//
//            switch response.result
//            {
//            case .success:
//                let data = response.data!
//                var dictonary = [String:AnyObject]()
//                do {
//                    dictonary =  (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject])!
//                    completion(dictonary as [String : AnyObject])
//
//                } catch let error as NSError {
//                    completion(["Error":error.localizedDescription as AnyObject])
//                }
//
//            case .failure(let error):
//
//                completion(["Error":error.localizedDescription as AnyObject])
//            }
//
//        }
//
//     }
    
    // MARK: - Profile Setting Update

    func doProfileSettingUpdate(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.ProfileSettingUpdate(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Close Friend List

    func doCloseFriendList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.CLoseFriendList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Close Friend

    func doCloseFriend(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.CloseFriend(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Blocked Friend List

    func doBlockedFriendList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.BlockedFriendList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Blocked Friend

    func doBlockedFriend(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.BlockedFriend(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Delete Comment

    func doDeleteComment(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.CommentDelete(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Faq

    func doFaq(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void) {
        self.sendRequest(.Faq(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Privacy Policy

    func doPrivacyPolicy(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.PrivacyPolicy(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Feedback Create

    func doFeedbackCreate(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.FeedbackCreate(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - User Achievements

    func doUserAchievements(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.UserAchievements(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    func getAchievements(_ parameter: [String: Any]?, completion: @escaping (PostsBase?, Error?) -> Void)
    {
        self.sendRequest(.UserAchievements(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let postsBase = try? JSONDecoder().decode(PostsBase.self, from: jsonData)
                    if postsBase?.status == 200 {
                        completion(postsBase, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: postsBase?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: postsBase?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }

    // MARK: - Top User PointList

    func doTopUserPointList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.TopUserPointList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Forgot Password

    func doForgotPassword(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.PasswordEmail(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - All User list
    func getUserList(_ parameter: [String: Any]?, completion: @escaping (UserBase?, Error?) -> Void)
    {
        self.sendRequest(.UserList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(UserBase.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    func getChatUserList(_ parameter: [String: Any]?, completion: @escaping (UserBase?, Error?) -> Void)
    {
        self.sendRequest(.ChatUserList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(UserBase.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    func sendMessage(_ parameter: [String: Any]?, completion: @escaping (MessageBase?, Error?) -> Void)
        {
            self.sendRequest(.SendMessage(parameter!)).responseJSON { response in
                
                switch response.result {
                case .success:
                    let jsonData = response.data!
                    
                    do {
                        let baseModel = try? JSONDecoder().decode(MessageBase.self, from: jsonData)
                        if baseModel?.status == 200 {
                            completion(baseModel, nil)
                        } else {
                            let error = NSError(domain: NSURLErrorDomain,
                                                code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                                userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                            completion(nil, error)
                        }
                    }
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    
    func sendGroupMessage(_ parameter: [String: Any]?, completion: @escaping (MessageBase?, Error?) -> Void)
        {
            self.sendRequest(.SendGroupMessage(parameter!)).responseJSON { response in
                
                switch response.result {
                case .success:
                    let jsonData = response.data!
                    
                    do {
                        let baseModel = try? JSONDecoder().decode(MessageBase.self, from: jsonData)
                        if baseModel?.status == 200 {
                            completion(baseModel, nil)
                        } else {
                            let error = NSError(domain: NSURLErrorDomain,
                                                code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                                userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                            completion(nil, error)
                        }
                    }
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    func getGroupList(_ parameter: [String: Any]?, completion: @escaping (GroupResponse?, Error?) -> Void)
        {
            self.sendRequest(.GroupList(parameter!)).responseJSON { response in
                
                switch response.result {
                case .success:
                    let jsonData = response.data!
                    
                    do {
                        let baseModel = try? JSONDecoder().decode(GroupResponse.self, from: jsonData)
                        if baseModel?.status == 200 {
                            completion(baseModel, nil)
                        } else {
                            let error = NSError(domain: NSURLErrorDomain,
                                                code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                                userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                            completion(nil, error)
                        }
                    }
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    
    func getGroupMessage(_ parameter: [String: Any]?, completion: @escaping (MessageBase?, Error?) -> Void)
        {
            self.sendRequest(.GroupChatLists(parameter!)).responseJSON { response in
                
                switch response.result {
                case .success:
                    let jsonData = response.data!
                    
                    do {
                        let baseModel = try? JSONDecoder().decode(MessageBase.self, from: jsonData)
                        if baseModel?.status == 200 {
                            completion(baseModel, nil)
                        } else {
                            let error = NSError(domain: NSURLErrorDomain,
                                                code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                                userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                            completion(nil, error)
                        }
                    }
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    func uploadContactList(contactData: Data?, parameter: [String: Any]?, onCompletion: (([String: AnyObject]) -> Void)? = nil, onError: ((Error?) -> Void)? = nil)
    {
    
        let endUrl = UPLOAD_CONTACT_LIST
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 300
        manager.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append(contactData!, withName: "file", fileName: "Contacts.vcf", mimeType: "application/json")
                
                for (key, value) in parameter! {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            },
            to: endUrl, method: .post, headers: headerAfterLogin, encodingCompletion: { encodingResult in
                print(self.headerAfterLogin)
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        let data = response.data!
                        var dictonary = [String: AnyObject]()
                        do {
                            dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                            onCompletion!(dictonary as [String: AnyObject])
                            
                        } catch let error as NSError {
                            onError?(error)
                        }
                        
                    }.uploadProgress { _ in // main queue by default
                    }
                    return
                    
                case .failure(let encodingError):
                    onError?(encodingError)
                }
            })
    }
    
    func getGroupMembers(_ parameter: [String: Any]?, completion: @escaping (UserBase?, Error?) -> Void)
        {
            self.sendRequest(.GroupMemberList(parameter!)).responseJSON { response in
                
                switch response.result {
                case .success:
                    let jsonData = response.data!
                    
                    do {
                        let baseModel = try? JSONDecoder().decode(UserBase.self, from: jsonData)
                        if baseModel?.status == 200 {
                            completion(baseModel, nil)
                        } else {
                            let error = NSError(domain: NSURLErrorDomain,
                                                code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                                userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                            completion(nil, error)
                        }
                    }
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    
    func createGroup(_ parameter: [String: Any]?, completion: @escaping (GroupResponse?, Error?) -> Void)
        {
            self.sendRequest(.CreateGroup(parameter!)).responseJSON { response in
                
                switch response.result {
                case .success:
                    let jsonData = response.data!
                    
                    do {
                        let baseModel = try? JSONDecoder().decode(GroupResponse.self, from: jsonData)
                        if baseModel?.status == 200 {
                            completion(baseModel, nil)
                        } else {
                            let error = NSError(domain: NSURLErrorDomain,
                                                code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                                userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                            completion(nil, error)
                        }
                    }
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    func addMember(_ parameter: [String: Any]?, completion: @escaping (MessageBase?, Error?) -> Void)
        {
            self.sendRequest(.AddMember(parameter!)).responseJSON { response in
                
                switch response.result {
                case .success:
                    let jsonData = response.data!
                    
                    do {
                        let baseModel = try? JSONDecoder().decode(MessageBase.self, from: jsonData)
                        if baseModel?.status == 200 {
                            completion(baseModel, nil)
                        } else {
                            let error = NSError(domain: NSURLErrorDomain,
                                                code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                                userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                            completion(nil, error)
                        }
                    }
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    

    // MARK: - Statistics

    func doStatistics(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.UserStatistics(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    // MARK: - Invite Friend Send

    func doInviteFriendSend(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.InviteFriend(parameter!)).responseJSON { response in
           
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                   
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
               
            case .failure(let error):
               
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Invite Friend List

    func doInviteFriendList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.InviteFriendList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Top Post List

    func doTopPostList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.TopPost(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    func getTopPostList(_ parameter: [String: Any]?, completion: @escaping (LeaderboardBase?, Error?) -> Void)
    {
        self.sendRequest(.TopPost(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let base = try? JSONDecoder().decode(LeaderboardBase.self, from: jsonData)
                    if base?.status == 200 {
                        completion(base, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: base?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: base?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }

    // MARK: - Store Point List

    func doStorePointList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.StorePoints(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }

    // MARK: - Comment Post List

    func getCommentPostList(_ parameter: [String: Any]?, completion: @escaping (CommentsBase?, Error?) -> Void)
    {
        self.sendRequest(.CommentPostList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(CommentsBase.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    func doCommentPostList(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.CommentPostList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
    
    func deletePost(_ parameter: [String: Any]?, completion: @escaping (GenericResponse?, Error?) -> Void)
    {
        self.sendRequest(.DeletePost(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(GenericResponse.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func reportPost(_ parameter: [String: Any]?, completion: @escaping (GenericResponse?, Error?) -> Void)
    {
        self.sendRequest(.ReportPost(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(GenericResponse.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func sharePost(_ parameter: [String: Any]?, completion: @escaping (GenericResponse?, Error?) -> Void)
    {
        self.sendRequest(.SharePost(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(GenericResponse.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func updateCommentSetting(_ parameter: [String: Any]?, completion: @escaping ([String: AnyObject]) -> Void)
    {
        self.sendRequest(.CommentSettings(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let data = response.data!
                var dictonary = [String: AnyObject]()
                do {
                    dictonary = (try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject])!
                    completion(dictonary as [String: AnyObject])
                    
                } catch let error as NSError {
                    completion(["Error": error.localizedDescription as AnyObject])
                }
                
            case .failure(let error):
                
                completion(["Error": error.localizedDescription as AnyObject])
            }
        }
    }
//MARK: - Coins
    func getCoinList(_ parameter: [String: Any]?, completion: @escaping (CoinBase?, Error?) -> Void)
    {
        self.sendRequest(.CoinList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(CoinBase.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func fetchSearchedUser(_ parameter: [String: Any]?, completion: @escaping (SearchBase?, Error?) -> Void)
    {
        self.sendRequest(.SearchUser(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(SearchBase.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func getAllNotifications(_ parameter: [String: Any]?, completion: @escaping (NotificationBase?, Error?) -> Void)
    {
        self.sendRequest(.AllNotifications(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(NotificationBase.self, from: jsonData)
                    if baseModel?.statusCode == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.statusCode ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func markReadAllNotifications(_ parameter: [String: Any]?, completion: @escaping (NotificationReadBase?, Error?) -> Void)
    {
        self.sendRequest(.MarkAllRead(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(NotificationReadBase.self, from: jsonData)
                    if baseModel?.statusCode == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.statusCode ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func markSingleReadNotifications(_ parameter: [String: Any]?, completion: @escaping (NotificationReadBase?, Error?) -> Void)
    {
        self.sendRequest(.SingleRead(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(NotificationReadBase.self, from: jsonData)
                    if baseModel?.statusCode == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.statusCode ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func chatRead(_ parameter: [String: Any]?, completion: @escaping (NotificationReadBase?, Error?) -> Void)
    {
        self.sendRequest(.ChatRead(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(NotificationReadBase.self, from: jsonData)
                    if baseModel?.statusCode == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.statusCode ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func memberLeft(_ parameter: [String: Any]?, completion: @escaping (GenericResponse?, Error?) -> Void)
    {
        self.sendRequest(.MemberLeft(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(GenericResponse.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func groupArchive(_ parameter: [String: Any]?, completion: @escaping (GenericResponse?, Error?) -> Void)
    {
        self.sendRequest(.GroupArchive(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(GenericResponse.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func deleteConversation(_ parameter: [String: Any]?, completion: @escaping (GenericResponse?, Error?) -> Void)
    {
        self.sendRequest(.DeleteConversation(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(GenericResponse.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func makeAdmin(_ parameter: [String: Any]?, completion: @escaping (GenericResponse?, Error?) -> Void)
    {
        self.sendRequest(.MakeAdmin(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(GenericResponse.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func removeMember(_ parameter: [String: Any]?, completion: @escaping (GenericResponse?, Error?) -> Void)
    {
        self.sendRequest(.RemoveMember(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(GenericResponse.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func savePost(_ parameter: [String: Any]?, completion: @escaping (GenericResponse?, Error?) -> Void)
    {
        self.sendRequest(.SavePost(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(GenericResponse.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func savedPostList(_ parameter: [String: Any]?, completion: @escaping (SavedPostBase?, Error?) -> Void)
    {
        self.sendRequest(.SavePostList(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(SavedPostBase.self, from: jsonData)
                    if baseModel?.status == 200 {
                        SavedPosts.shared.posts = baseModel?.savedPost
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func getPostDetail(_ parameter: [String: Any]?, completion: @escaping (PostsDetailBase?, Error?) -> Void)
    {
        self.sendRequest(.PostDetail(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(PostsDetailBase.self, from: jsonData)
                    if baseModel?.status == 200 {
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    func getSettings(_ parameter: [String: Any]?, completion: @escaping (SettingBase?, Error?) -> Void)
    {
        self.sendRequest(.Setting(parameter!)).responseJSON { response in
            
            switch response.result {
            case .success:
                let jsonData = response.data!
                
                do {
                    let baseModel = try? JSONDecoder().decode(SettingBase.self, from: jsonData)
                    if baseModel?.status == 200 {
                        SavedSetting.shared.setting = baseModel?.setting
                        completion(baseModel, nil)
                    } else {
                        let error = NSError(domain: NSURLErrorDomain,
                                            code: baseModel?.status ?? NSURLErrorNotConnectedToInternet,
                                            userInfo: [NSLocalizedDescriptionKey: baseModel?.message ?? self.genericErrorText])
                        completion(nil, error)
                    }
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}

