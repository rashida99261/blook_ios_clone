//
//  Router.swift
//  ProjectStructure
//
//  Created by Nectarbits on 23/03/17.
//  Copyright © 2017 nectarbits. All rights reserved.
//

import UIKit
import Alamofire

//let BasePath = "https://bloook.asthagroup.co/"
//let BasePath = "http://bloook.colure.co/"
//let BasePath = "https://www.harshawebtech.com/"
let BasePath = "https://dev.bloook.com/"

let LOGIN  = BasePath + "api/login"
let REGISTER  = BasePath + "api/signup"
let VERIFYOTP = BasePath + "api/verify"
let RESENDOTP = BasePath + "api/resend"
let PROFILEPICTURE = BasePath + "api/profile-picture"
let UPLOADPOST = BasePath + "api/upload/post"
let POSTLIKE = BasePath + "api/post/like"
let POST_COMMENTLIST = BasePath + "api/post/comment/list"
let ALL_POST_LIST = BasePath + "api/posts"
let LIKELIST = BasePath + "api/post/like/list"
let POST_COMMENT = BasePath + "api/post/comment"
let FOLLOW_USER = BasePath + "api/follow-user"
let FOLLOW_USER_LIST = BasePath + "api/follow-user/list"
let PROFILE_USER = BasePath + "api/profile-user"
let PROFILE_USER_UPDATE = BasePath + "api/profile-user/update"
let PROFILE_SETTING_UPDATE = BasePath + "api/profile/setting/update"
let CLOSE_FRIEND_LIST = BasePath + "api/close-friend/list"
let CLOSE_FRIEND = BasePath + "api/close-friend"
let BLOCKED_FRIEND_LIST = BasePath + "api/blocked-friend/list"
let BLOCKED_FRIEND = BasePath + "api/blocked-friend"
let COMMENT_DELETE = BasePath + "api/post/comment/delete"
let FAQ = BasePath + "api/faq"
let PRIVACY_POLICY = BasePath + "api/privacy-policy"
let FEEDBACK_CREATE = BasePath + "api/feedback/create"
let USER_ACHIEVEMENTS = BasePath + "api/user-achievements"
let TOP_USER_POINTLIST = BasePath + "api/top-user-pointlist"
let PASSWORD_EMAIL = BasePath + "api/password-email"
let SOCIAL_LOGIN = BasePath + "api/social/login"
let USER_LIST = BasePath + "api/user-list"
let USER_STATISTICS = BasePath + "api/user-statistics"
let INVITE_FRIEND = BasePath + "api/invite-friend"
let INVITE_FRIEND_LIST = BasePath + "api/invite-friend/list"
let TOP_POST = BasePath + "api/top-posts"
let STORE_POINTS = BasePath + "api/post/store-points"
let COMMENT_POST_LIST = BasePath + "api/post/post-list"
let POST_VIEWED = BasePath + "api/post/view-count"
let STORE_COINS = BasePath + "api/coin/store"
let AVAILABLE_COINS = BasePath + "api/coin/balance"
let COMMENT_SETTINGS = BasePath + "api/comment/settings"
let COIN_LIST = BasePath + "api/coin/get-coin"
let DELETE_POST = BasePath + "api/post/delete"
let EDIT_POST = BasePath + "api/post/edit"
let SHARE_POST = BasePath + "api/post/share"
let SEARCH_USER = BasePath + "api/people-post"
let BOUNCE_USER = BasePath + "api/bounce-list"
let CREATE_GROUP = BasePath + "api/create-group"
let ADD_MEMBER = BasePath + "api/add-member"
let SEND_GROUP_MESSAGE = BasePath + "api/send-group-message"
let GROUP_LIST = BasePath + "api/group-lists"
let CHAT_LIST = BasePath + "api/chat-lists"
let SEND_MESSAGE = BasePath + "api/send-message"
let GROUP_MEMBER_LIST = BasePath + "api/group-member-lists"
let CHAT_USER_LIST = BasePath + "api/chat-user-list"
let UPLOAD_CONTACT_LIST = BasePath + "api/upload-contact-lists"
let ALL_NOTIFICATION = BasePath + "api/all-notifications"
let MARK_ALL_READ = BasePath + "api/mark-all-read"
let SINGLE_READ = BasePath + "api/single-read"
let MEMBER_LEFT = BasePath + "api/member-left"
let CHAT_READ = BasePath + "api/chat-read"
let GROUP_ARCHIVE = BasePath + "api/group-archive"
let REPORT_POST = BasePath + "api/report-post"
let CHAT_Delete = BasePath + "api/chat-delete"
let MAKE_ADMIN = BasePath + "api/make-admin"
let REMOVE_MEMBER = BasePath + "api/remove-member"
let SAVE_POST = BasePath + "api/post/save"
let SAVE_POST_LIST = BasePath + "api/post/save/list"
let POST_DETAIL = BasePath + "api/post-details"
let SETTINGS = BasePath + "api/setting"



protocol Routable
{
    var path        : String {get}
    var method      : HTTPMethod {get}
    var parameters  : Parameters? {get}
}

enum Router: Routable, CustomDebugStringConvertible
{
    case Login(Parameters)
    case Register(Parameters)
    case VerifyOtp(Parameters)
    case ResendOtp(Parameters)
    case ProfilePicture(Parameters)
    case UploadPost(Parameters)
    case PostLike(Parameters)
    case PostCommentList(Parameters)
    case AllPostList(Parameters)
    case LikeList(Parameters)
    case PostComment(Parameters)
    case FolloUser(Parameters)
    case FollowUSerList(Parameters)
    case ProfileUser(Parameters)
    case ProfileUserUpdate(Parameters)
    case ProfileSettingUpdate(Parameters)
    case CLoseFriendList(Parameters)
    case CloseFriend(Parameters)
    case BlockedFriendList(Parameters)
    case BlockedFriend(Parameters)
    case CommentDelete(Parameters)
    case Faq(Parameters)
    case PrivacyPolicy(Parameters)
    case FeedbackCreate(Parameters)
    case UserAchievements(Parameters)
    case SendMessage(Parameters)
    case GroupMemberList(Parameters)
    case TopUserPointList(Parameters)
    case PasswordEmail(Parameters)
    case SocialLogin(Parameters)
    case UserList(Parameters)
    case UserStatistics(Parameters)
    case InviteFriend(Parameters)
    case InviteFriendList(Parameters)
    case TopPost(Parameters)
    case StorePoints(Parameters)
    case CommentPostList(Parameters)
    case PostViewed(Parameters)
    case StoreCoins(Parameters)
    case AvailableCoins(Parameters)
    case CommentSettings(Parameters)
    case CoinList(Parameters)
    case DeletePost(Parameters)
    case EditPost(Parameters)
    case SearchUser(Parameters)
    case BounceUserList(Parameters)
    case SharePost(Parameters)
    case ChatUserList(Parameters)
    case CreateGroup(Parameters)
    case AddMember(Parameters)
    case SendGroupMessage(Parameters)
    case GroupChatLists(Parameters)
    case GroupList(Parameters)
    case UploadContactLists(Parameters)
    case AllNotifications(Parameters)
    case MarkAllRead(Parameters)
    case SingleRead(Parameters)
    case ChatRead(Parameters)
    case MemberLeft(Parameters)
    case GroupArchive(Parameters)
    case DeleteConversation(Parameters)
    case ReportPost(Parameters)
    case MakeAdmin(Parameters)
    case RemoveMember(Parameters)
    case SavePost(Parameters)
    case SavePostList(Parameters)
    case PostDetail(Parameters)
    case Setting(Parameters)
    
    var debugDescription: String
    {
        var printString = ""
        
        printString     += "\n*********************************"
        printString     += "\nMethod : \(method)"
        printString     += "\nParameter : \(String(describing: parameters))"
        printString     += "\nPath : \(path)"
        printString     += "\n*********************************\n"
        
        return printString
    }
 }

extension Router
{
    var path: String
    {
        switch self
        {
        case .Login:
            return LOGIN
            
        case .Register:
            return REGISTER
            
        case .VerifyOtp:
            return VERIFYOTP
            
        case .ResendOtp:
            return RESENDOTP
            
        case .ProfilePicture:
            return PROFILEPICTURE
            
        case .UploadPost:
            return UPLOADPOST
            
        case .PostLike:
            return POSTLIKE
            
        case .PostCommentList:
            return POST_COMMENTLIST
            
        case .AllPostList:
            return ALL_POST_LIST
            
        case .LikeList:
            return LIKELIST
            
        case .PostComment:
            return POST_COMMENT
            
        case .FolloUser:
            return FOLLOW_USER
            
        case .FollowUSerList:
            return FOLLOW_USER_LIST
            
        case .ProfileUser:
            return PROFILE_USER
            
        case .ProfileUserUpdate:
            return PROFILE_USER_UPDATE
            
        case .ProfileSettingUpdate:
            return PROFILE_SETTING_UPDATE
            
        case .CLoseFriendList:
            return CLOSE_FRIEND_LIST
            
        case .CloseFriend:
            return CLOSE_FRIEND
            
        case .BlockedFriendList:
            return BLOCKED_FRIEND_LIST
            
        case .BlockedFriend:
            return BLOCKED_FRIEND
            
        case .CommentDelete:
            return COMMENT_DELETE
            
        case .Faq:
            return FAQ
            
        case .PrivacyPolicy:
            return PRIVACY_POLICY
            
        case .FeedbackCreate:
            return FEEDBACK_CREATE
            
        case .UserAchievements:
            return USER_ACHIEVEMENTS
            
        case .SendMessage:
            return SEND_MESSAGE
            
        case .GroupMemberList:
            return GROUP_MEMBER_LIST
            
        case .TopUserPointList:
            return TOP_USER_POINTLIST
            
        case .PasswordEmail:
            return PASSWORD_EMAIL
            
        case .SocialLogin:
            return SOCIAL_LOGIN
            
        case .UserList:
            return USER_LIST
            
        case .UserStatistics:
            return USER_STATISTICS
            
        case .InviteFriend:
            return INVITE_FRIEND
            
        case .InviteFriendList:
            return INVITE_FRIEND_LIST
            
        case .TopPost:
            return TOP_POST
            
        case .StorePoints:
            return STORE_POINTS
            
        case .CommentPostList:
            return COMMENT_POST_LIST
            
        case .PostViewed:
            return POST_VIEWED
        
        case .StoreCoins:
            return STORE_COINS
            
        case .AvailableCoins:
            return AVAILABLE_COINS
            
        case .CommentSettings:
            return COMMENT_SETTINGS
            
        case .CoinList:
            return COIN_LIST
            
        case .DeletePost:
            return DELETE_POST
        case .EditPost:
            return EDIT_POST
        case .SearchUser:
            return SEARCH_USER
        case .BounceUserList:
            return BOUNCE_USER
        case .SharePost:
            return SHARE_POST
        case .ChatUserList:
            return CHAT_USER_LIST
        case .CreateGroup:
            return CREATE_GROUP
        case .AddMember:
            return ADD_MEMBER
        case .SendGroupMessage:
            return SEND_GROUP_MESSAGE
        case .GroupList:
            return GROUP_LIST
        case .UploadContactLists:
            return UPLOAD_CONTACT_LIST
        case .AllNotifications:
            return ALL_NOTIFICATION
        case .MarkAllRead:
            return MARK_ALL_READ
        case .SingleRead:
            return SINGLE_READ
        case .GroupChatLists:
            return CHAT_LIST
        case .ChatRead:
            return CHAT_READ
        case .MemberLeft:
            return MEMBER_LEFT
        case .GroupArchive:
            return GROUP_ARCHIVE
        case .DeleteConversation:
            return CHAT_Delete
        case .ReportPost:
            return REPORT_POST
        case .MakeAdmin:
            return MAKE_ADMIN
        case .RemoveMember:
            return REMOVE_MEMBER
        case .SavePost:
            return SAVE_POST
        case .SavePostList:
            return SAVE_POST_LIST
        case .PostDetail:
            return POST_DETAIL
        case .Setting:
            return SETTINGS
        }
        
    }
}

extension Router
{
    var method: HTTPMethod
    {
        switch self
        {
        case .Login:
            return .post
            
        case .Register:
            return .post
            
        case .VerifyOtp:
            return .post
            
        case .ResendOtp:
            return .post
            
        case .ProfilePicture:
            return .post
            
        case .UploadPost:
            return .post
            
        case .PostLike:
            return .post
            
        case .PostCommentList:
            return .post
            
        case .AllPostList:
            return .post
            
        case .LikeList:
            return .post
            
        case .PostComment:
            return .post
            
        case .FolloUser:
            return .post
            
        case .FollowUSerList:
            return .post
         
        case .ProfileUser:
            return .post
            
        case .ProfileUserUpdate:
            return .post
            
        case .ProfileSettingUpdate:
            return.post
            
        case .CLoseFriendList:
            return .post
            
        case .CloseFriend:
            return .post
            
        case .BlockedFriendList:
            return .post
            
        case .BlockedFriend:
            return .post
            
        case .CommentDelete:
            return .post
            
        case .Faq:
            return .get
            
        case .PrivacyPolicy:
            return .get
            
        case .FeedbackCreate:
            return .post
            
        case .UserAchievements:
            return .post
            
        case .SendMessage:
            return .post
            
        case .GroupMemberList:
            return .post
            
        case .TopUserPointList:
            return .post
            
        case .PasswordEmail:
            return .post
            
        case .SocialLogin:
            return .post
            
        case .UserList:
            return .post
            
        case .UserStatistics:
            return .post
            
        case .InviteFriend:
            return .post
            
        case .InviteFriendList:
            return .post
            
        case .TopPost:
            return .post
            
        case .StorePoints:
            return .post
            
        case .CommentPostList:
            return .post
            
        case .PostViewed:
            return .post
        
        case .StoreCoins:
            return .post
            
        case .AvailableCoins:
            return .post
            
        case .CommentSettings, .CoinList, .DeletePost:
            return .post
        case .EditPost:
            return .post
        case .SearchUser, .BounceUserList, .SharePost:
            return .post
        case .ChatUserList, .CreateGroup, .AddMember, .SendGroupMessage, .GroupList:
            return .post
        case .UploadContactLists, .AllNotifications, .MarkAllRead, .SingleRead:
            return .post
        case .GroupChatLists:
            return .post
        case .ChatRead, .MemberLeft, .GroupArchive, .DeleteConversation:
            return .post
        case .ReportPost, .MakeAdmin:
            return .post
        case .RemoveMember, .SavePost, .SavePostList, .PostDetail, .Setting:
            return .post
        }
        
    }
    
}

extension Router
{
    var parameters: Parameters?
    {
        switch self
        {
        case .Login(let param):
             return param
            
        case .Register(let param):
             return param
            
        case .VerifyOtp(let param):
             return param
            
        case .ResendOtp(let param):
            return param
            
        case .ProfilePicture(let param):
            return param
            
        case .UploadPost(let param):
            return param
        
        case .PostLike(let param):
            return param
            
        case .PostCommentList(let param):
            return param
            
        case .AllPostList(let param):
            return param
            
        case .LikeList(let param):
            return param
            
        case .PostComment(let param):
            return param
            
        case .FolloUser(let param):
            return param
            
        case .FollowUSerList(let param):
            return param
            
        case .ProfileUser(let param):
            return param
            
        case .ProfileUserUpdate(let param):
            return param
            
        case .ProfileSettingUpdate(let param):
            return param
            
        case .CLoseFriendList(let param):
            return param
            
        case .CloseFriend(let param):
            return param
            
        case .BlockedFriendList(let param):
            return param
            
        case .BlockedFriend(let param):
            return param
            
        case .CommentDelete(let param):
            return param
            
        case .Faq(let param):
            return param
            
        case .PrivacyPolicy(let param):
            return param
            
        case .FeedbackCreate(let param):
            return param
            
        case .UserAchievements(let param):
            return param
            
        case .SendMessage(let param):
            return param
            
        case .GroupMemberList(let param):
            return param
            
        case .TopUserPointList(let param):
            return param
            
        case .PasswordEmail(let param):
            return param
            
        case .SocialLogin(let param):
            return param
            
        case .UserList(let param):
            return param
            
        case .UserStatistics(let param):
            return param
            
        case .InviteFriend(let param):
            return param
            
        case .InviteFriendList(let param):
            return param
            
        case .TopPost(let param):
            return param
            
        case .StorePoints(let param):
            return param
            
        case .CommentPostList(let param):
            return param
            
        case .PostViewed(let param):
            return param
            
        case .AvailableCoins(let param):
            return param
            
        case .StoreCoins(let param):
            return param
            
        case .CommentSettings(let param):
            return param
            
        case .CoinList(let param):
            return param
        case .DeletePost(let param):
            return param
            
        case .EditPost(let param):
            return param
        case .SearchUser(let param):
            return param
        case .BounceUserList(let param):
            return param
        case .SharePost(let param):
            return param
        case .ChatUserList(let param):
            return param
        case .CreateGroup(let param):
            return param
        case .AddMember(let param):
            return param
        case .SendGroupMessage(let param):
            return param
        case .GroupList(let param):
            return param
        case .UploadContactLists(let param):
            return param
        case .AllNotifications(let param):
            return param
        case .MarkAllRead(let param):
            return param
        case .SingleRead(let param):
            return param
        case .GroupChatLists(let param):
            return param
        case .ChatRead(let param):
            return param
        case .MemberLeft(let param):
            return param
        case .GroupArchive(let param):
            return param
        case .DeleteConversation(let param):
            return param
        case .ReportPost(let param):
            return param
        case .MakeAdmin(let param):
            return param
        case .RemoveMember(let param):
            return param
        case .SavePost(let param):
            return param
        case.SavePostList(let param):
            return param
        case .PostDetail(let param):
            return param
        case .Setting(let param):
            return param
        }
    }

}
