//
//  AppDelegate.swift
//  blook
//
//  Created by Tech Astha on 24/12/20.
//

import Contacts
import CoreData
import FirebaseMessaging
import Firebase
import IQKeyboardManagerSwift
import SwiftUI
import UIKit
import UserNotifications
import FBSDKCoreKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var deviceTokkenStr = NSString()
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller: UIViewController = (storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController)!
        let navigation = UINavigationController(rootViewController: controller)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        // navigation.navigationBar.isHidden = true
        let options: UIView.AnimationOptions = [.transitionCrossDissolve, .curveEaseOut]
        window?.rootViewController = navigation
        registerForPushNotif(application: application)
        let strOwnerId = "\(defult.object(forKey: Constants.ISLOGIN) ?? "")"
        print(strOwnerId)
        presentDashBoardViewController(true)
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
//        let arrayContacts = fetchAllContacts()
//        saveContactsInDocument(contacts: arrayContacts)
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    func presentLoginViewController(_ animated: Bool) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller: UIViewController = (storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController)!
        let navigation = UINavigationController(rootViewController: controller)
        navigation.navigationBar.isHidden = true
        if animated {
            let options: UIView.AnimationOptions = [.transitionCrossDissolve, .curveEaseOut]
            // UIView.transition(with: window!, duration: 0.4, options: options, animations: {
            window?.rootViewController = navigation
            //  })
        } else {
            window?.rootViewController = navigation
        }
    }

    func presentDashBoardViewController(_ animated: Bool) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objFirstView = storyboard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
        let navigation = UINavigationController(rootViewController: objFirstView)
        navigation.navigationBar.isHidden = true
         
        if animated {
            let options: UIView.AnimationOptions = [.transitionCrossDissolve, .curveEaseOut]
            // UIView.transition(with: window!, duration: 0.4, options: options, animations: {
            window?.rootViewController = navigation
            // })
        } else {
            window?.rootViewController = navigation
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        deviceTokkenStr = token as NSString
        UserDefaults.standard.set(deviceTokkenStr, forKey: Constants.FIREBASEDEVICETOKEN)
        deviceTokkenStr = "32234dfdfdffdgdd"
        UserDefaults.standard.set(deviceTokkenStr, forKey: Constants.DEVICETOKEN)
        Messaging.messaging().apnsToken = deviceToken
        print("APNS token: \(token)")
        
        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
            //self.fireBaseToken  = token as NSString
              UserDefaults.standard.set(token, forKey: Constants.FIREBASEDEVICETOKEN)

          }
        }


    }

    func registerForPushNotif(application: UIApplication) {
        // create the notificationCenter
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        // set the type as sound or badge
        center.requestAuthorization(options: [.sound, .alert, .badge]) { _, _ in
            // Enable or disable features based on authorization
        }
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        deviceTokkenStr = "32234dfdfdffdgdd"
        UserDefaults.standard.set(deviceTokkenStr, forKey: Constants.DEVICETOKEN)
        print("Registration failed!")
    }
    
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
          The persistent container for the application. This implementation
          creates and returns a container, having loaded the store for the
          application to it. This property is optional since there are legitimate
          error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "blook")
        container.loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler:
        @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        if #available(iOS 14.0, *) {
            completionHandler([[.banner, .sound]])
        } else {
            // Fallback on earlier versions
            completionHandler([[.sound]])
        }
    }

    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        completionHandler()
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(
        _ messaging: Messaging,
        didReceiveRegistrationToken fcmToken: String?
    ) {
        let tokenDict = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(
            name: Notification.Name("FCMToken"),
            object: nil,
            userInfo: tokenDict
        )
    }
}

extension AppDelegate {
    func saveContactsInDocument(contacts: [CNContact]) {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let fileURL = URL(fileURLWithPath: documentsPath.appending("/MyContacts.vcf"))
        
        let data: NSData?
        do {
            try data = CNContactVCardSerialization.data(with: contacts) as! NSData
            uploadContacts(data: data as! Data)
            do {
                try data?.write(to: fileURL, options: .atomic)
                print(fileURL.absoluteString)
            } catch {
                print("Failed to write!")
            }
        } catch {
            print("Failed!")
        }
    }
    
    func fetchAllContacts() -> [CNContact] {
        var contacts: [CNContact] = []
        let status = CNContactStore.authorizationStatus(for: .contacts)
        
        let contactStore = CNContactStore()
        let fetchReq = CNContactFetchRequest(keysToFetch: [CNContactVCardSerialization.descriptorForRequiredKeys()])
        
        do {
            try contactStore.enumerateContacts(with: fetchReq) { contact, _ in
            
                contacts.append(contact)
            }
        } catch {
            print("Failed to fetch")
        }
        
        return contacts
    }
                
    func uploadContacts(data: Data) {
        guard PSUtil.reachable() else {
            
            return
        }
        let defult = UserDefaults.standard
   
        let strUserId = "\(defult.object(forKey: Constants.KEY_ID) ?? "")"
        let imageData = data
        let strDeviceToken = "\(defult.object(forKey: Constants.DEVICETOKEN) ?? "")"
        let params = [Constants.KEY_MYUSER_ID: strUserId,
                      Constants.DEVICE_TYPE: "I",
                      Constants.KEY_DEVICE_ID: "\(strDeviceToken)"] as [String: Any]

        PSWebServiceAPI.uploadContactList(contactData: imageData, parameter: params, onCompletion: { response in
            PSUtil.hideProgressHUD()
            if response["Error"] == nil {
                let Status = response[Constants.KEY_SUCCESS] as? NSInteger
                if Status == 200 {
                    let responseDic = response[Constants.KEY_POSTDATA] as? NSDictionary ?? NSDictionary()
                    print(responseDic)
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    
                } else {
                    let msg = response[Constants.KEY_MESSAGE] as? String
                    if msg == nil {
                    } else {
                        print("Error uploading contacts= \(msg)")
                    }
                }
            } else {
                print("Error uploading contacts")
            }
        })
    }
}
