//
//  Int+Additions.swift
//  Bloook
//
//  Created by Suresh Varma on 26/07/21.
//

import Foundation
extension Int {
    func boolValue() -> Bool {
        if self == 1 {
            return true
        }
        return false
    }
    
    func stringValue() -> String {
        return "\(self)"
    }
    
    func secondsToHoursMinutesSeconds(_ seconds: Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func secondsToTime() -> String {
        let min = timeText(from: (self % 3600) / 60)
        let sec = timeText(from: (self % 3600) % 60)
        return "\(min):\(sec)"
    }
private func timeText(from number: Int) -> String {
        return number < 10 ? "0\(number)" : "\(number)"
    }
}
