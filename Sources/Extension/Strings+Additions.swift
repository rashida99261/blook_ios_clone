//
//  Strings+Additions.swift
//  Bloook
//
//  Created by Suresh Varma on 05/07/21.
//

import UIKit
extension String {
    // Convert string to Date using format
    func toDateWithFormat(_ format: String, locale: Locale? = nil) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if let locale = locale {
            dateFormatter.locale = locale
        }
        return dateFormatter.date(from: self)
    }

    func toDateForUSLocaleWithFormat(_ format: String) -> Date? {
        return toDateWithFormat(format, locale: Locale(identifier: "en_US_POSIX"))
    }

    func toIsoDateWithSeconds(plain: Bool = false) -> Date? {
        if plain {
            return toDateForUSLocaleWithFormat("YYYY-MM-dd HH:mm:ss")
        }
        return toDateForUSLocaleWithFormat("YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    }
    
    
    func toUTCDateWithSeconds() -> Date? {
        return toDateForUSLocaleWithFormat("YYYY-MM-dd'UTC'HH:mm:ss")
    }

    func toDate(plain: Bool = false) -> Date? {
        if self.contains("UTC") {
            return toUTCDateWithSeconds()
        }
        return toIsoDateWithSeconds(plain: plain)
    }

    func dateWithoutTimeComponent() -> String {
        return toIsoDateWithSeconds()?.toDateString(with: "MMM d, YYYY") ?? ""
    }

    func ddMMMYYYY() -> String {
        return toIsoDateWithSeconds()?.toDateString(with: "dd-MMM-YYYY") ?? ""
    }
    
    func isValidString() -> Bool  {
       if self == "" || self == "<null>" || self.isEmpty || self == "null"  {
           return false
       }
       return true
     }
    
    func image() -> UIImage {
        let size = CGSize(width: 80, height: 80)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.set()
        let rect = CGRect(origin: .zero, size: size)
        UIRectFill(CGRect(origin: .zero, size: size))
        (self as AnyObject).draw(in: rect, withAttributes: [.font: UIFont.systemFont(ofSize: 70)])
        if let image = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return image
        
        } else {
            return UIImage()
        }
    }
    
    func boolValue() -> Bool {
        if self == "1" {
            return true
        }
        return false
    }
    
    func formatPoints() -> String {
        let from = Int(self)
        let number = Double(from ?? 0)
        let thousand = number / 1000
        let million = number / 1000000
        let billion = number / 1000000000
        
        if billion >= 1.0 {
            return "\(round(billion*10)/10)B"
        } else if million >= 1.0 {
            return "\(round(million*10)/10)M"
        } else if thousand >= 1.0 {
            return ("\(round(thousand*10/10))K")
        } else {
            return "\(Int(number))"
        }
    }
}
