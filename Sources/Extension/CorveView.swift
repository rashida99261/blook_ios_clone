//
//  CorveView.swift
//  NightHawk
//
//  Created by ganesh on 11/17/18.
//  Copyright © 2018 Sumit Gajjar. All rights reserved.
//

import UIKit

class CorveView: UIView {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        let BottomPath = UIBezierPath.init()
        
        BottomPath.move(to: CGPoint.init(x: 0, y: 40))
        
        
        BottomPath.addQuadCurve(to: CGPoint.init(x: UIScreen.main.bounds.width, y: 40),
                      
                                
        controlPoint: CGPoint.init(x: UIScreen.main.bounds.width/2, y: 0))
        
        
        BottomPath.addLine(to: CGPoint.init(x: UIScreen.main.bounds.width,
                                           y: UIScreen.main.bounds.height))
        
        BottomPath.addLine(to: CGPoint.init(x: 0, y: UIScreen.main.bounds.height))
        
        
        let bottomShape = CAShapeLayer()
        bottomShape.frame = self.bounds
        self.layer.mask = bottomShape
        bottomShape.path = BottomPath.cgPath
        bottomShape.lineWidth = 4
        bottomShape.strokeColor = UIColor.black.cgColor
        
    }
 

}
