//
//  Constant.swift
//  Experience VR
//
//  Created by Cools on 3/22/18.
//  Copyright © 2018 Cools. All rights reserved.
//

import AVFoundation
import Foundation
import UIKit

let kChatPresenceTimeInterval: TimeInterval = 45
let kDialogsPageLimit: UInt = 100
let kMessageContainerWidthPadding: CGFloat = 40.0

struct Constants {
    let USER_DEFAULT = UserDefaults.standard
    var userDetails = UserRegistrationModal()
    
    static let ISLOGIN = "isLogin"
    static let LIVE_VERSION = "1"
    static let DEVICE_TYPE = "dt"
    static let KEY_DESCRIPTION = "description"
    static let DEVICETOKEN = "forDeviceToken"
    static let FIREBASEDEVICETOKEN = "firebaseDeviceToken"
    static let KEY_TYPE = "type"
    static let KEY_NUMBER = "number"
    static let KEY_EARN_TYPE = "earn_type"
    static let KEY_Id = "id"
    static let KEY_POST_USERId = "post_user_id"
    static let KEY_POST_DATE = "post_date"
    // Api Responce Key
    static let KEY_USER = "user"
    static let KEY_OWN_POST = "own_post"
    static let KEY_TOTAL_POINTS = "total_points"
    static let KEY_POINTS = "points"
    static let KEY_NAME = "name"
    static let KEY_COMMENT_COUNT = "commentCount"
    static let KEY_ALL_COMMENTS_COUNT = "comments_count"
    static let KEY_COMMENTS = "comments"
    static let KEY_COMMENT = "comment"
    static let KEY_COMMENT_ID = "comment_id"
    static let KEY_SUCCESS = "sc"
    static let KEY_MESSAGE = "m"
    static let KEY_MESSAGE_FULL = "message"
    static let KEY_POSTDATA = "d"
    static let KEY_DEVICE_ID = "di"
    static let KEY_USERNAME = "username"
    static let KEY_PASSWORD = "password"
    static let KEY_FIRSTNAME = "firstname"
    static let KEY_LASTNAME = "lastname"
    static let KEY_TITLE = "title"
    static let KEY_CREATE_DATE = "created_at"
    static let KEY_PASSWORD_CONFIRMATION = "password_confirmation"
    static let KEY_EMAIL = "email"
    static let KEY_MOBILE = "mobile"
    static let KEY_POST_ID = "post_id"
    static let KEY_OTP_CODE = "code"
    static let KEY_NO = "no"
    static let KEY_ID = "id"
    static let KEY_MYUSER_ID = "user_id"
    static let KEY_CLOSE_FRIEND_ID = "close_friend_id"
    static let KEY_PROFILE_PICTURE = "profile_photo"
    static let token = "access_token"
    static let KEY_FILE_NAME = "filename"
    static let KEY_ALLLIKES = "likes"
    static let KEY_LIKE = "like"
    static let KEY_USER_LIKE = "userLike"
    static let KEY_LIKE_COUNT = "likes_count"
    static let KEY_PARENT_COMMENT_ID = "parent_comment_id"
    static let KEY_PAGE = "page"
    static let KEY_LIMIT = "limit"
    static let KEY_PROFILE_PHOTO = "profile_photo"
    static let KEY_CHILDREN = "children"
    static let KEY_PROFILE_ID = "profile_id"
    static let KEY_FRIEND_DATA = "friend_data"
    static let KEY_POST_TYPE = "post_type"
    static let KEY_VIEWS_COUNT = "views_count"
    static let KEY_ANNUAL_POINTS = "annual_points"
    static let KEY_DAILY_POINTS = "daily_points"
    static let KEY_LIFETIME_POINTS = "lifetime_points"
    static let KEY_POST = "post"
    static let KEY_FILE = "file"
    static let KEY_SEARCH = "search"
    static let KEY_PARTNER_ID = "partner_id"
    
    // MARK: - Boost Visibility

    static let KEY_FOLLOWER_BOOST = "follower"
    static let KEY_PUBLIC = "public"
    static let KEY_FOLLOWES = "Followes"
    static let KEY_CLOSE_FRIENDS = "close_friends"
    static let KEY_CLOSE_FRIEND = "close_friend"
    static let KEY_BLOCKED_FRIEND = "blocked_friend"
    
    // MARK: - Follower

    static let KEY_FROM_USER_ID = "from_user_id"
    static let KEY_TO_USER_ID = "to_user_id"
    static let KEY_FOLLOWING = "following"
    static let KEY_FOLLOWERS = "followers"
    static let KEY_FOLLOW_ID = "follow_id"
    static let KEY_FOLLOWER_COUNT = "followers_count"
    static let KEY_POSTS_COUNT = "posts_count"
    static let KEY_BOUNCE_STATUS = "bounce_status"
    static let KEY_POSTS = "posts"

    // MARK: - Feedback

    static let KEY_RATING = "rating"

    // MARK: - Social Login

    static let KEY_PROVIDER_TYPE = "provider_type"
    static let KEY_PROVIDER_TOKEN = "provider_token"

    // Font Size
    static let Font_Size_Regular: CGFloat = 20
    static let SCREEN_WIDTH: CGFloat = UIScreen.main.bounds.size.width
    static let SCREEN_NATIVE_HEIGHT: CGFloat = UIScreen.main.nativeBounds.height
    // static let IPHONE_X_HEIGHT : CGFloat = 2436
    static let IPHONE_12PROMAX_HEIGHT: CGFloat = 2778
   
    enum colorConstants {
        public static let CLR_PRIMARY_THEME = UIColor(red: 0/255.0, green: 133/255.0, blue: 119/255.0, alpha: 1)
        public static let CLR_GREEN_THEME = UIColor(red: 25/255.0, green: 88/255.0, blue: 76/255.0, alpha: 1)
        public static let CLR_RED_THEME = UIColor(red: 228/255.0, green: 114/255.0, blue: 144/255.0, alpha: 1)
        public static let CLR_REDLIGHT_THEME = UIColor(red: 189/255.0, green: 31/255.0, blue: 47/255.0, alpha: 1)
        public static let CLR_GREYLIGHT_THEME = UIColor(red: 119/255.0, green: 119/255.0, blue: 119/255.0, alpha: 1)
        public static let CLR_REDMERHUN_THEME = UIColor(red: 156/255.0, green: 23/255.0, blue: 38/255.0, alpha: 1)
        public static let CLR_WHITERED_THEME = UIColor(red: 249/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1)
        public static let CLR_BLUE_THEME = UIColor(red: 64/255.0, green: 158/255.0, blue: 255/255.0, alpha: 1)
        public static let CLR_LIGHTWHITE_THEME = UIColor(red: 250/255.0, green: 250/255.0, blue: 250/255.0, alpha: 1)
        public static let CLR_LIGHTGRAY_THEME = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 0.5)
        public static let CLR_DARKGRAY_THEME = UIColor(red: 130.0/255.0, green: 140.0/255.0, blue: 166.0/255.0, alpha: 1.0)
    }
    
    enum Bounce {
        static let isBounceShown = "kisBounceShown"
        static let bounceUserId = "kbounceUserId"
        static let isBounceOn = "kisBounceOn"
    }
    
    enum Signup {
        static let firstName = "kfirstName"
        static let lastName = "klastName"
        static let userName = "kuserName"
        static let emailAddress = "kemailAddress"
        static let password = "kpassword"
        static let mobileNo = "kmobileno"
        static let pageNo = "kpageNo"
    }
    // Userdefault Key Exist or Not
    static func isKeyExistInUserDefault(kUsernameKey: String) -> Bool {
        return UserDefaults.standard.object(forKey: kUsernameKey) != nil
    }
    
    // Convert String to Date
    static func convertDate(_ mydate: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let convertedDate = dateFormatter.date(from: mydate)
        return convertedDate!
//        dateFormatter.dateStyle = DateFormatter.Style.long
    }
    
    static func convertStringToDate(_ mydate: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "dd/mm/yyyy HH:mm:ss"
        let convertedDate = dateFormatter.date(from: mydate)
        return convertedDate!
    }
    
    static func secondsToHoursMinutesSeconds(_ seconds: Int) -> String {
        var hour: String = ""
        var minite: String = ""
        var second: String = ""
        if String(seconds/3600).count == 1 {
            hour = "0\(seconds/3600)"
        } else {
            hour = "\(seconds/3600)"
        }
        
        if String((seconds % 3600)/60).count == 1 {
            minite = "0\((seconds % 3600)/60)"
        } else {
            minite = "\((seconds % 3600)/60)"
        }
        
        if String((seconds % 3600) % 60).count == 1 {
            second = "0\((seconds % 3600) % 60)"
        } else {
            second = "\((seconds % 3600) % 60)"
        }
        
        return "\(hour):\(minite):\(second)"
//        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    static func secondsToMinutesSeconds(_ seconds: Int) -> String {
        var minite: String = ""
        var second: String = ""
       
        if String((seconds % 3600)/60).count == 1 {
            minite = "0\((seconds % 3600)/60)"
        } else {
            minite = "\((seconds % 3600)/60)"
        }
       
        if String((seconds % 3600) % 60).count == 1 {
            second = "0\((seconds % 3600) % 60)"
        } else {
            second = "\((seconds % 3600) % 60)"
        }
       
        return "\(minite):\(second)"
//        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    static func validate(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result = phoneTest.evaluate(with: value)
        return result
    }
  
    static func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func convertStringToCurrentTimezoneDate(_ mydate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: mydate)
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let dateStr = dateFormatter.string(from: date!)
        return dateStr
    }
    
    static func convertDateToString(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-dd-MM"
        let dateStr = dateFormatter.string(from: date)
        return dateStr
    }
    
    static func convertTimeToString(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let dateStr = dateFormatter.string(from: date)
        return dateStr
    }
    
    static var QB_USERS_ENVIROMENT: String {
        #if DEBUG
        return "dev"
        #elseif QA
        return "qbqa"
        #else
        assert(false, "Not supported build configuration")
        return ""
        #endif
    }
}

extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r)/0xff,
            green: CGFloat(g)/0xff,
            blue: CGFloat(b)/0xff, alpha: 1
        )
    }
}

public extension String {
    /**
     Calculates the best height of the text for available width and font used.
     */
    func heightForWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let rect = NSString(string: self).boundingRect(
            with: CGSize(width: width, height: CGFloat(MAXFLOAT)),
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedString.Key.font: font],
            context: nil
        )
        return ceil(rect.height)
    }
}

extension UIFont {
    class var defaultFont: UIFont {
        return UIFont(name: "Arial-ItalicMT", size: 11)!
    }
}

public extension UIImage {
    /**
     Calculates the best height of the image for available width.
     */
    func height(forWidth width: CGFloat) -> CGFloat {
        let boundingRect = CGRect(
            x: 0,
            y: 0,
            width: width,
            height: CGFloat(MAXFLOAT)
        )
        let rect = AVMakeRect(
            aspectRatio: size,
            insideRect: boundingRect
        )
        return rect.size.height
    }
}

public func print(_ object: Any...) {
    #if DEBUG
    for item in object {
        Swift.print("\(item)")
    }
    #endif
}

public func print(_ object: Any) {
    #if DEBUG

    Swift.print("\(object)")

    #endif
}
